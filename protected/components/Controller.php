<?php
    /**
     * Controller is the customized base controller class.
     * All controller classes for this application should extend from this base class.
     */

    define('bDIR', dirname(Yii::app()->request->scriptFile));
    define('UPLOADS', bDIR . "/vendor/");
    define('THUMB', Yii::app()->request->baseUrl . "/vendor/image/timthumb.php?src=");


    class Controller extends CController {
        /**
         * @var string the default layout for the controller view. Defaults to 'column1',
         * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
         */
        public $layout = 'column1';
        /**
         * @var array context menu items. This property will be assigned to {@link CMenu::items}.
         */
        public $menu = array();
        /**
         * @var array the breadcrumbs of the current page. The value of this property will
         * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
         * for more details on how to specify this property.
         */
        public $breadcrumbs = array();


        public $languagesModel   = array();
        public $langList         = array();
        public $currentLanguage  = array();
        public $langId           = array();
        public $categories       = array();
        public $static_pages     = array();
        public $translationLabel = array();
        public $translation      = array();

        /* @var $_user Users */
        public $_user = array();


        public function init() {
            if (isset(Yii::app()->session['user'])) {
                $this->_user = Yii::app()->session['user'];
            }
            // Set the application language if provided by GET, session or cookie
            if (isset($_GET['language'])) {
                Yii::app()->language = $_GET['language'];
                $cookie = new CHttpCookie('v_lang', $_GET['language']);
                $cookie->expire = time() + (60 * 60 * 24 * 365); // (1 year)
                Yii::app()->request->cookies['v_lang'] = $cookie;
            }
            else if (isset(Yii::app()->request->cookies['v_lang'])) {
                Yii::app()->language = Yii::app()->request->cookies['v_lang']->value;
            }
        }

        public function createMultilanguageReturnUrl($lang = 'en') {
            if (count($_GET) > 0) {
                $arr = $_GET;
                $arr['language'] = $lang;
            }
            else {
                $arr = array('language' => $lang);
            }

            return $this->createUrl('', $arr);
        }

        public function beforeAction($action) {

            $this->languagesModel = Languages::model()->findAllByAttributes(array('status' => 1), array('order' => 'sort_order ASC'));

            foreach ($this->languagesModel AS $value) {
                $this->langList[$value->iso] = $value->name;
            }

            if (isset($_GET['language'])) {
                foreach ($this->languagesModel AS $value) {
                    if ($value->iso === $_GET['language']) {
                        $this->langId = $value->id;
                        break;
                    }
                }
            }
            else {
                $def_lang = Yii::app()->language;
                foreach ($this->languagesModel AS $value) {
                    if ($value->iso === $def_lang) {
                        $this->langId = $value->id;
                        break;
                    }
                }
            }
            $this->currentLanguage = isset($_GET['language']) ? $this->langList[$_GET['language']] : $this->langList[Yii::app()->language];


            $this->translationLabel = TranslationLabel::model()->with('translation')->findAllByAttributes(array("language_id" => $this->langId));

//		Helpers::pr($this->translationLabel);

            foreach ($this->translationLabel AS $value) {
                $this->translation[$value->translation->key] = $value->value;
            }

            $criteria = new CDbCriteria();
            $criteria->select = array('id');
            $criteria->condition = 'status = 1';
            $criteria->order = 't.sort_order ASC, t.id';
            $criteria->with = array(
                'categoryLabels' => array(
                    'select'    => array('categoryLabels.name'),
                    'condition' => 'categoryLabels.language_id = :language_id',
                    'params'    => array(':language_id' => $this->langId)
                )
            );
            $this->categories = Category::model()->findAll($criteria);

            $criteria = new CDbCriteria();
            $criteria->select = array('t.id');
            $criteria->condition = 't.status = 1';
            $criteria->order = 't.sort_order, t.id';
            $criteria->with = array(
                'staticPagesLabels' => array(
                    'select'    => array('staticPagesLabels.title'),
                    'condition' => 'staticPagesLabels.language_id = :language_id',
                    'params'    => array(':language_id' => $this->langId)
                )
            );
            $this->static_pages = StaticPages::model()->findAll($criteria);

//		Helpers::pr($this->static_pages);

            return parent::beforeAction($action);
        }

    }
