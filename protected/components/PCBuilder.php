<?php

    class PCBuilder extends CApplicationComponent {


        public static $types = array(
            'motherboard'    => 'Motherboard',
            'cpu'            => 'Cpu',
            'memory'         => 'Memory',
            'video_card'     => 'Video Card',
            'storage'        => 'Storage',
            'case'           => 'Case',
            'power_supply'   => 'Power Supply',
            'pci'            => 'Pci',
            'cooling_system' => 'Cooling System',
            'other'          => 'Other',
        );


        public static $types_gr = array(
            'motherboard'    => array(),
            'cpu'            => array(),
            'memory'         => array(),
            'video_card'     => array(),
            'storage'        => array(),
            'case'           => array(),
            'power_supply'   => array(),
            'pci'            => array(),
            'cooling_system' => array(),
            'other'          => array(),
        );


    }

?>