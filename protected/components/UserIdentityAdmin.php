<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentityAdmin extends CUserIdentity
{
	private $_id;

	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$this->password = md5($this->password);
		$user = Admins::model()->find(array('condition' => 'email = :email AND password = :pass', 'params' => array('email' => $this->username, ':pass' => $this->password)));

		if($user===null) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		}
		else
		{
			$this->_id=$user->id;
			$this->setState('admin_name', $user->name);
			$this->errorCode=self::ERROR_NONE;
		}
		return $this->errorCode==self::ERROR_NONE;
	}
	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->_id;
	}
}