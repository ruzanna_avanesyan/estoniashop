<?php
class LanguageSelector extends CWidget
{
    public function run(){
        $active_language = Yii::app()->getController()->currentLanguage;

        $languages = Yii::app()->getController()->langList;
        if(count($languages) > 1) {
            $this->render('languageSelector', array('languages' => $languages, 'active_lang' => $active_language));
        }
    }
}
?>