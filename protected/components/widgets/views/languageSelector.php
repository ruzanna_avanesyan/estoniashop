<div class="dropdown">
    <a id="language" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <?php echo $active_lang;?>
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" aria-labelledby="dLabel">
        <?php
        // Если хотим видить в виде флагов то используем этот код
        foreach($languages as $key => $lang) {
            if($lang != $active_lang) {
                echo '<li>';
                echo CHtml::link(
                    $lang,
                    $this->getOwner()->createMultilanguageReturnUrl($key));
                echo '</li>';
            };
        }
        ?>
    </ul>
</div>

