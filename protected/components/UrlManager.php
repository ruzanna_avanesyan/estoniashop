<?php
    class UrlManager extends CUrlManager
    {
        public function createUrl($route,$params=array(),$ampersand='&')
        {
            if (!isset($params['language'])) {
                if(isset(Yii::app()->request->cookies['v_lang']))
                    Yii::app()->language = Yii::app()->request->cookies['v_lang']->value;
                $params['language'] = Yii::app()->language;
            }
            return parent::createUrl($route, $params, $ampersand);
        }
    }
?>