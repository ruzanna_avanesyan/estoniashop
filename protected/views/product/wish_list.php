<div class="wish-list">
    <h1>
        Wishlist
    </h1>
    <div class="my-row">
        <hr>
    </div>
    <div class="clearfix">
        <div class="col-sm-2 pull-right">
            <select class="form-control">
                <option value="">Default select 1</option>
                <option value="">Default select 2</option>
                <option value="">Default select 3</option>
                <option value="">Default select 4</option>
                <option value="">Default select 5</option>
            </select>
        </div>
    </div>
    <div class="my-row">
        <hr>
    </div>
    <div class="items">
        <?php foreach ($model as $key => $value) : ?>
            <table class="table">
                <tr>
                    <td rowspan="4" class="wish-image">
                        <a href="<?php echo Yii::app()->createUrl('product/index', array('path' => $_GET['path'], 'id' => $value['id']));?>">
                            <img src="<?php echo THUMB;?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/<?php echo $value->productImages[0]->image;?>&w=190&h=190" alt="<?php echo $value->productLabels->name; ?>" />
                        </a>
                    </td>
                    <td class="wish-name">
                        <a href="<?php echo Yii::app()->createUrl('product/index', array('path' => $_GET['path'], 'id' => $value['id']));?>">
                            <?php echo $value->productLabels->name;?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td class="wish-category">
                        Category: <?php echo $value->category->categoryLabels->name;?>
                    </td>
                </tr>
                <tr>
                    <td class="wish-price">
                        <?php echo $value['price'];?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <textarea name="" data-url="<?php echo Yii::app()->createUrl('product/addWishNote',array('id' => $value->id));?>" class="form-control wish-note" rows="3" placeholder="Notes"><?php if ($notes && isset($notes[$value->id])) echo $notes[$value->id];?></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button class="btn btn-default wish-delete" data-url="<?php echo Yii::app()->createUrl('product/deleteWishItem', array('id'=>$value['id']));?>">
                            elete
                        </button>
                    </td>
                </tr>
            </table>
        <?php endforeach;?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.wish-note').on('change', function () {
            $.ajax({
                type : 'POST',
                url  : $(this).data('url'),
                data : 'value='+$(this).val(),
                success: function (data) {

                }
            });
        });

        $('.wish-delete').on('click', function (e) {
            e.preventDefault();
            $.ajax({
               'type': "GET",
                'url' : $(this).data('url'),
                success: function (data) {
                    if(data == 1){
                        location.reload();
                    }
                }
            });
        });
    });
</script>
