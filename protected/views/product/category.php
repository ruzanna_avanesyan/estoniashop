<style>
    .different-category:before {
        display: none;
        position: absolute;
        top: 0;
        height: 100%;
        width: 100%;
        left: 0;
        background-color: #ffffff;
        opacity: 0.8;
        content: "";

    }
    .different-category:after {
        display: none;
        content: "Cannot campare between different categories:";
        position: absolute;
        top: 50%;
        margin-top: -20px;
        width: 100%;
        left: 0;
        font-weight: bold;
        text-align: center;
        color: #333333;
    }
    .different-category:hover:after,
    .different-category:hover:before {
        display: block;
        pointer-events: none;
    }
</style>
<div class="cmpr-pnl-wrpr add-cmp-mr">
    <div class="cmpr-pnl__close">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/compare_sliderbutton.png" alt="C">
    </div>
    <div class="sidebaroverlay"></div>
    <div class="droppable cmpr-pnl sctn sctn--sdbr cmpr-pnl-list cmpr-pnl-list clearfix ui-front cmpr-pnl--bx-shdw">
        <div class="sctn__hdr clearfix">
            <div class="sctn__ttl">
                Compare Products
            </div>
            <div class="cmpr-pnl__close--alt js-cmpr-pnl-cls">
                <?php echo $this->translation['close'];?>
            </div>
        </div>
        <div class="sctn__inr">
            <div class="cmpr-pnl-list__item cmpr0" data-comparemspid="" data-subcategory="">
                <div class="cmpr-pnl-list__img-wrpr">
                    <img class="cmpr-pnl-list__img" src="<?php echo THUMB;?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/generic_product_image.png&w=60&h=60" alt="">
                </div>
                <div class="cmpr-pnl-list__item-dtls">
                    <div class="cmpr-pnl-list__item-ttl">
                        <div class="srch-wdgt srch-wdgt--s srch-wdgt--xs" data-url="http://www.mysmartprice.com/compare/auto_suggest.php?subcategory=">

                        </div>
                    </div>
                </div>
            </div>
            <div class="cmpr-pnl-list__item cmpr0" data-comparemspid="" data-subcategory="">
                <div class="cmpr-pnl-list__img-wrpr">
                    <img class="cmpr-pnl-list__img" src="<?php echo THUMB;?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/generic_product_image.png&w=60&h=60" alt="">
                </div>
                <div class="cmpr-pnl-list__item-dtls">
                    <div class="cmpr-pnl-list__item-ttl">
                        <div class="srch-wdgt srch-wdgt--s srch-wdgt--xs" data-url="http://www.mysmartprice.com/compare/auto_suggest.php?subcategory=">

                        </div>
                    </div>
                </div>
            </div>
            <div class="cmpr-pnl-list__item cmpr0" data-comparemspid="" data-subcategory="">
                <div class="cmpr-pnl-list__img-wrpr">
                    <img class="cmpr-pnl-list__img" src="<?php echo THUMB;?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/generic_product_image.png&w=60&h=60" alt="">
                </div>
                <div class="cmpr-pnl-list__item-dtls">
                    <div class="cmpr-pnl-list__item-ttl">
                        <div class="srch-wdgt srch-wdgt--s srch-wdgt--xs" data-url="http://www.mysmartprice.com/compare/auto_suggest.php?subcategory=">

                        </div>
                    </div>
                </div>
            </div>
            <div class="cmpr-pnl-list__item cmpr0" data-comparemspid="" data-subcategory="">
                <div class="cmpr-pnl-list__img-wrpr">
                    <img class="cmpr-pnl-list__img" src="<?php echo THUMB;?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/generic_product_image.png&w=60&h=60" alt="">
                </div>
                <div class="cmpr-pnl-list__item-dtls">
                    <div class="cmpr-pnl-list__item-ttl">
                        <div class="srch-wdgt srch-wdgt--s srch-wdgt--xs" data-url="http://www.mysmartprice.com/compare/auto_suggest.php?subcategory=">

                        </div>
                    </div>
                </div>
            </div>
            <div class="cmpr-pnl-list__item cmpr0" data-comparemspid="" data-subcategory="">
                <div class="cmpr-pnl-list__img-wrpr">
                    <img class="cmpr-pnl-list__img" src="<?php echo THUMB;?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/generic_product_image.png&w=60&h=60" alt="">
                </div>
                <div class="cmpr-pnl-list__item-dtls">
                    <div class="cmpr-pnl-list__item-ttl">
                        <div class="srch-wdgt srch-wdgt--s srch-wdgt--xs" data-url="http://www.mysmartprice.com/compare/auto_suggest.php?subcategory=">

                        </div>
                    </div>
                </div>
            </div>
            <div class="cmpr-pnl-list__item cmpr0" data-comparemspid="" data-subcategory="">
                <div class="cmpr-pnl-list__img-wrpr">
                    <img class="cmpr-pnl-list__img" src="<?php echo THUMB;?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/generic_product_image.png&w=60&h=60" alt="">
                </div>
                <div class="cmpr-pnl-list__item-dtls">
                    <div class="cmpr-pnl-list__item-ttl">
                        <div class="srch-wdgt srch-wdgt--s srch-wdgt--xs" data-url="http://www.mysmartprice.com/compare/auto_suggest.php?subcategory=">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <a class="transition sctn__compare-btn"  href="<?php echo Yii::app()->createUrl('/product/compare/', array('path' => $_GET['path']));?>">
            <?php echo $this->translation['compare'];?>
        </a>
    </div>
</div>

<div class="row main category-page">
    <div class="col-sm-3 hidden-xs filters-content">
        <div class="filter-item">
            <p>
                <?php echo $this->translation['results'];?>
            </p>
            <a href="#" class="clear_filtres">
                <i class="fa fa-caret-right"></i>
                <?php echo $this->translation['clear_filters'];?>
            </a>
        </div>
        <form id="filter-form">
            <?php
                if(isset($_GET['filter']))$url2 = explode(',', $_GET['filter']);
                if(isset($_GET['brand']))$url3 = explode(',', $_GET['brand']);
            ?>
            <?php if($model_brand):?>
            <div class="filter-item">
                <figure>
                    <p class="filter-head">
                        <?php echo $this->translation['brand'];?>
                    </p>
                    <?php foreach($model_brand as $value):?>
                        <?php if(count($value->products) > 0):?>
                            <div class="checkbox">
                                <label class="label--checkbox">
                                    <input name="Brand[]" type="checkbox" class="checkbox-styled" <?php if(isset($_GET['brand']) && in_array($value->id, $url3)) echo 'checked';?> value="<?php echo $value->id;?>">
                                    <?php echo $value->brandLabels->name;?>
                                    <span class="product-count">
                                        (
                                        <?php echo count($value->products);?>
                                        )
                                    </span>
                                </label>
                            </div>
                        <?php endif; ?>
                    <?php endforeach;?>
                </figure>
            </div>
            <?php endif; ?>
            <?php if($model_filters):?>
                <?php foreach($model_filters as $value):?>
                    <div class="filter-item" id="content-ltn">
                        <figure >
                            <p class="filter-head">
                                <?php echo $value->filterLabels->name;?>
                            </p>
                            <?php foreach($value->filterItems as $val):?>
                                <?php if(count($val->productHasFilters) > 0):?>
                                    <div class="checkbox">
                                        <label class="label--checkbox">
                                            <input name="Filter[]" type="checkbox" class="checkbox-styled" <?php if(isset($_GET['filter']) && in_array($val->id, $url2)) echo 'checked';?> value="<?php echo $val->id;?>">
                                            <?php echo $val->filterItemLabels->name;?>
                                            <span class="product-count">
                                                (
                                                <?php echo count($val->productHasFilters);?>
                                                )
                                            </span>
                                        </label>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </figure>
                    </div>
                <?php endforeach;?>
            <?php endif;?>
        </form>
    </div>
    <div class="products col-md-7 col-sm-7 col-xs-12">
        <div class="body-head clearfix">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <p>
                    <?php if(isset($_GET['path']) && $_GET['path'] == 'All') {?>
                        <?php echo $this->translation['all_categories'];?>
                    <?php }else{?>
                    <?php foreach($this->categories as $value) :?>
                        <?php if(isset($_GET['path']) && $value->id == $_GET['path']) :?>
                            <?php echo $value->categoryLabels->name;?>
                        <?php endif; ?>
                    <?php endforeach;?>
                    <?php }?>
                </p>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="head-bar">
                    <p class="search-resdisableult">
                        <?php echo $this->translation['found'];?>

                        <span>
                            <?php echo count($model)?>
                        </span>
                    </p>
                    <form>
                        <select name="Limit" id="input-limit">
                            <?php for($i=1;$i<=5;$i++):?>
                                <option value="<?php echo $i*12;?>"
                                <?php if(isset($_GET['limit']) && $_GET['limit'] ==  $i*12) echo 'selected="selected"';?>>
                                    <?php echo $i*12;?>
                                </option>
                            <?php endfor; ?>
                        </select>
                    </form>
                    <div class="view-config hidden-xs">
                        <a href="#" class = "list"><i class="fa fa-th-list"></i></span></a>
                        <a href="#" class = "grid active"><i class="fa fa-th"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix">
            <?php foreach($model as $value) :?>
                <div class="product-layout  product-grid prdct-item col-md-4 col-sm-6  col-xs-12 transition" data-mspid="<?php echo $value->id;?>" data-cat="<?php echo $value->category_id; ?>">
                    <div class="product-image">
                        <div class="pull-right product-date">
                            <?php echo $value->date?>
                        </div>
                        <a href ="<?php echo Yii::app()->createUrl('product/index', array('path' => $_GET['path'], 'id' => $value->id));?>">
                            <?php foreach($value->productImages as $val):?>
                                <?php if($val->general == 1) : ?>
                                    <img class="prdct-item__img draggable2" src="<?php echo THUMB;?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/<?php echo $val->image;?>&w=190&h=190" alt="<?php echo $value->productLabels->name; ?>" />
                                <?php endif;?>
                            <?php endforeach;?>
                        </a>
                    </div>
                    <div class="product-name text-uppercase">
                        <p>
                            <a class="transition prdct-item__name" href ="<?php echo Yii::app()->createUrl('product/index', array('path' => $_GET['path'], 'id' => $value->id));?>">
                                <?php echo $value->productLabels->name; ?>
                            </a>
                        </p>
                        <div>
                            <span class="product-photo">
                                <i class="fa fa-camera"></i>
                                <span class="product-photo-count">
                                    <?php echo count($value->productImages)?>
                                </span>
                            </span>
                            <?php if($value->productVideos) :?>
                                <span class="product-video">
                                    <i class="fa fa-youtube-play"></i>
                                </span>
                            <?php endif;?>
                            <button type="button" class="btn pull-right compare-button" data-cat="<?php echo $value->category_id; ?>">
                                <i class="fa fa-plus"></i>
                                Compare
                                <div class="prdct-item__cmpr">
                                    <label>
                                        <input  type="checkbox" name="compare" class="prdct-item__cmpr-chkbx js-add-to-cmpr-btn js-add-to-cmpr" data-cat="<?php echo $value->category_id; ?>" data-cookie="0">
                                        <span class="prdct-item__cmpr-img"></span>
                                    </label>
                                </div>
                            </button>
                        </div>
                    </div>
                    <div class="additional-info">
                        <a href ="<?php echo Yii::app()->createUrl('product/index', array('path' => $_GET['path'], 'id' => $value->id));?>" class="model transition">
                            <?php echo $value->productLabels->name; ?>
                        </a>
                        <div class="row">
                            <div class="category-spec col-sm-6 col-xs-6">
                                <ul>
                                    <?php $i=0; foreach($specifications as $key => $val1) {?>
                                        <?php foreach($val1->specification->specificationsItems(array('limit' => 10)) as $k=> $val):?>
                                            <li>
                                                <span>
                                                    <?php foreach($val->specificationsItemsValues as $v):?>
                                                        <?php if($chosen_specifications[$value->id] == $v->id) echo $v->value;?>
                                                    <?php endforeach; ?>
                                                    <?php echo $val->specificationsItemsLabels->name;?>
                                                </span>
                                            </li>
                                            <?php if($i==4) { ?>
                                                </ul>
                                            </div>
                                            <div class="category-spec col-sm-6 col-xs-6">
                                                 <ul>
                                            <?php } ?>
                                            <?php $i++;if($i == 10){break;} ?>
                                        <?php endforeach; ?>
                                        <?php if($i == 10){break;} ?>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="item-footer">
                        <button type="button" class="btn pull-left compare-button" data-cat="<?php echo $value->category_id; ?>">
                            <i class="fa fa-plus"></i>
                            <?php echo $this->translation['compare']?>
                            <div class="prdct-item__cmpr">
                                <label>
                                    <input type="checkbox" name="compare" class="prdct-item__cmpr-chkbx js-add-to-cmpr" data-cat="<?php echo $value->category_id; ?>" data-cookie="0">
                                    <span class="prdct-item__cmpr-img"></span>
                                </label>
                            </div>
                        </button>
                        <p class="text-uppercase">
                            <?php echo $this->translation['comments:']?>
                            <span>
                                (
                                <?php echo count($value->productComments);?>
                                )
                            </span>
                        </p>
                        <?php
                        $sum = 0;
                        foreach($value->productComments as $val) {
                            $sum += $val->rating;
                        }
                        ?>
                        <?php
                            $rating = 0;
                            if(count($value->productComments) > 0)
                                $rating = sprintf ("%.1f", $sum/count($value->productComments));

                        ;?>
                        <div class="stars pull-right">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/stars.png" alt="stars">
                            <div class="stars-bg" style="width: <?php if(isset($rating))echo  $rating*20;?>%"></div>
                            <div class="stars-bg2" style="width: <?php if(isset($rating)) echo 100-$rating*20;?>%"></div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div>
            <nav class="pagination col-md-12">
                <?php
                $this->widget('CLinkPager', array(
                    'pages' => $page,
                    'id' => '',
                    'itemCount' => $item_count,
                    'header' => '',
                    'maxButtonCount' => 5,
                    'selectedPageCssClass' => 'active',
                    'hiddenPageCssClass' => 'disable',
                    'nextPageLabel' => '>',         // »
                    'prevPageLabel' => '<',         // «
                    'lastPageCssClass' => 'hide',
                    'firstPageCssClass' => 'hide',
                    'htmlOptions' => array('class' => 'pagination__list'),
                    'nextPageCssClass'  => 'pagination__next btn-squae',
                    'internalPageCssClass' => 'pagination__page btn-squae',
                    'previousPageCssClass' => 'pagination__previous btn-squae',
                ));
                ?>
            </nav>
        </div>
    </div>
    <div class="col-sm-2 hidden-xs products-right-content">
        <div class="products-right">
            <div class="name">
                <a href ="#">
                    Microsoft Lumia
                </a>
            </div>
            <div class="product-image">
                <a href ="#">
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/imgpsh_fullsize.png" alt="samsung TV" />
                </a>
            </div>
        </div>
        <div class="products-right">
            <div class="name">
                <a href ="#">
                    Huawei Ascend G535
                </a>
            </div>
            <div class="product-image">
                <a href ="#">
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/imgpsh_fullsize1.png" alt="samsung TV" />
                </a>
            </div>
        </div>
        <div class="products-right">
            <div class="name">
                <a href ="#">
                    Microsoft Lumia
                </a>
            </div>
            <div class="product-image">
                <a href ="#">
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/imgpsh_fullsize.png" alt="samsung TV" />
                </a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript"><!--
//    document.onkeydown = fkey;
//
//    function fkey(e){
//        e = e || window.event;
//        if (e.keyCode == 116) {
//            history.go(0);
//            location.reload();
//        }
//    }
    $( document ).ready(function() {

        $.mCustomScrollbar.defaults.scrollButtons.enable=true; //enable scrolling buttons by default
        $.mCustomScrollbar.defaults.axis="yx"; //enable 2 axis scrollbars by default

        $("#content-ltn").mCustomScrollbar({
            theme:"light-thin"
        });

        $( ".list" ).on('click', function(e) {
            e.preventDefault();
            $(".product-layout").removeClass('product-grid col-md-4 col-sm-6').addClass('product-list');
            $(".additional-info, .item-footer").show();
            $(".product-name").hide();
            $('.grid').removeClass('active');
            $(this).addClass('active');
        });
        $( ".grid" ).on('click', function(e) {
            e.preventDefault();
            $(".product-layout").removeClass('product-list').addClass('product-grid col-md-4 col-sm-6 ');
            $(".additional-info, .item-footer").hide();
            $(".product-name").show();
            $('.list').removeClass('active');
            $(this).addClass('active');
        });
        $('.clear_filtres').on('click', function(e){
            e.preventDefault();
            $('input:checkbox').each(function(){
                this.checked = false;
            });
            createUrl();
        });

        $('.checkbox-styled, .search-button, #input-limit').on('click', function(e) {
            e.preventDefault();
            createUrl();
        });
        function createUrl(){
            var url;
            filter = [];
            brand = [];
            search = '';
            limit = '';

            $('input[name^=\'Filter\']:checked').each(function(element) {
                filter.push(this.value);

            });
            $('input[name^=\'Brand\']:checked').each(function(element) {
                brand.push(this.value);
            });

            var params = '';
            if(brand.length > 0){
                params += '&brand=' + brand.join(',')
            }
            if(filter.length > 0){
                params += '&filter=' + filter.join(',')
            }
            search = $('input[name^=\'Search\']');
            if(search.val() != '') {
                params += '&search=' + search.val();
            }
            limit = $('select[name^=\'Limit\']');
            if(limit.val() != 12) {
                params += '&limit=' + limit.val();
            }

            url = '<?php echo Yii::app()->createUrl('product/category', array('path' =>$_GET['path']));?>' + params;

            location = url;
        }
    });


//--></script>