
<?php
$sum = 0;
$have = 0;
$count = array(
    '5' => 0,
    '4' => 0,
    '3' => 0,
    '2' => 0,
    '1' => 0,
);
foreach ($model->productComments as $val) {
    $sum += $val->rating;
    if ($val->rating == 1) {
        $count[1]++;
    } elseif ($val->rating == 2) {
        $count[2]++;
    } elseif ($val->rating == 3) {
        $count[3]++;
    } elseif ($val->rating == 4) {
        $count[4]++;
    } elseif ($val->rating == 5) {
        $count[5]++;
    }
    if ($val->have == 1) {
        $have++;
    }
}
$max_count = max($count);
?>


<div class="cmpr-pnl-wrpr add-cmp-mr">
    <div class="cmpr-pnl__close">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/compare_sliderbutton.png" alt="C">
    </div>
    <div class="sidebaroverlay"></div>
    <div class="droppable cmpr-pnl sctn sctn--sdbr cmpr-pnl-list cmpr-pnl-list clearfix ui-front cmpr-pnl--bx-shdw">
        <div class="sctn__hdr clearfix">
            <div class="sctn__ttl">
                Compare Products
            </div>
            <div class="cmpr-pnl__close--alt js-cmpr-pnl-cls">
                <?php echo $this->translation['close']; ?>
            </div>
        </div>
        <div class="sctn__inr">
            <?php foreach($compare_products as $key => $value):?>
                <div class="cmpr-pnl-list__item" data-comparemspid="<?php echo $value->id?>" data-subcategory="">
                    <div class="cmpr-pnl-list__img-wrpr">
                        <img class="cmpr-pnl-list__img" src="<?php echo THUMB;?><?php echo Yii::app()->request->baseUrl?>/vendor/image/products/<?php echo $value->productImages[0]->image;?>&w=190&h=190" alt="<?php echo $value->productLabels->name;?>">
                    </div>
                    <div class="cmpr-pnl-list__item-dtls">
                        <div class="cmpr-pnl-list__item-ttl">
                            <?php echo $value->productLabels->name;?>
                        </div>
                    </div>
                    <span class="cmpr-pnl-list__item-rmv js-cmpr-itm-rmv" title="Close">&#x2715;</span>
                </div>
            <?php endforeach;?>
            <?php
            if(!isset($key))
                $key = 0;

            for($i = 0; $i < 6-$key; $i++):?>
                <div class="cmpr-pnl-list__item cmpr0" data-comparemspid="" data-subcategory="">
                    <div class="cmpr-pnl-list__img-wrpr">
                        <img class="cmpr-pnl-list__img"
                             src="<?php echo THUMB; ?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/generic_product_image.png&w=60&h=60" alt="">
                    </div>
                    <div class="cmpr-pnl-list__item-dtls">
                        <div class="cmpr-pnl-list__item-ttl">
                            <div class="srch-wdgt srch-wdgt--s srch-wdgt--xs"></div>
                        </div>
                    </div>
                </div>
            <?php endfor;?>
        </div>

        <a class="transition sctn__compare-btn"
           href="<?php echo Yii::app()->createUrl('/product/compare/', array('path' => $_GET['path'])); ?>">
            <?php echo $this->translation['compare']; ?>
        </a>
    </div>
</div>

<div class="bg clearfix">
    <div class="product" data-category="<?php echo $model->category_id; ?>" data-id="<?php echo $model->id; ?>">
        <div class="breadcrumb-content">
            <div>
                <ol class="breadcrumb">
                    <li><a class="transition" href="<?= Yii::app()->createUrl("site/index"); ?>">Home</a></li>
                    <li><a class="transition"
                           href="<?php $param = (isset($_GET['path']) && $_GET['path'] == 'All') ? 'All' : $category_name->category_id;
                           echo Yii::app()->createUrl('product/category', array('path' => $param)); ?>"><?php echo (isset($_GET['path']) && $_GET['path'] == 'All') ? 'All Categories' : $category_name->name; ?></a>
                    </li>
                    <li class="active">
                        <?php echo $model->productLabels->name; ?>
                    </li>
                </ol>
            </div>
        </div>

        <div class="row main clearfix">
            <div class="clearfix">
                <div class="col-sm-6 slideshow-content">
                    <div class="brand-content">
                        <div class="brand">
                            <img
                                src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/brands/<?php echo $model->brand->image; ?>"
                                alt="<?php echo $model->brand->image; ?>" class="img-responsive">
                        </div>
                        <div id="gallery" class="content">
                            <div id="controls" class="controls"></div>
                            <div class="slideshow-container">
                                <div id="loading" class="loader"></div>
                                <div id="slideshow" class="slideshow"></div>
                            </div>
                        </div>
                        <div>
                            <div id="thumbs" class="navigation">
                                <ul class="thumbs noscript clearfix">
                                    <?php if (!empty($model->productImages)): ?>
                                        <?php foreach ($model->productImages as $key => $value): ?>
                                            <li data-id="<?php echo $value->id; ?>">
                                                <a class="thumb" name="leaf"
                                                   href="<?php echo THUMB; ?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/<?php echo $value['image'] ?>&w=300&h=300"
                                                   title="<?php echo $value['image'] ?>">
                                                    <img src="<?php echo THUMB; ?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/<?php echo $value['image'] ?>&w=300&h=300" alt="<?php echo $value['image'] ?>"/>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                                <!-- Large modal -->
                                <button type="button" class="button-slider" data-toggle="modal"
                                        data-target=".bs-example-modal-lg"></button>
                                <?php if (!empty($model->productVideos)): ?>
                                    <div class="clearfix product-videos">
                                        <p class="videos-text">
                                            <?php echo $this->translation['videos']; ?>
                                        </p>
                                        <ul class="list-unstyled">
                                            <?php foreach ($model->productVideos as $value): ?>
                                                <li>
                                                    <a class="popup-youtube" href="<?php echo $value->video; ?>">
                                                        <iframe class="video" width="100%" height="60px"
                                                                src="<?php echo str_replace("watch", "embed", $value->video); ?>"
                                                                frameborder="0" allowfullscreen></iframe>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>
                            </div><!-- End Advanced Gallery Html Containers -->

                            <!-- modal -->
                            <div class="modal fade bs-example-modal-lg container" id="product_popup" tabindex="-1" role="dialog"
                                 aria-labelledby="myLargeModalLabel">
                                <div class="modal-dialog" role="document" style="width: 100%">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <i class="fa fa-eye"></i>
                                            <?php echo $model->productLabels->name; ?>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-sm-9 image text-center">
                                                <img src="" class="img-resp slider-big-image">
                                            </div>
                                            <div class="col-sm-3 images">
                                                <ul class="tclearfix list-inline">
                                                    <?php if (!empty($model->productImages)): ?>
                                                        <?php foreach ($model->productImages as $key => $value): ?>
                                                            <li class="slider-image-list transition <?=($key == 0) ? "active" : ""?>">
                                                                <img id="image-<?php echo $value->id; ?>"
                                                                     data-image="<?php echo THUMB; ?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/<?php echo $value['image'] ?>&w=550&h=550"
                                                                     src="<?php echo THUMB; ?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/<?php echo $value['image'] ?>&w=50&h=50"
                                                                     alt="<?php echo $value['image'] ?>"/>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
                <div class="col-sm-6 product-description-content">
                    <div class="product-name">
                        <div class="clearfix">
                            <div class="my-row">
                                <div class="col-sm-4">
                                    <h3>
                                        <?php echo $model->brand->brandLabels->name; ?>
                                    </h3>
                                </div>
                                <div class="col-sm-8 text-right">
                                    <button class="btn btn-default add-wishlist" data-id = "<?php echo $model->id;?>" <?php if (isset(Yii::app()->request->cookies['wish_list_id']) && (in_array($_GET['id'], explode(',', Yii::app()->request->cookies['wish_list_id']))) ) { echo 'disabled';}?>>
                                        <i class="fa fa-heart-o"></i>
                                        WISH LIST
                                    </button>
                                    <div class="product-layout prdct-item" data-mspid="<?php echo $model->id;?>" data-cat="3">
                                        <div class="product-image">
                                            <a href="/projects/estonaci/index.php?r=product/index&amp;path=All&amp;id=46">
                                                <img class="prdct-item__img draggable2 ui-draggable ui-draggable-handle" src="/projects/estonaci/vendor/image/timthumb.php?src=/projects/estonaci/vendor/image/products/a03a4a597736267d6e0b1a7df8a4843d.jpg&amp;w=190&amp;h=190" alt="BenQ RL2455HM">
                                            </a>
                                        </div>
                                        <div class="product-name text-uppercase">
                                            <p>
                                                <a class="transition prdct-item__name" href="#">
                                                    <?php echo $model->productLabels->name; ?>
                                                </a>
                                            </p>
                                        </div>
                                        <div class="">
                                            <?php if($compare_button_disable['disable']){?>
                                                <button type="button" class="btn btn-default add-compare compare-button" data-cat="<?php echo $model->category_id;?>" data-toggle="tooltip" data-placement="top" title="<?php echo $compare_button_disable['text'];?>" <?php if($compare_button_disable['disable']) echo "disabled";?>>
                                            <?php } else {?>
                                                <button type="button" class="btn btn-default add-compare compare-button" data-cat="<?php echo $model->category_id;?>">
                                            <?php }?>
                                                <i class="fa fa-plus"></i>
                                                COMPARE
                                                <div class="prdct-item__cmpr">
                                                    <label>
                                                        <input type="checkbox" name="compare" class="prdct-item__cmpr-chkbx .js-add-to-cmpr-btn js-add-to-cmpr" data-cat="<?php echo $model->category_id;?>" data-cookie="0">
                                                        <span class="prdct-item__cmpr-img"></span>
                                                    </label>
                                                </div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span>
                             <?php echo $model->productLabels->name ?>
                        </span>
                        <br>
                        <span class="product-price">
                            <?php echo $price; ?>
                        </span>
                        <div class="product-model text-right">
                            <?php if($model->model) : ?>
                                <span>
                                    <?php echo $model->model;?>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="product_rating">
                        <?php if (count($model->productComments) > 0) $rating = sprintf("%.1f", $sum / count($model->productComments)); ?>

                        <div class="stars">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/stars.png" alt="stars">
                            <div class="stars-bg" style="width: <?php if (isset($rating)) echo $rating * 20; ?>%"></div>
                            <div class="stars-bg2"
                                 style="width: <?php if (isset($rating)) echo 100 - $rating * 20; ?>%"></div>
                        </div>
                            <span class="reviews">
                                <?php echo count($model->productComments); ?> reviews
                            </span>
                    </div>
                    <div class="product-parameters">
                        <div class="features">
                            <b>
                                Features
                            </b>
                            <a href="#" id="view-all-spec">
                                (view all)
                            </a>
                        </div>
                        <div class="clearfix">
                            <?php foreach (array_chunk($specifications, 2) as $children) { ?>
                                <ul class="col-sm-6">
                                    <?php $i = 0;
                                    foreach ($children as $key => $value) { ?>
                                        <?php foreach ($value->specification->specificationsItems(array('limit' => 5)) as $k => $val): ?>
                                            <li>
                                                <?php foreach ($val->specificationsItemsValues as $v): ?>
                                                    <?php if (isset($chosen_specifications[$val->id]) && ($chosen_specifications[$val->id] == $v->id)) echo $v->value; ?>
                                                <?php endforeach; ?>
                                                <?php echo $val->specificationsItemsLabels->name; ?>
                                            </li>
                                            <?php $i++;
                                            if ($i == 5) {
                                                break;
                                            } ?>
                                        <?php endforeach; ?>
                                        <?php if ($i == 5) {
                                            break;
                                        } ?>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product-tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#overview" class="overview" aria-controls="overview" role="tab" data-toggle="tab">
                            <i class="fa fa-info-circle"></i>
                            <?php echo $this->translation['overview']; ?>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#specs" class="specs" aria-controls="specs" role="tab" data-toggle="tab">
                            <i class="fa fa-th-list"></i>
                            <?php echo $this->translation['specs']; ?>
                        </a>
                    </li>
                    <li role="presentation" class="hide">
                        <a href="#photos" aria-controls="photos" role="tab" data-toggle="tab">
                            <i class="fa fa-picture-o"></i>
                            Photos
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#comments" class="comments-tabs" aria-controls="comments" role="tab" data-toggle="tab">
                            <?php echo $this->translation['comments']; ?>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="ratings">
                <div class="clearfix">
                    <div class="col-sm-4 text-center">
                        <div class="sum-of-rating">
                            <?php if (count($model->productComments) > 0) {
                                echo $rating = sprintf("%.1f", $sum / count($model->productComments));
                            } else {
                                echo $rating = 0;
                            }; ?>
                        </div>
                        <div class="stars">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/stars.png" alt="stars">
                            <div class="stars-bg" style="width: <?php if (isset($rating)) echo $rating * 20; ?>%"></div>
                            <div class="stars-bg2"
                                 style="width: <?php if (isset($rating)) echo 100 - $rating * 20; ?>%"></div>
                        </div>
                        <div>
                            <?php echo $this->translation['rated_by']; ?>
                            <?php echo count($model->productComments); ?>
                            <?php echo $this->translation['users']; ?>
                        </div>
                    </div>
                    <div class="col-sm-6 star-diagram">
                        <table>
                            <?php foreach ($count as $k => $v): ?>
                                <tr>
                                    <td>
                                        <?php echo $k; ?>
                                    </td>
                                    <td>
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/star.png"
                                             alt="star">
                                    </td>
                                    <td style="width: <?php echo ($max_count != 0) ? ($max_count == $v) ? 200 : ($v * 200) / $max_count : 0; ?>px">
                                    </td>
                                    <td>
                                        <?php echo $v; ?>x
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                        <table>
                            <tr>
                                <td>
                                    <?php echo $have; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $this->translation['have_it']; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php if (isset($rating)) echo $rating * 20; ?> %
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $this->translation['customers_recommend']; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-2 add-review-content">
                        <a href="#" id="add-review">
                            <i class="fa fa-pencil"></i>
                            <?php echo $this->translation['add_a_review']; ?>
                        </a>
                    </div>
                </div>
                <?php if (count($model->productComments) == 0) : ?>
                    <div class="no-comments">
                        <div>
                            Here is no comments yet
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <div class="clearfix">
                <div class="product-info">
                    <div class="col-sm-8">

                        <div class="tab-content product-tab-content">
                            <div role="tabpanel" class="tab-pane active" id="overview">
                                <div class="col-sm-12">
                                    <?php echo $model->productLabels->name ?>
                                    <table class="table">
                                        <tbody>
                                            <?php
                                                if($site_price_list_has_product):
                                                    foreach($site_price_list_has_product as $key => $value):
                                            ?>
                                                        <tr class="price_tbl_tr">
                                                            <td><img class = "price_tbl_img" src="<?=Yii::app()->request->BaseUrl?>/vendor/image/sites_prices_list/<?=$value->sitePriceList->image?>"></td>
                                                            <td><i class="price_tbl_fa_icon fa fa-check-circle-o" aria-hidden="true"></i><?=$value->sitePriceList->sipping_date?></td>
                                                            <td><?=$value->price?></td>
                                                            <td><a href="<?=$value->url?>" class="text-uppercase price_tbl_a" target="_blank">Go to store ></a></td>
                                                        </tr>
                                            <?php
                                                    endforeach;
                                                endif;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-12">
                                    <?php echo $model->productLabels->description ?>
                                </div>
                                <div class="specification">
                                    <h3>
                                        <?php echo $this->translation['specification']; ?>
                                    </h3>
                                    <div>
                                        <div class="specification-body">
                                            <?php foreach ($specifications as $key => $value) : ?>
                                                <table class="table">
                                                    <tr class="head">
                                                        <td colspan="2">
                                                            <?php echo $value->specification->specificationsGroupLabels->name; ?>
                                                        </td>
                                                    </tr>
                                                    <?php foreach ($value->specification->specificationsItems as $k => $val): ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $val->specificationsItemsLabels->name; ?>
                                                            </td>
                                                            <td>
                                                                <?php foreach ($val->specificationsItemsValues as $v): ?>
                                                                    <?php if (isset($chosen_specifications[$val->id]) && ($chosen_specifications[$val->id] == $v->id)) echo $v->value; ?>
                                                                <?php endforeach; ?>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </table>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="more-specification hide">
                                        <i class="fa fa-plus"></i>
                                        <?php echo $this->translation['more']; ?>
                                    </div>
                                </div>
                                <?php if ($model->productComments): ?>
                                    <div class="comment-content clearfix">
                                        <h3>
                                            <?php echo $this->translation['comments']; ?>
                                        </h3>
                                        <?php foreach ($model->productComments as $key => $value) : ?>
                                            <div class="comments-items">
                                                <div class="comment comment-content-items clearfix">
                                                    <div class="col-sm-2">
                                                        <div class="comment-date">
                                                            <p>
                                                                <?php echo $value->name; ?>
                                                            </p>
                                                            <span class="date">
                                                                <?php echo date('d/m/Y', strtotime($value->created_date)); ?>
                                                            </span>
                                                            <div class="product_rating">
                                                                <div class="stars">
                                                                    <img
                                                                        src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/stars.png"
                                                                        alt="stars">
                                                                    <div class="stars-bg"
                                                                         style="width: <?php echo $value->rating * 20; ?>%"></div>
                                                                    <div class="stars-bg2"
                                                                         style="width: <?php echo 100 - $value->rating * 20; ?>%"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <div class="">
                                                            <div class="clearfix">
                                                            </div>
                                                            <div class="headline">
                                                                <?php echo $value->content; ?>
                                                            </div>
                                                            <div class="row">
                                                                <?php foreach($value->productCommentNegPosReviews as $val):?>
                                                                    <div class="col-sm-6 comment-plus">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                    <span>
                                                                        <i class="fa fa-plus-circle"></i>
                                                                    </span>
                                                                                </td>
                                                                                <td>
                                                                                    <?php echo $val->plus; ?>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                    <div class="col-sm-6 comment-minus">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                     <span>
                                                                         <i class="fa fa-minus-circle"></i>
                                                                    </span>
                                                                                </td>
                                                                                <td>
                                                                                    <?php echo $val->minus; ?>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                <?php endforeach;?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            </div>

                            <div role="tabpanel" class="tab-pane" id="specs">
                                <div class="specification">
                                    <h3>
                                        <?php echo $this->translation['specification']; ?>
                                    </h3>
                                    <div>
                                        <div class="specification-body">
                                            <?php foreach ($specifications as $key => $value) : ?>
                                                <?php if ($key % 2 == 0): ?>
                                                    <div class="clearfix">
                                                <?php endif; ?>
                                                <table class="table">
                                                    <tr class="head">
                                                        <td colspan="2">
                                                            <?php echo $value->specification->specificationsGroupLabels->name; ?>
                                                        </td>
                                                    </tr>
                                                    <?php foreach ($value->specification->specificationsItems as $k => $val): ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $val->specificationsItemsLabels->name; ?>
                                                            </td>
                                                            <td>
                                                                <?php foreach ($val->specificationsItemsValues as $v): ?>
                                                                    <?php if (isset($chosen_specifications[$val->id]) && ($chosen_specifications[$val->id] == $v->id)) echo $v->value; ?>
                                                                <?php endforeach; ?>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </table>
                                                <?php if ($key % 2 == 1): ?>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                            <?php if ($key % 2 == 0 && $key != 0): ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="more-specification hide">
                                    <i class="fa fa-plus"></i>
                                    <?php echo $this->translation['more']; ?>
                                </div>
                            </div>
                        <div role="tabpanel" class="tab-pane" id="comments">
                            <div class="comment-content clearfix">
                                <h3>
                                    <?php echo $this->translation['comments']; ?>
                                </h3>
                                <?php foreach ($model->productComments as $key => $value) : ?>
                                    <div class="comments-items">
                                        <div class="comment comment-content-items clearfix">
                                            <div class="col-sm-2">
                                                <div class="comment-date">
                                                    <p>
                                                        <?php echo $value->name; ?>
                                                    </p>
                                            <span class="date">
                                                 <?php echo date('d/m/Y', strtotime($value->created_date)); ?>
                                            </span>
                                                    <div class="product_rating">
                                                        <div class="stars">
                                                            <img
                                                                src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/stars.png"
                                                                alt="stars">
                                                            <div class="stars-bg"
                                                                 style="width: <?php echo $value->rating * 20; ?>%"></div>
                                                            <div class="stars-bg2"
                                                                 style="width: <?php echo 100 - $value->rating * 20; ?>%"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-10">
                                                <div class="">
                                                    <div class="clearfix">
                                                    </div>
                                                    <div class="headline">
                                                        <?php echo $value->content; ?>
                                                    </div>
                                                    <div class="row">
                                                        <?php foreach($value->productCommentNegPosReviews as $val):?>
                                                            <div class="col-sm-6 comment-plus">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                    <span>
                                                                        <i class="fa fa-plus-circle"></i>
                                                                    </span>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $val->plus; ?>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="col-sm-6 comment-minus">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                     <span>
                                                                         <i class="fa fa-minus-circle"></i>
                                                                    </span>
                                                                        </td>
                                                                        <td>
                                                                            <?php echo $val->minus; ?>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        <?php endforeach;?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        </div>

                        <div class="add-comment">
                        <div class="write-review">
                        <span class="user-comments">
                             <?php echo $this->translation['user_comments']; ?>
                        </span>
                            <button type="button" class="close close-comment" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <hr>
                            <form id="form" class="form-horizontal">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">
                                        <?php echo $this->translation['name']; ?>
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="text" name="Review[product_id]" value="<?php echo $_GET['id']; ?>"
                                               class="hide">
                                        <input type="text" name="Review[name]" class="form-control" id="inputEmail3"
                                               required>
                                        <div class="name-error"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">
                                        <?php echo $this->translation['email']; ?>
                                        <span class="required">*</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="email" name="Review[email]" class="form-control"
                                               id="inputPassword3" required>
                                        <div class="email-error"></div>
                                    </div>
                                </div>
                                <div class="">
                                    <label for="Review-content" class="control-label">
                                        <?php echo $this->translation['content']; ?>
                                    </label>
                                    <div class="">
                                        <textarea name="Review[content]" class="form-control" rows="7"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label for="Review-plus" class="control-label">
                                            <?php echo $this->translation['plus']; ?>
                                        </label>
                                        <div class="">
                                            <input name="Review[pl-min][0][plus]" class="form-control" id="Review-plus">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="Review-minus" class="control-label">
                                            <?php echo $this->translation['minus']; ?>
                                        </label>
                                        <div class="">
                                            <input name="Review[pl-min][0][minus]" class="form-control review-minus" data-addnext="0">
                                        </div>
                                    </div>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="Review[have]" type="checkbox" value="1"> I hav it
                                    </label>
                                </div>
                                <div class="clearfix text-left">
                                    <label for="inputEmail3" class="pull-left">
                                        <?php echo $this->translation['overall_rating:']; ?>
                                    </label>
                                    <div class="col-sm-6">
                                 <span class="star-rating">
                                    <input type="radio" name="Review[rating]" value="1"><i></i>
                                    <input type="radio" name="Review[rating]" value="2"><i></i>
                                    <input type="radio" name="Review[rating]" value="3"><i></i>
                                    <input type="radio" name="Review[rating]" value="4"><i></i>
                                    <input type="radio" name="Review[rating]" value="5"><i></i>
                                </span>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success send-comment">
                                        <?php echo $this->translation['send']; ?>
                                    </button>
                                    <button type="button" class="btn close-comment">
                                        <?php echo $this->translation['close']; ?>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    </div>
                    <div class="col-sm-4">
                        <?php if($similar_products) :?>
                            <div class="text-uppercase similar-products-text">
                                similar products
                            </div>
                        <div class="similar-products">
                            <table class="table">
                            <?php foreach($similar_products as $key => $value):?>
                                <?php
                                $sum_s = 0;

                                $count_s = array(
                                    '5' => 0,
                                    '4' => 0,
                                    '3' => 0,
                                    '2' => 0,
                                    '1' => 0,
                                );
                                foreach ($value->productComments as $val) {
                                    $sum_s += $val->rating;
                                    if ($val->rating == 1) {
                                        $count_s[1]++;
                                    } elseif ($val->rating == 2) {
                                        $count_s[2]++;
                                    } elseif ($val->rating == 3) {
                                        $count_s[3]++;
                                    } elseif ($val->rating == 4) {
                                        $count_s[4]++;
                                    } elseif ($val->rating == 5) {
                                        $count_s[5]++;
                                    }
                                }
                                $max_count_s = max($count);
                                ?>

                                <tr>
                                    <td>
                                        <a href ="<?php echo Yii::app()->createUrl('product/index', array('path' => $_GET['path'], 'id' => $value->id));?>">
                                            <img src="<?php echo THUMB;?><?php Yii::app()->request->baseUrl?>/vendor/image/products/<?php echo $value->productImages[0]->image;?>&w=80" alt="<?php echo $value->productLabels->name;?>">
                                        </a>
                                    </td>
                                    <td>
                                        <div class="similar-products-name">
                                            <span>
                                                <a href ="<?php echo Yii::app()->createUrl('product/index', array('path' => $_GET['path'], 'id' => $value->id));?>">
                                                    <?php echo $value->productLabels->name;?>
                                                </a>
                                            </span>
                                        </div>
                                        <div>
                                            <span class="product_rating">
                                                <?php if (count($value->productComments) > 0) $rating_s = sprintf("%.1f", $sum_s / count($value->productComments)); ?>

                                                <div class="stars">
                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/stars.png" alt="stars">
                                                    <div class="stars-bg" style="width: <?php if (isset($rating_s)) echo $rating_s * 20; ?>%"></div>
                                                    <div class="stars-bg2" style="width: <?php if (isset($rating_s)) echo 100 - $rating_s * 20; ?>%"></div>
                                                </div>
                                            </span>
                                            <span class="reviews">
                                                <?php echo count($value->productComments); ?> reviews
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        158$
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </table>
                        </div>
                    <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {

        var that;
        $('.add-wishlist').on('click', function(){
            that = $(this);
            $.ajax({
                url : '<?php echo Yii::app()->createUrl("product/addWishList");?>',
                type: 'POST',
                data: 'id='+$(this).data('id'),
                success: function (data) {
                    if(data == 'ok'){
                        that.attr('disabled', 'disabled');
                    }
                }
            });
        });


//        $('.add-compare').on('click', function(){
//            $('.prdct-item__cmpr-chkbx').trigger('click');
//        });
        var k =1;
        $(document).on('click','.review-minus', function(){
            if($(this).data('addnext') == 0) {
                $(this).data('addnext', 1);
                $(this).parents().closest('.form-group').append('<div class="col-sm-6">' +
                    '<label for="Review-plus" class="control-label"> <?php echo $this->translation['plus']; ?> </label>  ' +
                    '<div class=""> ' +
                    '<input name="Review[pl-min][' + k + '][plus]" class="form-control" id="Review-plus">' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-sm-6">' +
                    '<label for="Review-minus" class="control-label"> <?php echo $this->translation['minus']; ?> </label> ' +
                    '<div class=""> ' +
                    '<input name="Review[pl-min][' + k + '][minus]" class="form-control review-minus" data-addnext="0">' +
                    '</div>' +
                    '</div>');
                k++;
            }
        });

        $('.button-slider').on('click', function () {
            var id = $('.thumbs li.selected').data('id');
            var image = $('#image-' + id).data('image');
            $('#image-' + id).parent().find('active').removeClass('active');
            $('#image-' + id).parent().addClass('active');
            $('.slider-big-image').attr('src', image);
        });

        $('.slider-image-list').on('click', function () {
            $('.slider-image-list').removeClass('active');
            $(this).addClass('active');
            var image = $(this).children('img').data('image');
            $('.slider-big-image').attr('src', image);
        });
        $('#product_popup').bind('mousewheel', function(e){
            var x = $('.slider-image-list.transition.active');
            $(x).removeClass('active');

            var elem = "";
            if($(x).next().length != 0){
                elem = $(x).next();
                elem.addClass('active');
            }else{
                elem = $('.slider-image-list.transition:first');
                elem.addClass('active');
            }

            var image = elem.children('img').data('image');
            $('.slider-big-image').attr('src', image);
        });
        $(".write-review").hide(0);

        $('.specs').on('click', function () {
            $(".write-review").hide(0);
        });

        $('.video').on('click', function (e) {
            e.preventDefault();

        });
        $(document).on('click', '.product-videos li', function () {
            $(this).children('a')[0].click();
        });
        $('.popup-youtube').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,

            fixedContentPos: false
        });

        $('.close-comment').on('click', function (e) {
            e.preventDefault();
            $(".write-review").hide('slow');
        });

        $('.overview, .specs').on('click', function () {
            $(".write-review").hide('fast');
        });

        $('.comments-tabs').on('click', function () {
            $(".write-review").show('fast');
        });

        $('#view-all-spec').on('click', function (e) {
            e.preventDefault();
            $('.nav-tabs li .specs').trigger('click')

            $('html,body').animate({
                    scrollTop: $('.nav-tabs li .specs').offset().top
                },
                'slow');
        });

        $('#add-review').on('click', function (e) {
            e.preventDefault();

            if ($('.nav-tabs li.active').children('a').hasClass('specs')) {
                $('.comments-tabs').trigger('click')
            }

            $(".write-review").show('fast');
            $('html,body').animate({
                    scrollTop: $(".write-review").offset().top
                },
                'slow');
        });

        $('.send-comment').on('click', function (e) {
            e.preventDefault();
            var data = $('#form').serialize();
            $.ajax({
                type: 'post',
                url: '<?php echo Yii::app()->createUrl("product/addComment");?>',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data == 'ok') {
                        $(".write-review").hide('slow');
                        $('.add-comment').append('<p>Thanks for commenting!</p>')
                        location.reload();
                    }
                    if (data == 'no ok') {
                        $('.add-comment').append('<p>Something has wrong! Please contact with us.</p>')
                    }
                    if (data.name.length > 0) {
                        $('.name-error').html('<p style="color: red">' + data.name + '</p>');
                    }
                    if (data.email.length > 0) {
                        $('.email-error').html('<p style="color: red">' + data.email + '</p>');
                    }
                }
            });
        });

        $('.more-specification').on('click', function () {
            var data = 'cat_id=' + $('.product').data('category') + '&id=' + $('.product').data('id');
            $.ajax({
                type: 'post',
                url: '<?php echo Yii::app()->createUrl("product/getAllSpec");?>',
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data != 0) {
                        var tag = '';
                        $.each(data['spec'], function (key, value) {
                            if (key % 2 == 0) {
                                tag += '<div class="clearfix">';
                            }
                            tag += ' <table class="table"><tr class="head"><td colspan="2">' +
                                value.specification.specificationsGroupLabels.name
                                + '</td></tr>';
                            $.each(value.specification.specificationsItems, function (k, val) {
                                tag += '<tr><td>' +
                                    val.specificationsItemsLabels.name
                                    + '</td><td>';
                                $.each(val.specificationsItemsValues, function (index, v) {
                                    if (data['chosen'][val.id] == v.id) {
                                        tag += v.value;
                                    }
                                });
                            });
                            tag += ' </td></tr></table>';
                            if (key % 2 == 1) {
                                tag += '</div>';
                            }
                            $('.specification-body').html(tag);
                            $('.more-specification').remove();
                        });
                    }
                }
            });
        });
        var lastScrollTop = 0;
        $(document).on("keydown", function (e) {
            if (e.keyCode == 40) {
                console.log(111);
                var st = $(this).scrollTop();
                if (st > lastScrollTop) {
                    $('.slider-image-list.active').next().trigger('click');

                } else {
                    $('.slider-image-list.active').prev().trigger('click');
                }
                lastScrollTop = st;
            }
        });

    })
</script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {

        // We only want these styles applied when javascript is enabled
        $('div.navigation').addClass('clearfix').css({'width': '30%', 'float': 'left'});
        $('div.content').css('display', 'block');

        // Initially set opacity on thumbs and add
        // additional styling for hover effect on thumbs
        var onMouseOutOpacity = 0.67;
        $('#thumbs ul.thumbs li').opacityrollover({
            mouseOutOpacity: onMouseOutOpacity,
            mouseOverOpacity: 1.0,
            fadeSpeed: 'fast',
            exemptionSelector: '.selected'
        });

        // Initialize Advanced Galleriffic Gallery
        var gallery = $('#thumbs').galleriffic({
            delay: 2500,
            numThumbs: 6,
            preloadAhead: 10,
            enableTopPager: false,
            enableBottomPager: true,
            maxPagesToShow: 7,
            imageContainerSel: '#slideshow',
            controlsContainerSel: '#controls',
            captionContainerSel: '#caption',
            loadingContainerSel: '#loading',
            renderSSControls: false,
            renderNavControls: true,
            playLinkText: false,
            pauseLinkText: false,
            prevLinkText: '<',
            nextLinkText: '>',
            enableHistory: false,
            autoStart: false,
            syncTransitions: true,
            defaultTransitionDuration: 900,
            onSlideChange: function (prevIndex, nextIndex) {
                // 'this' refers to the gallery, which is an extension of $('#thumbs')
                this.find('ul.thumbs').children()
                    .eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
                    .eq(nextIndex).fadeTo('fast', 1.0);
            },
            onPageTransitionOut: function (callback) {
                this.fadeTo('fast', 0.0, callback);
            },
            onPageTransitionIn: function () {
                this.fadeTo('fast', 1.0);
            }
        });

        /**** Functions to support integration of galleriffic with the jquery.history plugin ****/

        // PageLoad function
        // This function is called when:
        // 1. after calling $.historyInit();
        // 2. after calling $.historyLoad();
        // 3. after pushing "Go Back" button of a browser
        function pageload(hash) {
            // alert("pageload: " + hash);
            // hash doesn't contain the first # character.
            if (hash) {
                $.galleriffic.gotoImage(hash);
            } else {
                gallery.gotoIndex(0);
            }
        }


        /****************************************************************************************/
    });
</script>
