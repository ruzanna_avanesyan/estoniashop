<div class="page-body compare compare-page">
    <div id="compareFix" class="container compare-toprow center fix-container">
        <div class="compare-tbl__row clearfix">
            <div class="compare-tbl__col col-xs-2 compare-tbl__spec">
            </div>
            <?php foreach($model as $key => $value) :?>
                <?php
                $sum = 0;
                foreach($value->productComments as $val) {
                    $sum += $val->rating;
                }
                ?>
                <div class="compare-tbl__col col-xs-2" data-mspid="<?php echo $value->id;?>">
                    <i class="close-item remove-compare fa fa-times-circle-o" data-id="<?php echo $value->id;?>"></i>
                    <a href="<?php echo Yii::app()->createUrl('product/index', array('path' => $_GET['path'], 'id' => $value->id));?>">
                        <img src="<?php echo THUMB;?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/<?php echo $value->productImages[0]->image;?>&w=48" class="pull-left fix-container__product-img"/>
                    </a>
                    <div class="pull-left fix-container__right-div">
                        <a class="transition item-title fix-container__product-title" href="<?php echo Yii::app()->createUrl('product/index', array('path' => $_GET['path'], 'id' => $value->id));?>">
                            <?php echo $value->productLabels->name;?>
                        </a>
                        <span>
                            <?php echo $value['price'];?>
                        </span>
                        <?php $rating = 0; if(count($value->productComments) > 0) $rating = sprintf ("%.1f", $sum/count( $value->productComments)) ;?>
                        <div class="stars">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/stars.png" alt="stars">
                            <div class="stars-bg" style="width: <?php if(isset($rating))echo  $rating*20;?>%"></div>
                            <div class="stars-bg2" style="width: <?php if(isset($rating)) echo 100-$rating*20;?>%"></div>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
            <?php for($i= 0;$i < (5-$key-1);$i++){?>
                <div class="compare-tbl__col col-xs-2 text-center" data-mspid="9998">
                <img src="<?php echo THUMB;?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/generic_product_image.png&w=48" class="fix-container__product-img"/>
                    <div class="c-entry-item-cont">

                        <?php
                        $this->widget('ext.myAutoComplete', array(
                            'id'=>'c-item1-'.$i,
                            'name'=>'c-item1-'.$i,
                            'source'=>'js: function(request, response) {
                                $.ajax({
                                    url: "'.$this->createUrl('product/suggestAddCompareItem').'",
                                    dataType: "json",
                                    data: {
                                        term: request.term,
                                    },
                                    success: function (data) {
                                        response(data);
                                    }
                                })
                            }',
                            'options' => array(
                                'showAnim' => 'fold',
                                'select'=>"js:function(event, ui) {
                                     $.ajax({
                                        type: 'post',
                                        url: '". $this->createUrl('product/addCompareItemToCookie') ."',
                                        data: 'product_id='+ui.item.id,
                                        success: function (data) {
                                            if (data = 1){
                                                location.reload()
                                            }
                                        }
                                    });
                                }"
                            ),
                            'htmlOptions' => array(
                                "placeholder" => "Enter product name",
                                "class"=>"c-entry-item"
                            ),
                            'methodChain'=>'.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                                return $( "<div class=\'drop_class\'></div>" )
                                    .data( "item.autocomplete", item )
                                    .append( "<div style=\'width:22%; float:left;\'><img height=50 width=50 src=\'' . Yii::app()->request->baseUrl.'/vendor/image/products/' . '" + item.label + "\'></div><div style=\'width:70%;float:left;margin-left:10px\'>" +item.value +  "</div>" )
                                    .append("<div style=\'clear:both;\'></div>")
                                    .appendTo( ul );
                            };'
                        ));
                        ?>
                    </div>
                </div>
            <?php }?>
        </div>
    </div>
    <div class="row main">
        <div class="col-xs-12">
            <div class="tab-content">
                <div class="table-responsive">
                    <table id="draggable-table" class="table compare-table">
                        <thead>
                            <tr>
                                <th class="col-xs-2"></th>
                                <?php foreach($model as $key => $value) :?>
                                    <?php
                                        $sum = 0;
                                        foreach($value->productComments as $val) {
                                            $sum += $val->rating;
                                        }
                                    ?>
                                    <th class="drag-enable col-xs-2">
                                        <div class="head-item">
                                            <div class="head-tools">
                                                <i class="change-position fa fa-arrows-h table-handle" data-id="<?php echo $value->id;?>"></i>
                                                <i class="close-item remove-compare fa fa-times-circle-o" data-id="<?php echo $value->id;?>"></i>
                                            </div>
                                            <div class="head-image text-center">
                                                <a href="<?php echo Yii::app()->createUrl('product/index', array('path' => $_GET['path'], 'id' => $value->id));?>">
                                                    <img src="<?php echo THUMB;?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/<?php echo $value->productImages[0]->image;?>&w=150&h=150" />
                                                </a>
                                            </div>
                                            <div class="head-name text-center">
                                                <a class="transition" href="<?php echo Yii::app()->createUrl('product/index', array('path' => $_GET['path'], 'id' => $value->id));?>">
                                                    <p>
                                                        <?php echo $value->productLabels->name;?>
                                                    </p>
                                                </a>
                                                <span>
                                                    <?php echo $value['price'];?>
                                                </span>
                                                <p class="stars-content">
                                                    <?php $rating = 0; if(count($value->productComments) > 0) $rating = sprintf ("%.1f", $sum/count( $value->productComments)) ;?>
                                                    <div class="stars">
                                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/stars.png" alt="stars">
                                                        <div class="stars-bg" style="width: <?php if(isset($rating))echo  $rating*20;?>%"></div>
                                                        <div class="stars-bg2" style="width: <?php if(isset($rating)) echo 100-$rating*20;?>%"></div>
                                                    </div>
                                                </p>
                                            </div>
                                        </div>
                                    </th>
                                <?php endforeach;?>
                                <?php for($i= 0;$i < (5-$key-1);$i++){?>
                                    <th  class="drag-enable col-xs-2">
                                        <div class="head-item no-compare">
                                            <div class="head-tools">
                                                <i class="change-position fa fa-arrows-h"></i>
                                                <i class="close-item fa fa-times-circle-o"></i>
                                            </div>
                                            <div class="head-image text-center">
                                                <img src="<?php echo THUMB;?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/generic_product_image.png&w=150&h=150" />
                                                <div class="c-entry-item-cont">
                                                    <?php
                                                    $this->widget('ext.myAutoComplete', array(
                                                        'id'=>'c-item-'.$i,
                                                        'name'=>'c-item-'.$i,
                                                        'source'=>'js: function(request, response) {
                                                            $.ajax({
                                                                url: "'.$this->createUrl('product/suggestAddCompareItem').'",
                                                                dataType: "json",
                                                                data: {
                                                                    term: request.term,
                                                                },
                                                                success: function (data) {
                                                                    response(data);
                                                                }
                                                            })
                                                        }',
                                                        'options' => array(
                                                            'showAnim' => 'fold',
                                                            'select'=>"js:function(event, ui) {
                                                                 $.ajax({
                                                                    type: 'post',
                                                                    url: '". $this->createUrl('product/addCompareItemToCookie') ."',
                                                                    data: 'product_id='+ui.item.id,
                                                                    success: function (data) {
                                                                        if (data = 1){
                                                                            location.reload()
                                                                        }
                                                                    }
                                                                });
                                                            }"
                                                        ),
                                                        'htmlOptions' => array(
                                                            "placeholder" => "Enter product name",
                                                            "class"=>"c-entry-item"
                                                        ),
                                                        'methodChain'=>'.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                                return $( "<div class=\'drop_class\'></div>" )
                                    .data( "item.autocomplete", item )
                                    .append( "<div style=\'width:22%; float:left;\'><img height=50 width=50 src=\'' . Yii::app()->request->baseUrl.'/vendor/image/products/' . '" + item.label + "\'></div><div style=\'width:70%;float:left;margin-left:10px\'>" +item.value +  "</div>" )
                                    .append("<div style=\'clear:both;\'></div>")
                                    .appendTo( ul );
                            };'
                                                    ));
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </th>
                                <?php }?>
                            </tr>
                        </thead>
                        <tbody class="sortable sortable-table">
                        <?php foreach($specifications as $key => $value):?>
                            <tr class="active text-uppercase">
                                <td class="col-xs-2">
                                    <?php echo $value->specification->specificationsGroupLabels->name; ?>
                                </td>
                                <td class="col-xs-2"></td>
                                <td class="col-xs-2"></td>
                                <td class="col-xs-2"></td>
                                <td class="col-xs-2"></td>
                                <td class="col-xs-2"></td>
                            </tr>
                            <?php foreach($value->specification->specificationsItems as $k=> $val):?>
                                <tr>
                                    <td class="handle col-xs-2">
                                        <?php echo $val->specificationsItemsLabels->name;?>
                                    </td>
                                    <td class="col-xs-2">
                                        <?php if(isset($model_id[0])){?>
                                            <?php foreach($val->specificationsItemsValues as $k => $v){?>
                                                <?php if(isset($chosen_specifications[$model_id[0]][$val->id]) && ($chosen_specifications[$model_id[0]][$val->id] == $v->id)) echo $v->value;?>
                                            <?php } ?>
                                        <?php } ?>
                                    </td>
                                    <td class="col-xs-2">
                                        <?php if(isset($model_id[1])){?>
                                            <?php foreach($val->specificationsItemsValues as $k => $v){?>
                                                <?php if(isset($chosen_specifications[$model_id[1]][$val->id]) && ($chosen_specifications[$model_id[1]][$val->id] == $v->id)) echo $v->value;?>
                                            <?php } ?>
                                        <?php } ?>
                                    </td>
                                    <td class="col-xs-2">
                                        <?php if(isset($model_id[2])){?>
                                            <?php foreach($val->specificationsItemsValues as $k => $v){?>
                                                <?php if(isset($chosen_specifications[$model_id[2]][$val->id]) && ($chosen_specifications[$model_id[2]][$val->id] == $v->id)) echo $v->value;?>
                                            <?php } ?>
                                        <?php } ?>
                                    </td>
                                    <td class="col-xs-2">
                                        <?php if(isset($model_id[3])){?>
                                            <?php foreach($val->specificationsItemsValues as $k => $v){?>
                                                <?php if(isset($chosen_specifications[$model_id[3]][$val->id]) && ($chosen_specifications[$model_id[3]][$val->id] == $v->id)) echo $v->value;?>
                                            <?php } ?>
                                        <?php } ?>
                                    </td>
                                    <td class="col-xs-2">
                                        <?php if(isset($model_id[4])){?>
                                            <?php foreach($val->specificationsItemsValues as $k => $v){?>
                                                <?php if(isset($chosen_specifications[$model_id[4]][$val->id]) && ($chosen_specifications[$model_id[4]][$val->id] == $v->id)) echo $v->value;?>
                                            <?php } ?>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="compare-comments">
            <h3>
                <?php echo $this->translation['comments'];?>
            </h3>
            <ul class="nav nav-tabs" role="tablist">
                <?php foreach($model as $key => $value) :?>
                    <li class="tooltip top <?php echo $key==0 ? 'active' : ''?>" role="presentation" >
                        <a href="#tab_<?php echo $key;?>" aria-controls="home" role="tab" data-toggle="tab">
                            <?php echo $value->productLabels->name;?>
                        </a>
                        <div class="tooltip-arrow"></div>
                    </li>
                <?php endforeach;?>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content comment-content">
                <?php foreach($model as $key => $value) :?>
                    <div role="tabpanel" class="tab-pane <?php echo $key==0 ? 'active' : ''?>" id="tab_<?php echo $key;?>">
                        <?php foreach( $value->productComments as $k => $val) :?>
                            <div class="comments-items">
                                <div class="comment comment-content-items clearfix">
                                    <div class="col-xs-2">
                                        <div class="comment-date">
                                            <p>
                                                <?php echo $val->name;?>
                                            </p>
                                        <span class="date">
                                            <?php echo date('d/m/Y',strtotime($val->created_date));?>
                                        </span>
                                            <div class="product_rating">
                                                <div class="stars">
                                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/stars.png" alt="stars">
                                                    <div class="stars-bg" style="width: <?php echo $val->rating*20;?>%"></div>
                                                    <div class="stars-bg2" style="width: <?php echo 100-$val->rating*20;?>%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="">
                                            <div class="clearfix">
                                            </div>
                                            <div class="headline">
                                                <?php echo $val->content;?>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6 comment-plus">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                            <span>
                                                                <i class="fa fa-plus-circle"></i>
                                                            </span>
                                                            </td>
                                                            <td>
                                                                <?php echo $val->plus;?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-sm-6 comment-minus">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                             <span>
                                                                 <i class="fa fa-minus-circle"></i>
                                                            </span>
                                                            </td>
                                                            <td>
                                                                <?php echo $val->minus;?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.add-item-compare').on('click', function(){
            $(this).hide();
            $(this).parent().children('.c-entry-item-cont').show();
        });


        var top = $('.sortable-table').offset().top;
        $(window).scroll(function(e){
            if($(window).scrollTop() >= top){
                $('.fix-container').addClass('open');
            }else {
                $('.fix-container').removeClass('open');
            }
        });

//        $('.sortable').sortable({
//            handle: '.handle'
//        });

        $( ".compare-table thead tr th:last-child .head-item div:last-child").addClass('last-child-btn');

        $( ".compare-table thead tr th:last-child .head-item div:last-child button span").addClass('glyphicon glyphicon-plus');


        $('#draggable-table').dragtable({
            dragaccept: '.drag-enable',
            dragHandle: '.table-handle',
            persistState: function(table){
                if(table.startIndex < table.endIndex){
                    var that = $('.compare-tbl__row').find( "div.compare-tbl__col" ).eq( table.startIndex - 1 ).clone();
                    $( that ).insertAfter( $('.compare-tbl__row').find( "div.compare-tbl__col" ).eq( table.endIndex - 1 ) );
                    $('.compare-tbl__row').find( "div.compare-tbl__col" ).eq( table.startIndex - 1 ).remove();
                }else{
                    var that = $('.compare-tbl__row').find( "div.compare-tbl__col" ).eq( table.startIndex - 1 ).clone();
                    $('.compare-tbl__row').find( "div.compare-tbl__col" ).eq( table.startIndex - 1 ).remove();
                    $( that ).insertBefore( $('.compare-tbl__row').find( "div.compare-tbl__col" ).eq( table.endIndex - 1 ) );
                }
            }
        });

        $('.remove-compare').on('click', function(){
            var url = '<?php echo Yii::app()->createUrl('product/deleteComparePanelAjax');?>';
            $.ajax({
                type: "post",
                url: url,
                data: 'id='+$(this).data('id'),
                success: function (data) {
                    if (data = 1){
                        location.reload()
                    }
                }
            });
        });
    });
</script>