
<div class="bg clearfix">
    <div class="product col-sm-10">
        <div class="breadcrumb-content">
            <div>
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Monitors</a></li>
                    <li class="active">Monitor Benq XL2414H</li>
                </ol>
                <a href="#" class="send">
                    <span class="">
                        <i class="fa fa-envelope"></i>
                        SEND
                    </span>
                </a>
            </div>
        </div>
        <div class="row main clearfix">
            <div class="clearfix">
                <div class="col-sm-6 slideshow-content">
                    <div class="brand-content">
                        <div class="brand">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/brands/BenQ-logo.png" class="img-responsive">
                        </div>

                        <div id="gallery" class="content">
                            <div id="controls" class="controls"></div>
                            <div class="slideshow-container">
                                <div id="loading" class="loader"></div>
                                <div id="slideshow" class="slideshow"></div>
                            </div>
                        </div>
                        <div>
                            <div id="thumbs" class="navigation">
                                <ul class="thumbs noscript clearfix">
                                    <li>
                                        <a class="thumb" name="leaf" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/benq.png" title="Title #0">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/benq1.png" alt="Title #0" />
                                        </a>
                                    </li>
                                    <li>
                                        <a class="thumb" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/benq2.png" title="Title #10">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/benq3.png" alt="Title #10" />
                                        </a>
                                    </li>
                                    <li>
                                        <a class="thumb" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/benq.png" title="Title #16">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/benq1.png" />
                                        </a>
                                    </li>
                                    <li>
                                        <a class="thumb" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/benq2.png" title="Title #23">
                                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/benq3.png" alt="Title #23" />
                                        </a>
                                    </li>
                                </ul>
                                <div class="clearfix product-videos">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a class="popup-youtube" href="https://www.youtube.com/embed/F7-WyFj_aOk">
                                                <iframe class="video" width="70%" height="60px" src="https://www.youtube.com/embed/F7-WyFj_aOk" frameborder="0" allowfullscreen></iframe>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="popup-youtube" href="https://www.youtube.com/embed/F7-WyFj_aOk">
                                                <iframe class="video" width="70%" height="60px" src="https://www.youtube.com/embed/F7-WyFj_aOk" frameborder="0" allowfullscreen></iframe>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div><!-- End Advanced Gallery Html Containers -->

                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
                <div class="col-sm-6 product-description-content">
                    <div class="product-name">
                        <h3>
                            BenQ
                        </h3>
                            <span>
                               XL2411Z Gaming Monitor
                            </span>
                    </div>
                    <div class="product_rating">
                            <span class="star-rating">
                              <input type="radio" name="rating5" value="1" checked><i></i>
                              <input type="radio" name="rating5" value="2" checked><i></i>
                              <input type="radio" name="rating5" value="3" checked><i></i>
                              <input type="radio" name="rating5" value="4" checked><i></i>
                              <input type="radio" name="rating5" value="5" checked><i></i>
                          </span>
                            <span class="reviews">
                                5 reviews
                            </span>
                    </div>
                    <div class="product-parameters">
                        <ul>
                            <li>LED Backlit</li>
                            <li>1920x1080 Resolution</li>
                            <li>120000000:1 Contrast Ratio</li>
                            <li>350 cd/m Brightness</li>
                            <li>1ms Respose Time</li>
                            <li>1x Ananlogue Input</li>
                            <li>1x DVI-D</li>
                            <li>2x hdmi</li>
                            <li>2x hdmi</li>
                            <li>2x hdmi</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="product-tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#overview" aria-controls="overview" role="tab" data-toggle="tab">
                            <i class="fa fa-info-circle"></i>
                            Overview
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#specs" aria-controls="specs" role="tab" data-toggle="tab">
                            <i class="fa fa-th-list"></i>
                            Specs
                        </a>
                    </li>
                    <li role="presentation" class="hide">
                        <a href="#photos" aria-controls="photos" role="tab" data-toggle="tab">
                            <i class="fa fa-picture-o"></i>
                            Photos
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#comments" aria-controls="comments" role="tab" data-toggle="tab">
                            Comments
                        </a>
                    </li>
                </ul>
            </div>
            <div class="tab-content product-tab-content">
                <div role="tabpanel" class="tab-pane active" id="overview">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                    industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
                    scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
                    into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                    release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                    software like Aldus PageMaker including versions of Lorem Ipsum.
                    <div class="text-center">
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/benq.png" alt="">
                    </div>
                    Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical
                    Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at
                    Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from
                    a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the
                    undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et
                    Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the
                    theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum
                    dolor sit amet..", comes from a line in section 1.10.32.
                    There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration
                    in some form, by injected humour, or randomised words which don't look even slightly believable. If
                    you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing
                    hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined
                    chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of
                    over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum
                    which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected
                    humour, or non-characteristic words etc.

                    <div class="specification">
                        <h3>
                            Specification
                        </h3>
                        <div>
                            <div class="clearfix">
                                <table class="table">
                                    <tr class="head">
                                        <td colspan="2"> Display </td>
                                    </tr>
                                    <tr>
                                        <td>Screen size</td>
                                        <td>24"</td>
                                    </tr>
                                    <tr>
                                        <td>Display resolution</td>
                                        <td>2560 X 1440 pixels</td>
                                    </tr>
                                    <tr>
                                        <td>Dynamic contrast ratio</td>
                                        <td>2000000:1</td>
                                    </tr>
                                    <tr>
                                        <td>Display type</td>
                                        <td>Led</td>
                                    </tr>
                                    <tr>
                                        <td>3D compatible</td>
                                        <td> <i class="fa fa-times"></i> No</td>
                                    </tr>
                                    <tr>
                                        <td>Graphic resolution</td>
                                        <td>2560 X 1440</td>
                                    </tr>
                                    <tr>
                                        <td>Video mode</td>
                                        <td>1400p</td>
                                    </tr>
                                    <tr>
                                        <td>Mobile High-Definition Link</td>
                                        <td><i class="fa fa-check"></i> Yes</td>
                                    </tr>
                                    <tr>
                                        <td>HDCP</td>
                                        <td><i class="fa fa-check"></i> Yes</td>
                                    </tr>
                                    <tr>
                                        <td>Screen size</td>
                                        <td>24"</td>
                                    </tr>
                                    <tr>
                                        <td>Display resolution</td>
                                        <td>2560 X 1440 pixels</td>
                                    </tr>
                                    <tr>
                                        <td>Dynamic contrast ratio</td>
                                        <td>2000000:1</td>
                                    </tr>
                                    <tr>
                                        <td>Display type</td>
                                        <td>Led</td>
                                    </tr>
                                    <tr>
                                        <td>3D compatible</td>
                                        <td> <i class="fa fa-times"></i> No</td>
                                    </tr>
                                    <tr>
                                        <td>Graphic resolution</td>
                                        <td>2560 X 1440</td>
                                    </tr>
                                </table>
                                <table class="table">
                                    <tr class="head">
                                        <td colspan="2"> Power </td>
                                    </tr>
                                    <tr>
                                        <td>Input current</td>
                                        <td>1.5A</td>
                                    </tr>
                                    <tr>
                                        <td>Current rating</td>
                                        <td>1.5A</td>
                                    </tr>
                                    <tr>
                                        <td>Typical power consuption</td>
                                        <td>35W</td>
                                    </tr>
                                    <tr>
                                        <td>Standby power consuption</td>
                                        <td>0.5W</td>
                                    </tr>
                                    <tr>
                                        <td>Maximum power consuption</td>
                                        <td>87W</td>
                                    </tr>
                                    <tr>
                                        <td>Input current</td>
                                        <td>1.5A</td>
                                    </tr>
                                    <tr>
                                        <td>Current rating</td>
                                        <td>1.5A</td>
                                    </tr>
                                    <tr>
                                        <td>Typical power consuption</td>
                                        <td>35W</td>
                                    </tr>
                                    <tr>
                                        <td>Standby power consuption</td>
                                        <td>0.5W</td>
                                    </tr>
                                    <tr>
                                        <td>Maximum power consuption</td>
                                        <td>87W</td>
                                    </tr>
                                    <tr class="head">
                                        <td colspan="2"> Power </td>
                                    </tr>
                                    <tr>
                                        <td>Input current</td>
                                        <td>1.5A</td>
                                    </tr>
                                    <tr>
                                        <td>Current rating</td>
                                        <td>1.5A</td>
                                    </tr>
                                    <tr>
                                        <td>Typical power consuption</td>
                                        <td>35W</td>
                                    </tr>
                                    <tr>
                                        <td>Standby power consuption</td>
                                        <td>0.5W</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="more-specification">
                                <i class="fa fa-plus"></i> More
                            </div>
                        </div>
                    </div>
                    <div class="comment-content clearfix">
                        <h3>
                            Comments
                        </h3>
                        <div class="col-sm-10">
                            <div class="comment-content-items clearfix">
                                <div class="col-sm-3">
                                    <div class="comment-image">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/no-image-icon-md.png" alt="No image" class="img-responsive">
                                    </div>
                                    <div class="comment-date">
                                        2016
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="comment">
                                        <div class="clearfix">
                                        <span class="star-rating">
                                          <input type="radio" name="rating5" value="1" checked=""><i></i>
                                          <input type="radio" name="rating5" value="2" checked=""><i></i>
                                          <input type="radio" name="rating5" value="3" checked=""><i></i>
                                          <input type="radio" name="rating5" value="4" checked=""><i></i>
                                          <input type="radio" name="rating5" value="5" checked=""><i></i>
                                      </span>
                                        </div>
                                        <div class="headline">
                                            Headline
                                        </div>
                                        <div>
                                            Coments
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <div class="comment-content-items clearfix">
                                <div class="col-sm-3">
                                    <div class="comment-image">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/no-image-icon-md.png" alt="No image" class="img-responsive">
                                    </div>
                                    <div class="comment-date">
                                        2016
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="comment">
                                        <div class="clearfix">
                                        <span class="star-rating">
                                          <input type="radio" name="rating5" value="1" checked=""><i></i>
                                          <input type="radio" name="rating5" value="2" checked=""><i></i>
                                          <input type="radio" name="rating5" value="3" checked=""><i></i>
                                          <input type="radio" name="rating5" value="4" checked=""><i></i>
                                          <input type="radio" name="rating5" value="5" checked=""><i></i>
                                      </span>
                                        </div>
                                        <div class="headline">
                                            Headline
                                        </div>
                                        <div>
                                            Coments
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <div class="comment-content-items clearfix">
                                <div class="col-sm-3">
                                    <div class="comment-image">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/no-image-icon-md.png" alt="No image" class="img-responsive">
                                    </div>
                                    <div class="comment-date">
                                        2016
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="comment">
                                        <div class="clearfix">
                                        <span class="star-rating">
                                          <input type="radio" name="rating5" value="1" checked=""><i></i>
                                          <input type="radio" name="rating5" value="2" checked=""><i></i>
                                          <input type="radio" name="rating5" value="3" checked=""><i></i>
                                          <input type="radio" name="rating5" value="4" checked=""><i></i>
                                          <input type="radio" name="rating5" value="5" checked=""><i></i>
                                      </span>
                                        </div>
                                        <div class="headline">
                                            Headline
                                        </div>
                                        <div>
                                            Coments
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <div class="write-review">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">
                                        Your name <span>*</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="inputEmail3"  required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">
                                        Your email address <span>*</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="email" class="form-control" id="inputPassword3" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">
                                        Summary <span>*</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="Summary" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">
                                        Rating <span>*</span>
                                    </label>
                                    <div class="col-sm-3">
                                        <select class="form-control">
                                            <option>5 excellent</option>
                                            <option>4 excellent</option>
                                            <option>3 excellent</option>
                                            <option>2 excellent</option>
                                            <option>1 excellent</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">
                                        Your option
                                    </label>
                                    <div class="col-sm-6">
                                        <textarea class="form-control" rows="7"></textarea>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-info text-uppercase">submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="specs">
                    <div class="specification">
                        <h3>
                            Specification
                        </h3>
                        <div>
                            <div class="clearfix">
                                <table class="table">
                                    <tr class="head">
                                        <td colspan="2"> Display </td>
                                    </tr>
                                    <tr>
                                        <td>Screen size</td>
                                        <td>24"</td>
                                    </tr>
                                    <tr>
                                        <td>Display resolution</td>
                                        <td>2560 X 1440 pixels</td>
                                    </tr>
                                    <tr>
                                        <td>Dynamic contrast ratio</td>
                                        <td>2000000:1</td>
                                    </tr>
                                    <tr>
                                        <td>Display type</td>
                                        <td>Led</td>
                                    </tr>
                                    <tr>
                                        <td>3D compatible</td>
                                        <td> <i class="fa fa-times"></i> No</td>
                                    </tr>
                                    <tr>
                                        <td>Graphic resolution</td>
                                        <td>2560 X 1440</td>
                                    </tr>
                                    <tr>
                                        <td>Video mode</td>
                                        <td>1400p</td>
                                    </tr>
                                    <tr>
                                        <td>Mobile High-Definition Link</td>
                                        <td><i class="fa fa-check"></i> Yes</td>
                                    </tr>
                                    <tr>
                                        <td>HDCP</td>
                                        <td><i class="fa fa-check"></i> Yes</td>
                                    </tr>
                                    <tr>
                                        <td>Screen size</td>
                                        <td>24"</td>
                                    </tr>
                                    <tr>
                                        <td>Display resolution</td>
                                        <td>2560 X 1440 pixels</td>
                                    </tr>
                                    <tr>
                                        <td>Dynamic contrast ratio</td>
                                        <td>2000000:1</td>
                                    </tr>
                                    <tr>
                                        <td>Display type</td>
                                        <td>Led</td>
                                    </tr>
                                    <tr>
                                        <td>3D compatible</td>
                                        <td> <i class="fa fa-times"></i> No</td>
                                    </tr>
                                    <tr>
                                        <td>Graphic resolution</td>
                                        <td>2560 X 1440</td>
                                    </tr>
                                </table>
                                <table class="table">
                                    <tr class="head">
                                        <td colspan="2"> Power </td>
                                    </tr>
                                    <tr>
                                        <td>Input current</td>
                                        <td>1.5A</td>
                                    </tr>
                                    <tr>
                                        <td>Current rating</td>
                                        <td>1.5A</td>
                                    </tr>
                                    <tr>
                                        <td>Typical power consuption</td>
                                        <td>35W</td>
                                    </tr>
                                    <tr>
                                        <td>Standby power consuption</td>
                                        <td>0.5W</td>
                                    </tr>
                                    <tr>
                                        <td>Maximum power consuption</td>
                                        <td>87W</td>
                                    </tr>
                                    <tr>
                                        <td>Input current</td>
                                        <td>1.5A</td>
                                    </tr>
                                    <tr>
                                        <td>Current rating</td>
                                        <td>1.5A</td>
                                    </tr>
                                    <tr>
                                        <td>Typical power consuption</td>
                                        <td>35W</td>
                                    </tr>
                                    <tr>
                                        <td>Standby power consuption</td>
                                        <td>0.5W</td>
                                    </tr>
                                    <tr>
                                        <td>Maximum power consuption</td>
                                        <td>87W</td>
                                    </tr>
                                    <tr class="head">
                                        <td colspan="2"> Power </td>
                                    </tr>
                                    <tr>
                                        <td>Input current</td>
                                        <td>1.5A</td>
                                    </tr>
                                    <tr>
                                        <td>Current rating</td>
                                        <td>1.5A</td>
                                    </tr>
                                    <tr>
                                        <td>Typical power consuption</td>
                                        <td>35W</td>
                                    </tr>
                                    <tr>
                                        <td>Standby power consuption</td>
                                        <td>0.5W</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="more-specification">
                                <i class="fa fa-plus"></i> More
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="photos">
                    Photos
                </div>
                <div role="tabpanel" class="tab-pane" id="comments">
                    <div class="comment-content clearfix">
                        <h3>
                            Comments
                        </h3>
                        <div class="col-sm-10">
                            <div class="comment-content-items clearfix">
                                <div class="col-sm-3">
                                    <div class="comment-image">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/no-image-icon-md.png" alt="No image" class="img-responsive">
                                    </div>
                                    <div class="comment-date">
                                        2016
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="comment">
                                        <div class="clearfix">
                                        <span class="star-rating">
                                          <input type="radio" name="rating5" value="1" checked=""><i></i>
                                          <input type="radio" name="rating5" value="2" checked=""><i></i>
                                          <input type="radio" name="rating5" value="3" checked=""><i></i>
                                          <input type="radio" name="rating5" value="4" checked=""><i></i>
                                          <input type="radio" name="rating5" value="5" checked=""><i></i>
                                      </span>
                                        </div>
                                        <div class="headline">
                                            Headline
                                        </div>
                                        <div>
                                            Coments
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <div class="comment-content-items clearfix">
                                <div class="col-sm-3">
                                    <div class="comment-image">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/no-image-icon-md.png" alt="No image" class="img-responsive">
                                    </div>
                                    <div class="comment-date">
                                        2016
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="comment">
                                        <div class="clearfix">
                                        <span class="star-rating">
                                          <input type="radio" name="rating5" value="1" checked=""><i></i>
                                          <input type="radio" name="rating5" value="2" checked=""><i></i>
                                          <input type="radio" name="rating5" value="3" checked=""><i></i>
                                          <input type="radio" name="rating5" value="4" checked=""><i></i>
                                          <input type="radio" name="rating5" value="5" checked=""><i></i>
                                      </span>
                                        </div>
                                        <div class="headline">
                                            Headline
                                        </div>
                                        <div>
                                            Coments
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <div class="comment-content-items clearfix">
                                <div class="col-sm-3">
                                    <div class="comment-image">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/no-image-icon-md.png" alt="No image" class="img-responsive">
                                    </div>
                                    <div class="comment-date">
                                        2016
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="comment">
                                        <div class="clearfix">
                                        <span class="star-rating">
                                          <input type="radio" name="rating5" value="1" checked=""><i></i>
                                          <input type="radio" name="rating5" value="2" checked=""><i></i>
                                          <input type="radio" name="rating5" value="3" checked=""><i></i>
                                          <input type="radio" name="rating5" value="4" checked=""><i></i>
                                          <input type="radio" name="rating5" value="5" checked=""><i></i>
                                      </span>
                                        </div>
                                        <div class="headline">
                                            Headline
                                        </div>
                                        <div>
                                            Coments
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <div class="write-review">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">
                                        Your name <span>*</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="inputEmail3"  required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">
                                        Your email address <span>*</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="email" class="form-control" id="inputPassword3" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">
                                        Summary <span>*</span>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="Summary" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">
                                        Rating <span>*</span>
                                    </label>
                                    <div class="col-sm-3">
                                        <select class="form-control">
                                            <option>5 excellent</option>
                                            <option>4 excellent</option>
                                            <option>3 excellent</option>
                                            <option>2 excellent</option>
                                            <option>1 excellent</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">
                                        Your option
                                    </label>
                                    <div class="col-sm-6">
                                        <textarea class="form-control" rows="7"></textarea>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-info text-uppercase">submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <div class="col-sm-2"></div>
</div>
<script>
    $(document).ready(function() {
        $('.video').on('click', function(e){
            e.preventDefault();

        });
        $('.product-videos li').on('click', function(){
            $(this).children('a').trigger('click');
        });
        $('.popup-youtube').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,

            fixedContentPos: false
        });
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        // We only want these styles applied when javascript is enabled
        $('div.navigation').addClass('clearfix').css({'width' : '30%', 'float' : 'left'});
        $('div.content').css('display', 'block');

        // Initially set opacity on thumbs and add
        // additional styling for hover effect on thumbs
        var onMouseOutOpacity = 0.67;
        $('#thumbs ul.thumbs li').opacityrollover({
            mouseOutOpacity:   onMouseOutOpacity,
            mouseOverOpacity:  1.0,
            fadeSpeed:         'fast',
            exemptionSelector: '.selected'
        });

        // Initialize Advanced Galleriffic Gallery
        var gallery = $('#thumbs').galleriffic({
            delay:                     2500,
            numThumbs:                 6,
            preloadAhead:              10,
            enableTopPager:            false,
            enableBottomPager:         true,
            maxPagesToShow:            7,
            imageContainerSel:         '#slideshow',
            controlsContainerSel:      '#controls',
            captionContainerSel:       '#caption',
            loadingContainerSel:       '#loading',
            renderSSControls:          false,
            renderNavControls:         true,
            playLinkText:              false,
            pauseLinkText:             false,
            prevLinkText:              '<',
            nextLinkText:              '>',
            enableHistory:             false,
            autoStart:                 false,
            syncTransitions:           true,
            defaultTransitionDuration: 900,
            onSlideChange:             function(prevIndex, nextIndex) {
                // 'this' refers to the gallery, which is an extension of $('#thumbs')
                this.find('ul.thumbs').children()
                    .eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
                    .eq(nextIndex).fadeTo('fast', 1.0);
            },
            onPageTransitionOut:       function(callback) {
                this.fadeTo('fast', 0.0, callback);
            },
            onPageTransitionIn:        function() {
                this.fadeTo('fast', 1.0);
            }
        });

        /**** Functions to support integration of galleriffic with the jquery.history plugin ****/

        // PageLoad function
        // This function is called when:
        // 1. after calling $.historyInit();
        // 2. after calling $.historyLoad();
        // 3. after pushing "Go Back" button of a browser
        function pageload(hash) {
            // alert("pageload: " + hash);
            // hash doesn't contain the first # character.
            if(hash) {
                $.galleriffic.gotoImage(hash);
            } else {
                gallery.gotoIndex(0);
            }
        }

        // Initialize history plugin.
        // The callback is called at once by present location.hash.
        $.historyInit(pageload, "advanced.html");

        // set onlick event for buttons using the jQuery 1.3 live method
        $("a[rel='history']").live('click', function(e) {
            if (e.button != 0) return true;

            var hash = this.href;
            hash = hash.replace(/^.*#/, '');

            // moves to a new page.
            // pageload is called at once.
            // hash don't contain "#", "?"
            $.historyLoad(hash);

            return false;
        });

        /****************************************************************************************/
    });
</script>