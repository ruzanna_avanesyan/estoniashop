<?php
$this->pageTitle = Yii::app()->name . ' - '. $this->translation['contact_us'];
$this->breadcrumbs = array(
    $this->translation['contact_us'],
);
?>
<div class="contact-us">

    <h1 class="blue-color text-uppercase">
        <?php echo $this->translation['contact_us'];?>
    </h1>

    <?php if (Yii::app()->user->hasFlash('contact')): ?>

        <div class="flash-success">
            <?php echo Yii::app()->user->getFlash('contact'); ?>
        </div>

    <?php else: ?>
        <div class="form">
            <?php $form = $this->beginWidget('CActiveForm'); ?>

            <?php echo $form->errorSummary($model); ?>
            <div class="form-group clearfix">
                <?php echo $form->labelEx($model, 'name', array('class' => 'col-sm-2 col-md-1 control-label')); ?>
                <div class="col-sm-10 col-lg-11">
                    <?php echo $form->textField($model, 'name', array('class' => 'form-control')); ?>
                </div>
            </div>

            <div class="form-group clearfix">
                <?php echo $form->labelEx($model, 'email', array('class' => 'col-sm-2 col-md-1 control-label')); ?>
                <div class="col-sm-10 col-lg-11">
                    <?php echo $form->textField($model, 'email', array('class' => 'form-control')); ?>
                </div>
            </div>

            <div class="form-group clearfix">
                <?php echo $form->labelEx($model, 'subject', array('class' => 'col-sm-2 col-md-1 control-label')); ?>
                <div class="col-sm-10 col-lg-11">
                    <?php echo $form->textField($model, 'subject', array('size' => 60, 'maxlength' => 128, 'class' => 'form-control')); ?>
                </div>
            </div>

            <div class="form-group clearfix">
                <?php echo $form->labelEx($model, 'body', array('class' => 'col-sm-2 col-md-1 control-label')); ?>
                <div class="col-sm-10 col-lg-11">
                    <?php echo $form->textArea($model, 'body', array('rows' => 6, 'cols' => 50, 'class' => 'form-control')); ?>
                </div>
            </div>

            <div class="form-group clearfix submit">
                <?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-default pull-right blue-bg-color tra')); ?>
            </div>

            <?php $this->endWidget(); ?>

        </div><!-- form -->

    <?php endif; ?>
</div>
