<?php
    /* @var $this SiteController */
    /* @var $model Users */
    /* @var $form CActiveForm */
?>
<div class="row main category-page padding-bottom small-labels">

    <div class="col-md-6 padding-top col-md-offset-3">
        <?php if ($sent) { ?>
            <p class="text-center"><?=$this->translation['your_receive_email_for_password']?></p>
        <?php }
        else { ?>
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id'                   => 'reset-form',
                'enableAjaxValidation' => true,
            )); ?>

            <div class="row">
                <div class="col-md-12">
                    <h3><?= $this->translation['reset_password'] ?></h3>
                </div>
            </div>

            <div class="row <?php echo $model->getErrors('email') ? 'has-error' : '' ?>">
                <div class="col-md-12">
                    <?php echo $form->error($model, 'email'); ?>
                    <?php echo $form->labelEx($model, 'email'); ?>
                    <?php echo $form->textField($model, 'email', array('class' => 'form-control')); ?>
                </div>
            </div>

            <div class="row margin-top">
                <div class="col-md-12">
                    <?php
                        echo CHtml::hiddenField('login', 'yes');
                        echo CHtml::submitButton($this->translation['submit'], array('class' => 'btn btn-login pull-right')); ?>
                </div>
            </div>

            <?php $this->endWidget();
        } ?>
    </div>

</div>