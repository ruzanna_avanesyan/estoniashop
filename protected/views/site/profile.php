<?php
    /* @var $this SiteController */
    /* @var $model_edit Users */
    /* @var $model_change_pass Users */
    /* @var $form CActiveForm */
?>

<div class="row main category-page small-labels">
    <div class="col-md-4 profile-menu min-h padding-top-sm padding-bottom">
        <h3 class="title"><?= $this->translation['my_account'] ?></h3>
        <ul>
            <li class="active"><a
                    href="<?= $this->createUrl('site/profile') ?>"><?= $this->translation['personal_details'] ?></a>
            </li>
            <li><a href="<?= $this->createUrl('site/logout') ?>"><?= $this->translation['sign_out'] ?></a></li>
        </ul>
    </div>
    <div class="col-md-8 profile-content">
        <div class="row">
            <div class="col-md-12">
                <h3 class="personal-title"><?= $this->translation['personal_details'] ?></h3>
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-md-12">
                <h4 class="personal-title"><?= $this->translation['name_and_email'] ?></h4>
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id'                   => 'edit-form',
                    'enableAjaxValidation' => true,
                )); ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo $form->error($model_edit, 'username'); ?>
                        <?php echo $form->labelEx($model_edit, 'username'); ?>
                        <?php echo $form->textField($model_edit, 'username', array('class' => 'form-control', 'disabled' => "disabled")); ?>
                    </div>
                </div>

                <div class="row margin-top <?php echo $model_edit->getErrors('email') ? 'has-error' : '' ?>">
                    <div class="col-md-12">
                        <?php echo $form->error($model_edit, 'email'); ?>
                        <?php echo $form->labelEx($model_edit, 'email'); ?>
                        <?php echo $form->textField($model_edit, 'email', array('class' => 'form-control')); ?>
                    </div>
                </div>

                <div class="row margin-top <?php echo $model_edit->getErrors('current_password') ? 'has-error' : '' ?>">
                    <div class="col-md-12">
                        <?php echo $form->error($model_edit, 'current_password'); ?>
                        <?php echo $form->labelEx($model_edit, 'current_password'); ?>
                        <?php echo $form->passwordField($model_edit, 'current_password', array('class' => 'form-control')); ?>
                        <span class="personal-note"><?= $this->translation['details_change_note'] ?></span>
                    </div>
                </div>

                <div class="row margin-top">
                    <div class="col-md-12">
                        <?php
                            echo CHtml::hiddenField('edit', 'yes');
                            echo CHtml::submitButton($this->translation['change_details'], array('class' => 'btn btn-login pull-right')); ?>
                    </div>
                </div>

                <?php $this->endWidget(); ?>
            </div>
        </div>
        <hr>
        <div class="row margin-top">
            <div class="col-md-12">
                <h4 class="personal-title"><?= $this->translation['change_password'] ?></h4>
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id'                   => 'password-form',
                    'enableAjaxValidation' => true,
                )); ?>

                <div class="row margin-top <?php echo $model_change_pass->getErrors('password') ? 'has-error' : '' ?>">
                    <div class="col-md-12">
                        <?php echo $form->error($model_change_pass, 'password'); ?>
                        <?php echo $form->labelEx($model_change_pass, 'password'); ?>
                        <?php echo $form->passwordField($model_change_pass, 'password', array('class' => 'form-control')); ?>
                    </div>
                </div>

                <div class="row margin-top <?php echo $model_change_pass->getErrors('confirm_password') ? 'has-error' : '' ?>">
                    <div class="col-md-12">
                        <?php echo $form->error($model_change_pass, 'confirm_password'); ?>
                        <?php echo $form->labelEx($model_change_pass, 'confirm_password'); ?>
                        <?php echo $form->passwordField($model_change_pass, 'confirm_password', array('class' => 'form-control')); ?>
                    </div>
                </div>

                <div class="row margin-top <?php echo $model_change_pass->getErrors('current_password') ? 'has-error' : '' ?>">
                    <div class="col-md-12">
                        <?php echo $form->error($model_change_pass, 'current_password'); ?>
                        <?php echo $form->labelEx($model_change_pass, 'current_password'); ?>
                        <?php echo $form->passwordField($model_change_pass, 'current_password', array('class' => 'form-control')); ?>
                    </div>
                </div>

                <div class="row margin-top">
                    <div class="col-md-12">
                        <?php
                            echo CHtml::hiddenField('change_password', 'yes');
                            echo CHtml::submitButton($this->translation['change_my_password'], array('class' => 'btn btn-login pull-right')); ?>
                    </div>
                </div>

                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>