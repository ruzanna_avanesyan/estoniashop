<?php
$this->pageTitle = Yii::app()->name . ' - '. $model->staticPagesLabels->title;
$this->breadcrumbs = array(
    $model->staticPagesLabels->title,
);
?>

<div class="static-page">
    <h1 class="blue-color text-uppercase">
        <?php echo  $model->staticPagesLabels->title;?>
    </h1>
    <div class="description">
        <?php echo  $model->staticPagesLabels->description;?>
    </div>
</div>
