
    <div class="row main">
        <div class="col-sm-3 hidden-xs filters-content">
            <div class="filter-item">
                <p>
                    Results:
                </p>
                <a href="#" class="clear_filtres">
                    <span class="glyphicon glyphicon-triangle-right"></span>
                    Clear filtres
                </a>
            </div>
            <form>
                <div class="filter-item">
                    <figure>
                        <p class="filter-head">
                            brand
                        </p>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                samsung
                                <span class="product-count">(1)</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                lg
                                <span class="product-count">(1)</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                philips
                                <span class="product-count">(1)</span>
                            </label>
                        </div>
                    </figure>
                </div>
                <div class="filter-item" id="content-ltn">
                    <figure >
                        <p class="filter-head">screen size</p>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                19.0 '' <span class="product-count">(1)</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                19.0 '' <span class="product-count">(1)</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                19.0 '' <span class="product-count">(1)</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                19.0 '' <span class="product-count">(1)</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                19.0 '' <span class="product-count">(1)</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                19.0 '' <span class="product-count">(1)</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                19.0 '' <span class="product-count">(1)</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                19.0 '' <span class="product-count">(1)</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                19.0 '' <span class="product-count">(1)</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                19.0 '' <span class="product-count">(1)</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                19.0 '' <span class="product-count">(1)</span>
                            </label>
                        </div>

                    </figure>
                </div>
                <div class="filter-item">
                    <figure>
                        <p class="filter-head">resolution</p>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                4K ULTRA HD 3840 x 2160 <span class="product-count">(1)</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                4K ULTRA HD 3840 x 2160 <span class="product-count">(1)</span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox-styled">
                                4K ULTRA HD 3840 x 2160 <span class="product-count">(1)</span>
                            </label>
                        </div>

                    </figure>
                </div>
                <div class="filter-item">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="checkbox-styled">
                            Samsung
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="checkbox-styled">
                            Samsung
                        </label>
                    </div>

                </div>
            </form>
        </div>
        <div class="products col-md-7 col-sm-9 col-xs-12">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tv">
                    <div class="content">
                        <div class="body-head clearfix">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <p>tv</p>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="head-bar">
                                    <p class="search-result">
                                        Found : <span>1</span>
                                    </p>
                                    <form>
                                        <select>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </form>
                                    <div class="view-config hidden-xs">
                                        <a href="#" class = "grid"><i class="fa fa-th"></i></a>
                                        <a href="#" class = "list"><i class="fa fa-th-list"></i></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php for($i=0;$i < 9; $i++){?>
                            <div class="product-layout product-grid col-md-4 col-sm-6  col-xs-12">
                                <div class="product-image">
                                    <div class="pull-right product-date">2016</div>
                                    <a href ="<?php echo Yii::app()->createUrl('site/product');?>">
                                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/galaxy-s6-topic.png" alt="samsung TV" />
                                    </a>
                                </div>
                                <div class="product-name text-uppercase">
                                    <a href ="<?php echo Yii::app()->createUrl('site/product');?>">Samsung galaxy</a>
                                    <div>
                                        <span class="product-photo">
                                            <i class="fa fa-camera"></i>
                                            <span class="product-photo-count">
                                                2
                                            </span>
                                        </span>
                                        <span class="product-video">
                                            <i class="fa fa-youtube-play"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="additional-info">
                                    <a href ="<?php echo Yii::app()->createUrl('site/product');?> class="model">SAMSUNG 32 UE32J5502</a>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <ul>
                                                <li><span>Screen</span></li>
                                                <li><span>Screen</span></li>
                                                <li><span>Screen</span></li>
                                                <li><span>Screen</span></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <ul>
                                                <li><span>Screen</span></li>
                                                <li><span>Screen</span></li>
                                                <li><span>Screen</span></li>
                                                <li><span>Screen</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-footer">
                                    <a class="text-uppercase" href="#">compare</a>
                                    <p class="text-uppercase">
                                        Comments: <span>(0)</span>
                                    </p>
                                    <span class="star-rating">
                                        <input type="radio" name="rating5" value="1"><i></i>
                                        <input type="radio" name="rating5" value="2"><i></i>
                                        <input type="radio" name="rating5" value="3"><i></i>
                                        <input type="radio" name="rating5" value="4"><i></i>
                                        <input type="radio" name="rating5" value="5"><i></i>
                                    </span>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="mobile">
                    <div class="content">
                        <div class="body-head clearfix">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <p>mobiles</p>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="head-bar">
                                    <p class="search-result">
                                        Found : <span>1</span>
                                    </p>
                                    <form>
                                        <select>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </form>
                                    <div class="view-config hidden-xs">
                                        <a href="#" class = "grid"><span class="glyphicon glyphicon-th"></span></a>
                                        <a href="#" class = "list"><span class="glyphicon glyphicon-th-list"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-layout product-grid col-md-4 col-sm-6  col-xs-12">
                            <div class="product-image">
                                <a href ="#">
                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/phone.jpg" alt="samsung TV" />
                                </a>
                            </div>
                            <div class="product-name">
                                <a href ="#">Samsung</a>
                            </div>
                            <div class="additional-info">
                                <a href ="#" class="model">SAMSUNG 32 UE32J5502</a>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <ul>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="item-footer">
                                <a class="text-uppercase" href="#">compare</a>
                                <p class="text-uppercase">
                                    Comments: <span>(0)</span>
                                </p>
                            </div>
                        </div>
                        <div class="product-layout product-grid col-md-4 col-sm-6  col-xs-12">
                            <div class="product-image">
                                <a href ="#">
                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/phone.jpg" alt="samsung TV" />
                                </a>
                            </div>
                            <div class="product-name">
                                <a href ="#">Samsung</a>
                            </div>
                            <div class="additional-info">
                                <a href ="#" class="model">SAMSUNG 32 UE32J5502</a>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <ul>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="item-footer">
                                <a class="text-uppercase" href="#">compare</a>
                                <p class="text-uppercase">
                                    Comments: <span>(0)</span>
                                </p>
                            </div>
                        </div>
                        <div class="product-layout product-grid col-md-4 col-sm-6  col-xs-12">
                            <div class="product-image">
                                <a href ="#">
                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/phone.jpg" alt="samsung TV" />
                                </a>
                            </div>
                            <div class="product-name">
                                <a href ="#">Samsung</a>
                            </div>
                            <div class="additional-info">
                                <a href ="#" class="model">SAMSUNG 32 UE32J5502</a>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <ul>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="item-footer">
                                <a class="text-uppercase" href="#">compare</a>
                                <p class="text-uppercase">
                                    Comments: <span>(0)</span>
                                </p>
                            </div>
                        </div>
                        <div class="product-layout product-grid col-md-4 col-sm-6  col-xs-12">
                            <div class="product-image">
                                <a href ="#">
                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/phone.jpg" alt="samsung TV" />
                                </a>
                            </div>
                            <div class="product-name">
                                <a href ="#">Samsung</a>
                            </div>
                            <div class="additional-info">
                                <a href ="#" class="model">SAMSUNG 32 UE32J5502</a>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <ul>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="item-footer">
                                <a class="text-uppercase" href="#">compare</a>
                                <p class="text-uppercase">
                                    Comments: <span>(0)</span>
                                </p>
                            </div>
                        </div>
                        <div class="product-layout product-grid col-md-4 col-sm-6  col-xs-12">
                            <div class="product-image">
                                <a href ="#">
                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/phone.jpg" alt="samsung TV" />
                                </a>
                            </div>
                            <div class="product-name">
                                <a href ="#">Samsung</a>
                            </div>
                            <div class="additional-info">
                                <a href ="#" class="model">SAMSUNG 32 UE32J5502</a>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <ul>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="item-footer">
                                <a class="text-uppercase" href="#">compare</a>
                                <p class="text-uppercase">
                                    Comments: <span>(0)</span>
                                </p>
                            </div>
                        </div>
                        <div class="product-layout product-grid col-md-4 col-sm-6  col-xs-12">

                            <div class="product-image">
                                <a href ="#">
                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/phone.jpg" alt="samsung TV" />
                                </a>
                            </div>
                            <div class="product-name">
                                <a href ="#">Samsung</a>
                            </div>
                            <div class="additional-info">
                                <a href ="#" class="model">SAMSUNG 32 UE32J5502</a>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <ul>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="item-footer">
                                <a class="text-uppercase" href="#">compare</a>
                                <p class="text-uppercase">
                                    Comments: <span>(0)</span>
                                </p>
                            </div>
                        </div>
                        <div class="product-layout product-grid col-md-4 col-sm-6  col-xs-12">
                            <div class="product-image">
                                <a href ="#">
                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/phone.jpg" alt="samsung TV" />
                                </a>
                            </div>
                            <div class="product-name">
                                <a href ="#">Samsung</a>
                            </div>
                            <div class="additional-info">
                                <a href ="#" class="model">SAMSUNG 32 UE32J5502</a>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <ul>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="item-footer">
                                <a class="text-uppercase" href="#">compare</a>
                                <p class="text-uppercase">
                                    Comments: <span>(0)</span>
                                </p>
                            </div>
                        </div>
                        <div class="product-layout product-grid col-md-4 col-sm-6  col-xs-12">
                            <div class="product-image">
                                <a href ="#">
                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/phone.jpg" alt="samsung TV" />
                                </a>
                            </div>
                            <div class="product-name">
                                <a href ="#">Samsung</a>
                            </div>
                            <div class="additional-info">
                                <a href ="#" class="model">SAMSUNG 32 UE32J5502</a>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <ul>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="item-footer">
                                <a class="text-uppercase" href="#">compare</a>
                                <p class="text-uppercase">
                                    Comments: <span>(0)</span>
                                </p>
                            </div>
                        </div>
                        <div class="product-layout product-grid col-md-4 col-sm-6  col-xs-12">
                            <div class="product-image">
                                <a href ="#">
                                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/phone.jpg" alt="samsung TV" />
                                </a>
                            </div>
                            <div class="product-name">
                                <a href ="#">Samsung</a>
                            </div>
                            <div class="additional-info">
                                <a href ="#" class="model">SAMSUNG 32 UE32J5502</a>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <ul>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                            <li><span>Screen</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="item-footer">
                                <a class="text-uppercase" href="#">compare</a>
                                <p class="text-uppercase">
                                    Comments: <span>(0)</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-2 hidden-xs products-right-content">
            <div class="products-right">
                <div class="name">
                    <a href ="#">
                        Microsoft Lumia
                    </a>
                </div>
                <div class="product-image">
                    <a href ="#">
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/imgpsh_fullsize.png" alt="samsung TV" />
                    </a>
                </div>
            </div>
            <div class="products-right">
                <div class="name">
                    <a href ="#">
                        Huawei Ascend G535
                    </a>
                </div>
                <div class="product-image">
                    <a href ="#">
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/imgpsh_fullsize1.png" alt="samsung TV" />
                    </a>
                </div>
            </div>
            <div class="products-right">
                <div class="name">
                    <a href ="#">
                        Microsoft Lumia
                    </a>
                </div>
                <div class="product-image">
                    <a href ="#">
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/imgpsh_fullsize.png" alt="samsung TV" />
                    </a>
                </div>
            </div>
        </div>
    </div>





