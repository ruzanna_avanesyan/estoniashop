<?php
    /* @var $this SiteController */
    /* @var $model Users */
    /* @var $model_r Users */
    /* @var $form CActiveForm */
?>
<div class="row main category-page padding-bottom small-labels">

    <div class="col-md-6 padding-top">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id'                   => 'login-form',
            'enableAjaxValidation' => true,
        )); ?>

        <div class="row">
            <div class="col-md-12">
                <h3><?= $this->translation['sign_in'] ?></h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php echo $form->error($model, 'username'); ?>
                <?php echo $form->labelEx($model, 'username'); ?>
                <?php echo $form->textField($model, 'username', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="row margin-top">
            <div class="col-md-12">
                <?php echo $form->error($model, 'password'); ?>
                <?php echo $form->labelEx($model, 'password'); ?>
                <?php echo $form->passwordField($model, 'password', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="row margin-top">
            <div class="col-md-12">
                <?php
                    echo CHtml::hiddenField('login', 'yes');
                    echo CHtml::submitButton($this->translation['login'], array('class' => 'btn btn-login pull-right')); ?>
            </div>
        </div>

        <div class="row margin-top">
            <div class="col-md-12">
                <?php echo CHtml::link($this->translation['forgotten_password'], $this->createUrl('site/ForgotPassword'), array('class' => 'gray-link')) ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>

    <div class="col-md-6 padding-top">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id'                   => 'register-form',
            'enableAjaxValidation' => true,
        )); ?>

        <div class="row">
            <div class="col-md-12">
                <h3><?= $this->translation['join'] ?></h3>
            </div>
        </div>

        <?php if ($model_r->hasErrors()) { ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger alert-dismissible custom-alert" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong><?=$this->translation['sorry']?></strong> </br>
                        <?=$this->translation['register_error_message']?>
                    </div>
                </div>
            </div>
        <?php } ?>

        <div class="row <?php echo $model_r->getErrors('email') ? 'has-error' : '' ?>">
            <div class="col-md-12">
                <?php echo $form->error($model_r, 'email'); ?>
                <?php echo $form->labelEx($model_r, 'email'); ?>
                <?php echo $form->textField($model_r, 'email', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="row margin-top <?php echo $model_r->getErrors('username') ? 'has-error' : '' ?>">
            <div class="col-md-12">
                <?php echo $form->error($model_r, 'username'); ?>
                <?php echo $form->labelEx($model_r, 'username'); ?>
                <?php echo $form->textField($model_r, 'username', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="row margin-top <?php echo $model_r->getErrors('password') ? 'has-error' : '' ?>">
            <div class="col-md-12">
                <?php echo $form->error($model_r, 'password'); ?>
                <?php echo $form->labelEx($model_r, 'password'); ?>
                <?php echo $form->passwordField($model_r, 'password', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="row margin-top <?php echo $model_r->getErrors('confirm_password') ? 'has-error' : '' ?>">
            <div class="col-md-12">
                <?php echo $form->error($model_r, 'confirm_password'); ?>
                <?php echo $form->labelEx($model_r, 'confirm_password'); ?>
                <?php echo $form->passwordField($model_r, 'confirm_password', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="row margin-top">
            <div class="col-md-12">
                <?php
                    echo CHtml::hiddenField('register', 'yes');
                    echo CHtml::submitButton($this->translation['register'], array('class' => 'btn btn-login pull-right')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>

</div>