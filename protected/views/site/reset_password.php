<?php
    /* @var $this SiteController */
    /* @var $model_change_pass Users */
    /* @var $form CActiveForm */
?>
<div class="row main category-page padding-bottom small-labels">

    <div class="col-md-6 padding-top col-md-offset-3">

        <?php $form = $this->beginWidget('CActiveForm', array(
            'id'                   => 'reset-form',
            'enableAjaxValidation' => true,
        )); ?>

        <div class="row">
            <div class="col-md-12">
                <h3><?= $this->translation['reset_password'] ?></h3>
            </div>
        </div>

        <div class="row margin-top <?php echo $model_change_pass->getErrors('password') ? 'has-error' : '' ?>">
            <div class="col-md-12">
                <?php echo $form->error($model_change_pass, 'password'); ?>
                <?php echo $form->labelEx($model_change_pass, 'password'); ?>
                <?php echo $form->passwordField($model_change_pass, 'password', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="row margin-top <?php echo $model_change_pass->getErrors('confirm_password') ? 'has-error' : '' ?>">
            <div class="col-md-12">
                <?php echo $form->error($model_change_pass, 'confirm_password'); ?>
                <?php echo $form->labelEx($model_change_pass, 'confirm_password'); ?>
                <?php echo $form->passwordField($model_change_pass, 'confirm_password', array('class' => 'form-control')); ?>
            </div>
        </div>

        <div class="row margin-top">
            <div class="col-md-12">
                <?php
                    echo CHtml::hiddenField('login', 'yes');
                    echo CHtml::submitButton($this->translation['submit'], array('class' => 'btn btn-login pull-right')); ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>

</div>