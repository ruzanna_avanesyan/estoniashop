<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/magnific-popup.css">
    <link rel="stylesheet"
          href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/font-awesome-4.4.0/css/font-awesome.min.css"
          type="text/css">

    <link rel="stylesheet"
          href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/tooltip/tooltipster.css"
          type="text/css">

    <link rel="stylesheet"
          href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/circle.css"
          type="text/css">

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jquery-2.1.4.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/bootstrap.js"></script>
    <script type="text/javascript"
            src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jspatch.js"></script>

    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/galleriffic-3.css"
          type="text/css" />
    <script type="text/javascript"
            src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jquery.magnific-popup.js"></script>
    <script type="text/javascript"
            src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jquery.history.js"></script>
    <script type="text/javascript"
            src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jquery.galleriffic.js"></script>
    <script type="text/javascript"
            src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jquery.opacityrollover.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/dragtable.css" />
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jquery.dragtable.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/tooltip/jquery.tooltipster.js"></script>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/style.css">
</head>
<body>
<header>
    <div class="header-top">
        <div class="container">
            <div class="col-md-4 col-sm-6 header-menu">
                <ul class="list-inline">
                    <li>
                        <a href="<?= Yii::app()->createUrl("site/index"); ?>">
                            <?php echo $this->translation['home']; ?>
                        </a>
                    </li>
                    <?php if ($this->static_pages[1]): ?>
                        <li>
                            <a href="<?php echo Yii::app()->createUrl('site/page', array('id' => $this->static_pages[1]->id)); ?>">
                                <?php echo $this->static_pages[1]->staticPagesLabels->title; ?>
                            </a>
                        </li>
                    <?php endif; ?>
                    <li>
                        <a href="<?php echo Yii::app()->createUrl('site/contact'); ?>">
                            <?php echo $this->translation['contact_us']; ?>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-8 col-sm-6">
                <form class="form-inline search-form">
                    <div class="header-top-content">
                        <div class="dropdown">
                            <?php $t = false;
                                foreach ($this->categories as $value) : ?>
                                    <?php if (isset($_GET['path']) && $value->id == $_GET['path']) { ?>
                                        <a id="dLabel" data-id="<?php echo $value->id; ?>" href="#"
                                           data-toggle="dropdown" role="button" aria-haspopup="true"
                                           aria-expanded="false">
                                            <?php echo $value->categoryLabels->name; ?>
                                            <span class="caret"></span>
                                        </a>
                                        <?php $t = true;
                                    } ?>
                                <?php endforeach; ?>
                            <?php if (!$t) { ?>
                                <a id="dLabel" data-id="All" href="#" data-toggle="dropdown" role="button"
                                   aria-haspopup="true" aria-expanded="false">
                                    <?php echo $this->translation['all_categories']; ?>
                                    <span class="caret"></span>
                                </a>
                            <?php } ?>
                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                <?php if (isset($_GET['path']) && $_GET['path'] != 'All') : ?>
                                    <li>
                                        <a class="search_category" data-id="All"
                                           href="<?php echo Yii::app()->createUrl('product/category', array('path' => 'All')); ?>">
                                            <?php echo $this->translation['all_categories']; ?>
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <?php foreach ($this->categories as $value) : ?>

                                    <li>
                                        <a class="search_category" data-id="<?php echo $value->id; ?>"
                                           href="<?php echo Yii::app()->createUrl('product/category', array('path' => $value->id)); ?>">
                                            <?php echo $value->categoryLabels->name; ?>
                                        </a>
                                    </li>

                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="search-content">
                            <div class="form-group">
                                <input type="text" class="form-control" name="Search"
                                       value="<?php if (isset($_GET['search'])) {
                                           echo $_GET['search'];
                                       } ?>">
                            </div>
                            <button type="submit" class="btn search-button">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div id="language-selector">
            <?php
                $this->widget('application.components.widgets.LanguageSelector');
            ?>
        </div>
    </div>

    <div class="inner-header" role="tablist">
        <div class="container">
            <ul class="list-inline">
                <?php foreach ($this->categories as $value) : ?>
                    <li>
                        <a href="<?php echo Yii::app()->createUrl('product/category', array('path' => $value->id)); ?>"
                           class="header-item">
						<span>
							<?php echo $value->categoryLabels->name; ?>
						</span>
                        </a>
                    </li>
                <?php endforeach; ?>
                <li>
                    <a href="<?php echo Yii::app()->createUrl('product/wishList', array('path' => isset($_GET['path']) ? $_GET['path'] : null)); ?>"
                       class="header-item">
						<span>
							WISH LIST
						</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->createUrl('Builder/index'); ?>"
                       class="header-item">
                        <span><?php echo $this->translation['build_pc']; ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->createUrl('Builder/List'); ?>"
                       class="header-item">
                        <span><?php echo $this->translation['see_all_compiled_builds']; ?></span>
                    </a>
                </li>
                <li class="pull-right">
                    <?php if (!$this->_user) { ?>
                        <a href="<?php echo Yii::app()->createUrl('site/login'); ?>" class="header-item">
                            <span><?php echo $this->translation['sign_in']; ?></span>
                        </a>
                    <?php }
                    else { ?>
                        <a href="<?php echo Yii::app()->createUrl('site/profile'); ?>"  class="header-item">
                            <span><?php echo $this->translation['profile']; ?></span>
                        </a>
                    <?php } ?>
                </li>
            </ul>
        </div>
    </div>
</header>

<div class="page-body">
    <div class="container">
        <?php echo $content; ?>
    </div>
</div>

<footer>
    <div class="container">
        <div class="footer">
            <ul class="list-inline">
                <li>
                    <a href="<?= Yii::app()->createUrl("site/index"); ?>">
                        <?php echo $this->translation['home']; ?>
                    </a>
                </li>
                <?php if ($this->static_pages): ?>
                    <?php foreach ($this->static_pages as $value) : ?>
                        <li>
                            <a href="<?php echo Yii::app()->createUrl('site/page', array('id' => $value->id)); ?>">
                                <?php echo $value->staticPagesLabels->title; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>
                <li>
                    <a href="<?php echo Yii::app()->createUrl('site/contact'); ?>">
                        <?php echo $this->translation['contact_us']; ?>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</footer>

</body>
</html>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jquery.mCustomScrollbar.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/jqueryupload/js/jquery.fileupload.js"></script>

<script>
    <?php if(Yii::app()->controller->action->id != 'category'):?>
    $('.search_category').on('click', function (e) {
        e.preventDefault();
        $('.search_category').removeClass('active');
        $(this).addClass('active');
        var text = $(this).text();

        $(this).text($('#dLabel').text());
        $('#dLabel').text(text);
    });

    $('.search-button').on('click', function (e) {
        e.preventDefault();
        var params = '';
        var url = '';

        if ($('.search_category.active').length > 0) {
            params += '&path=' + $('.search_category.active').data('id');
        } else {
            params += '&path=' + $('#dLabel').data('id');
        }

        search = $('input[name^=\'Search\']');
        if (search.val() != '') {
            params += '&search=' + search.val();
        }

        url = '<?php echo Yii::app()->createUrl('product/category');?>' + params;

        location = url;
    });
    <?php endif;?>

    function getAjaxUrl() {
        return '<?php echo Yii::app()->createUrl('product/fillComparePanelAjax');?>';
    }
    function getDeleteAjaxUrl() {
        return '<?php echo Yii::app()->createUrl('product/deleteComparePanelAjax');?>';
    }
    function getCompareItemsFromCookieAjaxUrl() {
        return '<?php echo Yii::app()->createUrl('product/compareItemsFromCookieAjaxUrl');?>';
    }
    function bootsrapNote(type,message){
        $('#b_note_top').remove();
        var i = Math.random() * (10000 - 100) + 100;
        var push = $('<div id="b_note_top"><div class="alert alert-'+type+' alert-dismissible text-center fade in" role="alert">'
            +'<button type="button" class="close" id="alert_button_from_flash'+i+'" data-dismiss="alert">' +
            '<span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'
            +'<strong>'+message+'</strong>'
            +'</div></div>').css({display:'none'});
        $('body').prepend(push);
        push.fadeIn();
        setTimeout(function(){ $('#alert_button_from_flash'+i).trigger('click') },5000);
    }
</script>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/main.js"></script>