<?php
    /* @var $this BuilderController */
    /* @var $data Product */
    $_GET['path'] = 'All';
?>

<div class="product-layout  prdct-item  col-xs-12 transition product-list"
     data-mspid="<?php echo $data->id; ?>" data-cat="<?php echo $data->category_id; ?>">
    <div class="product-image">
        <div class="pull-right product-date">
            <?php echo $data->date ?>
        </div>
        <a href="<?php echo Yii::app()->createUrl('product/index', array('path' => $_GET['path'], 'id' => $data->id)); ?>">
            <?php foreach ($data->productImages as $val): ?>
                <?php if ($val->general == 1) : ?>
                    <img class="prdct-item__img draggable2"
                         src="<?php echo THUMB; ?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/<?php echo $val->image; ?>&w=190&h=190"
                         alt="<?php echo $data->productLabels->name; ?>" />
                <?php endif; ?>
            <?php endforeach; ?>
        </a>

        <div class="item-footer text-center link-color" style="display: block;" id="tooltip_<?= $data->id ?>">
            <?php
                $sum = 0;
                foreach ($data->productComments as $val) {
                    $sum += $val->rating;
                }
            ?>
            <?php
                $rating = 0;
                if (count($data->productComments) > 0) {
                    $rating = sprintf("%.1f", $sum / count($data->productComments));
                }; ?>
            <div class="stars">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/stars.png" alt="stars"
                     class="remove-padding">

                <div class="stars-bg" style="width: <?php if (isset($rating)) {
                    echo $rating * 20;
                } ?>%"></div>
                <div class="stars-bg2" style="width: <?php if (isset($rating)) {
                    echo 100 - $rating * 20;
                } ?>%"></div>
            </div>
            <span><?php echo count($data->productComments); ?></span>
            <?php echo $this->translation['review:'] ?>
        </div>
    </div>
    <div class="additional-info" style="display: block;">
        <div class="row">
            <a href="<?php echo Yii::app()->createUrl('product/index', array('path' => $_GET['path'], 'id' => $data->id)); ?>"
               class="model transition pull-left">
                <?php echo $data->productLabels->name; ?>
            </a>
            <button class="btn btn-success btn-sm add-to-pc pull-right" data-id="<?= $data->id ?>"><i
                    class="fa fa-plus"></i></button>
        </div>
        <div class="row pr-code">
            <div class="col-md-6">
                <?= $this->translation['product_code'] ?> : <?php echo $data->model; ?>
            </div>
            <div class="col-md-6">
                <a class="pull-right compare-button" data-cat="<?php echo $data->category_id; ?>">
                    <i class="fa fa-exchange"></i>
                    <?php echo $this->translation['compare'] ?>
                    <div class="prdct-item__cmpr">
                        <label>
                            <input type="checkbox" name="compare" class="prdct-item__cmpr-chkbx js-add-to-cmpr"
                                   data-cat="<?php echo $data->category_id; ?>" data-cookie="0">
                            <span class="prdct-item__cmpr-img"></span>
                        </label>
                    </div>
                </a>
                <a class="add-wishlist pull-right" data-id="<?= $data->id ?>">
                    <i class="fa fa-heart"></i>
                    <?php echo $this->translation['wish_list'] ?>
                </a>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="category-spec col-sm-12">
                <ul>
                    <?php foreach ($data->productHasSpecificationsLimit as $sp) { ?>
                        <li><span><?= $sp->specificationItem->specificationsItemsLabels->name ?></span></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="hidden" id="get_t_<?=$data->id?>">
    <?=$this->renderPartial('/builder/_rev',array('data'=>$data))?>
</div>
<script>
    $(document).ready(function () {
        $('#tooltip_<?=$data->id?>').tooltipster({
            interactive: true,
            content: 'Loading...',
            contentCloning: false,
            contentAsHTML: true,
            animation: 'fade',
            trigger: 'hover',
            position: 'bottom',
            functionBefore: function (origin, continueTooltip) {
                continueTooltip();
                origin.tooltipster('content', $('#get_t_<?=$data->id?>').html());
            }
        });
    });
</script>