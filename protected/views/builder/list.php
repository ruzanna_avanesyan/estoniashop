<?php
    /* @var $this BuilderController */
    /* @var $filter_model Product */
    /* @var $filters Filter[] */
    /* @var $form CActiveForm */
    /* @var $min_price PcBuildItem */
    /* @var $max_price PcBuildItem */
?>

<div class="row main category-page back35">
    <div class="col-md-3 filters-content padding-bottom remove-padding">
        <div class="row hrrow">
            <div class="col-md-12">
                <h4 class="pull-left"><?= $this->translation['results'] ?></h4>
                <a href="" class="clear-all"><i
                        class="fa fa-play"></i> <?= $this->translation['clear_all'] ?></a>
            </div>
        </div>
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id'                   => 'filter-form',
            'action'               => $this->createUrl('builder/list'),
            'method'               => 'get',
            'htmlOptions'          => array('enctype' => "multipart/form-data"),
            'enableAjaxValidation' => false,
        )); ?>
        <div class="row hide-notes">
            <?php echo $form->labelEx($filter_model, 'category_id'); ?>
            <?php echo $form->checkBoxList($filter_model, 'category_id',
                                           $category,
                                           array('template' => '<label class="control control--checkbox">{label}{input}<div class="control__indicator"></div></label>', 'container' => '', 'separator' => '')) ?>
            <hr>
            <?php foreach ($filters as $f) { ?>
                <label><?php echo $f->filterLabels->name; ?></label>
                <?php echo CHtml::checkBoxList('Product[filter_id][' . $f->id . '][]',
                                               isset($filter_model->filter_id[$f->id]) ? $filter_model->filter_id[$f->id] : array(),
                                               CHtml::listData($f->filterItems, 'id', 'filterItemLabels.name'),
                                               array('template'  => '<label class="control control--checkbox">{label}{input}<div class="control__indicator"></div></label>',
                                                     'container' => '',
                                                     'separator' => '',
                                               )
                ) ?>
                <hr>
            <?php } ?>
        </div>
        <?php echo $form->hiddenField($filter_model, 'price_from') ?>
        <?php echo $form->hiddenField($filter_model, 'price_to') ?>
        <?php $this->endWidget(); ?>
    </div>
    <div class="col-md-9 padding-bottom remove-padding">
        <div class="row hrrow">
            <div class="col-md-2">
                <h4><?= $this->translation['build_your_pc'] ?></h4>
            </div>
            <div class="col-md-1 text-right margin-top-md-text">
                <span id="from-price">€<?php echo $filter_model->price_from ?></span>
            </div>
            <div class="col-md-4 margin-top-md">
                <div id="price-range"></div>
            </div>
            <div class="col-md-1 text-left margin-top-md-text">
                <span id="to-price">€<?php echo $filter_model->price_to ?></span>
            </div>
        </div>

        <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider'  => $provider,
                'itemView'      => '_compiled_items',
                'template'      => '<div class="row"><div class="col-md-12 products">{items}</div></div>
                                    <div class="row"><nav class="pagination col-md-12">{pager}</nav></div>',
                'pagerCssClass' => '',
                'summaryText'   => 'Showing {start} to {end} of {count} entries',
                'ajaxUpdate'    => false,
                'pager'         => array(
                    'maxButtonCount'       => 5,
                    'selectedPageCssClass' => 'active',
                    'hiddenPageCssClass'   => 'disable',
                    'nextPageLabel'        => '>',         // »
                    'prevPageLabel'        => '<',         // «
                    'lastPageCssClass'     => 'hide',
                    'firstPageCssClass'    => 'hide',
                    'nextPageCssClass'     => 'pagination__next btn-squae',
                    'internalPageCssClass' => 'pagination__page btn-squae',
                    'previousPageCssClass' => 'pagination__previous btn-squae',
                    'htmlOptions'          => array('class' => 'pagination__list'),
                    'firstPageLabel'       => '',
                    'header'               => '',
                ),
            )); ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#price-range").slider({
            range: true,
            min: <?=($min_price ? $min_price->total : 0)?>,
            max: <?=($max_price ? $max_price->total : 0)?>,
            values: [$('#Product_price_from').val(), $('#Product_price_to').val()],
            slide: function (event, ui) {
                $("#from-price").html('€' + ui.values[0]);
                $("#to-price").html("€" + ui.values[1]);
            },
            change: function (event, ui) {
                $('#Product_price_from').val(ui.values[0]);
                $('#Product_price_to').val(ui.values[1]).trigger('change');
            }
        });

        var that;
        $('.add-wishlist').on('click', function () {
            that = $(this);
            $.ajax({
                url: '<?php echo Yii::app()->createUrl("product/addWishList");?>',
                type: 'POST',
                data: 'id=' + $(this).data('id'),
                success: function (data) {
                    if (data == 'ok') {
                        that.attr('disabled', 'disabled');
                    }
                }
            });
        });

        $(document).on('change', '#filter-form input', function () {
            $('#filter-form').submit();
        });


        $(document).on('click', '.add-to-pc', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            $.ajax({
                url: '<?=$this->createUrl('builder/AddToPC')?>',
                type: 'get',
                data: {id: id},
                dataType: 'json',
                success: function (data) {
                    if (data.status) {
                        $('#init-pc').html(data.html);
                    } else {
                        bootsrapNote('danger', data.error);
                    }
                }
            });
        });


        $(document).on('click', '.remove-from-pc', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            $.ajax({
                url: '<?=$this->createUrl('builder/RemoveFromPc')?>',
                type: 'get',
                data: {id: id},
                dataType: 'json',
                success: function (data) {
                    if (data.status) {
                        $('#init-pc').html(data.html);
                    } else {
                        bootsrapNote('danger', data.error);
                    }
                }
            });
        });
    });
</script>