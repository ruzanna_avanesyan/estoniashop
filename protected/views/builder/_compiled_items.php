<?php
    /* @var $this BuilderController */
    /* @var $data PcBuildItem */
?>

<div class="col-md-12 margin-top">
    <div class="thumbnail">
        <div class="row padding-bottom-sm">
            <div class="col-md-12">
                <?= $data->title ?>
                <hr>
            </div>
            <div class="col-md-3">
                <?php if ($data->pcItemImages) { ?>
                    <div class="thumbnail smp4">
                        <img
                            src="<?php echo THUMB; ?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/<?php echo $data->pcItemImages[0]->image; ?>&w=190&h=190" />
                    </div>
                <?php } ?>
            </div>
            <div class="col-md-6">
                <ul class="nostyle">
                    <?php foreach ($data->pcItemHasProducts as $pr) { ?>
                        <li><?= $pr->product->productLabels->name?></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-md-3 jumbotron text-center minh190">
                <h4><b>€<?= $data->total ?></b></h4>

                <p><?= $this->translation['comments'] ?>: <?= count($data->comments) ?></p>
                <a class="btn btn-default"
                   href="<?= $this->createUrl('builder/view', array('id' => $data->id)) ?>"><?= $this->translation['details'] ?></a>
            </div>
        </div>
    </div>
</div>