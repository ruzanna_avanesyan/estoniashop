<?php
    /* @var $this BuilderController */
    /* @var $model PcBuildItem */
    /* @var $form CActiveForm */
    $pc_build = array();
    foreach ($model->pcItemHasProducts as $pr) {
        $pc_build[$pr->product->category_id] = isset($pc_build[$pr->product->category_id]) ? $pc_build[$pr->product->category_id] :
            array('title' => $pr->product->category->categoryLabels->name, 'items' => array());
        $pc_build[$pr->product->category_id]['items'][] = $pr->product;
    }
?>
<div class="row main padding-bottom back35">
    <div class="col-md-3 filters-content padding-bottom remove-padding" id="init-pc">
        <div class="users-name">
            <?= $model->user->username ?>
        </div>
        <div class="extra-margin-top">
            <?php foreach ($pc_build as $category) { ?>
                <div class="col-md-12">
                    <h4 class="link-color"><b><?= $category['title'] ?></b></h4>
                </div>
                <?php foreach ($category['items'] as $product) { ?>
                    <div class="row hrrow margin-top pc-item-pr" data-id="<?= $product->id ?>">
                        <div class="col-md-12 price-div">
                            <p><?php echo $product->productLabels->name ?></p>
                            <span><?= $product->price_item ? $product->price_item->price : 0 ?></span>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
    <div class="col-md-9 padding-bottom remove-padding">
        <div class="row hrrow">
            <div class="col-md-12">
                <h4><?= $model->title ?></h4>
            </div>
        </div>
        <div class="row margin-top-md">
            <div class="col-md-9 hrrow">
                <p><?= $this->translation['description'] ?></p>
            </div>
            <div class="col-md-12 margin-top">
                <p><?= $model->description ?></p>
            </div>
        </div>
        <div class="row margin-top-md" id="image_container">
            <?php foreach ($model->pcItemImages as $val) { ?>
                <div class="col-md-2">
                    <div class="thumbnail smp4">
                        <img
                            src="<?php echo THUMB; ?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/<?php echo $val->image; ?>&w=190&h=190"
                            data-mfp-src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/<?php echo $val->image; ?>" />
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="row hrrow extra-margin-top">
            <div class="col-md-11">
                <h5><?= $this->translation['WRITE_A_REVIEW'] ?></h5>
            </div>
        </div>
        <div class="row margin-top-md">
            <?= CHtml::beginForm(); ?>
            <div class="col-md-11">
                <?= CHtml::textArea('comment', '', array('class' => 'form-control', 'placeholder' => $this->translation['write_a_comment'])); ?>
            </div>
            <div class="col-md-11 margin-top">
                <?= CHtml::button($this->translation['submit_review'], array('class' => 'btn btn-lg btn-red pull-right', 'id' => 'write-review')); ?>
            </div>
            <?= CHtml::endForm(); ?>
        </div>
        <div class="row">
            <hr>
            <?php foreach ($model->comments as $comment) { ?>
                <div class="col-md-12">
                    <p>
                        <b class="<?= $comment->user_id == $model->user_id ? 'is_author' : '' ?>"><?= $comment->user->username ?></b>
                        <span class="sm-date">| <?= $comment->created ?></span>
                    </p>

                    <p><?= $comment->comment ?></p>
                    <a class="answer-comment" data-id="<?= $comment->id ?>"><?= $this->translation['answer'] ?></a>
                    <hr>
                    <div class="col-md-11 col-md-offset-1 jumbotron" id="make_answer<?= $comment->id ?>"
                         style="display: none;">
                        <?= CHtml::beginForm(); ?>
                        <div class="col-md-12">
                            <?= CHtml::textArea('comment', '', array('class' => 'form-control minh', 'placeholder' => $this->translation['write_a_comment'])); ?>
                        </div>
                        <div class="col-md-12 margin-top">
                            <?= CHtml::button($this->translation['post'], array('class' => 'btn btn-sm btn-primary pull-left send-answer', 'data-id' => $comment->id)); ?>
                            <a class="cancel-answer" href=""><?= $this->translation['cancel'] ?></a>
                        </div>
                        <?= CHtml::endForm(); ?>
                    </div>
                    <?php foreach ($comment->pcCommentAnswers as $answer) { ?>

                        <div class="col-md-11 col-md-offset-1">
                            <p>
                                <b class="<?= $answer->user_id == $answer->user_id ? 'is_author' : '' ?>"><?= $answer->user->username ?></b>
                                <span class="sm-date">| <?= $answer->created ?></span>
                            </p>

                            <p><?= $answer->comment ?></p>
                            <hr>
                        </div>

                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#image_container').magnificPopup({
            delegate: 'img',
            type: 'image',
            gallery: {
                enabled: true
            }
        });

        $(document).on('click', '#write-review', function (e) {
            e.preventDefault();
            var tt = $(this).closest('form').find('textarea');
            var text = tt.val();
            if (text.length != 0) {
                $.ajax({
                    url: '<?=$this->createUrl('builder/WriteReview',array('id'=>$model->id))?>',
                    type: 'post',
                    data: {'comment': text},
                    dataType: 'json',
                    success: function (data) {
                        bootsrapNote(data.msg_type, data.msg);
                        if (data.msg_type == 'success') {
                            tt.val('');
                        }
                    }
                });
            } else {
                bootsrapNote('danger', 'PLease write a comment before submit.');
            }
        });

        $(document).on('click', '.send-answer', function (e) {
            e.preventDefault();
            var tt = $(this).closest('form').find('textarea');
            var text = tt.val();
            if (text.length != 0) {
                $.ajax({
                    url: '<?=$this->createUrl('builder/WriteReviewAnswer')?>/id/' + $(this).attr('data-id'),
                    type: 'post',
                    data: {'comment': text},
                    dataType: 'json',
                    success: function (data) {
                        bootsrapNote(data.msg_type, data.msg);
                        if (data.msg_type == 'success') {
                            tt.val('');
                            tt.closest('div.jumbotron').hide();
                        }
                    }
                });
            } else {
                bootsrapNote('danger', 'PLease write a comment before submit.');
            }
        });

        $(document).on('click', '.answer-comment', function (e) {
            e.preventDefault();
            $('#make_answer' + $(this).attr('data-id')).show();
        });

        $(document).on('click', '.cancel-answer', function (e) {
            e.preventDefault();
            $(this).closest('div.jumbotron').hide();
        });
    });
</script>