<?php
    /* @var $this BuilderController */
    /* @var $filter_model Product */
    /* @var $filters Filter[] */
    /* @var $form CActiveForm */
    /* @var $min_price SitePriceListHasProduct */
    /* @var $max_price SitePriceListHasProduct */
?>
<div class="cmpr-pnl-wrpr add-cmp-mr">
    <div class="cmpr-pnl__close">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/compare_sliderbutton.png" alt="C">
    </div>
    <div class="sidebaroverlay"></div>
    <div class="droppable cmpr-pnl sctn sctn--sdbr cmpr-pnl-list cmpr-pnl-list clearfix ui-front cmpr-pnl--bx-shdw">
        <div class="sctn__hdr clearfix">
            <div class="sctn__ttl">
                Compare Products
            </div>
            <div class="cmpr-pnl__close--alt js-cmpr-pnl-cls">
                <?php echo $this->translation['close']; ?>
            </div>
        </div>
        <div class="sctn__inr">
            <div class="cmpr-pnl-list__item cmpr0" data-comparemspid="" data-subcategory="">
                <div class="cmpr-pnl-list__img-wrpr">
                    <img class="cmpr-pnl-list__img"
                         src="<?php echo THUMB; ?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/generic_product_image.png&w=60&h=60"
                         alt="">
                </div>
                <div class="cmpr-pnl-list__item-dtls">
                    <div class="cmpr-pnl-list__item-ttl">
                        <div class="srch-wdgt srch-wdgt--s srch-wdgt--xs"
                             data-url="http://www.mysmartprice.com/compare/auto_suggest.php?subcategory=">

                        </div>
                    </div>
                </div>
            </div>
            <div class="cmpr-pnl-list__item cmpr0" data-comparemspid="" data-subcategory="">
                <div class="cmpr-pnl-list__img-wrpr">
                    <img class="cmpr-pnl-list__img"
                         src="<?php echo THUMB; ?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/generic_product_image.png&w=60&h=60"
                         alt="">
                </div>
                <div class="cmpr-pnl-list__item-dtls">
                    <div class="cmpr-pnl-list__item-ttl">
                        <div class="srch-wdgt srch-wdgt--s srch-wdgt--xs"
                             data-url="http://www.mysmartprice.com/compare/auto_suggest.php?subcategory=">

                        </div>
                    </div>
                </div>
            </div>
            <div class="cmpr-pnl-list__item cmpr0" data-comparemspid="" data-subcategory="">
                <div class="cmpr-pnl-list__img-wrpr">
                    <img class="cmpr-pnl-list__img"
                         src="<?php echo THUMB; ?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/generic_product_image.png&w=60&h=60"
                         alt="">
                </div>
                <div class="cmpr-pnl-list__item-dtls">
                    <div class="cmpr-pnl-list__item-ttl">
                        <div class="srch-wdgt srch-wdgt--s srch-wdgt--xs"
                             data-url="http://www.mysmartprice.com/compare/auto_suggest.php?subcategory=">

                        </div>
                    </div>
                </div>
            </div>
            <div class="cmpr-pnl-list__item cmpr0" data-comparemspid="" data-subcategory="">
                <div class="cmpr-pnl-list__img-wrpr">
                    <img class="cmpr-pnl-list__img"
                         src="<?php echo THUMB; ?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/generic_product_image.png&w=60&h=60"
                         alt="">
                </div>
                <div class="cmpr-pnl-list__item-dtls">
                    <div class="cmpr-pnl-list__item-ttl">
                        <div class="srch-wdgt srch-wdgt--s srch-wdgt--xs"
                             data-url="http://www.mysmartprice.com/compare/auto_suggest.php?subcategory=">

                        </div>
                    </div>
                </div>
            </div>
            <div class="cmpr-pnl-list__item cmpr0" data-comparemspid="" data-subcategory="">
                <div class="cmpr-pnl-list__img-wrpr">
                    <img class="cmpr-pnl-list__img"
                         src="<?php echo THUMB; ?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/generic_product_image.png&w=60&h=60"
                         alt="">
                </div>
                <div class="cmpr-pnl-list__item-dtls">
                    <div class="cmpr-pnl-list__item-ttl">
                        <div class="srch-wdgt srch-wdgt--s srch-wdgt--xs"
                             data-url="http://www.mysmartprice.com/compare/auto_suggest.php?subcategory=">

                        </div>
                    </div>
                </div>
            </div>
            <div class="cmpr-pnl-list__item cmpr0" data-comparemspid="" data-subcategory="">
                <div class="cmpr-pnl-list__img-wrpr">
                    <img class="cmpr-pnl-list__img"
                         src="<?php echo THUMB; ?><?php echo Yii::app()->request->baseUrl; ?>/vendor/image/generic_product_image.png&w=60&h=60"
                         alt="">
                </div>
                <div class="cmpr-pnl-list__item-dtls">
                    <div class="cmpr-pnl-list__item-ttl">
                        <div class="srch-wdgt srch-wdgt--s srch-wdgt--xs"
                             data-url="http://www.mysmartprice.com/compare/auto_suggest.php?subcategory=">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <a class="transition sctn__compare-btn"
           href="<?php echo Yii::app()->createUrl('/product/compare/', array('path' => 'All')); ?>">
            <?php echo $this->translation['compare']; ?>
        </a>
    </div>
</div>

<div class="row main category-page back35">
    <div class="col-md-3 filters-content padding-bottom remove-padding drop2" id="init-pc">
        <?php echo $this->renderPartial('/builder/_builder'); ?>
    </div>
    <div class="col-md-9 padding-bottom remove-padding">
        <div class="row hrrow">
            <div class="col-md-2">
                <h4><?= $this->translation['build_your_pc'] ?></h4>
            </div>
            <div class="col-md-1 text-right margin-top-md-text">
                <span id="from-price">€<?php echo $filter_model->price_from ?></span>
            </div>
            <div class="col-md-4 margin-top-md">
                <div id="price-range"></div>
            </div>
            <div class="col-md-1 text-left margin-top-md-text">
                <span id="to-price">€<?php echo $filter_model->price_to ?></span>
            </div>
        </div>
        <div class="row remove-all-shit">
            <div class="col-md-9 vertical-line-right">
                <div class="row">
                    <?php
                        $this->widget('zii.widgets.CListView', array(
                            'dataProvider'  => $provider,
                            'itemView'      => '_pc_items',
                            'template'      => '<div class="row"><div class="col-md-12 products">{items}</div></div>
                                    <div class="row"><nav class="pagination col-md-12">{pager}</nav></div>',
                            'pagerCssClass' => '',
                            'summaryText'   => 'Showing {start} to {end} of {count} entries',
                            'ajaxUpdate'    => false,
                            'pager'         => array(
                                'maxButtonCount'       => 5,
                                'selectedPageCssClass' => 'active',
                                'hiddenPageCssClass'   => 'disable',
                                'nextPageLabel'        => '>',         // »
                                'prevPageLabel'        => '<',         // «
                                'lastPageCssClass'     => 'hide',
                                'firstPageCssClass'    => 'hide',
                                'nextPageCssClass'     => 'pagination__next btn-squae',
                                'internalPageCssClass' => 'pagination__page btn-squae',
                                'previousPageCssClass' => 'pagination__previous btn-squae',
                                'htmlOptions'          => array('class' => 'pagination__list'),
                                'firstPageLabel'       => '',
                                'header'               => '',
                            ),
                        )); ?>
                </div>
            </div>
            <div class="col-md-3 vertical-line padding-top-sm">
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id'                   => 'filter-form',
                    'action'               => $this->createUrl('builder/index'),
                    'method'               => 'get',
                    'htmlOptions'          => array('enctype' => "multipart/form-data"),
                    'enableAjaxValidation' => false,
                )); ?>
                <div class="row hide-notes">
                    <?php echo $form->labelEx($filter_model, 'category_id'); ?>
                    <?php echo $form->checkBoxList($filter_model, 'category_id',
                                                   $category,
                                                   array('template' => '<label class="control control--checkbox">{label}{input}<div class="control__indicator"></div></label>', 'container' => '', 'separator' => '')) ?>
                    <hr>
                    <?php foreach ($filters as $f) { ?>
                        <label><?php echo $f->filterLabels->name; ?></label>
                        <?php echo CHtml::checkBoxList('Product[filter_id][' . $f->id . '][]',
                                                       isset($filter_model->filter_id[$f->id]) ? $filter_model->filter_id[$f->id] : array(),
                                                       CHtml::listData($f->filterItems, 'id', 'filterItemLabels.name'),
                                                       array('template'  => '<label class="control control--checkbox">{label}{input}<div class="control__indicator"></div></label>',
                                                             'container' => '',
                                                             'separator' => '',
                                                       )
                        ) ?>
                        <hr>
                    <?php } ?>
                </div>
                <?php echo $form->hiddenField($filter_model, 'price_from') ?>
                <?php echo $form->hiddenField($filter_model, 'price_to') ?>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#price-range").slider({
            range: true,
            min: <?=$min_price->price?>,
            max: <?=$max_price->price?>,
            values: [$('#Product_price_from').val(), $('#Product_price_to').val()],
            slide: function (event, ui) {
                $("#from-price").html('€' + ui.values[0]);
                $("#to-price").html("€" + ui.values[1]);
            },
            change: function (event, ui) {
                $('#Product_price_from').val(ui.values[0]);
                $('#Product_price_to').val(ui.values[1]).trigger('change');
            }
        });

        var that;
        $('.add-wishlist').on('click', function () {
            that = $(this);
            $.ajax({
                url: '<?php echo Yii::app()->createUrl("product/addWishList");?>',
                type: 'POST',
                data: 'id=' + $(this).data('id'),
                success: function (data) {
                    if (data == 'ok') {
                        that.attr('disabled', 'disabled');
                    }
                }
            });
        });

        $(document).on('change', '#filter-form input', function () {
            $('#filter-form').submit();
        });


        $(document).on('click', '.add-to-pc', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            $.ajax({
                url: '<?=$this->createUrl('builder/AddToPC')?>',
                type: 'get',
                data: {id: id},
                dataType: 'json',
                success: function (data) {
                    if(data.status){
                        $('#init-pc').html(data.html);
                    }else{
                        bootsrapNote('danger',data.error);
                    }
                }
            });
        });


        $(document).on('click', '.remove-from-pc', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            $.ajax({
                url: '<?=$this->createUrl('builder/RemoveFromPc')?>',
                type: 'get',
                data: {id: id},
                dataType: 'json',
                success: function (data) {
                    if(data.status){
                        $('#init-pc').html(data.html);
                    }else{
                        bootsrapNote('danger',data.error);
                    }
                }
            });
        });
    });
</script>