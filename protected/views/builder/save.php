<?php
    /* @var $this BuilderController */
    /* @var $model PcBuildItem */
    /* @var $form CActiveForm */
?>
<div class="row main category-page back35 padding-bottom">
    <div class="col-md-3 filters-content padding-bottom remove-padding drop2" id="init-pc">
        <?php echo $this->renderPartial('/builder/_builder'); ?>
    </div>
    <div class="col-md-9 padding-bottom remove-padding">
        <div class="row hrrow">
            <div class="col-md-12">
                <h4><?= $this->translation['save_pc_build'] ?></h4>
            </div>
        </div>
        <div class="row margin-top-md">
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id'                   => 'pcbuilder-form',
                'htmlOptions'          => array('enctype' => "multipart/form-data"),
                'enableAjaxValidation' => false,
            )); ?>
            <div class="col-md-11">
                <div class="row">
                    <div class="col-md-2 text-right">
                        <?php echo $form->labelEx($model, 'title'); ?>
                    </div>
                    <div class="col-md-10">
                        <?php echo $form->textField($model, 'title', array('class' => 'form-control')); ?>
                        <?php echo $form->error($model, 'title'); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-11 margin-top">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo $form->labelEx($model, 'description'); ?>
                        <?php echo $form->textArea($model, 'description', array('class' => 'form-control minh')); ?>
                        <?php echo $form->error($model, 'description'); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-11 margin-top">
                <button class="btn btn-success" id="file_upload_button"><i
                        class="fa fa-plus"></i> <?= $this->translation['add_files'] ?></button>
            </div>
            <div class="col-md-11 margin-top">
                <table class="table table-striped" id="images_table">
                    <?php if ($model->images) {
                        foreach ($model->images as $image) { ?>
                            <tr>
                                <td>
                                    <img src="<?=Yii::app()->baseUrl?>/tmp_path/<?=$image?>" class="small-image"/>
                                    <input type="hidden" value="<?=$image?>" name="PcBuildItem[images][]"/>
                                </td>
                                <td>
                                    <?php $size = round(filesize(Yii::app()->basePath . '/../tmp_path/' . $image) / 1024, 2) . 'KB'; ?>
                                    <p><?=$size?></p>
                                    <div class="progress progress-striped">
                                        <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                                    </div>
                                </td>
                            </tr>
                        <?php }
                    } ?>
                </table>
                <?php echo $form->error($model, 'images'); ?>
            </div>
            <div class="col-md-11 margin-top">
                <?=CHtml::submitButton($this->translation['add'],array('class'=>'btn btn-primary pull-right'))?>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
<input style="display: none;" type="file" name="file" id="fileupload" />
<script>
    $(document).ready(function () {

        $(document).on('click', '.remove-from-pc', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-id');
            $.ajax({
                url: '<?=$this->createUrl('builder/RemoveFromPc')?>',
                type: 'get',
                data: {id: id},
                dataType: 'json',
                success: function (data) {
                    if (data.status) {
                        $('#init-pc').html(data.html);
                    } else {
                        bootsrapNote('danger', data.error);
                    }
                }
            });
        });

        $('#file_upload_button').on('click', function (e) {
            e.preventDefault();
            $('#fileupload').trigger('click');
        });
        $('#fileupload').fileupload({
            url: '<?=$this->createUrl('builder/TmpUpload')?>',
            dataType: 'json',
            beforeSend: function () {
                $('#images_table').append('<tr>' +
                    '<td><img id="file-img" class="small-image"/>' +
                    '<input type="hidden" id="file-input" name="PcBuildItem[images][]"/>' +
                    '</td>' +
                    '<td>' +
                    '<p id="file-size"></p>' +
                    '<div class="progress progress-striped active">' +
                    '<div class="progress-bar progress-bar-success"></div>' +
                    '</div>' +
                    '</td>' +
                    '</tr>');
            },
            done: function (e, data) {
                $('#file-img').attr('src', data.result.files.full_path).removeAttr('id');
                $('#file-input').val(data.result.files.file).removeAttr('id');
                $('#file-size').html(data.result.files.size).removeAttr('id');
                $('.active.progress').removeClass('active');
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.active.progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

    });
</script>