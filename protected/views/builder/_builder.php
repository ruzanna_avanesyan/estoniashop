<?php
    /* @var $this BuilderController */
    /* @var $product Product */
?>

<div class="row hrrow">
    <div class="col-md-12">
        <h4 class="pull-left"><?= $this->translation['results'] ?></h4>
        <a href="<?= $this->createUrl('builder/ClearPcBuild') ?>" class="clear-all"><i
                class="fa fa-play"></i> <?= $this->translation['clear_all'] ?></a>
    </div>
</div>
<div class="row hrrow margin-top">
    <div class="col-md-10 col-md-offset-1">
        <div class="pull-left margin-right">
            <p class="est-title"><?= $this->translation['PARTS'] ?></p>

            <p class="link-color"><?= $this->_pc_build['count'] ?></p>
        </div>
        <div class="pull-left">
            <p class="est-title"><?= $this->translation['TOTAL'] ?></p>

            <p>€<?= $this->_pc_build['total'] ?></p>
        </div>
    </div>
    <div class="col-md-10 col-md-offset-1">
        <p class="est-title"><?= $this->translation['estimated_wattage'] ?></p>

        <p class="link-color"><?= $this->_pc_build['w'] ?>W</p>
    </div>
</div>

<?php foreach ($this->_pc_build['items'] as $category) { ?>

        <div class="col-md-12">
            <h4 class="link-color"><b><?= $category['title'] ?></b></h4>
            <div class="pr-triangle"></div>
        </div>

    <?php foreach ($category['items'] as $product) { ?>
        <div class="row hrrow margin-top pc-item-pr" data-id="<?= $product->id ?>">
            <div class="col-md-12 price-div">
                <p><?php echo $product->productLabels->name ?></p>
                <span><?= $product->price_item ? $product->price_item->price : 0 ?></span>
                <a class="remove-from-pc" data-id="<?=$product->id?>"><i class="fa fa-times-circle-o"></i></a>
            </div>
        </div>
    <?php } ?>

<?php } ?>

<div class="row hrrow extra-margin-top">
    <div class="col-md-12">
        <a class="save-this" href="<?=$this->createUrl('builder/save')?>"><i class="fa fa-save"></i> <?=$this->translation['save_this']?></a>
    </div>
</div>