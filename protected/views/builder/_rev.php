<?php
    /* @var $data Product */

?>
<?php
    $sum = 0;
    $rates = array(
        5 => 0,
        4 => 0,
        3 => 0,
        2 => 0,
        1 => 0,
        0 => 0,
    );
    foreach ($data->productComments as $val) {
        $sum += $val->rating;
        $rates[$val->rating]++;
    }
    $rating = 0;
    if (count($data->productComments) > 0) {
        $rating = sprintf("%.1f", $sum / count($data->productComments));
    };

    $str_prc = round($rating != 0 ? ($rating/5*100) : 0) ;
?>
<div class="review_strarts">
    <div class="col-sm-4">
        <div class="c100 p<?=$str_prc?> small green">
            <span><?= $rating ?><i class="fa fa-star"></i></span>

            <div class="slice">
                <div class="bar"></div>
                <div class="fill"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-8 vertical-line">
        <p>Based on <?php echo count($data->productComments); ?> reviews</p>
        <?php foreach ($rates as $k => $v) {
             $prc = round(count($data->productComments) != 0 ? ($v/count($data->productComments)*100) : 0); ?>
            <div class="row">
                <div class="start_count">
                    <?=$k?> star
                </div>
                <div class="progress">
                    <div class="progress-bar star<?=$k?>" style="width: <?=$prc?>%">
                        <span class="sr-only"></span>
                    </div>
                </div>
                <div class="countss">
                    <?=$v?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<div class="review_footer">
    <a href="<?php echo Yii::app()->createUrl('product/index', array('path' => 'All', 'id' => $data->id)); ?>">READ ALL
                                                                                                               REVIEWS
                                                                                                               ></a>
</div>