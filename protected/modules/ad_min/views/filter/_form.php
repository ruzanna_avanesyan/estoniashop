<?php
/* @var $this FilterController */
/* @var $model Filter */
/* @var $form CActiveForm */
?>

<div class="col-xs-12" style="margin-top: 50px">
	<div class="box box-info">

		<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'filter-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
		'enctype'=>'multipart/form-data'
	)
)); ?>
		<div class="box-body">
			<p class="note">Fields with <span class="required">*</span> are required.</p>

			<?php echo $form->errorSummary($model); ?>

			<div class="form-group">
				<?php echo $form->labelEx($model,'status', array('class'=>'col-sm-1 control-label')); ?>
				<div class="col-sm-5">
					<?php echo $form->dropDownList($model, 'status', Yii::app()->params['adminStatusTypes'], array('class'=>'form-control select-toggleizer'));?>
				</div>
				<?php echo $form->error($model,'status'); ?>
			</div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'sort_order', array('class'=>'col-sm-1 control-label')); ?>
				<div class="col-sm-5">
					<?php echo $form->textField($model,'sort_order', array('class'=>'form-control'));?>
				</div>
				<?php echo $form->error($model,'sort_order'); ?>
			</div>

			<div class="form-group">
				<label class="col-sm-1 control-label" for="Filter_title">
					Filter Group Name
				</label>
				<div class="col-sm-5">
					<?php foreach(Languages::model()->findAll() as $key => $value) : ?>
						<div class="input-group">
							<span class="input-group-addon">
								<?php echo $value->name?>
							</span>
							<input class="form-control" name="Filter[title][<?php echo $value->id;?>]" id="Filter_title"
								   type="text" value="<?php echo !$model->isNewRecord ? FilterLabel::model()->findByAttributes(array('filter_id' => $_GET['id'], 'language_id' => $value->id))->name : "";?>">
						</div>
					<?php endforeach;?>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-12">
					<table id="filter" class="table table-striped table-bordered table-hover">
						<thead>
						<tr>
							<td class="text-left required">Filter Name</td>
							<td class="text-right">Sort Order</td>
							<td></td>
						</tr>
						</thead>
						<tbody>
							<?php if(!$model->isNewRecord) :?>
								<?php foreach(FilterItem::model()->findAllByAttributes(array('filter_id' => $_GET['id']))  as $key => $val ):?>
									<tr>
										<td class="text-left" style="width: 70%;">
											<?php foreach(Languages::model()->findAll() as $k => $value):?>
												<div class="input-group">
													<span class="input-group-addon">
														<?php echo $value->name?>
													</span>
													<input type="hidden" name="Filter[items][<?php echo $key;?>][filter_item_id]" value="<?php echo $val->id;?>">
													<input type="text" name="Filter[items][<?php echo $key;?>][filter_description][<?php echo $value->id;?>]" value="<?php echo FilterItemLabel::model()->findByAttributes(array('filter_item_id' => $val->id, 'language_id' => $value->id))->name;?>" placeholder="Filter Name" class="form-control">
												</div>
											<?php endforeach;?>
										</td>
										<td class="text-right">
											<input type="text" name="Filter[items][<?php echo $key;?>][sort_order]" value="<?php echo $val->sort_order;?>" placeholder="Sort Order" id="input-sort-order" class="form-control">
										</td>
										<td class="text-left">
											<button type="button" data-toggle="tooltip" title="Remove" data-id="<?php echo $val->id;?>" class="btn btn-danger remove_filter_item"><i class="fa fa-minus-circle"></i></button>
										</td>
									</tr>
								<?php endforeach;?>
							<?php endif;?>
						</tbody>
						<tfoot>
						<tr>
							<td colspan="2"></td>
							<td class="text-left">
								<a href="#" id="add_filter" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add Filter"><i class="fa fa-plus-circle"></i></a>
							</td>
						</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="box-footer">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info pull-right save')); ?>
			</div>
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div><!-- form -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
	$(document).ready(function(){
		var k = 0;
		<?php if(!$model->isNewRecord):?>
			k = <?php echo $key;?>;
			k++;
		<?php endif;?>
		$('#add_filter').on('click', function(e){
			e.preventDefault();
			$('table tbody').append('<tr>' +
				'<td class="text-left" style="width: 70%;">' +
				<?php foreach(Languages::model()->findAll() as $k => $value):?>
				'<div class="input-group">'+
				'<span class="input-group-addon">'+
				'<?php echo $value->name?>'+
				'</span>'+
				'<input type="text" name="Filter[items]['+ k +'][filter_description][<?php echo $value->id;?>]" value="" placeholder="Filter Name" class="form-control">'+
				'</div>'+
				<?php endforeach;?>
				'</td>'+
				'<td class="text-right">'+
				'<input type="text" name="Filter[items][' + k + '][sort_order]" value="0" placeholder="Sort Order" id="input-sort-order" class="form-control"></td>'+
				'<td class="text-left">'+
					'<button type="button" data-toggle="tooltip" title="Remove" class="btn btn-danger remove_added_filter_item">'+
						'<i class="fa fa-minus-circle"></i>'+
					'</button>'+
				'</td>'+
				'</tr>');
			k++;
		});

		$(document).on('click', '.remove_added_filter_item', function(){
			$(this).parent().parent().hide('slow').remove();
		});

		$('.remove_filter_item').on('click', function(){
			var that = $(this);
			$.ajax({
				type: 'POST',
				url: '<?php echo Yii::app()->createUrl('ad_min/filter/deleteItem');?>',
				data: 'id='+$(this).data('id'),
				success: function(data){
					if(data) {
						that.parent().parent().hide('slow').remove();
					}else {
						alert('Please contact developers team');
					}
				}
			});
		});
	});
</script>