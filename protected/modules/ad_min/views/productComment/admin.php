<h1>Manage Product Comments</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'product-comment-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'ajaxUpdate'=>false,
	'columns'=>array(
		'id',
		array(
			'header' => 'Name',
			'name'   => 'name',
			'value'  => '$data->product ? $data->product->productLabels->name: "" '
		),
		'created_date',
		'name',
		'email',
		array(
			'class' => 'CButtonColumn',
			'header'=>"View",
			'template' => '{update}',
			'buttons'=>array (
				'update'=>array(
					'label'=>'<span><i class="fa fa-eye"></i></span>',
					'imageUrl'=>false,
					'url'=>'Yii::app()->createUrl("ad_min/productComment/view", array("id" => $data->id))',
					'options'=>array(
						'class'=>'cbutton update',
						'title' => false
					),
				),
			),
		),
		array(
			'class' => 'CButtonColumn',
			'header'=>"Delete",
			'template' => '{delete}',
			'buttons'=>array (
				'delete'=>array(
					'label'=>'<span><i class="fa fa-times"></i></span>',
					'imageUrl'=>false,
					'url'=>'Yii::app()->createUrl("ad_min/productComment/delete", array("id"=>$data->id))',
					'options'=>array(
						'class'=>'cbutton delete delete_ajax',
						'title' => false
					),
				),
			),
		),
	),
)); ?>
<script>
	function delete_confirm(){
		var r = confirm("Are you sure you want to delete this item?");
		return r;
	}
	$(document).ready(function(){

		$('.delete_ajax').on('click', function (e) {
			if(delete_confirm()) {
				$(this).parent().parent().fadeOut();
				e.preventDefault();
				$.ajax({
					type: 'POST',
					url: $(this).attr('href'),
					dataType: 'json',
					success: function (data) {
						$('.results').html(data);
					}
				});
				return false;
			}
		});
	});
</script>
