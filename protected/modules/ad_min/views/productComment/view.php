<style>
    .pluse{
        border: 1px solid #82a606;
        border-radius: 50%;
        width: 15px;
        height: 15px;
        display: inline-block;
        text-align: center;
        line-height: 15px;
        color: #ffffff;
        font-weight: bold;
        background-color: #82a606;
    }
    .minus{
        border: 1px solid red;
        border-radius: 50%;
        width: 15px;
        height: 15px;
        display: inline-block;
        text-align: center;
        line-height: 15px;
        color: #ffffff;
        font-weight: bold;
        background-color: red;
    }
</style>
<?php
/* @var $this ProductCommentController */
/* @var $model ProductComment */

$this->breadcrumbs = array(
    'Product Comments' => array('index'),
    $model->name,
);
?>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<h1>View ProductComment #<?php echo $model->id; ?></h1>
<table class="detail-view" id="yw0">
    <tbody>
    <tr class="odd">
        <th>ID</th>
        <td><?php echo $model->id;?></td>
    </tr>
    <tr class="even">
        <th>Created Date</th>
        <td><?php echo $model->created_date;?></td>
    </tr>
    <tr class="odd">
        <th>Name</th>
        <td><?php echo $model->name;?></td>
    </tr>
    <tr class="even">
        <th>Email</th>
        <td><?php echo $model->email;?></td>
    </tr>
    <tr class="odd">
        <th>Content</th>
        <td>
            <?php echo $model->content;?>
        </td>
    </tr>
    <?php foreach($model->productCommentNegPosReviews as $value) : ?>
        <tr class="even">
            <th>Plus <span class="pluse">+</span></th>
            <td>
                <?php echo $value->plus;?>
            </td>
        </tr>
        <tr class="odd">
            <th>Minus <span class="minus">-</span></th>
            <td>
                <?php echo $value->minus;?>
            </td>
        </tr>
    <?php endforeach;?>

    </tbody>
</table>
<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(

        'rating',

    ),
)); ?>
