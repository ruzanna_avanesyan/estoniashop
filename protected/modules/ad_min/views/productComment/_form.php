<?php
/* @var $this ProductCommentController */
/* @var $model ProductComment */
/* @var $form CActiveForm */
?>

<div class="col-xs-12" style="margin-top: 50px">
	<div class="box box-info">

		<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-comment-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
		'enctype'=>'multipart/form-data'
	)
)); ?>
		<div class="box-body">
			<p class="note">Fields with <span class="required">*</span> are required.</p>

			<?php echo $form->errorSummary($model); ?>

							<div class="form-group">
					<?php echo $form->labelEx($model,'product_id'); ?>
					<div class="col-sm-5">
						<?php echo $form->textField($model,'product_id',array('size'=>11,'maxlength'=>11)); ?>
					</div>
					<?php echo $form->error($model,'product_id'); ?>
				</div>

								<div class="form-group">
					<?php echo $form->labelEx($model,'status'); ?>
					<div class="col-sm-5">
						<?php echo $form->textField($model,'status'); ?>
					</div>
					<?php echo $form->error($model,'status'); ?>
				</div>

								<div class="form-group">
					<?php echo $form->labelEx($model,'created_date'); ?>
					<div class="col-sm-5">
						<?php echo $form->textField($model,'created_date'); ?>
					</div>
					<?php echo $form->error($model,'created_date'); ?>
				</div>

								<div class="form-group">
					<?php echo $form->labelEx($model,'name'); ?>
					<div class="col-sm-5">
						<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
					</div>
					<?php echo $form->error($model,'name'); ?>
				</div>

								<div class="form-group">
					<?php echo $form->labelEx($model,'email'); ?>
					<div class="col-sm-5">
						<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
					</div>
					<?php echo $form->error($model,'email'); ?>
				</div>

								<div class="form-group">
					<?php echo $form->labelEx($model,'content'); ?>
					<div class="col-sm-5">
						<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
					</div>
					<?php echo $form->error($model,'content'); ?>
				</div>

								<div class="form-group">
					<?php echo $form->labelEx($model,'plus'); ?>
					<div class="col-sm-5">
						<?php echo $form->textArea($model,'plus',array('rows'=>6, 'cols'=>50)); ?>
					</div>
					<?php echo $form->error($model,'plus'); ?>
				</div>

								<div class="form-group">
					<?php echo $form->labelEx($model,'minus'); ?>
					<div class="col-sm-5">
						<?php echo $form->textArea($model,'minus',array('rows'=>6, 'cols'=>50)); ?>
					</div>
					<?php echo $form->error($model,'minus'); ?>
				</div>

								<div class="form-group">
					<?php echo $form->labelEx($model,'rating'); ?>
					<div class="col-sm-5">
						<?php echo $form->textField($model,'rating'); ?>
					</div>
					<?php echo $form->error($model,'rating'); ?>
				</div>

							<div class="box-footer">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
			</div>
		</div>

		<?php $this->endWidget(); ?>

	</div>
</div><!-- form -->