<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form CActiveForm */
?>

<div class="col-xs-12" style="margin-top: 50px">
	<div class="box box-info">

		<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'category-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
		'enctype'=>'multipart/form-data'
	)
)); ?>
		<div class="box-body">
			<p class="note">Fields with <span class="required">*</span> are required.</p>

			<?php echo $form->errorSummary($model); ?>

			<div class="form-group">
				<?php echo $form->labelEx($model,'status', array('class'=>'col-sm-1 control-label')); ?>
				<div class="col-sm-5">
					<?php echo $form->dropDownList($model, 'status', Yii::app()->params['adminStatusTypes'], array('class'=>'form-control select-toggleizer'));?>
				</div>
				<?php echo $form->error($model,'status'); ?>
			</div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'sort_order', array('class'=>'col-sm-1 control-label')); ?>
				<div class="col-sm-5">
					<?php echo $form->textField($model,'sort_order', array('class'=>'form-control'));?>
				</div>
				<?php echo $form->error($model,'sort_order'); ?>
			</div>

			<div class="form-group">
				<label class="col-sm-1 control-label" for="Filter_title">
					Category Name
				</label>
				<div class="col-sm-5">
					<?php foreach(Languages::model()->findAll() as $key => $value) : ?>
						<div class="input-group">
							<span class="input-group-addon">
								<?php echo $value->name?>
							</span>
							<input class="form-control" name="Category[title][<?php echo $value->id;?>]" id="Category_title"
								   type="text" value="<?php echo !$model->isNewRecord ? CategoryLabel::model()->findByAttributes(array('category_id' => $_GET['id'], 'language_id' => $value->id))->name : "";?>">
						</div>
					<?php endforeach;?>
				</div>
			</div>
			<?php if($model->isNewRecord != 'Create') $active_specification = CHtml::listData(CategoryHasSpecification::model()->findAllByAttributes(array('category_id' => intval($_GET['id']))), 'id','specification_id');?>
			<div class="form-group">
				<label class="col-sm-1 control-label">
					Specifications
				</label>
				<div class="col-sm-5">
					<select class="form-control phone select-tag" name="Category[specification][]" multiple>
						<?php foreach($specification AS $key =>$value) :?>
							<option value="<?php echo $key;?>"
								<?php if($model->isNewRecord != 'Create'){
									echo (in_array($key, $active_specification)) ? "selected" : "";
								}?>>
								<?php echo $value;?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<?php if($model->isNewRecord != 'Create') $active_filter = CHtml::listData(CategoryHasFilter::model()->findAllByAttributes(array('category_id' => intval($_GET['id']))), 'id','filter_id');?>
			<div class="form-group">
				<label class="col-sm-1 control-label">
					Filters
				</label>
				<div class="col-sm-5">
					<select class="form-control phone select-tag" name="Category[filter][]" multiple>
						<?php foreach($filter AS $key =>$value) :?>
							<option value="<?=$key?>"
								<?php if($model->isNewRecord != 'Create'){
									echo (in_array($key, $active_filter)) ? "selected" : "";
								}?>>
								<?=$value?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>

			<div class="box-footer">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info pull-right save')); ?>
			</div>
		</div>

		<?php $this->endWidget(); ?>

	</div>
</div><!-- form -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/plugins/select2/dist/css/select2.min.css"/>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/plugins/select2/dist/js/select2.min.js"></script>
<script>
	$(function(){
		$('.select-tag').select2();
	});
</script>