<h1>Manage Contact Messages</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'contact-messages-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'ajaxUpdate'=>false,
	'columns'=>array(
		'id',
		array(
			'header' => 'Status',
			'name' => 	'status',
			'value' => 'Yii::app()->params["adminMessageStatus"][$data->status]',
			'cssClassExpression'=> '"id_" . $data->id ." ". ($data->status ? "read" : "unread")',
		),
		'created_date',
		'name',
		'email',
		'subject',

		array(
			'class' => 'CButtonColumn',
			'header'=>"edit",
			'template' => '{view}',
			'buttons'=>array (
				'update'=>array(
					'label'=>'<span><i class="fa fa-pencil-square-o"></i></span>',
					'imageUrl'=>false,
					'url'=>'Yii::app()->createUrl("english_ad_min/contactMessage/view", array("id"=>$data->id))',
					'options'=>array(
						'class'=>'cbutton update',
						'title' => false
					),
				),
			),
		),
		array(
			'class' => 'CButtonColumn',
			'header'=>"delete",
			'template' => '{delete}',
			'buttons'=>array (
				'delete'=>array(
					'label'=>'<span><i class="fa fa-times"></i></span>',
					'imageUrl'=>false,
					'url'=>'Yii::app()->createUrl("english_ad_min/contactMessages/delete", array("id"=>$data->id))',
					'options'=>array(
						'class'=>'cbutton delete delete_ajax',
						'title' => false
					),
				),
			),
		),
	),
)); ?>



<script>
	function delete_confirm(){
		var r = confirm("Are you sure you want to delete this item?");
		return r;
	}
	$(document).ready(function(){
		var row = $('.unread').parent();
		row.css({
			'background-color' : '#FFAEAE'
		});
		row.on('click', function(){
			var str = $(this).children('.unread').attr('class');
			var thet = $(this);
			var n = str.indexOf(" ");
			var n2 = str.indexOf("_");
			var id = str.substr(n2+1, n-n2);

			$.ajax({
				type: 'POST',
				url: '<?=Yii::app()->createUrl("ad_min/contactMessages/read")?>',
				data: 'id='+id,
				dataType: 'json',
				success: function (data) {
					if(data){
						thet.css({
							'background-color' :'#fff'
						});
						thet.children('.unread').text('Read');
					}
				}
			});
		});

		$('.delete_ajax').on('click', function (e) {
			if(delete_confirm()) {
				$(this).parent().parent().fadeOut();
				e.preventDefault();
				$.ajax({
					type: 'POST',
					url: $(this).attr('href'),
					dataType: 'json',
					success: function (data) {
						$('.results').html(data);
					}
				});
				return false;
			}
		});
	});
</script>
