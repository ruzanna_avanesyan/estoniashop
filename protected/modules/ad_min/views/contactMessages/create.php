<?php
/* @var $this ContactMessagesController */
/* @var $model ContactMessages */

$this->breadcrumbs=array(
	'Contact Messages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ContactMessages', 'url'=>array('index')),
	array('label'=>'Manage ContactMessages', 'url'=>array('admin')),
);
?>

<h1>Create ContactMessages</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>