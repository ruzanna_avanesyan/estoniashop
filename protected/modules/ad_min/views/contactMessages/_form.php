<?php
/* @var $this ContactMessagesController */
/* @var $model ContactMessages */
/* @var $form CActiveForm */
?>

<div class="col-xs-12" style="margin-top: 50px">
	<div class="box box-info">

		<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-messages-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
		'enctype'=>'multipart/form-data'
	)
)); ?>
		<div class="box-body">
			<p class="note">Fields with <span class="required">*</span> are required.</p>

			<?php echo $form->errorSummary($model); ?>

							<div class="form-group">
					<?php echo $form->labelEx($model,'status'); ?>
					<div class="col-sm-5">
						<?php echo $form->textField($model,'status'); ?>
					</div>
					<?php echo $form->error($model,'status'); ?>
				</div>

								<div class="form-group">
					<?php echo $form->labelEx($model,'created_date'); ?>
					<div class="col-sm-5">
						<?php echo $form->textField($model,'created_date'); ?>
					</div>
					<?php echo $form->error($model,'created_date'); ?>
				</div>

								<div class="form-group">
					<?php echo $form->labelEx($model,'name'); ?>
					<div class="col-sm-5">
						<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
					</div>
					<?php echo $form->error($model,'name'); ?>
				</div>

								<div class="form-group">
					<?php echo $form->labelEx($model,'email'); ?>
					<div class="col-sm-5">
						<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
					</div>
					<?php echo $form->error($model,'email'); ?>
				</div>

								<div class="form-group">
					<?php echo $form->labelEx($model,'subject'); ?>
					<div class="col-sm-5">
						<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>255)); ?>
					</div>
					<?php echo $form->error($model,'subject'); ?>
				</div>

								<div class="form-group">
					<?php echo $form->labelEx($model,'body'); ?>
					<div class="col-sm-5">
						<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>
					</div>
					<?php echo $form->error($model,'body'); ?>
				</div>

							<div class="box-footer">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
			</div>
		</div>

		<?php $this->endWidget(); ?>

	</div>
</div><!-- form -->