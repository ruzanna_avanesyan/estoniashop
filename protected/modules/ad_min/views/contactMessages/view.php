<?php
/* @var $this ContactMessagesController */
/* @var $model ContactMessages */

$this->breadcrumbs=array(
	'Contact Messages'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List ContactMessages', 'url'=>array('index')),
	array('label'=>'Create ContactMessages', 'url'=>array('create')),
	array('label'=>'Update ContactMessages', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ContactMessages', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ContactMessages', 'url'=>array('admin')),
);
?>

<h1>View ContactMessages #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',

		'created_date',
		'name',
		'email',
		'subject',
		'body',
	),
)); ?>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>