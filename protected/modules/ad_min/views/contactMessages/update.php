<?php
/* @var $this ContactMessagesController */
/* @var $model ContactMessages */

$this->breadcrumbs=array(
	'Contact Messages'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ContactMessages', 'url'=>array('index')),
	array('label'=>'Create ContactMessages', 'url'=>array('create')),
	array('label'=>'View ContactMessages', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ContactMessages', 'url'=>array('admin')),
);
?>

<h1>Update ContactMessages <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>