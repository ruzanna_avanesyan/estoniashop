<?php
/* @var $this BrandController */
/* @var $model Brand */
/* @var $form CActiveForm */
?>

<div class="col-xs-12" style="margin-top: 50px">
	<div class="box box-info">

		<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'brand-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
		'enctype'=>'multipart/form-data'
	)
)); ?>
		<div class="box-body">
			<p class="note">Fields with <span class="required">*</span> are required.</p>

			<?php echo $form->errorSummary($model); ?>

			<div class="form-group">
				<?php echo $form->labelEx($model,'status', array('class'=>'col-sm-1 control-label')); ?>
				<div class="col-sm-5">
					<?php echo $form->dropDownList($model, 'status', Yii::app()->params['adminStatusTypes'], array('class'=>'form-control select-toggleizer'));?>
				</div>
				<?php echo $form->error($model,'status'); ?>
			</div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'sort_order', array('class'=>'col-sm-1 control-label')); ?>
				<div class="col-sm-5">
					<?php echo $form->textField($model,'sort_order', array('class'=>'form-control'));?>
				</div>
				<?php echo $form->error($model,'sort_order'); ?>
			</div>

			<div class="form-group">
				<label class="col-sm-1 control-label" for="Filter_title">
					Brand Name
				</label>
				<div class="col-sm-5">
					<?php foreach(Languages::model()->findAll() as $key => $value) : ?>
						<div class="input-group">
							<span class="input-group-addon">
								<?php echo $value->name?>
							</span>
							<input class="form-control" name="Brand[name][<?php echo $value->id;?>]" id="Category_title"
								   type="text" value="<?php echo !$model->isNewRecord ? BrandLabel::model()->findByAttributes(array('brand_id' => $_GET['id'], 'language_id' => $value->id))->name : "";?>">
						</div>
					<?php endforeach;?>
				</div>
			</div>

			<div class="clearfix">
				<div class="col-sm-1"></div>
				<!-- image upload-->
				<div class="form-group">
					<div class="col-sm-5">
						<small>
							Recommended size 100x100
						</small>
						<br>
						<?php if($model->isNewRecord != 'Create' && !empty($model->image)):?>
							<img class="img-responsive previewing_" src="<?=Yii::app()->request->baseUrl; ?>/vendor/image/brands/<?php echo $model->image?>" />
						<?php endif;?>

						<?php echo $form->fileField($model, 'image',array('accept'=>"image/*",'size'=>60,'maxlength'=>255,'class'=>'form-control file-chose hidden file')); ?>

						<span class="btn btn-info btn-gradient fileinput-button margin-right-sm">
							<i class="glyphicon glyphicon-plus"></i>
							<span>
								<?php echo $model->isNewRecord ? 'Add image' : 'Change image'?>...
							</span>
						</span>
					</div>
					<?php echo $form->error($model,'image'); ?>
				</div>
			</div>

			<div class="box-footer">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info pull-right save')); ?>
			</div>
		</div>

		<?php $this->endWidget(); ?>

	</div>
</div><!-- form -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
	$(document).ready(function(){
		$('.fileinput-button').on('click', function(){
			$('.file-chose').trigger('click');
		});
	});
</script>