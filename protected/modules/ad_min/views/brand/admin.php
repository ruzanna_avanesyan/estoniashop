<style>
	#brand-grid img{
		width: 100px;
		height: auto;
	}
	#brand-grid tr td:nth-child(6){
		width: 120px;
		text-align: center;
	}
</style>
<h1>Manage Brands</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
<div class="text-right">
	<a href="<?= Yii::app()->createUrl("ad_min/brand/create");?>" class="btn btn-success">
		<span class="fa fa-plus"></span>
		Create New Brand
	</a>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'brand-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'ajaxUpdate'=>false,
	'columns'=>array(
		'id',
		array(
			'header' => 'Status',
			'name' => 	'status',
			'value' => 'Yii::app()->params["adminStatusTypes"][$data->status]',
			'filter' => CHtml::dropDownList('Admins[status]', $model->status,
				Yii::app()->params["adminStatusTypes"],
				array(
					'empty' => 'All',
					'class' => 'form-control'
				)
			),
		),
		'sort_order',
		'created_date',
		array(
			'header' => 'Brand Name',
			'name' => 	'name',
			'value' => '$data->brandLabels ? $data->brandLabels->name : ""',
		),
		array(
			'header' => 'Image',
			'type' => 'raw',
			'value' => '$data->image ? CHtml::image(Yii::app()->baseUrl."/vendor/image/brands/" . $data->image) : "" '
		),

		array(
			'class' => 'CButtonColumn',
			'header'=>"Edit",
			'template' => '{update}',
			'buttons'=>array (
				'update'=>array(
					'label'=>'<span><i class="fa fa-pencil-square-o"></i></span>',
					'imageUrl'=>false,
					'url'=>'Yii::app()->createUrl("ad_min/brand/update", array("id" => $data->id))',
					'options'=>array(
						'class'=>'cbutton update',
						'title' => false
					),
				),
			),
		),
		array(
			'class' => 'CButtonColumn',
			'header'=>"Delete",
			'template' => '{delete}',
			'buttons'=>array (
				'delete'=>array(
					'label'=>'<span><i class="fa fa-times"></i></span>',
					'imageUrl'=>false,
					'url'=>'Yii::app()->createUrl("ad_min/brand/delete", array("id"=>$data->id))',
					'options'=>array(
						'class'=>'cbutton delete delete_ajax',
						'title' => false
					),
				),
			),
		),
	),
)); ?>
<script>
	function delete_confirm(){
		var r = confirm("Are you sure you want to delete this item?");
		return r;
	}
	$(document).ready(function(){

		$('.delete_ajax').on('click', function (e) {
			if(delete_confirm()) {
				$(this).parent().parent().fadeOut();
				e.preventDefault();
				$.ajax({
					type: 'POST',
					url: $(this).attr('href'),
					dataType: 'json',
					success: function (data) {
						$('.results').html(data);
					}
				});
				return false;
			}
		});
	});
</script>
