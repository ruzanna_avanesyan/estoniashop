<?php
/* @var $this TranslationController */
/* @var $model Translation */
/* @var $form CActiveForm */
?>

<div class="col-xs-12" style="margin-top: 50px">
	<div class="box box-info">

		<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'translation-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
		'enctype'=>'multipart/form-data'
	)
)); ?>
		<div class="box-body">
			<p class="note">Fields with <span class="required">*</span> are required.</p>

			<?php echo $form->errorSummary($model); ?>

			<ul class="nav nav-tabs" role="tablist">
				<?php foreach(Languages::model()->findAll() AS $key => $language): ?>
					<li role="presentation" class="custom-language <?=($key == 0) ? "active" : ""?>">
						<a href="#lang<?=$key?>" aria-controls="home" role="tab" data-toggle="tab">
							<?=$language->name?>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
			<div class="tab-content">
				<?php foreach(Languages::model()->findAll() AS $key => $language): ?>
					<div role="tabpanel" class="tab-pane <?=($key == 0) ? "active" : ""?>" id="lang<?=$key?>">
						<div>
							<label>Title<br><?=$language->name?></label>
							<input type="text"
								   name="Translation[tab][<?=$language->id?>]"
								   placeholder="Title"
								   class="form-control"
								   size="40"
								   maxlength="255"
								   value="<?=(isset($_GET['id'], TranslationLabel::model()->findByAttributes(array('translation_id' => intval($_GET['id']), 'language_id' => $language->id))->value))
									   ? TranslationLabel::model()->findByAttributes(array('translation_id' => intval($_GET['id']), 'language_id' => $language->id))->value
									   : "" ?>">
						</div>
					</div>
				<?php endforeach; ?>
			</div>


			<div class="box-footer">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info pull-right save')); ?>
			</div>
		</div>

		<?php $this->endWidget(); ?>

	</div>
</div><!-- form -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>