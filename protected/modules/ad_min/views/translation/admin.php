

<h1>Manage Translations</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'translation-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'ajaxUpdate'=>false,
	'columns'=>array(
		'id',
		'modifie_date',
		'key',
		array(
			'header' => 'Title',
			'name' => 	'value',
			'value' => '$data->translationLabels ? $data->translationLabels->value : ""',
		),
		array(
			'class' => 'CButtonColumn',
			'header'=>"Edit",
			'template' => '{update}',
			'buttons'=>array (
				'update'=>array(
					'label'=>'<span><i class="fa fa-pencil-square-o"></i></span>',
					'imageUrl'=>false,
					'url'=>'Yii::app()->createUrl("ad_min/translation/update", array("id" => $data->id))',
					'options'=>array(
						'class'=>'cbutton update',
						'title' => false
					),
				),
			),
		),
	),
)); ?>
