<?php
/* @var $this LanguagesController */
/* @var $model Languages */
/* @var $form CActiveForm */
?>

<div class="col-xs-12" style="margin-top: 50px">
	<div class="box box-info">

		<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'languages-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
		'enctype'=>'multipart/form-data'
	)
)); ?>
		<div class="box-body">
			<p class="note">Fields with <span class="required">*</span> are required.</p>

			<?php echo $form->errorSummary($model); ?>

			<div class="form-group">
				<?php echo $form->labelEx($model,'status', array('class'=>'col-sm-1 control-label')); ?>
				<div class="col-sm-5">
					<?php echo $form->dropDownList($model, 'status', Yii::app()->params['adminStatusTypes'], array('class'=>'form-control select-toggleizer'));?>
				</div>
				<?php echo $form->error($model,'status'); ?>
			</div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'sort_order', array('class'=>'col-sm-1 control-label')); ?>
				<div class="col-sm-5">
					<?php echo $form->textField($model,'sort_order', array('class'=>'form-control'));?>
				</div>
				<?php echo $form->error($model,'sort_order'); ?>
			</div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'iso', array('class'=>'col-sm-1 control-label')); ?>
				<div class="col-sm-5">
					<?php echo $form->textField($model,'iso',array('size'=>8,'maxlength'=>8, 'class'=>'form-control'));?>
				</div>
				<?php echo $form->error($model,'iso'); ?>
			</div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'name', array('class'=>'col-sm-1 control-label')); ?>
				<div class="col-sm-5">
					<?php echo $form->textField($model,'name',array('size'=>32,'maxlength'=>32, 'class'=>'form-control'));?>
				</div>
				<?php echo $form->error($model,'name'); ?>
			</div>
			<div class="box-footer">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info pull-right save')); ?>
			</div>
		</div>

		<?php $this->endWidget(); ?>

	</div>
</div><!-- form -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>