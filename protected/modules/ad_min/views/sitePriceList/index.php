<?php
/* @var $this SitePriceListController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Site Price Lists',
);

$this->menu=array(
	array('label'=>'Create SitePriceList', 'url'=>array('create')),
	array('label'=>'Manage SitePriceList', 'url'=>array('admin')),
);
?>

<h1>Site Price Lists</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
