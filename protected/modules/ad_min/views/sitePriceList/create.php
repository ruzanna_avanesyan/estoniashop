<?php
/* @var $this SitePriceListController */
/* @var $model SitePriceList */

$this->breadcrumbs=array(
	'Site Price Lists'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SitePriceList', 'url'=>array('index')),
	array('label'=>'Manage SitePriceList', 'url'=>array('admin')),
);
?>

<h1>Create SitePriceList</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>