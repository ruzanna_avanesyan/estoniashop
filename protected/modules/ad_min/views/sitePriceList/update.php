<?php
/* @var $this SitePriceListController */
/* @var $model SitePriceList */

$this->breadcrumbs=array(
	'Site Price Lists'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SitePriceList', 'url'=>array('index')),
	array('label'=>'Create SitePriceList', 'url'=>array('create')),
	array('label'=>'View SitePriceList', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SitePriceList', 'url'=>array('admin')),
);
?>

<h1>Update SitePriceList <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>