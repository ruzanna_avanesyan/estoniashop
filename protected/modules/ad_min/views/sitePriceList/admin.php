<style>
    #site-price-list-grid img{
        width: 150px;
        height: auto;
    }
</style>
<h1>Manage Site Price Lists</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
<!---->
<!--<div class="text-right">-->
<!--    <a href="--><?//= Yii::app()->createUrl("ad_min/sitePriceList/create");?><!--" class="btn btn-success">-->
<!--        <span class="fa fa-plus"></span>-->
<!--        Create New Specification-->
<!--    </a>-->
<!--</div>-->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'site-price-list-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
        array(
            'header' => 'Status',
            'name' => 	'status',
            'value' => 'Yii::app()->params["adminStatusTypes"][$data->status]',
            'filter' => CHtml::dropDownList('Admins[status]', $model->status,
                Yii::app()->params["adminStatusTypes"],
                array(
                    'empty' => 'All',
                    'class' => 'form-control'
                )
            ),
        ),
		'sort_order',
		'name',
		'sipping_date',
        array(
            'header' => 'Image',
            'type' => 'raw',
            'value' => '$data->image ? CHtml::image(Yii::app()->baseUrl."/vendor/image/sites_prices_list/" . $data->image) : "" '
        ),
        array(
            'class' => 'CButtonColumn',
            'header'=>"Edit",
            'template' => '{update}',
            'buttons'=>array (
                'update'=>array(
                    'label'=>'<span><i class="fa fa-pencil-square-o"></i></span>',
                    'imageUrl'=>false,
                    'url'=>'Yii::app()->createUrl("ad_min/sitePriceList/update", array("id" => $data->id))',
                    'options'=>array(
                        'class'=>'cbutton update',
                        'title' => false
                    ),
                ),
            ),
        ),
    ),
)); ?>
