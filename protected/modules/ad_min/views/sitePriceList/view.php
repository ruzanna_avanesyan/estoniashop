<?php
/* @var $this SitePriceListController */
/* @var $model SitePriceList */

$this->breadcrumbs=array(
	'Site Price Lists'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List SitePriceList', 'url'=>array('index')),
	array('label'=>'Create SitePriceList', 'url'=>array('create')),
	array('label'=>'Update SitePriceList', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SitePriceList', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SitePriceList', 'url'=>array('admin')),
);
?>

<h1>View SitePriceList #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'status',
		'sort_order',
		'name',
		'image',
		'tag_name',
		'sipping_date',
	),
)); ?>
