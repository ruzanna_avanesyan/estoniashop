<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/summernote/summernote.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">


    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/select.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/admin.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/bootstrap-select.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo Yii::app()->createUrl('ad_min/admins');?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg">
                <b>Admin</b>
            </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown messages-menu">
                        <a href="<?= Yii::app()->createUrl("ad_min/contactMessages");?>">
                            <i class="fa fa-envelope-o"></i>
                            <span class="label label-success">
                                <?php echo $this->messages_count;?>
                            </span>
                        </a>
                    </li>

                    <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="">
                    <p style="color: #fff;">
                        <?php echo Yii::app()->user->admin_name;?>
                    </p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">
                    MAIN NAVIGATION
                </li>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-tags fa-fw"></i>
                        <span>Catalog</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu menu-open">
                        <li>
                            <a href="<?= Yii::app()->createUrl("ad_min/category");?>">
                                <i class="fa fa-circle-o"></i>
                                Categories
                            </a>
                        </li>
                        <li>
                            <a href="<?= Yii::app()->createUrl("ad_min/product");?>">
                                <i class="fa fa-circle-o"></i>
                                Products
                            </a>
                        </li>
                        <li>
                            <a href="<?= Yii::app()->createUrl("ad_min/specificationsGroup");?>">
                                <i class="fa fa-circle-o"></i>
                                Specifications
                            </a>
                        </li>
                        <li>
                            <a href="<?= Yii::app()->createUrl("ad_min/filter");?>">
                                <i class="fa fa-circle-o"></i>
                                Filters
                            </a>
                        </li>
                        <li>
                            <a href="<?= Yii::app()->createUrl("ad_min/brand");?>">
                                <i class="fa fa-circle-o"></i>
                                Brands
                            </a>
                        </li>
                        <li>
                            <a href="<?= Yii::app()->createUrl("ad_min/productComment");?>">
                                <i class="fa fa-circle-o"></i>
                                Comments
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="<?= Yii::app()->createUrl("ad_min/sitePriceList");?>">
                        <i class="fa fa-eur"></i>
                        <span>
                            Price sites
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?= Yii::app()->createUrl("ad_min/languages");?>">
                        <i class="fa fa-globe"></i>
                        <span>
                            Languages
                        </span>
                    </a>
                </li>

                <li>
                    <a href="<?= Yii::app()->createUrl("ad_min/admins");?>">
                        <i class="fa fa-user"></i>
                        <span>
                            Admins
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?= Yii::app()->createUrl("ad_min/staticPages");?>">
                        <i class="fa fa-file-o"></i>
                        <span>
                            Pages
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?= Yii::app()->createUrl("ad_min/translation");?>">
                        <i class="fa fa-text-width"></i>
                        <span>
                            Translations
                        </span>
                    </a>
                </li>
                <li>
                    <a href="<?= Yii::app()->createUrl("ad_min/PcBuildOptions");?>">
                        <i class="fa fa-info"></i>
                        <span>
                            Pc Bulid Items Options
                        </span>
                    </a>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div id="content" style="padding: 0 25px">
            <?php echo $content; ?>
        </div><!-- content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <?=date('Y')?> All rights
        reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li>
                <a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
                <!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">Tasks Progress</h3>


            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">General Settings</h3>

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Report panel usage
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Some information about this general settings option
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Allow mail redirect
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Other sets of options are available
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Expose author name in posts
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Allow the user to show his name in blog posts
                        </p>
                    </div>
                    <!-- /.form-group -->

                    <h3 class="control-sidebar-heading">Chat Settings</h3>

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Show me as online
                            <input type="checkbox" class="pull-right" checked>
                        </label>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Turn off notifications
                            <input type="checkbox" class="pull-right">
                        </label>
                    </div>
                    <!-- /.form-group -->

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Delete chat history
                            <a href="javascript::;" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                        </label>
                    </div>
                    <!-- /.form-group -->
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


<!-- jQuery 2.1.4 -->
<!--<script src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/vendor_admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>-->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/jQuery/jquery.ba-bbq.js"></script>
<!-- jQuery UI 1.11.4 -->

<!-- Bootstrap 3.3.5 -->
<!--<script src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/vendor_admin/plugins/bootstrap/js/bootstrap.min.js"></script>-->

<script src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.js"></script>

<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/morris/morris.min.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/summernote/summernote.js"></script>
<!-- Sparkline -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<!--<script src="--><?php //echo Yii::app()->request->baseUrl; ?><!--/vendor_admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>-->
<!-- Slimscroll -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<!-- AdminLTE for demo purposes -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/dist/js/demo.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/dist/js/bootstrap-select.js"></script>


<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/select-toggleizer.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select-toggleizer').toggleize();
        $('select.selectpicker').selectpicker({
            width: '270px',
            style: 'btn btn-xs btn-default'
        });
    })
</script>

</body>
</html>
