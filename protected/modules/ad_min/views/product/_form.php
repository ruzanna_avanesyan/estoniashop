<?php
    /* @var $form CActiveForm */
    /* @var $model_pc ProductPcBuild */
    /* @var $this ProductController */


?>
<style>
    .content-wrapper {
        min-height : 100% !important;
    }

    .img-thumbnail {
        width      : 150px;
        text-align : center;
    }

    .img-thumbnail img {
        width  : 100%;
        height : auto;
    }
</style>

<div class="col-xs-12" style="margin-top: 50px">
    <div class="box box-info">

        <?php $form = $this->beginWidget('CActiveForm', array(
            'id'                   => 'product-form',
            'enableAjaxValidation' => false,
            'htmlOptions'          => array(
                'class'   => 'form-horizontal',
                'enctype' => 'multipart/form-data'
            )
        )); ?>
        <div class="box-body">
            <p class="note">Fields with <span class="required">*</span> are required.</p>
            <?php echo $form->errorSummary($model); ?>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab"
                                                              data-toggle="tab">General</a></li>
                    <li role="presentation"><a href="#images" aria-controls="images" role="tab" data-toggle="tab">Images
                                                                                                                  and
                                                                                                                  Videos</a>
                    </li>
                    <li role="presentation"><a href="#url" aria-controls="images" role="tab" data-toggle="tab">URL</a>
                    </li>
                    <li role="presentation"><a href="#pcbuild" aria-controls="images" role="tab" data-toggle="tab">PC
                                                                                                                   Build</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="general">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-5">
                                <?php echo $form->dropDownList($model, 'status', Yii::app()->params['adminStatusTypes'], array('class' => 'form-control select-toggleizer')); ?>
                            </div>
                            <?php echo $form->error($model, 'status'); ?>
                        </div>

                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'sort_order', array('class' => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-5">
                                <?php echo $form->textField($model, 'sort_order', array('class' => 'form-control')); ?>
                            </div>
                            <?php echo $form->error($model, 'sort_order'); ?>
                        </div>

                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'category_id', array('class' => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-5">
                                <?php echo $form->dropDownList($model, 'category_id', $categories, array('class' => 'form-control change-category')); ?>
                            </div>
                            <?php echo $form->error($model, 'category_id'); ?>
                        </div>

                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'brand_id', array('class' => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-5">
                                <?php echo $form->dropDownList($model, 'brand_id', $brands, array('class' => 'form-control')); ?>
                            </div>
                            <?php echo $form->error($model, 'brand_id'); ?>
                        </div>

                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'model', array('class' => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-5">
                                <?php echo $form->textField($model, 'model', array('class' => 'form-control')); ?>
                            </div>
                            <?php echo $form->error($model, 'model'); ?>
                        </div>

                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'price_url', array('class' => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-5">
                                <?php echo $form->textField($model, 'price_url', array('class' => 'form-control')); ?>
                            </div>

                            <div class="col-sm-5">
                                <select class="selectpicker" name="Product[site_price_list_id]" id="select_site">
                                    <option value="0">Select site</option>
                                    <?php
                                        if ($site_price_list):
                                            foreach ($site_price_list as $key => $value):
                                                $s = "";
                                                if (!$model->isNewRecord && $model->site_price_list_id == $value->id) {
                                                    $s = "selected";
                                                }
                                                ?>
                                                <option <?= $s ?> value="<?= $value->id ?>"
                                                                  data-thumbnail="<?= Yii::app()->request->BaseUrl ?>/vendor/image/sites_prices_list/<?= $value->image ?>">
                                                    <?= $value->name ?>
                                                </option>
                                                <?php
                                            endforeach;
                                        endif;
                                    ?>
                                </select>
                            </div>
                            <?php echo $form->error($model, 'price_url'); ?>
                        </div>

                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'date', array('class' => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-5">
                                <?php echo $form->textField($model, 'date', array('class' => 'form-control')); ?>
                            </div>
                            <?php echo $form->error($model, 'date'); ?>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-1 control-label" for="Product_specification_id">
                                Filters
                            </label>

                            <div class="col-sm-5">
                                <select class="form-control phone select-tag" name="Product[filters][]" multiple>
                                    <?php foreach ($model_filters AS $value) : ?>
                                        <optgroup label="<?php echo $value->filterLabels->name; ?>">
                                            <?php foreach ($value->filterItems AS $key => $val) : ?>
                                                <option value="<?php echo $val->id; ?>"
                                                    <?php if ($model->isNewRecord != 'Create') {
                                                        echo (in_array($val->id, $chosen_filters)) ? "selected" : "";
                                                    } ?>>
                                                    <?php echo $val->filterItemLabels->name; ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </optgroup>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-1 control-label" for="Product_specification_id">
                                Specifications
                            </label>

                            <div class="col-sm-5 specifications">
                                <?php foreach ($specifications as $key => $value) : ?>
                                    <table class="table" style="border: 1px solid #d1d1d1">
                                        <tbody>
                                        <tr class="head">
                                            <td colspan="2">
                                                <b>
                                                    <?php echo $value->specification->specificationsGroupLabels->name; ?>
                                                </b>
                                            </td>
                                        </tr>
                                        <?php foreach ($value->specification->specificationsItems as $k => $val): ?>
                                            <tr>
                                                <td>
                                                    <?php echo $val->specificationsItemsLabels->name; ?>
                                                </td>
                                                <td class="text-right">
                                                    <select
                                                        name="Product[specifications][<?php echo $value->specification->id; ?>][<?php echo $val->id; ?>]">

                                                        <?php foreach ($val->specificationsItemsValues as $index => $v)
                                                        { ?>
                                                            <?php $t = false;
                                                            if (!$model->isNewRecord && isset($chosen_specifications[$val->id]) && ($chosen_specifications[$val->id] == $v->id)) {
                                                                $t = true;
                                                            } ?>>
                                                        <?php } ?>
                                                        <?php if (!$t) { ?>
                                                            <option></option>
                                                        <?php } ?>
                                                        <?php foreach ($val->specificationsItemsValues as $index => $v)
                                                        { ?>
                                                            <option value="<?php echo $v->id ?>" <?php $t = false;
                                                                if (!$model->isNewRecord && isset($chosen_specifications[$val->id]) && ($chosen_specifications[$val->id] == $v->id)) {
                                                                    $t = true;
                                                                    echo 'selected';
                                                                } ?>>
                                                                <?php echo $v->value; ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                <?php endforeach; ?>
                            </div>
                        </div>

                        <ul class="nav nav-tabs" role="tablist">
                            <?php foreach (Languages::model()->findAll() AS $key => $language): ?>
                                <li role="presentation"
                                    class="custom-language <?php echo ($key == 0) ? "active" : "" ?>">
                                    <a href="#lang<?php echo $key ?>" aria-controls="home" role="tab" data-toggle="tab">
                                        <?php echo $language->name ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach (Languages::model()->findAll() AS $key => $language): ?>
                                <div role="tabpanel" class="tab-pane <?php echo ($key == 0) ? "active" : "" ?>"
                                     id="lang<?php echo $key ?>">
                                    <div>
                                        <label>Title<br><?php echo $language->name ?></label>
                                        <input type="text"
                                               name="Product[tab][<?php echo $language->id ?>][name]"
                                               placeholder="Title"
                                               class="form-control"
                                               size="40"
                                               maxlength="255"
                                               value="<?php echo (isset($_GET['id'], ProductLabel::model()->findByAttributes(array('product_id' => intval($_GET['id']), 'language_id' => $language->id))->name))
                                                   ? ProductLabel::model()->findByAttributes(array('product_id' => intval($_GET['id']), 'language_id' => $language->id))->name
                                                   : "" ?>">
                                    </div>
                                    <div>
                                        <label>Description<br>
                                            <?php echo $language->name ?>
                                        </label>

                                        <div>
										<textarea id="editor_<?php echo $key ?>"
                                                  class="description_<?php echo $key ?> hidden" type="text"
                                                  name="Product[tab][<?php echo $language->id ?>][description]"
                                                  placeholder="Answer"
                                                  class="form-control"
                                                  rows="10"
                                                  cols="80"
                                            ><?php echo (isset($_GET['id'], ProductLabel::model()->findByAttributes(array('product_id' => intval($_GET['id']), 'language_id' => $language->id))->description))
                                                ? ProductLabel::model()->findByAttributes(array('product_id' => intval($_GET['id']), 'language_id' => $language->id))->description
                                                : "" ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="images">
                        <div class="clearfix">
                            <div class="table-responsive" style="margin-top: 15px">
                                <table class="table all-images table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <td class="text-left">
                                            <b>
                                                Images
                                            </b>
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            <small>
                                                Recommended size 570x570
                                            </small>
                                        </td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody class="images">
                                    <?php if (!$model->isNewRecord) : ?>
                                        <?php foreach ($model->productImages as $value) : ?>
                                            <tr class="image-block">
                                                <td class="text-left">
                                                    <a href="#" data-toggle="image"
                                                       class="img-thumbnail" <?php echo $value->general ? 'style="border: 2px solid #d43f3a"' : '' ?> >
                                                        <img
                                                            src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/products/<?php echo $value->image; ?>"
                                                            alt="" title="">
                                                    </a>
                                                    <button type="button" class="btn btn-success ajax-general-image"
                                                            data-imageId="<?php echo $value->id ?>"
                                                            data-productId="<?php echo $value->id ?>"
                                                            data-url="<?php echo $this->createUrl('/ad_min/product/makeGeneralImage', array('id' => $value->id, 'pid' => $model->id)) ?>">
                                                        Make General
                                                    </button>
                                                </td>
                                                <td class="text-left">
                                                    <button type="button" data-toggle="tooltip"
                                                            class="btn btn-danger ajax-delete-image"
                                                            data-original-title="Remove"
                                                            data-imageId="<?php echo $value->id ?>"
                                                            data-productId="<?php echo $model->id ?>"
                                                            data-url="<?php echo $this->createUrl('/ad_min/product/deleteImage', array('id' => $value->id, 'pid' => $model->id)) ?>">
                                                        <i class="fa fa-minus-circle"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td></td>
                                        <td class="text-left">
                                            <button type="button" data-toggle="tooltip" title=""
                                                    class="btn btn-primary add-new-row" data-original-title="Add Image">
                                                <i class="fa fa-plus-circle"></i></button>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <td colspan="2">
                                            <b>
                                                Videos
                                            </b>
                                            <br>
                                            <small>this must be the video youtube link</small>
                                        </td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody class="videos">
                                    <?php if ($model->productVideos && !$model->isNewRecord): ?>
                                        <?php foreach ($model->productVideos as $key => $value): ?>
                                            <tr class="video-block">
                                                <td class="text-right" colspan="2">
                                                    <input type="text" name="Product[videos][<?php echo $key; ?>]"
                                                           value="<?php echo $value->video; ?>" placeholder="Video"
                                                           class="form-control">
                                                </td>
                                                <td class="text-left">
                                                    <button type="button" data-toggle="tooltip" title="Remove"
                                                            class="btn btn-danger delete-video"
                                                            data-imageId="<?php echo $value->id ?>"
                                                            data-productId="<?php echo $model->id ?>"
                                                            data-url="<?php echo $this->createUrl('/ad_min/product/deleteVideo', array('id' => $value->id, 'pid' => $model->id)) ?>">
                                                        <i class="fa fa-minus-circle"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td class="text-left">
                                            <button type="button" data-toggle="tooltip" title=""
                                                    class="btn btn-primary add-video" data-original-title="Add Video"><i
                                                    class="fa fa-plus-circle"></i></button>
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="url">
                        <div class="clearfix">
                            <?php
                                if ($site_price_list):
                                    foreach ($site_price_list as $key => $value):
                                        $url = "";
                                        if (isset($value->sitePriceListHasProduct)) {
                                            $url = $value->sitePriceListHasProduct->url;
                                        }
                                        ?>
                                        <div class="form-group">
                                            <label class="col-sm-1 control-label">
                                                <img
                                                    src="<?= Yii::app()->request->BaseUrl ?>/vendor/image/sites_prices_list/<?= $value->image ?>"
                                                    class="img-responsive" />
                                            </label>

                                            <div class="col-sm-11">
                                                <input type="text" value="<?= $url ?>"
                                                       name="ProductsUrls[<?= $value->id ?>]" class="form-control" />
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                            ?>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="pcbuild">
                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'is_pc_build', array('class' => 'col-sm-4 control-label')); ?>
                            <div class="col-sm-5">
                                <?php echo $form->dropDownList($model, 'is_pc_build', array('1' => 'Yes', '0' => 'No'), array('class' => 'form-control select-toggleizer')); ?>
                            </div>
                            <?php echo $form->error($model, 'is_pc_build'); ?>
                        </div>
                        <div class="form-group hidden" id="pc_build_item_type">
                            <?php echo $form->labelEx($model_pc, 'pc_type', array('class' => 'col-sm-4 control-label')); ?>
                            <div class="col-sm-5">
                                <?php echo $form->dropDownList($model_pc, 'pc_type', PCBuilder::$types, array('class' => 'form-control', 'empty' => 'Choose Type')); ?>
                            </div>
                            <?php echo $form->error($model_pc, 'pc_type'); ?>
                        </div>
                        <div id="pc_build_options" class="hidden">
                            <?php if (!$model_pc->isNewRecord && $model_pc->pc_type != 'other') {
                                echo $this->renderPartial('pc_build/' . $model_pc->pc_type, array(
                                    'product' => $model,
                                    'type'    => $model_pc->pc_type
                                ));
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info pull-right save')); ?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- form -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>

<script>
    $(document).ready(function () {

        $(document).on('change', '#Product_is_pc_build', function () {
            if ($(this).val() == 1) {
                $('#pc_build_item_type').removeClass('hidden');
                $('#pc_build_options').removeClass('hidden');
            } else {
                $('#pc_build_item_type').addClass('hidden');
                $('#pc_build_options').addClass('hidden');
            }
        });
        $('#Product_is_pc_build').trigger('change');

        $(document).on('change', '#ProductPcBuild_pc_type', function () {
            if ($(this).val().length != 0 && $(this).val() != 'other') {
                $.ajax({
                    url: '<?=$this->createUrl('/ad_min/product/PcItem')?>',
                    type: 'get',
                    data: {'type': $(this).val()},
                    dataType: 'html',
                    success: function (html) {
                        $('#pc_build_options').html(html);
                    }
                });
            } else {
                $('#pc_build_options').html('');
            }
        });

        $(document).on('click', '.save', function (e) {

            if ($('#select_site').val() == 0) {
                alert('select site');
                e.preventDefault();
            } else {
                return true;
            }

        });
        $(document).on('click', '.img-thumbnail', function (e) {
            e.preventDefault();
        });
        $(document).on('click', '.img-thumb', function (e) {
            e.preventDefault();
            $(this).parent().children('input').trigger('click');
        });
        var k = 0;
        $('.add-new-row').on('click', function () {
            $('.images').append('<tr id="image-row0">' +
                '<td class="text-left">' +
                '<a href="#" data-toggle="image" class="img-thumbnail img-thumb">' +
                '<img src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/image/no_image-100x100.png" alt="" title="">' +
                '</a>' +
                '<input type="file" name="Product[product_image][' + k + ']" value="" class="input-image hide">' +
                '</td>' +
                '<td class="text-left">' +
                '<button type="button" data-toggle="tooltip" title="" class="btn btn-danger delete-new-image" data-original-title="Remove"><i class="fa fa-minus-circle"></i></button>' +
                '</td>' +
                '</tr>');
            k++;
        });

        $(document).on('click', '.delete-new-image, .delete-new-videos', function () {
            $(this).parent().parent().remove();
        });

        $('.ajax-delete-image').on('click', function (e) {
            var imageBlock = $(this).closest('.image-block');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                success: function (data) {
                    if (data == 'ok') {
                        imageBlock.hide('slow');
                    } else {
                        var str = '<div class="alert alert-danger alert-dismissible" role="alert">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                            '<strong>Warning!</strong> Something wrong. Can\'t delete image.' +
                            '</div>';
                        $('.all-images').prepend(str);
                    }
                }
            });
            return false;
        });

        $('.delete-video').on('click', function (e) {
            var imageBlock = $(this).closest('.video-block');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                success: function (data) {
                    if (data == 'ok') {
                        imageBlock.hide('slow').remove();
                    } else {
                        var str = '<div class="alert alert-danger alert-dismissible" role="alert">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                            '<strong>Warning!</strong> Something wrong. Can\'t delete video.' +
                            '</div>';
                        $('.all-images').prepend(str);
                    }
                }
            });
            return false;
        });

        $('.ajax-general-image').on('click', function (e) {
            var imageBlock = $(this).closest('.image-block');
            var url = $(this).data('url');
            $.ajax({
                url: url,
                success: function (data) {
                    if (data == 'ok') {
                        $('.images').find('a').css({
                            backgroundColor: '#fff',
                            border: '1px solid #ddd'
                        });

                        imageBlock.find('a').css({
                            border: '2px solid #d43f3a'
                        });
                    } else {
                        var str = '<div class="alert alert-danger alert-dismissible" role="alert">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                            '<strong>Warning!</strong> Something wrong. Can\'t make general this image.' +
                            '</div>';
                        $('.images').prepend(str);
                    }
                }
            });
            return false;
        });
        var i = 0;
        <?php if(!$model->isNewRecord):?>
        i = <?php echo $key; ?>;
        i++;
        <?php endif;?>
        $('.add-video').on('click', function () {
            $('.videos').append('<tr>' +
                '<td class="text-right" colspan="2">' +
                '<input type="text" name="Product[videos][' + i + ']" value="" placeholder="Video" class="form-control">' +
                '</td>' +
                '<td class="text-left">' +
                '<button type="button" data-toggle="tooltip" title="Remove" class="btn btn-danger delete-new-videos"><i class="fa fa-minus-circle"></i>' +
                '</button>' +
                '</td>' +
                '</tr>');
            i++;
        });

        //change specification
        $('.change-category').on('change', function () {
            var url = "<?php echo Yii::app()->createUrl('ad_min/product/getSpecifications');?>";
            $.ajax({
                type: 'post',
                url: url,
                data: 'id=' + $(this).val(),
                success: function (data) {
                    if (data != 'empty') {
                        var tag = '';
                        $.each(JSON.parse(data), function (key, value) {
                            tag += '<table class="table" style="border: 1px solid #D1D1D1"><tbody>';
                            tag += '<tr class="head"><td colspan="2"><b>' +
                                value.specification.specificationsGroupLabels.name +
                                '</b></td></tr>';
                            $.each(value.specification.specificationsItems, function (k, val) {
                                tag += '<tr><td>' +
                                    val.specificationsItemsLabels.name +
                                    '</td><td class="text-right">' +
                                    '<select name = "Product[specifications][' + value.specification.id + '][' + val.id + ']" >' +
                                    '<option></option>';

                                $.each(val.specificationsItemsValues, function (k1, v) {
                                    tag += '<option value = "' + v.id + '" >' +
                                        v.value +
                                        '</option >';
                                });
                                tag += '</select></td ></tr >';
                            });

                            tag += '</tbody></table>';
                        });
                        $('.specifications').html(tag);
                    } else {
                        $('.specifications').html('This category has not specifications');
                    }
                }
            });
            return false;
        });
    });

</script>

<script src="<?php echo Yii::app()->request->BaseUrl ?>/vendor/assets/ckeditor/ckeditor.js"></script>
<script>
    $(function () {
        <?php foreach(Languages::model()->findAll() AS $key => $language): ?>
        CKEDITOR.replace('editor_<?php echo $key?>',
            {
                filebrowserBrowseUrl: '<?php echo $this->CreateUrl('product/browse',array('type'=>'Files'));?>',
                filebrowserUploadUrl: '<?php echo $this->CreateUrl('product/upload',array('type'=>'Files'));?>'
            }
        );
        <?php endforeach;?>
    });
</script>

<link rel="stylesheet" type="text/css"
      href="<?php echo Yii::app()->request->baseUrl; ?>/plugins/select2/dist/css/select2.min.css" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/plugins/select2/dist/js/select2.min.js"></script>
<script>
    $(function () {
        $('.select-tag').select2();
    });
</script>