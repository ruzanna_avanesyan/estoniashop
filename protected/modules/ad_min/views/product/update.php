<?php
    /* @var $this ProductController */
    /* @var $model Product */

    $this->breadcrumbs = array(
        'Products' => array('index'),
        $model->id => array('view', 'id' => $model->id),
        'Update',
    );

    $this->menu = array(
        array('label' => 'List Product', 'url' => array('index')),
        array('label' => 'Create Product', 'url' => array('create')),
        array('label' => 'View Product', 'url' => array('view', 'id' => $model->id)),
        array('label' => 'Manage Product', 'url' => array('admin')),
    );
?>

    <h1>Update Product <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array(
    'model'                 => $model,
    'model_pc'              => $model_pc,
    'categories'            => $categories,
    'specifications'        => $specifications,
    'chosen_specifications' => $chosen_specifications,
    'brands'                => $brands,
    'model_filters'         => $model_filters,
    'chosen_filters'        => $chosen_filters,
    'site_price_list'       => $site_price_list,
)); ?>