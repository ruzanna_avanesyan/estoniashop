<?php
    /* @var $this ProductController */
    /* @var $product Product */
    /* @var $type string */
    $options = CHtml::listData(PcBuildOptions::model()->findAllByAttributes(array('pc_type' => 'cpu')), 'option', 'option');
    $val = $product ? $product->pc_cpu : null;
    $pwr = $product ? $product->pc_power : null;
?>

<div class="form-group">
    <label class="col-sm-4 control-label"><?= PCBuilder::$types[$type] ?> Options</label>

    <div class="col-sm-5">
        <table class="table">
            <tr>
                <td>
                    Type *
                </td>
                <td>
                    <?= CHtml::dropDownList('pc[val][]', $val, $options, array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'cpu') ?>
                </td>
            </tr>
            <tr>
                <td>
                    Power in watt *
                </td>
                <td>
                    <?= CHtml::textField('pc[val][]', $pwr, array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'pw') ?>
                </td>
            </tr>
        </table>
    </div>
</div>
