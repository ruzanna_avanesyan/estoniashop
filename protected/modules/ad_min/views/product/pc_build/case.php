<?php
    /* @var $this ProductController */
    /* @var $product Product */
    /* @var $type string */
    $options = CHtml::listData(PcBuildOptions::model()->findAllByAttributes(array('pc_type' => $type)), 'option', 'option');
?>

<div class="form-group">
    <label class="col-sm-4 control-label"><?= PCBuilder::$types[$type] ?> Options</label>

    <div class="col-sm-5">
        <table class="table">
            <?php foreach ($options as $op) { ?>
                <tr>
                    <td>
                        Type *
                    </td>
                    <td>
                        <?=$op?>
                    </td>
                    <td>
                        <?php
                        $v = 0;
                            if($product){
                                $v = $product->getCaseByVal($op);
                            }
                        ?>
                        <?= CHtml::dropDownList('pc[has_case][]', $v,array(0=>'No',1=>'Yes'),array('class'=>'form-control')) ?>
                        <?= CHtml::hiddenField('pc[val][]', $op) ?>
                        <?= CHtml::hiddenField('pc[type][]', 'case') ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>
