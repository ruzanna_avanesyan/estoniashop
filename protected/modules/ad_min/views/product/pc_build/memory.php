<?php
    /* @var $this ProductController */
    /* @var $product Product */
    /* @var $type string */
    $options = CHtml::listData(PcBuildOptions::model()->findAllByAttributes(array('pc_type' => $type)), 'option', 'option');
    $val = $product ? $product->getPc_memory(true) : null;
    $pwr = $product ? $product->pc_power : null;
    $model = $product ? $product->pc_model : null;
    $mhz = $product ? $product->pc_mhz : null;
?>

<div class="form-group">
    <label class="col-sm-4 control-label"><?= PCBuilder::$types[$type] ?> Options</label>

    <div class="col-sm-5">
        <table class="table">
            <tr>
                <td>
                    Type *
                </td>
                <td>
                    Type *
                    <?= CHtml::dropDownList('pc[val][]', $val ? $val->val : '', $options, array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'memory') ?>
                </td>
                <td>
                    Count *
                    <?= CHtml::textField('pc[count][]', $val ? $val->count : '', array('class' => 'form-control')) ?>
                </td>
            </tr>
            <tr>
                <td>
                    Model *
                </td>
                <td colspan="2">
                    <?= CHtml::textField('pc[val][]', $model, array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'model') ?>
                </td>
            </tr>
            <tr>
                <td>
                    MHZ *
                </td>
                <td colspan="2">
                    <?= CHtml::textField('pc[val][]', $mhz, array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'mhz') ?>
                </td>
            </tr>
            <tr>
                <td>
                    Power in watt *
                </td>
                <td colspan="2">
                    <?= CHtml::textField('pc[val][]', $pwr, array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'pw') ?>
                </td>
            </tr>
        </table>
    </div>
</div>
