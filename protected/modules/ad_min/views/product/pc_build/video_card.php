<?php
    /* @var $this ProductController */
    /* @var $product Product */
    /* @var $type string */
    $options = CHtml::listData(PcBuildOptions::model()->findAllByAttributes(array('pc_type' => 'pci')), 'option', 'option');
    $val = $product ? $product->pc_pci : null;
    $pwr = $product ? $product->pc_power : null;
    $model = $product ? $product->pc_model : null;
    $multi = $product ? $product->pc_multi : null;
?>

<div class="form-group">
    <label class="col-sm-4 control-label"><?= PCBuilder::$types[$type] ?> Options</label>

    <div class="col-sm-5">
        <table class="table">
            <tr>
                <td>
                    Type *
                </td>
                <td>
                    <?= CHtml::dropDownList('pc[val][]', $val, $options, array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'pci') ?>
                </td>
            </tr>
            <tr>
                <td>
                    Model *
                </td>
                <td>
                    <?= CHtml::textField('pc[val][]', $model, array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'model') ?>
                </td>
            </tr>
            <tr>
                <td>
                    Power in watt *
                </td>
                <td>
                    <?= CHtml::textField('pc[val][]', $pwr, array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'pw') ?>
                </td>
            </tr>
            <tr>
                <td>
                    SLI or CROSSFIRE (YES) *
                </td>
                <td>
                    <?= CHtml::dropDownList('pc[val][]', $multi, array('0'=>'No','1'=>'Yes'),array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'multi') ?>
                </td>
            </tr>
        </table>
    </div>
</div>
