<?php
    /* @var $this ProductController */
    /* @var $product Product */
    /* @var $type string */
    $pci_options = CHtml::listData(PcBuildOptions::model()->findAllByAttributes(array('pc_type' => 'pci')), 'option', 'option');
    $memory_options = CHtml::listData(PcBuildOptions::model()->findAllByAttributes(array('pc_type' => 'memory')), 'option', 'option');
    $storage_options = CHtml::listData(PcBuildOptions::model()->findAllByAttributes(array('pc_type' => 'storage')), 'option', 'option');
    $case_options = CHtml::listData(PcBuildOptions::model()->findAllByAttributes(array('pc_type' => 'case')), 'option', 'option');
    $options = CHtml::listData(PcBuildOptions::model()->findAllByAttributes(array('pc_type' => 'cpu')), 'option', 'option');

    $pwr = $product ? $product->pc_power : null;
    $case = $product ? $product->pc_case_type : null;
    $val = $product ? $product->getPc_cpu(true) : null;
    $memory = $product ? $product->getPc_memory(true) : null;
    $storage = $product ? $product->getPc_storage(true) : null;
?>

<div class="form-group">
    <label class="col-sm-4 control-label"><?= PCBuilder::$types[$type] ?> Options</label>

    <div class="col-sm-5">
        <table class="table">
            <tr>
                <td>
                    CPU *
                </td>
                <td>
                    Type *
                    <?= CHtml::dropDownList('pc[val][]', $val ? $val->val : '', $options, array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'cpu') ?>
                </td>
                <td>
                    Count *
                    <?= CHtml::textField('pc[count][]', $val ? $val->count : 0, array('class' => 'form-control', 'placeholder' => 'Count')) ?>
                </td>
            </tr>
            <tr>
                <td>
                    Memory *
                </td>
                <td>
                    Type *
                    <?= CHtml::dropDownList('pc[val][]', $memory ? $memory->val : '', $memory_options, array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'memory') ?>
                </td>
                <td>
                    Count *
                    <?= CHtml::textField('pc[count][]', $memory ? $memory->count : 0, array('class' => 'form-control', 'placeholder' => 'Count')) ?>
                </td>
            </tr>
            <tr>
                <td>
                    Storage *
                </td>
                <td>
                    Type *
                    <?= CHtml::dropDownList('pc[val][]', $storage ? $storage->val : '', $storage_options, array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'storage') ?>
                </td>
                <td>
                    Count *
                    <?= CHtml::textField('pc[count][]', $storage ? $storage->count : 0, array('class' => 'form-control', 'placeholder' => 'Count')) ?>
                </td>
            </tr>
            <?php foreach ($pci_options as $pci_op) { ?>
                <tr>
                    <td>
                        Pci *
                    </td>
                    <td>
                        <?= $pci_op ?>
                    </td>
                    <td>
                        Count *
                        <?php
                            $v = 0;
                            if($product){
                                $v = $product->getPciCount($pci_op);
                            }
                        ?>
                        <?= CHtml::hiddenField('pc[val][]', $pci_op) ?>
                        <?= CHtml::hiddenField('pc[type][]', 'pci') ?>
                        <?= CHtml::textField('pc[count][]', $v, array('class' => 'form-control', 'placeholder' => 'Count')) ?>
                    </td>
                </tr>
            <?php } ?>
            <tr>
                <td>
                    Form Factor *
                </td>
                <td colspan="2">
                    <?= CHtml::dropDownList('pc[val][]', $case, $case_options, array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'case') ?>
                </td>
            </tr>
            <tr>
                <td>
                    Power in watt *
                </td>
                <td colspan="2">
                    <?= CHtml::textField('pc[val][]', $pwr, array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'pw') ?>
                </td>
            </tr>
        </table>
    </div>
</div>
