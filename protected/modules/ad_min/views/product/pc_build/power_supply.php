<?php
    /* @var $this ProductController */
    /* @var $product Product */
    /* @var $type string */
    $pwr = $product ? $product->pc_power : null;
?>

<div class="form-group">
    <label class="col-sm-4 control-label"><?= PCBuilder::$types[$type] ?> Options</label>

    <div class="col-sm-5">
        <table class="table">
            <tr>
                <td>
                    Power in watt *
                </td>
                <td>
                    <?= CHtml::textField('pc[val][]', $pwr, array('class' => 'form-control')) ?>
                    <?= CHtml::hiddenField('pc[type][]', 'pw') ?>
                </td>
            </tr>
        </table>
    </div>
</div>
