<?php
    /* @var $this ProductController */
    /* @var $model Product */

    $this->breadcrumbs = array(
        'Products' => array('index'),
        'Create',
    );

    $this->menu = array(
        array('label' => 'List Product', 'url' => array('index')),
        array('label' => 'Manage Product', 'url' => array('admin')),
    );
?>

    <h1>Create Product</h1>

<?php $this->renderPartial('_form', array(
    'model'           => $model,
    'model_pc'        => $model_pc,
    'categories'      => $categories,
    'specifications'  => $specifications,
    'brands'          => $brands,
    'model_filters'   => $model_filters,
    'site_price_list' => $site_price_list,
)); ?>