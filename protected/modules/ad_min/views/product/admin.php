
<h1>Manage Products</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>


<div class="text-right">
	<a href="<?= Yii::app()->createUrl("ad_min/product/create");?>" class="btn btn-success">
		<span class="fa fa-plus"></span>
		Create New Product
	</a>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'product-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'ajaxUpdate'=>false,
	'columns'=>array(
		'id',
		array(
			'header' => 'Status',
			'name' => 	'status',
			'value' => 'Yii::app()->params["adminStatusTypes"][$data->status]',
			'filter' => CHtml::dropDownList('Admins[status]', $model->status,
				Yii::app()->params["adminStatusTypes"],
				array(
					'empty' => 'All',
					'class' => 'form-control'
				)
			),
		),
		array(
			'name' => 'category_id',
			'filter' => CHTml::dropDownList("Product[category_id]",$model->category_id,
				CHtml::listData(CategoryLabel::model()->findAllByAttributes(array('language_id' =>1)), 'category_id', 'name'),
				array('empty' => 'All', 'class' => 'form-control')
			),
			'value' => '$data->category->categoryLabels->name'
		),
		array(
			'name' => 'brand_id',
			'filter' => CHTml::dropDownList("Product[brand_id]",$model->category_id,
				CHtml::listData(BrandLabel::model()->findAllByAttributes(array('language_id' =>1)), 'brand_id', 'name'),
				array('empty' => 'All', 'class' => 'form-control')
			),
			'value' => '$data->brand->brandLabels->name'
		),

		'sort_order',
		'created_date',
		'modify_date',
		array(
			'header' => 'Product Name',
			'name' => 	'product_name',
			'value' => '$data->productLabels ? $data->productLabels->name : ""',
		),
		/*
		'reviews',
		*/
		array(
			'class' => 'CButtonColumn',
			'header'=>"Edit",
			'template' => '{update}',
			'buttons'=>array (
				'update'=>array(
					'label'=>'<span><i class="fa fa-pencil-square-o"></i></span>',
					'imageUrl'=>false,
					'url'=>'Yii::app()->createUrl("ad_min/product/update", array("id" => $data->id))',
					'options'=>array(
						'class'=>'cbutton update',
						'title' => false
					),
				),
			),
		),
		array(
			'class' => 'CButtonColumn',
			'header'=>"Delete",
			'template' => '{delete}',
			'buttons'=>array (
				'delete'=>array(
					'label'=>'<span><i class="fa fa-times"></i></span>',
					'imageUrl'=>false,
					'url'=>'Yii::app()->createUrl("ad_min/product/delete", array("id"=>$data->id))',
					'options'=>array(
						'class'=>'cbutton delete delete_ajax',
						'title' => false
					),
				),
			),
		),
	),
)); ?>
<script>
	function delete_confirm(){
		var r = confirm("Are you sure you want to delete this item?");
		return r;
	}
	$(document).ready(function(){

		$('.delete_ajax').on('click', function (e) {
			if(delete_confirm()) {
				$(this).parent().parent().fadeOut();
				e.preventDefault();
				$.ajax({
					type: 'POST',
					url: $(this).attr('href'),
					dataType: 'json',
					success: function (data) {
						$('.results').html(data);
					}
				});
				return false;
			}
		});
	});
</script>
