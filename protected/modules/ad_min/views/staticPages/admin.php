
<h1>Manage Static Pages</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<div class="text-right">
	<a href="<?= Yii::app()->createUrl("ad_min/staticPages/create");?>" class="btn btn-success">
		<span class="fa fa-plus"></span>
		Create New Page
	</a>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'static-pages-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'ajaxUpdate'=>false,
	'columns'=>array(
		'id',
		array(
			'header' => 'Status',
			'name' => 	'status',
			'value' => 'Yii::app()->params["adminStatusTypes"][$data->status]',
			'filter' => CHtml::dropDownList('Admins[status]', $model->status,
				Yii::app()->params["adminStatusTypes"],
				array(
					'empty' => 'All',
					'class' => 'form-control'
				)
			),
		),
		'sort_order',
		'created_date',
		'modify_date',
		array(
			'header' => 'Title',
			'name' => 	'title',
			'value' => '$data->staticPagesLabels ? $data->staticPagesLabels->title : ""',
		),
		array(
			'class' => 'CButtonColumn',
			'header'=>"Edit",
			'template' => '{update}',
			'buttons'=>array (
				'update'=>array(
					'label'=>'<span><i class="fa fa-pencil-square-o"></i></span>',
					'imageUrl'=>false,
					'url'=>'Yii::app()->createUrl("ad_min/staticPages/update", array("id" => $data->id))',
					'options'=>array(
						'class'=>'cbutton update',
						'title' => false
					),
				),
			),
		),
		array(
			'class' => 'CButtonColumn',
			'header'=>"Delete",
			'template' => '{delete}',
			'buttons'=>array (
				'delete'=>array(
					'label'=>'<span><i class="fa fa-times"></i></span>',
					'visible'=>'$data->id != 3',
					'imageUrl'=>false,
					'url'=>'Yii::app()->createUrl("ad_min/staticPages/delete", array("id"=>$data->id))',
					'options'=>array(
						'class'=>'cbutton delete delete_ajax',
						'title' => false
					),
				),
			),
		),
	),
)); ?>
<script>
	function delete_confirm(){
		var r = confirm("Are you sure you want to delete this item?");
		return r;
	}
	$(document).ready(function(){

		$('.delete_ajax').on('click', function (e) {
			if(delete_confirm()) {
				$(this).parent().parent().fadeOut();
				e.preventDefault();
				$.ajax({
					type: 'POST',
					url: $(this).attr('href'),
					dataType: 'json',
					success: function (data) {
						$('.results').html(data);
					}
				});
				return false;
			}
		});
	});
</script>
