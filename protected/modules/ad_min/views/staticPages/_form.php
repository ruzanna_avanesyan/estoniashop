<?php
/* @var $this StaticPagesController */
/* @var $model StaticPages */
/* @var $form CActiveForm */
?>

<div class="col-xs-12" style="margin-top: 50px">
	<div class="box box-info">

		<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'static-pages-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
		'enctype'=>'multipart/form-data'
	)
)); ?>
		<div class="box-body">
			<p class="note">Fields with <span class="required">*</span> are required.</p>

			<?php echo $form->errorSummary($model); ?>

			<div class="form-group">
				<?php echo $form->labelEx($model,'status', array('class'=>'col-sm-1 control-label')); ?>
				<div class="col-sm-5">
					<?php echo $form->dropDownList($model, 'status', Yii::app()->params['adminStatusTypes'], array('class'=>'form-control select-toggleizer'));?>
				</div>
				<?php echo $form->error($model,'status'); ?>
			</div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'sort_order', array('class'=>'col-sm-1 control-label')); ?>
				<div class="col-sm-5">
					<?php echo $form->textField($model,'sort_order', array('class'=>'form-control'));?>
				</div>
				<?php echo $form->error($model,'sort_order'); ?>
			</div>

			<div>
				<div class="clearfix">
					<ul class="nav nav-tabs" role="tablist">
						<?php foreach(Languages::model()->findAll() AS $key => $language): ?>
							<li role="presentation" class="custom-language <?=($key == 0) ? "active" : ""?>">
								<a href="#lang<?=$key?>" aria-controls="home" role="tab" data-toggle="tab">
									<?php echo $language->name?>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>

					<div class="tab-content">
						<?php foreach(Languages::model()->findAll() AS $key => $language): ?>
							<div role="tabpanel" class="tab-pane <?=($key == 0) ? "active" : ""?>" id="lang<?=$key?>">
								<div>
									<label>Title<br><?=$language->name?></label>
									<input type="text"
										   name="StaticPages[tab][<?=$language->id?>][title]"
										   placeholder="Title"
										   class="form-control"
										   size="40"
										   maxlength="255"
										   value="<?=(isset($_GET['id'], StaticPagesLabel::model()->findByAttributes(array('static_page_id' => intval($_GET['id']), 'language_id' => $language->id))->title))
											   ? StaticPagesLabel::model()->findByAttributes(array('static_page_id' => intval($_GET['id']), 'language_id' => $language->id))->title
											   : "" ?>">
								</div>
								<div>
									<label>Description<br>
										<?=$language->name?>
									</label>
									<div>
										<textarea id="editor_<?=$key?>"
												  class="description_<?=$key?> hidden" type="text"
												  name="StaticPages[tab][<?=$language->id?>][description]"
												  placeholder="Answer"
												  class="form-control"
												  rows="10"
												  cols="80"
										><?=(isset($_GET['id'], StaticPagesLabel::model()->findByAttributes(array('static_page_id' => intval($_GET['id']), 'language_id' => $language->id))->description))
												? StaticPagesLabel::model()->findByAttributes(array('static_page_id' => intval($_GET['id']), 'language_id' => $language->id))->description
												: "" ?></textarea>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>

            <div class="box-footer">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info pull-right save')); ?>
            </div>
		</div>

		<?php $this->endWidget(); ?>

	</div>
</div><!-- form -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="//cdn.ckeditor.com/4.5.10/full/ckeditor.js"></script>

<script>
	$(function () {
		<?php foreach(Languages::model()->findAll() AS $key => $language): ?>
		CKEDITOR.replace('editor_<?=$key?>');
		<?php endforeach;?>

	});
</script>