<?php
/* @var $this SpecificationsGroupController */
/* @var $model SpecificationsGroup */
/* @var $form CActiveForm */
?>
<style>
	div.font{
		float: left;
		margin-left: 45px;
		color: #337ab7;
	}
	div.font span{
		margin: 5px;
	}
	div.font span:hover{
		cursor: pointer;
	}
</style>
<div class="col-xs-12" style="margin-top: 50px">
	<div class="box box-info">

		<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'specifications-group-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
		'enctype'=>'multipart/form-data'
	)
)); ?>
		<div class="box-body">
			<p class="note">Fields with <span class="required">*</span> are required.</p>

			<?php echo $form->errorSummary($model); ?>

			<div class="form-group">
				<?php echo $form->labelEx($model,'status', array('class'=>'col-sm-1 control-label')); ?>
				<div class="col-sm-5">
					<?php echo $form->dropDownList($model, 'status', Yii::app()->params['adminStatusTypes'], array('class'=>'form-control select-toggleizer'));?>
				</div>
				<?php echo $form->error($model,'status'); ?>
			</div>

			<div class="form-group">
				<?php echo $form->labelEx($model,'sort_order', array('class'=>'col-sm-1 control-label')); ?>
				<div class="col-sm-5">
					<?php echo $form->textField($model,'sort_order', array('class'=>'form-control'));?>
				</div>
				<?php echo $form->error($model,'sort_order'); ?>
			</div>

			<div class="form-group">
				<label class="col-sm-1 control-label" for="SpecificationsGroup_name">
					Specification Group Name
				</label>
				<div class="col-sm-5">
					<?php foreach(Languages::model()->findAll() as $key => $value) : ?>
						<div class="input-group">
							<span class="input-group-addon">
								<?php echo $value->name?>
							</span>
							<input class="form-control" name="SpecificationsGroup[name][<?php echo $value->id;?>]" id="SpecificationsGroup_title"
								   type="text" value="<?php echo !$model->isNewRecord ? SpecificationsGroupLabel::model()->findByAttributes(array('specifications_id' => $_GET['id'], 'language_id' => $value->id))->name : "";?>">
						</div>
					<?php endforeach;?>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-12">
					<table id="filter" class="table table-striped table-bordered table-hover">
						<thead>
						<tr>
							<td class="text-left required">Specification Name</td>
							<td class="text-left">Specification Value</td>
							<td></td>
						</tr>
						</thead>
						<tbody>
						<?php if(!$model->isNewRecord) :?>
							<?php foreach(SpecificationsItems::model()->findAllByAttributes(array('specification_id' => $_GET['id']))  as $key => $val ):?>
								<tr>
									<td class="text-left" style="width: 30%;">
										<?php foreach(Languages::model()->findAll() as $k => $value):?>
											<div class="input-group">
													<span class="input-group-addon">
														<?php echo $value->name?>
													</span>
												<input type="hidden" name="SpecificationsGroup[items][<?php echo $key;?>][specification_item_id]" value="<?php echo $val->id;?>">
												<input type="text" name="SpecificationsGroup[items][<?php echo $key;?>][specification_description][<?php echo $value->id;?>]" value="<?php echo SpecificationsItemsLabel::model()->findByAttributes(array('specifications_item_id' => $val->id, 'language_id' => $value->id))->name;?>" placeholder="Specification Name" class="form-control">
											</div>
										<?php endforeach;?>
									</td>
									<td class="text-right">
										<div id="values">
											<?php foreach(SpecificationsItemsValues::model()->findAllByAttributes(array('specifications_item_id' => $val->id)) as $index => $v):?>
												<div style ="margin-bottom:5px;">
													<input type="hidden" name="SpecificationsGroup[items][<?php echo $key;?>][value][<?php echo $index;?>][value_id]" value="<?php echo $v->id;?>">
													<input style="width: 50%;display: inline-block;float: left;" type="text" name="SpecificationsGroup[items][<?php echo $key;?>][value][<?php echo $index;?>][value]" value='<?php echo $v->value;?>' placeholder="Specification Value" class="form-control"/>
													<div class="font">
														<span><i class="fa fa-times"></i> No</span>
														<span><i class="fa fa-check"></i> Yes</span>
													</div>
													<button type="button" data-toggle="tooltip" data-id="<?php echo $v->id;?>" class="btn btn-warning remove_specification_value" data-original-title="Remove"><i class="fa fa-minus-circle"></i></button>
												</div>
											<?php endforeach;;?>
										</div>
										<a href="#" data-k="<?php echo $key;?>" data-toggle="tooltip" title="" class="add_specification_value btn btn-success" data-original-title="Add Specification Value"><i class="fa fa-plus-circle"></i></a>
									</td>
									<td class="text-left">
										<button type="button" data-toggle="tooltip" title="Remove" data-id="<?php echo $val->id;?>" class="btn btn-danger remove_specification_item"><i class="fa fa-minus-circle"></i></button>
									</td>
								</tr>
							<?php endforeach;?>
						<?php endif;?>
						</tbody>
						<tfoot>
						<tr>
							<td colspan="2"></td>
							<td class="text-left">
								<a href="#" id="add_specification" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Add Specification"><i class="fa fa-plus-circle"></i></a>
							</td>
						</tr>
						</tfoot>
					</table>
				</div>
			</div>

			<div class="box-footer">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info pull-right save')); ?>
			</div>
		</div>

		<?php $this->endWidget(); ?>

	</div>
</div><!-- form -->
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>

<script>
	$(document).ready(function(){
		var k = 0;
		<?php if(!$model->isNewRecord):?>
			k = <?php echo $key;?>+1;
		<?php endif;?>
		$('#add_specification').on('click', function(e){
			e.preventDefault();
			$('table tbody').append('<tr>' +
				'<td class="text-left" style="width: 30%;">' +
					<?php foreach(Languages::model()->findAll() as $k => $value):?>
						'<div class="input-group">'+
							'<span class="input-group-addon">'+
							'<?php echo $value->name?>'+
							'</span>'+
							'<input type="text" name="SpecificationsGroup[items]['+ k +'][specification_description][<?php echo $value->id;?>]" value="" placeholder="Specification Name" class="form-control">'+
						'</div>'+
					<?php endforeach;?>
				'</td>'+
				'<td class="text-right">'+
					'<div id="values">'+
						'<div style ="margin-bottom:5px;">' +
							'<input style="width: 50%;display: inline-block;float: left;" type="text" name="SpecificationsGroup[items]['+ k +'][value][]" value="" placeholder="Specification Value" class="form-control">' +
								'<div class="font">'+
									'<span><i class="fa fa-times"></i> No</span>' +
									'<span><i class="fa fa-check"></i> Yes</span>'+
								'</div>' +
							'<button type="button" data-toggle="tooltip" title="" class="btn btn-warning remove_added_specification_value" data-original-title="Remove"><i class="fa fa-minus-circle"></i></button>'+
						'</div>'+
					'</div>'+
					'<a href="#" data-k="'+ k +'" data-toggle="tooltip" title="" class="add_specification_value btn btn-success" data-original-title="Add Specification Value"><i class="fa fa-plus-circle"></i></a>' +
				'</td>'+
				'<td class="text-left">'+
					'<button type="button" data-toggle="tooltip" title="Remove" class="btn btn-danger remove_added_specification_item">'+
					'<i class="fa fa-minus-circle"></i>'+
				'</button>'+
				'</td>'+
				'</tr>');
			k++;
		});
		$(document).on('click','.add_specification_value', function(e){
			e.preventDefault();
			$(this).parent().children('div#values').append('<div style ="margin-bottom:5px;">' +
				'<input style="width: 50%;display: inline-block;float: left;" type="text" name="SpecificationsGroup[items]['+ $(this).data("k") +'][value][]" value="" placeholder="Specification Value" class="form-control">' +
				'<div class="font">'+
					'<span><i class="fa fa-times"></i> No</span>' +
					'<span><i class="fa fa-check"></i> Yes</span>'+
				'</div>' +
				'<button type="button" data-toggle="tooltip" title="" class="btn btn-warning remove_added_specification_value" data-original-title="Remove"><i class="fa fa-minus-circle"></i></button>'+
				'</div>');
		});
		$('.remove_specification_item').on('click', function(e){
			var that = $(this);
			$.ajax({
				type: 'POST',
				url: '<?php echo Yii::app()->createUrl('ad_min/specificationsGroup/deleteItem');?>',
				data: 'id='+$(this).data('id'),
				success: function(data){
					if(data) {
						that.parent().parent().hide('slow').remove();
					}else {
						alert('Please contact developers team');
					}
				}
			});
		});
		$('.remove_specification_value').on('click', function(e){
			var that = $(this);
			$.ajax({
				type: 'POST',
				url: '<?php echo Yii::app()->createUrl('ad_min/specificationsGroup/deleteItemValue');?>',
				data: 'id='+$(this).data('id'),
				success: function(data){
					if(data) {
						that.parent().hide('slow').remove();
					}else {
						alert('Please contact developers team');
					}
				}
			});
		});
		$(document).on('click', '.remove_added_specification_value', function(){
			$(this).parent().remove();
		});
		$(document).on('click', '.remove_added_specification_item', function(){
			$(this).parent().parent().remove();
		});

		$(document).on('click', '.font span',function(){
			$(this).parent().parent().children('input[type="text"]').val($(this).html());
		});
	});
</script>