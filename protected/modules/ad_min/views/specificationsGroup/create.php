<?php
/* @var $this SpecificationsGroupController */
/* @var $model SpecificationsGroup */

$this->breadcrumbs=array(
	'Specifications Groups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SpecificationsGroup', 'url'=>array('index')),
	array('label'=>'Manage SpecificationsGroup', 'url'=>array('admin')),
);
?>

<h1>Create SpecificationsGroup</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>