<?php
/* @var $this SpecificationsGroupController */
/* @var $model SpecificationsGroup */

$this->breadcrumbs=array(
	'Specifications Groups'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SpecificationsGroup', 'url'=>array('index')),
	array('label'=>'Create SpecificationsGroup', 'url'=>array('create')),
	array('label'=>'Update SpecificationsGroup', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SpecificationsGroup', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SpecificationsGroup', 'url'=>array('admin')),
);
?>

<h1>View SpecificationsGroup #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'sort_order',
		'status',
		'created_date',
		'modify_date',
	),
)); ?>
