<?php
/* @var $this SpecificationsGroupController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Specifications Groups',
);

$this->menu=array(
	array('label'=>'Create SpecificationsGroup', 'url'=>array('create')),
	array('label'=>'Manage SpecificationsGroup', 'url'=>array('admin')),
);
?>

<h1>Specifications Groups</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
