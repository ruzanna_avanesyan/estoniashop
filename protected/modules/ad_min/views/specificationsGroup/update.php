<?php
/* @var $this SpecificationsGroupController */
/* @var $model SpecificationsGroup */

$this->breadcrumbs=array(
	'Specifications Groups'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SpecificationsGroup', 'url'=>array('index')),
	array('label'=>'Create SpecificationsGroup', 'url'=>array('create')),
	array('label'=>'View SpecificationsGroup', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SpecificationsGroup', 'url'=>array('admin')),
);
?>

<h1>Update SpecificationsGroup <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>