<?php
/* @var $this PcBuildOptionsController */
/* @var $model PcBuildOptions */

$this->breadcrumbs=array(
	'Pc Build Options'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PcBuildOptions', 'url'=>array('index')),
	array('label'=>'Manage PcBuildOptions', 'url'=>array('admin')),
);
?>

<h1>Create PcBuildOptions</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>