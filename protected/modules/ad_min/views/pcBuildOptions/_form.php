<?php
    /* @var $this PcBuildOptionsController */
    /* @var $model PcBuildOptions */
    /* @var $items PcBuildOptions[] */
    /* @var $form CActiveForm */
?>

<div class="col-xs-12" style="margin-top: 50px">
    <div class="box box-info">

        <?php $form = $this->beginWidget('CActiveForm', array(
            'id'                   => 'pc-build-options-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
            'htmlOptions'          => array(
                'class'   => 'form-horizontal',
                'enctype' => 'multipart/form-data'
            )
        )); ?>
        <div class="box-body">
            <p class="note">Fields with <span class="required">*</span> are required.</p>

            <?php foreach($items as $item) { ?>
                <div class="form-group">
                    <?php echo $form->labelEx($item, 'option',array('class'=>'control-label col-sm-4')); ?>
                    <div class="col-sm-4">
                        <?php echo $form->textField($item, 'option', array('size' => 60, 'maxlength' => 255,'class'=>'form-control item-option')); ?>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-danger remove-item"><i class="fa fa-remove"></i></button>
                    </div>
                </div>
            <?php } ?>

            <div class="form-group">

                <div class="col-sm-4 pull-right">
                    <button class="btn btn-success" id="add_option"><i class="fa fa-plus"></i></button>
                </div>
            </div>

            <div class="box-footer">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info pull-right save','id'=>'submit-form')); ?>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div>
</div><!-- form -->

<div class="hidden">
    <div id="opt-tmp">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'option',array('class'=>'control-label col-sm-4')); ?>
            <div class="col-sm-4">
                <?php echo $form->textField($model, 'option', array('size' => 60, 'maxlength' => 255,'class'=>'form-control item-option')); ?>
            </div>
            <div class="col-sm-2">
                <button class="btn btn-danger remove-item"><i class="fa fa-remove"></i></button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor_admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script>
    $(document).ready(function(){
        $(document).on('click','#add_option',function(e){
            e.preventDefault();
            var t = $('#opt-tmp').html();
            $(t).insertBefore($(this).closest('div.form-group'));
        });
        $(document).on('click','.remove-item',function(e){
            e.preventDefault();
            $(this).closest('div.form-group').remove();
        });

        $('#submit-form').on('click',function(e){
            e.preventDefault();
            var check = true;
            $('#pc-build-options-form .error').remove();
            if($('#pc-build-options-form .item-option').length == 0 ){
                check = false;
                $('#pc-build-options-form').prepend('<div class="error text-center">Must be at least one option.</div>');
            }
            $.each($('#pc-build-options-form .item-option'),function(){
                if($(this).val().length == 0){
                    check = false;
                    $('<div class="error text-center">Option cannot be blank.</div>').insertAfter($(this));
                }
            });

            if(check){
                $.each($('#pc-build-options-form .item-option'),function(k,v){
                    if (typeof  $(this).attr('name') != 'undefined') {
                        $(this).attr('name', $(this).attr('name').replace('[', '[' + k + ']['));
                    }
                });
                $('#pc-build-options-form').submit();
            }
        });
    });
</script>