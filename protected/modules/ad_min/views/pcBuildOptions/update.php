<?php
/* @var $this PcBuildOptionsController */
/* @var $model PcBuildOptions */

$this->breadcrumbs=array(
	'Pc Build Options'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PcBuildOptions', 'url'=>array('index')),
	array('label'=>'Create PcBuildOptions', 'url'=>array('create')),
	array('label'=>'View PcBuildOptions', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PcBuildOptions', 'url'=>array('admin')),
);
?>

<h1><?=ucwords($model->pc_type)?> Options</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'items'=>$items)); ?>