<?php
/* @var $this PcBuildOptionsController */
/* @var $model PcBuildOptions */

$this->breadcrumbs=array(
	'Pc Build Options'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PcBuildOptions', 'url'=>array('index')),
	array('label'=>'Create PcBuildOptions', 'url'=>array('create')),
	array('label'=>'Update PcBuildOptions', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PcBuildOptions', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PcBuildOptions', 'url'=>array('admin')),
);
?>

<h1>View PcBuildOptions #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'pc_type',
		'option',
	),
)); ?>
