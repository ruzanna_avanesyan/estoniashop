<?php
    /* @var $this PcBuildOptionsController */
    /* @var $model PcBuildOptions */

    $this->breadcrumbs = array(
        'Pc Build Options' => array('index'),
        'Manage',
    );

    $this->menu = array(
        array('label' => 'List PcBuildOptions', 'url' => array('index')),
        array('label' => 'Create PcBuildOptions', 'url' => array('create')),
    );

    Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pc-build-options-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Pc Build Options</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'           => 'pc-build-options-grid',
    'dataProvider' => $model->search(),
    'filter'       => $model,
    'columns'      => array(
        'pc_type',
        'option',
        array(
            'class' => 'CButtonColumn',
            'header'=>"Edit",
            'template' => '{update}',
            'buttons'=>array (
                'update'=>array(
                    'label'=>'<span><i class="fa fa-pencil-square-o"></i></span>',
                    'imageUrl'=>false,
                    'url'=>'Yii::app()->createUrl("ad_min/PcBuildOptions/update", array("type" => $data->pc_type))',
                    'options'=>array(
                        'class'=>'cbutton update',
                        'title' => false
                    ),
                ),
            ),
        ),
    ),
)); ?>
