<?php
/* @var $this PcBuildOptionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pc Build Options',
);

$this->menu=array(
	array('label'=>'Create PcBuildOptions', 'url'=>array('create')),
	array('label'=>'Manage PcBuildOptions', 'url'=>array('admin')),
);
?>

<h1>Pc Build Options</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
