<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
define('bDIR', dirname(Yii::app()->request->scriptFile));
define('UPLOADS', bDIR."/vendor/");
define('THUMB', Yii::app()->request->baseUrl."/vendor/img/timthumb.php?src=");

class Controller extends CController
{
	public $breadcrumbs=array();
	public $menu=array();
	public $messages_count;


	public function init(){
//		Helpers::pr(Yii::app()->user);
		if(Yii::app()->user->isGuest && !strpos(Yii::app()->getRequest()->getQueryString(), '/login')){
			$this->redirect(Yii::app()->createUrl('ad_min/admins/login'));
		}
		$this->messages_count = ContactMessages::model()->countByAttributes(array('status' => 0));
	}
}