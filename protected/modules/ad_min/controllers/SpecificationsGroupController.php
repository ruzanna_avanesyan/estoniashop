<?php

class SpecificationsGroupController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	public $defaultAction = "admin";

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SpecificationsGroup;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SpecificationsGroup']))
		{
			$model->attributes=$_POST['SpecificationsGroup'];
			if($model->save()){
				foreach($_POST['SpecificationsGroup']['name'] as $key => $value){
					$model_label = new SpecificationsGroupLabel;
					$model_label->specifications_id = $model->id;
					$model_label->language_id = $key;
					$model_label->name = $value;
					$model_label->save();
				}
				if(isset($_POST['SpecificationsGroup']['items'])) {
					foreach ($_POST['SpecificationsGroup']['items'] as $val) {
						$model_items = new SpecificationsItems;
						$model_items->specification_id = $model->id;
						if ($model_items->save()) {
							foreach ($val['specification_description'] as $k => $v) {
								$model_items_label = new SpecificationsItemsLabel;
								$model_items_label->specifications_item_id = $model_items->id;
								$model_items_label->language_id = $k;
								$model_items_label->name = $v;
								$model_items_label->save();
							}
							foreach ($val['value'] as $v) {
								$model_items_value = new SpecificationsItemsValues;
								$model_items_value->specifications_item_id = $model_items->id;
								$model_items_value->value = $v;
								$model_items_value->save();
							}
						}
					}
				}
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SpecificationsGroup']))
		{
			$model->attributes=$_POST['SpecificationsGroup'];
			$model->modify_date = date('Y-m-d h:i:s');
			if($model->save()){

				SpecificationsGroupLabel::model()->deleteAllByAttributes(array('specifications_id' => intval($_GET['id'])));
				foreach($_POST['SpecificationsGroup']['name'] as $key => $value){
					$model_label = new SpecificationsGroupLabel;
					$model_label->specifications_id = $model->id;
					$model_label->language_id = $key;
					$model_label->name = $value;
					$model_label->save();
				}
				if(isset($_POST['SpecificationsGroup']['items'])) {

//					$old_model_item = SpecificationsItems::model()->findAllByAttributes(array('specification_id' => intval($_GET['id'])));
//					Helpers::pr($_POST['SpecificationsGroup']['items']);

//					SpecificationsItems::model()->deleteAllByAttributes(array('specification_id' => intval($_GET['id'])));

					foreach ($_POST['SpecificationsGroup']['items'] as $val) {
						$model_items = array();
						if(isset($val['specification_item_id'])){
							$model_items = SpecificationsItems::model()->findByPk($val['specification_item_id']);

						}else{
							$model_items = new SpecificationsItems;
						}

						$model_items->specification_id = $model->id;

						if ($model_items->save()) {

							SpecificationsItemsLabel::model()->deleteAllByAttributes(array('specifications_item_id' => $model_items->id));
							foreach ($val['specification_description'] as $k => $v) {
								$model_items_label = new SpecificationsItemsLabel;
								$model_items_label->specifications_item_id = $model_items->id;
								$model_items_label->language_id = $k;
								$model_items_label->name = $v;
								$model_items_label->save();
							}
//							SpecificationsItemsValues::model()->deleteAllByAttributes(array('specifications_item_id' => $model_items->id));

							if(isset($val['value'])) {
								foreach ($val['value'] as $v) {
									if(isset($v)) {
										if (isset($v['value_id'])) {
											$model_items_value = SpecificationsItemsValues::model()->findByPk($v['value_id']);
											$model_items_value->value = $v['value'];
										} else {
											$model_items_value = new SpecificationsItemsValues;
											$model_items_value->value = $v;
										}
										$model_items_value->specifications_item_id = $model_items->id;
										$model_items_value->save(false);
									}
								}
							}

						}
					}
				}
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDeleteItem(){
		if(SpecificationsItems::model()->deleteByPk($_POST['id'])){
			ProductHasSpecification::model()->deleteAllByAttributes(array('specification_item_id' => intval($_POST['id'])));
			echo 1;die;
		}else{
			echo 0;die;
		}
	}
	public function actionDeleteItemValue(){
		if(SpecificationsItemsValues::model()->deleteByPk($_POST['id'])){
			echo 1;die;
		}else{
			echo 0;die;
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SpecificationsGroup');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SpecificationsGroup('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SpecificationsGroup']))
			$model->attributes=$_GET['SpecificationsGroup'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SpecificationsGroup the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SpecificationsGroup::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SpecificationsGroup $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='specifications-group-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
