<?php

class LanguagesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	public $defaultAction = "admin";

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Languages;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Languages']))
		{
			$model->attributes=$_POST['Languages'];
			if($model->save()) {
				$model_label = TranslationLabel::model()->findAll();
				foreach($model_label as $value){
					$model_label_new =  new TranslationLabel();
					$model_label_new->translation_id = $value['translation_id'];
					$model_label_new->language_id = $model->id;;
					$model_label_new->save();
				}
				$model_label = BrandLabel::model()->findAll();
				foreach($model_label as $value){
					$model_label_new =  new BrandLabel();
					$model_label_new->brand_id = $value['brand_id'];
					$model_label_new->language_id = $model->id;;
					$model_label_new->save();
				}

				$model_label = CategoryLabel::model()->findAll();
				foreach($model_label as $value){
					$model_label_new =  new CategoryLabel();
					$model_label_new->category_id = $value['category_id'];
					$model_label_new->language_id = $model->id;;
					$model_label_new->save();
				}


				$model_label = FilterLabel::model()->findAll();
				foreach($model_label as $value){
					$model_label_new =  new FilterLabel();
					$model_label_new->filter_id = $value['filter_id'];
					$model_label_new->language_id = $model->id;;
					$model_label_new->save();
				}

				$model_label = FilterItemLabel::model()->findAll();
				foreach($model_label as $value){
					$model_label_new =  new FilterItemLabel();
					$model_label_new->filter_item_id = $value['filter_item_id'];
					$model_label_new->language_id = $model->id;;
					$model_label_new->save();
				}

				$model_label = ProductLabel::model()->findAll();
				foreach($model_label as $value){
					$model_label_new =  new ProductLabel();
					$model_label_new->product_id = $value['product_id'];
					$model_label_new->language_id = $model->id;;
					$model_label_new->save();
				}

				$model_label = SpecificationsGroupLabel::model()->findAll();
				foreach($model_label as $value){
					$model_label_new =  new SpecificationsGroupLabel();
					$model_label_new->specifications_id = $value['specifications_id'];
					$model_label_new->language_id = $model->id;;
					$model_label_new->save();
				}

				$model_label = SpecificationsItemsLabel::model()->findAll();
				foreach($model_label as $value){
					$model_label_new =  new SpecificationsItemsLabel();
					$model_label_new->specifications_item_id = $value['specifications_item_id'];
					$model_label_new->language_id = $model->id;;
					$model_label_new->save();
				}

				$model_label = StaticPagesLabel::model()->findAll();
				foreach($model_label as $value){
					$model_label_new =  new StaticPagesLabel();
					$model_label_new->static_page_id = $value['static_page_id'];
					$model_label_new->language_id = $model->id;;
					$model_label_new->save();
				}

				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Languages']))
		{
			$model->attributes=$_POST['Languages'];
			$model->modify_date = date('Y-m-d h:i:s');
			if($model->save()){
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Languages');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Languages('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Languages']))
			$model->attributes=$_GET['Languages'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Languages the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Languages::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Languages $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='languages-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
