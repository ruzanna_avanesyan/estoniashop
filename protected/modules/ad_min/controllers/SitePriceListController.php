<?php

class SitePriceListController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
    public $defaultAction = 'admin';

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SitePriceList;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SitePriceList']))
		{
			$model->attributes=$_POST['SitePriceList'];
            if(!empty($_FILES['SitePriceList']['name']['image'])){
                $name  = $_FILES['SitePriceList']['name']['image'];
                $name_md5 =  explode('.',$name);
                $count = count($name_md5)-1;
                $name_md5 =  md5(mktime().$name).'.'.$name_md5[$count];

                $model->image = $name_md5;
            }

			if($model->save()) {
                $uploads_dir = UPLOADS . 'image/sites_prices_list';
                if(!empty($_FILES['SitePriceList']['name']['image'])) {
                    $tmp_name = $_FILES["SitePriceList"]["tmp_name"]['image'];
                    move_uploaded_file($tmp_name, "$uploads_dir/$name_md5");
                }

                $this->redirect(array('admin'));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SitePriceList']))
		{


            if(!empty($_FILES['SitePriceList']['name']['image'])){
                $model->attributes = $_POST['SitePriceList'];

                $name  = $_FILES['SitePriceList']['name']['image'];
                $name_md5 =  explode('.',$name);
                $count = count($name_md5)-1;
                $name_md5 =  md5(mktime().$name).'.'.$name_md5[$count];
                $model->image = $name_md5;
            }else{
                $_POST['SitePriceList']['image'] = $model->attributes['image'];
                $model->attributes = $_POST['SitePriceList'];
            }

			if($model->save()) {
                $uploads_dir = UPLOADS . 'image/sites_prices_list';
                if(!empty($_FILES['SitePriceList']['name']['image'])) {
                    $tmp_name = $_FILES["SitePriceList"]["tmp_name"]['image'];
                    move_uploaded_file($tmp_name, "$uploads_dir/$name_md5");
                }

                $this->redirect(array('admin'));
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SitePriceList');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SitePriceList('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SitePriceList']))
			$model->attributes=$_GET['SitePriceList'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SitePriceList the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SitePriceList::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SitePriceList $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='site-price-list-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
