<?php

class FilterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	public $defaultAction = "admin";


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Filter;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Filter']))
		{

//			Helpers::pr($_POST);

			$model->attributes=$_POST['Filter'];
			if($model->save()) {

				foreach($_POST['Filter']['title'] as $key=>$value){
					$model_label = new FilterLabel();
					$model_label->filter_id = $model->id;
					$model_label->language_id = $key;
					$model_label->name = $value;
					$model_label->save();
				}
				if(isset($_POST['Filter']['items'])) {
					foreach ($_POST['Filter']['items'] as $val) {
						$model_item = new FilterItem();
						$model_item->filter_id = $model->id;
						$model_item->sort_order = $val['sort_order'];
						if ($model_item->save(false)) {
							foreach ($val['filter_description'] as $k => $v) {
								$model_item_label = new FilterItemLabel();
								$model_item_label->filter_item_id = $model_item->id;
								$model_item_label->language_id = $k;
								$model_item_label->name = $v;
								$model_item_label->save(false);
							}
						}
					}
				}

				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Filter']))
		{
//			Helpers::pr($_POST);
			$model->attributes = $_POST['Filter'];
			$model->modify_date = date('Y-m-d h:i:s');
			if($model->save()) {
				FilterLabel::model()->deleteAllByAttributes(array('filter_id' => intval($_GET['id'])));
				foreach($_POST['Filter']['title'] as $key=>$value){
					$model_label = new FilterLabel();
					$model_label->filter_id = $id;
					$model_label->language_id = $key;
					$model_label->name = $value;
					$model_label->save();
				}
				if(isset($_POST['Filter']['items'])) {
//					Helpers::pr($_POST['Filter']);
//					FilterItem::model()->deleteAllByAttributes(array('filter_id' => intval($_GET['id'])));

					foreach ($_POST['Filter']['items'] as $val) {
						if(isset($val['filter_item_id'])) {
							$model_item = FilterItem::model()->findByPk($val['filter_item_id']);
						}else{
							$model_item = new FilterItem();
						}
						$model_item->filter_id = $id;
						$model_item->sort_order = $val['sort_order'];
						if ($model_item->save(false)) {
							FilterItemLabel::model()->deleteAllByAttributes(array('filter_item_id' => $model_item->id));
							foreach ($val['filter_description'] as $k => $v) {
								$model_item_label = new FilterItemLabel();
								$model_item_label->filter_item_id = $model_item->id;
								$model_item_label->language_id = $k;
								$model_item_label->name = $v;
								$model_item_label->save(false);
							}
						}
					}
				}

				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	public function actionDeleteItem(){
		if(FilterItem::model()->deleteByPk($_POST['id'])){
			echo 1;die;
		}else{
			echo 0;die;
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Filter');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Filter('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Filter']))
			$model->attributes=$_GET['Filter'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Filter the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Filter::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Filter $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='filter-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
