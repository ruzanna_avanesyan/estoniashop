<?php

    class ProductController extends Controller {
        /**
         * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
         * using two-column layout. See 'protected/views/layouts/column2.php'.
         */
        public $layout        = '//layouts/column2';
        public $defaultAction = "admin";

        /**
         * Displays a particular model.
         *
         * @param integer $id the ID of the model to be displayed
         */
        public function actionView($id) {
            $this->render('view', array(
                'model' => $this->loadModel($id),
            ));
        }

        /**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        public function actionCreate() {
            $model = new Product;
            $model_pc = new ProductPcBuild();
            $category_model = Category::model()->with('categoryLabels')->findAll();

            $categories = array();
            foreach ($category_model as $value) {
                $categories[$value->id] = $value->categoryLabels->name;
            }

            $brand_model = Brand::model()->with('brandLabels')->findAll();
            $brands = array();
            foreach ($brand_model as $value) {
                $brands[$value->id] = $value->brandLabels->name;
            }
            if (isset($_POST['Product'])) {
                $model->attributes = $_POST['Product'];
                $model->created_date = date('Y-m-d H:i:s');
                if ($model->save()) {
                    foreach ($_POST['Product']['tab'] as $key => $value) {
                        $model_label = new ProductLabel();
                        $model_label->product_id = $model->id;
                        $model_label->language_id = $key;
                        $model_label->name = $value['name'];
                        $model_label->description = $value['description'];
                        $model_label->save();
                    }
                    if (isset($_FILES['Product'])) {
                        foreach ($_FILES['Product']['name']['product_image'] as $key => $value) {
                            if (!empty($value)) {

                                $model_images = new ProductImages;
                                $model_images->product_id = $model->id;
                                if ($key == 0) {
                                    $model_images->general = 1;
                                }
                                $name2 = explode(".", $value);
                                $name_md5 = md5(time() . "$value") . '.' . $name2[1];
                                $model_images->image = $name_md5;

                                $uploads_dir = UPLOADS . 'image/products';
                                $tmp_name = $_FILES["Product"]["tmp_name"]['product_image'][$key];
                                if (move_uploaded_file($tmp_name, "$uploads_dir/$name_md5")) {
                                    $model_images->save();
                                }
                            }
                        }
                    }
                    if (isset($_POST['Product']['videos'])) {
                        foreach ($_POST['Product']['videos'] as $value) {
                            $model_videos = new ProductVideos;
                            $model_videos->product_id = $model->id;
                            $model_videos->video = $value;
                            $model_videos->save(false);
                        }
                    }
                    if (isset($_POST['Product']['filters'])) {
                        foreach ($_POST['Product']['filters'] as $value) {
                            $model_filter = new ProductHasFilter;
                            $model_filter->product_id = $model->id;
                            $model_filter->filter_id = $value;
                            $model_filter->save();
                        }
                    }

                    if (isset($_POST['Product']['specifications'])) {
                        foreach ($_POST['Product']['specifications'] as $key => $value) {
                            foreach ($value as $k => $val) {
                                $model_product_has_sp = new ProductHasSpecification();
                                $model_product_has_sp->product_id = $model->id;
                                $model_product_has_sp->specification_group_id = $key;
                                $model_product_has_sp->specification_item_id = $k;
                                $model_product_has_sp->specificatin_items_value_id = $val;
                                $model_product_has_sp->save(false);
                            }
                        }
                    }
                    if (isset($_POST['ProductsUrls']) && !empty($_POST['ProductsUrls'])) {
                        foreach ($_POST['ProductsUrls'] as $key => $value) {
                            if (!empty($value)) {
                                $price_ = '';
                                $site_price_list_tag_name = SitePriceList::model()->findByPk($key)->tag_name;
                                $html = file_get_contents($value);
                                header("Content-Type: text/html; charset=utf-8");
                                $doc = new DOMDocument();
                                $doc->strictErrorChecking = false;
                                $doc->recover = true;
                                $doc->encoding = 'utf-8';
                                libxml_use_internal_errors(true);
                                $doc->loadHTML(mb_convert_encoding("<html><body>" . $html . "</body></html>", 'HTML-ENTITIES', 'UTF-8'));


                                $xpath = new DOMXpath($doc);
                                /*[local-name()='author']/*[local-name()='first-name']*/
                                $elements = $xpath->query($site_price_list_tag_name);
                                if ($elements->length == 2) {
                                    $price_ = explode($elements->item(1)->nodeValue, $elements->item(0)->nodeValue)[0];
                                }
                                else {
                                    if (!is_null($elements)) {
                                        foreach ($elements as $element) {

                                            $nodes = $element->childNodes;
                                            foreach ($nodes as $node) {
                                                $price_ .= $node->nodeValue . "\n";
                                            }
                                        }
                                    }
                                }

                                $site_has_urls = new SitePriceListHasProduct();
                                $site_has_urls->site_price_list = $key;
                                $site_has_urls->product_id = $model->id;
                                $site_has_urls->url = $value;
                                $site_has_urls->price = trim($price_);
                                $site_has_urls->save();
                            }
                        }
                    }

                    if ($model->is_pc_build) {
                        $model_pc->attributes = $_POST['ProductPcBuild'];
                        $model_pc->product_id = $model->id;
                        $model_pc->save();
                        if ($model_pc->pc_type != 'other'  && $model_pc->pc_type != 'case') {
                            $save_me = array();
                            foreach ($_POST['pc']['val'] as $k => $val) {
                                $save_me[] = array(
                                    'product_pc_build_id' => $model_pc->id,
                                    'op_type'             => $_POST['pc']['type'][$k],
                                    'val'                 => $_POST['pc']['val'][$k],
                                    'count'               => isset($_POST['pc']['count']) ? ( isset($_POST['pc']['count'][$k]) ? $_POST['pc']['count'][$k] : 0) : 0,
                                );
                            }
                            if ($save_me) {
                                $builder = Yii::app()->db->schema->commandBuilder;
                                $command = $builder->createMultipleInsertCommand('product_pc_build_options', $save_me);
                                $command->execute();
                            }
                        }elseif($model_pc->pc_type == 'case'){
                            $save_me = array();
                            foreach ($_POST['pc']['has_case'] as $k => $val) {
                                if($val == '1'){
                                    $save_me[] = array(
                                        'product_pc_build_id' => $model_pc->id,
                                        'op_type'             => $_POST['pc']['type'][$k],
                                        'val'                 => $_POST['pc']['val'][$k],
                                        'count'               => 0,
                                    );
                                }
                            }
                            if ($save_me) {
                                $builder = Yii::app()->db->schema->commandBuilder;
                                $command = $builder->createMultipleInsertCommand('product_pc_build_options', $save_me);
                                $command->execute();
                            }
                        }
                    }

                    $this->redirect(array('admin'));
                }
            }

            $criteria = new CDbCriteria();
            $criteria->select = array('t.id');
            $criteria->condition = 't.category_id = :cat_id';
            $criteria->params = array('cat_id' => key($categories));
            $criteria->with = array(
                'specification' => array(
                    'select'    => array('specification.id'),
                    'condition' => 'specification.status = 1',
                    'order'     => 'specification.sort_order ASC, specification.id',
                    'with'      => array(
                        'specificationsGroupLabels' => array(
                            'select'    => array('specificationsGroupLabels.name'),
                            'condition' => 'specificationsGroupLabels.language_id = 1',
                        ),
                        'specificationsItems'       => array(
                            'with' => array(
                                'specificationsItemsLabels' => array(
                                    'condition' => 'specificationsItemsLabels.language_id = 1',
                                ),
                                'specificationsItemsValues'
                            )
                        )
                    )
                )
            );

            $specifications = CategoryHasSpecification::model()->findAll($criteria);
//		Helpers::pr($categories);

            $criteria = new CDbCriteria();
            $criteria->select = array('t.id');
            $criteria->condition = 't.status = 1';
            $criteria->order = 't.sort_order ASC, t.id';
            $criteria->with = array(
                'categoryHasFilters' => array(
                    'select'    => array('categoryHasFilters.id'),
                    'condition' => 'categoryHasFilters.category_id = :cat_id',
                    'params'    => array(':cat_id' => key($categories))
                ),
                'filterLabels'       => array(
                    'select'    => array('filterLabels.name'),
                    'condition' => 'filterLabels.language_id = 1',
                ),
                'filterItems'        => array(
                    'select' => array('filterItems.id'),
                    'order'  => 'filterItems.sort_order ASC, filterItems.id',
                    'with'   => array(
                        'filterItemLabels' => array(
                            'select'    => array('filterItemLabels.name'),
                            'condition' => 'filterItemLabels.language_id = 1',
                        )
                    )
                )
            );

            $model_filters = Filter::model()->findAll($criteria);

            $site_price_list = SitePriceList::model()->findAllByAttributes(array('status' => 1), array('order' => 'sort_order'));
            $this->render('create', array(
                'model'           => $model,
                'categories'      => $categories,
                'brands'          => $brands,
                'specifications'  => $specifications,
                'model_filters'   => $model_filters,
                'site_price_list' => $site_price_list,
                'model_pc'        => $model_pc,
            ));

        }

        public function actionGetSpecifications() {
            $criteria = new CDbCriteria();
            $criteria->select = array('t.id');
            $criteria->condition = 't.category_id = :cat_id';
            $criteria->params = array('cat_id' => $_POST['id']);
            $criteria->with = array(
                'specification' => array(
                    'select'    => array('specification.id'),
                    'condition' => 'specification.status = 1',
                    'with'      => array(
                        'specificationsGroupLabels' => array(
                            'select'    => array('specificationsGroupLabels.name'),
                            'condition' => 'specificationsGroupLabels.language_id = 1',
                        ),
                        'specificationsItems'       => array(
                            'with' => array(
                                'specificationsItemsLabels' => array(
                                    'condition' => 'specificationsItemsLabels.language_id = 1',
                                ),
                                'specificationsItemsValues'
                            )
                        )
                    )
                )
            );

            $specifications = CategoryHasSpecification::model()->findAll($criteria);
            if ($specifications) {
                echo CJSON::encode($this->convertModelToArray($specifications));
                die;
            }
            else {
                echo 'empty';
                die;
            }
        }

        public function convertModelToArray($models) {
            if (is_array($models)) {
                $arrayMode = TRUE;
            }
            else {
                $models = array($models);
                $arrayMode = FALSE;
            }
            $result = array();

            foreach ($models as $model) {
                if ($model) {
                    $attributes = $model->getAttributes();

                    $relations = array();
                    foreach ($model->relations() as $key => $related) {
                        if ($model->hasRelated($key)) {
                            $relations[$key] = $this->convertModelToArray($model->$key);
                        }
                    }
                    $all = array_merge($attributes, $relations);
                    $all = array_filter($all, function ($var) { return !is_null($var); });

                    if ($arrayMode) {
//					Helpers::pr($all, true);
                        array_push($result, $all);
                    }
                    else {
                        $result = $all;
                    }
                }
            }

            return $result;
        }

        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         *
         * @param integer $id the ID of the model to be updated
         */
        public function actionUpdate($id) {
            $model = $this->loadModel($id);
            $model_pc = $model->pc_build ? ProductPcBuild::model()->findByPk($model->pc_build->id) : new ProductPcBuild();

            $category_model = Category::model()->with('categoryLabels')->findAll();
            $categories = array();
            foreach ($category_model as $value) {
                $categories[$value->id] = $value->categoryLabels->name;
            }

            $brand_model = Brand::model()->with('brandLabels')->findAll();
            $brands = array();
            foreach ($brand_model as $value) {
                $brands[$value->id] = $value->brandLabels->name;
            }

            if (isset($_POST['Product'])) {
                $model->attributes = $_POST['Product'];
                $model->modify_date = date('Y-m-d h:i:s');
                if ($model->save()) {
                    ProductLabel::model()->deleteAllByAttributes(array('product_id' => $id));
                    foreach ($_POST['Product']['tab'] as $key => $value) {
                        $model_label = new ProductLabel();
                        $model_label->product_id = $id;
                        $model_label->language_id = $key;
                        $model_label->name = $value['name'];
                        $model_label->description = $value['description'];
                        $model_label->save();
                    }
                    if (isset($_FILES['Product'])) {
                        foreach ($_FILES['Product']['name']['product_image'] as $key => $value) {
                            if (!empty($value)) {

                                $model_images = new ProductImages;
                                $model_images->product_id = $model->id;
//							if($key == 0){
//								$model_images->general = 1;
//							}
                                $name2 = explode(".", $value);
                                $name_md5 = md5(time() . "$value") . '.' . $name2[1];
                                $model_images->image = $name_md5;

                                $uploads_dir = UPLOADS . 'image/products';
                                $tmp_name = $_FILES["Product"]["tmp_name"]['product_image'][$key];
                                if (move_uploaded_file($tmp_name, "$uploads_dir/$name_md5")) {
                                    $model_images->save();
                                }
                            }
                        }
                    }
                    if (isset($_POST['Product']['videos'])) {
                        ProductVideos::model()->deleteAllByAttributes(array('product_id' => $id));
                        foreach ($_POST['Product']['videos'] as $value) {
                            $model_videos = new ProductVideos;
                            $model_videos->product_id = $id;
                            $model_videos->video = $value;
                            $model_videos->save();
                        }
                    }
                    if (isset($_POST['Product']['filters'])) {
                        ProductHasFilter::model()->deleteAllByAttributes(array('product_id' => $id));
                        foreach ($_POST['Product']['filters'] as $value) {
                            $model_filter = new ProductHasFilter;
                            $model_filter->product_id = $id;
                            $model_filter->filter_id = $value;
                            $model_filter->save();
                        }
                    }
                    if (isset($_POST['Product']['specifications'])) {
                        ProductHasSpecification::model()->deleteAllByAttributes(array('product_id' => $id));
                        foreach ($_POST['Product']['specifications'] as $key => $value) {
                            foreach ($value as $k => $val) {
                                $model_product_has_sp = new ProductHasSpecification();
                                $model_product_has_sp->product_id = $model->id;
                                $model_product_has_sp->specification_group_id = $key;
                                $model_product_has_sp->specification_item_id = $k;
                                $model_product_has_sp->specificatin_items_value_id = $val;
                                $model_product_has_sp->save(false);
                            }
                        }
                    }

                    if (isset($_POST['ProductsUrls']) && !empty($_POST['ProductsUrls'])) {
                        SitePriceListHasProduct::model()->deleteAllByAttributes(array('product_id' => $model->id));
                        foreach ($_POST['ProductsUrls'] as $key => $value) {
                            if (!empty($value)) {
                                $price_ = '';
                                $site_price_list_tag_name = SitePriceList::model()->findByPk($key)->tag_name;
                                $html = file_get_contents($value);
                                header("Content-Type: text/html; charset=utf-8");
                                $doc = new DOMDocument();
                                $doc->strictErrorChecking = false;
                                $doc->recover = true;
                                $doc->encoding = 'utf-8';
                                libxml_use_internal_errors(true);
                                $doc->loadHTML(mb_convert_encoding("<html><body>" . $html . "</body></html>", 'HTML-ENTITIES', 'UTF-8'));


                                $xpath = new DOMXpath($doc);
                                /*[local-name()='author']/*[local-name()='first-name']*/
                                $elements = $xpath->query($site_price_list_tag_name);
                                $price_ = '';
                                if ($elements->length == 2) {
                                    $price_ = explode($elements->item(1)->nodeValue, $elements->item(0)->nodeValue)[0];
                                }
                                else {


                                    if (!is_null($elements)) {
                                        foreach ($elements as $element) {

                                            $nodes = $element->childNodes;
                                            foreach ($nodes as $node) {
                                                $price_ .= $node->nodeValue . "\n";
                                            }
                                        }
                                    }
                                }

                                $site_has_urls = new SitePriceListHasProduct();
                                $site_has_urls->site_price_list = $key;
                                $site_has_urls->product_id = $model->id;
                                $site_has_urls->url = $value;
                                $site_has_urls->price = trim($price_);
                                $site_has_urls->save();
                            }
                        }
                    }

                    if ($model->is_pc_build) {
                        $model_pc->attributes = $_POST['ProductPcBuild'];
                        $model_pc->product_id = $model->id;
                        $model_pc->save();
                        ProductPcBuildOptions::model()->deleteAllByAttributes(array('product_pc_build_id'=>$model_pc->id));
                        if ($model_pc->pc_type != 'other'  && $model_pc->pc_type != 'case') {
                            $save_me = array();
                            foreach ($_POST['pc']['val'] as $k => $val) {
                                $save_me[] = array(
                                    'product_pc_build_id' => $model_pc->id,
                                    'op_type'             => $_POST['pc']['type'][$k],
                                    'val'                 => $_POST['pc']['val'][$k],
                                    'count'               => isset($_POST['pc']['count']) ? (isset($_POST['pc']['count'][$k]) ? $_POST['pc']['count'][$k] : 0) : 0,
                                );
                            }
                            if ($save_me) {
                                $builder = Yii::app()->db->schema->commandBuilder;
                                $command = $builder->createMultipleInsertCommand('product_pc_build_options', $save_me);
                                $command->execute();
                            }
                        }elseif($model_pc->pc_type == 'case'){
                            $save_me = array();
                            foreach ($_POST['pc']['has_case'] as $k => $val) {
                                if($val == '1'){
                                    $save_me[] = array(
                                        'product_pc_build_id' => $model_pc->id,
                                        'op_type'             => $_POST['pc']['type'][$k],
                                        'val'                 => $_POST['pc']['val'][$k],
                                        'count'               => 0,
                                    );
                                }
                            }
                            if ($save_me) {
                                $builder = Yii::app()->db->schema->commandBuilder;
                                $command = $builder->createMultipleInsertCommand('product_pc_build_options', $save_me);
                                $command->execute();
                            }
                        }
                    }else{
                        ProductPcBuild::model()->deleteAllByAttributes(array('product_id'=>$model->id));
                    }

                    $this->redirect(array('admin'));
                }
            }

            $criteria = new CDbCriteria();
            $criteria->select = array('t.id');
            $criteria->condition = 't.category_id = :cat_id';
            $criteria->params = array('cat_id' => $model->category_id);
            $criteria->with = array(
                'specification' => array(
                    'select'    => array('specification.id'),
                    'condition' => 'specification.status = 1',
                    'order'     => 'specification.sort_order ASC, specification.id',
                    'with'      => array(
                        'specificationsGroupLabels' => array(
                            'select'    => array('specificationsGroupLabels.name'),
                            'condition' => 'specificationsGroupLabels.language_id = 1',
                        ),
                        'specificationsItems'       => array(
                            'with' => array(
                                'specificationsItemsLabels' => array(
                                    'condition' => 'specificationsItemsLabels.language_id = 1',
                                ),
                                'specificationsItemsValues'
                            )
                        )
                    )
                )
            );

            $specifications = CategoryHasSpecification::model()->findAll($criteria);

            $chosen_specifications_model = ProductHasSpecification::model()->findAllByAttributes(array('product_id' => $id));

            $chosen_specifications = array();
            foreach ($chosen_specifications_model as $value) {
                $chosen_specifications[$value->specification_item_id] = $value->specificatin_items_value_id;
            }


            $criteria = new CDbCriteria();
            $criteria->select = array('t.id');
            $criteria->condition = 't.status = 1';
            $criteria->order = 't.sort_order ASC, t.id';
            $criteria->with = array(
                'categoryHasFilters' => array(
                    'select'    => array('categoryHasFilters.id'),
                    'condition' => 'categoryHasFilters.category_id = :cat_id',
                    'params'    => array(':cat_id' => $model->category_id)
                ),
                'filterLabels'       => array(
                    'select'    => array('filterLabels.name'),
                    'condition' => 'filterLabels.language_id = 1',
                ),
                'filterItems'        => array(
                    'select' => array('filterItems.id'),
                    'order'  => 'filterItems.sort_order ASC, filterItems.id',
                    'with'   => array(
                        'filterItemLabels' => array(
                            'select'    => array('filterItemLabels.name'),
                            'condition' => 'filterItemLabels.language_id = 1',
                        )
                    )
                )
            );

            $model_filters = Filter::model()->findAll($criteria);


            $chosen_filters_model = ProductHasFilter::model()->findAllByAttributes(array('product_id' => $id));

            $chosen_filters = array();
            foreach ($chosen_filters_model as $value) {
                $chosen_filters[] = $value->filter_id;
            }


            $criteria = new CDbCriteria();
            $criteria->condition = "status = 1";
            $criteria->order = "sort_order ASC";
            $criteria->with = array(
                'sitePriceListHasProducts' => array(),
                'together'                 => false
            );
            $site_price_list = SitePriceList::model()->findAll($criteria);


            $this->render('update', array(
                'model'                 => $model,
                'categories'            => $categories,
                'brands'                => $brands,
                'specifications'        => $specifications,
                'chosen_specifications' => $chosen_specifications,
                'model_filters'         => $model_filters,
                'chosen_filters'        => $chosen_filters,
                'site_price_list'       => $site_price_list,
                'model_pc'              => $model_pc,
            ));
        }

        public function actionMakeGeneralImage() {
            if (!Yii::app()->request->isAjaxRequest) {
                $this->redirect($this->createUrl('/'));
            }

            if (isset($_GET['id'], $_GET['pid'])) {
                $imageId = intval($_GET['id']);
                $imageName = ProductImages::model()->findByPk($imageId);
                if ($imageName) {
                    ProductImages::model()->updateAll(array('general' => 0), 'product_id=' . intval($_GET['pid']));
                    $imageName->general = 1;
                    if ($imageName->update()) {
                        echo "ok";
                        die;
                    }
                }
            }
            echo "nok";
            die;
        }

        public function actionDeleteImage() {
            if (!Yii::app()->request->isAjaxRequest) {
                $this->redirect($this->createUrl('/'));
            }


            if (isset($_GET['id'], $_GET['pid'])) {
                $imageId = intval($_GET['id']);
                $imageName = ProductImages::model()->findByPk($imageId);
                if ($imageName) {
                    $imageDir = UPLOADS . "image/products/" . $imageName->image;

                    @unlink($imageDir);
                    if (ProductImages::model()->deleteByPk($imageId)) {
                        echo "ok";
                        die;
                    }
                }
            }
            echo "nok";
            die;
        }

        public function actionDeleteVideo() {
            if (!Yii::app()->request->isAjaxRequest) {
                $this->redirect($this->createUrl('/'));
            }

            if (isset($_GET['id'], $_GET['pid'])) {
                if (ProductVideos::model()->deleteByPk(intval($_GET['id']))) {
                    echo 'ok';
                }
                die;
            }
            echo "nok";
            die;
        }

        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         *
         * @param integer $id the ID of the model to be deleted
         */
        public function actionDelete($id) {
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        }

        /**
         * Lists all models.
         */
        public function actionIndex() {
            $dataProvider = new CActiveDataProvider('Product');
            $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
        }

        /**
         * Manages all models.
         */
        public function actionAdmin() {
            $model = new Product('search');
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['Product'])) {
                $model->attributes = $_GET['Product'];
            }

            $this->render('admin', array(
                'model' => $model,
            ));
        }

        /**
         * Returns the data model based on the primary key given in the GET variable.
         * If the data model is not found, an HTTP exception will be raised.
         *
         * @param integer $id the ID of the model to be loaded
         *
         * @return Product the loaded model
         * @throws CHttpException
         */
        public function loadModel($id) {
            $model = Product::model()->findByPk($id);
            if ($model === null) {
                throw new CHttpException(404, 'The requested page does not exist.');
            }

            return $model;
        }

        /**
         * Performs the AJAX validation.
         *
         * @param Product $model the model to be validated
         */
        protected function performAjaxValidation($model) {
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'product-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
        }

        public function actionUpload() {
            $this->layout = false;

            $this->render('upload', array('_GET' => $_GET));
        }

        public function actionBrowse() {
            $this->layout = false;

            $this->render('browse', array('_GET' => $_GET));
        }

        public function actionPcItem($type) {
            $this->layout = false;
            $this->render('pc_build/' . $type, array(
                'product' => null,
                'type'    => $type
            ));
        }
    }
