<?php

class StaticPagesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	public $defaultAction = "admin";

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new StaticPages;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['StaticPages']))
		{
//			Helpers::pr($_POST);
			$model->attributes=$_POST['StaticPages'];
			if($model->save()) {
				foreach($_POST['StaticPages']['tab'] as $key => $value) {
					$model_label = new StaticPagesLabel;
					$model_label->static_page_id = $model->id;
					$model_label->language_id = $key;
					$model_label->title = $value['title'];
					$model_label->description = $value['description'];
					$model_label->save();
				}

				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['StaticPages']))
		{
			$model->attributes=$_POST['StaticPages'];
			$model->modify_date = date('Y-m-d h:i:s');
			if($model->save()){
				StaticPagesLabel::model()->deleteAllByAttributes(array('static_page_id' => $id));
				foreach($_POST['StaticPages']['tab'] as $key => $value) {
					$model_label = new StaticPagesLabel;
					$model_label->static_page_id = $model->id;
					$model_label->language_id = $key;
					$model_label->title = $value['title'];
					$model_label->description = $value['description'];
					$model_label->save();
				}
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('StaticPages');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new StaticPages('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['StaticPages']))
			$model->attributes=$_GET['StaticPages'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return StaticPages the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=StaticPages::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param StaticPages $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='static-pages-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
