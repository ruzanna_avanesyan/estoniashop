<?php

/**
 * This is the model class for table "specifications_items".
 *
 * The followings are the available columns in table 'specifications_items':
 * @property string $id
 * @property string $specification_id
 *
 * The followings are the available model relations:
 * @property SpecificationsGroup $specification
 * @property SpecificationsItemsLabel[] $specificationsItemsLabels
 * @property SpecificationsItemsValues[] $specificationsItemsValues
 */
class SpecificationsItems extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'specifications_items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('specification_id', 'required'),
			array('specification_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, specification_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'specification' => array(self::BELONGS_TO, 'SpecificationsGroup', 'specification_id'),
			'specificationsItemsLabels' => array(self::HAS_ONE, 'SpecificationsItemsLabel', 'specifications_item_id'),
			'specificationsItemsValues' => array(self::HAS_MANY, 'SpecificationsItemsValues', 'specifications_item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'specification_id' => 'Specification',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('specification_id',$this->specification_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SpecificationsItems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
