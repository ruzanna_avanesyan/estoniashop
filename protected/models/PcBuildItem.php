<?php

    /**
     * This is the model class for table "pc_build_item".
     *
     * The followings are the available columns in table 'pc_build_item':
     *
     * @property string             $id
     * @property string             $user_id
     * @property string             $title
     * @property string             $description
     * @property double             $total
     * @property double             $waltage
     *
     * The followings are the available model relations:
     * @property Users              $user
     * @property PcItemHasProduct[] $pcItemHasProducts
     * @property PcItemImages[]     $pcItemImages
     * @property PcComments[]       $comments
     */
    class PcBuildItem extends CActiveRecord {
        public $images;

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return 'pc_build_item';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('user_id, title,waltage, description, total,images', 'required'),
                array('total', 'numerical'),
                array('user_id', 'length', 'max' => 11),
                array('title', 'length', 'max' => 255),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array('id, user_id, title, description, total', 'safe', 'on' => 'search'),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'user'              => array(self::BELONGS_TO, 'Users', 'user_id'),
                'pcItemHasProducts' => array(self::HAS_MANY, 'PcItemHasProduct', 'pc_item_id'),
                'pcItemImages'      => array(self::HAS_MANY, 'PcItemImages', 'pc_item_id'),
                'comments'          => array(self::HAS_MANY, 'PcComments', 'pc_build_item_id'),
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'          => 'ID',
                'user_id'     => 'User',
                'title'       => 'Title',
                'description' => 'Description',
                'total'       => 'Total',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id, true);
            $criteria->compare('user_id', $this->user_id, true);
            $criteria->compare('title', $this->title, true);
            $criteria->compare('description', $this->description, true);
            $criteria->compare('total', $this->total);

            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return PcBuildItem the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }
    }
