<?php

/**
 * This is the model class for table "product_comment".
 *
 * The followings are the available columns in table 'product_comment':
 * @property string $id
 * @property string $product_id
 * @property integer $status
 * @property string $created_date
 * @property string $name
 * @property string $email
 * @property string $content
 * @property string $plus
 * @property string $minus
 * @property integer $rating
 * @property integer $have
 *
 * The followings are the available model relations:
 * @property Product $product
 * @property ProductCommentNegPosReviews[] $productCommentNegPosReviews
 */
class ProductComment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product_comment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, name, email', 'required'),
			array('status, rating, have', 'numerical', 'integerOnly'=>true),
			array('product_id', 'length', 'max'=>11),
			array('name, email', 'length', 'max'=>255),
			array('content, plus, minus', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, product_id, status, created_date, name, email, content, plus, minus, rating, have', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
			'productCommentNegPosReviews' => array(self::HAS_MANY, 'ProductCommentNegPosReviews', 'product_comment_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_id' => 'Product',
			'status' => 'Status',
			'created_date' => 'Created Date',
			'name' => 'Name',
			'email' => 'Email',
			'content' => 'Content',
			'plus' => 'Plus',
			'minus' => 'Minus',
			'rating' => 'Rating',
			'have' => 'Have',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('product_id',$this->product_id,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('plus',$this->plus,true);
		$criteria->compare('minus',$this->minus,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('have',$this->have);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductComment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
