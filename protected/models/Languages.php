<?php

/**
 * This is the model class for table "languages".
 *
 * The followings are the available columns in table 'languages':
 * @property string $id
 * @property integer $sort_order
 * @property integer $status
 * @property string $created_date
 * @property string $modify_date
 * @property string $iso
 * @property string $name
 * @property string $image
 *
 * The followings are the available model relations:
 * @property CategoryLabel[] $categoryLabels
 * @property FilterItemLabel[] $filterItemLabels
 * @property FilterLabel[] $filterLabels
 * @property ProductLabel[] $productLabels
 * @property SpecificationsLabel[] $specificationsLabels
 * @property StaticPageLabel[] $staticPageLabels
 */
class Languages extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'languages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('iso', 'required'),
			array('sort_order, status', 'numerical', 'integerOnly'=>true),
			array('iso', 'length', 'max'=>8),
			array('name', 'length', 'max'=>32),
			array('image', 'length', 'max'=>255),
			array('modify_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sort_order, status, created_date, modify_date, iso, name, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categoryLabels' => array(self::HAS_MANY, 'CategoryLabel', 'language_id'),
			'filterItemLabels' => array(self::HAS_MANY, 'FilterItemLabel', 'language_id'),
			'filterLabels' => array(self::HAS_MANY, 'FilterLabel', 'language_id'),
			'productLabels' => array(self::HAS_MANY, 'ProductLabel', 'language_id'),
			'specificationsLabels' => array(self::HAS_MANY, 'SpecificationsLabel', 'language_id'),
			'staticPageLabels' => array(self::HAS_MANY, 'StaticPageLabel', 'language_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sort_order' => 'Sort Order',
			'status' => 'Status',
			'created_date' => 'Created Date',
			'modify_date' => 'Modify Date',
			'iso' => 'Iso',
			'name' => 'Name',
			'image' => 'Image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('sort_order',$this->sort_order);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('modify_date',$this->modify_date,true);
		$criteria->compare('iso',$this->iso,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('image',$this->image,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Languages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
