<?php

    /**
     * This is the model class for table "category".
     *
     * The followings are the available columns in table 'category':
     *
     * @property string                     $id
     * @property integer                    $sort_order
     * @property integer                    $status
     * @property string                     $created_date
     * @property string                     $modify_date
     *
     * The followings are the available model relations:
     * @property CategoryHasFilter[]        $categoryHasFilters
     * @property CategoryHasSpecification[] $categoryHasSpecifications
     * @property CategoryLabel              $categoryLabels
     * @property Product[]                  $products
     */
    class Category extends CActiveRecord {
        public $name;

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return 'category';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('sort_order, status', 'numerical', 'integerOnly' => true),
                array('modify_date', 'safe'),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array('id, sort_order, status, created_date, modify_date, name', 'safe', 'on' => 'search'),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'categoryHasFilters'        => array(self::HAS_MANY, 'CategoryHasFilter', 'category_id'),
                'categoryHasSpecifications' => array(self::HAS_MANY, 'CategoryHasSpecification', 'category_id'),
                'categoryLabels'            => array(self::HAS_ONE, 'CategoryLabel', 'category_id'),
                'products'                  => array(self::HAS_MANY, 'Product', 'category_id'),
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'           => 'ID',
                'sort_order'   => 'Sort Order',
                'status'       => 'Status',
                'created_date' => 'Created Date',
                'modify_date'  => 'Modify Date',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('t.id', $this->id, true);
            $criteria->compare('t.sort_order', $this->sort_order);
            $criteria->compare('t.status', $this->status);
            $criteria->compare('t.created_date', $this->created_date, true);
            $criteria->compare('t.modify_date', $this->modify_date, true);
            $criteria->with = array('categoryLabels');
            $criteria->compare('categoryLabels.name', $this->name, true);

            return new CActiveDataProvider($this, array(
                'criteria'   => $criteria,
                'pagination' => array('pageSize' => 100),
                'sort'       => array(
                    'attributes'   => array(
                        'name' => array(
                            'asc'  => 'filterLabels.name',
                            'desc' => 'filterLabels.name DESC',
                        ),
                        '*',
                    ),
                    'defaultOrder' => 't.id DESC',
                ),
            ));
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return Category the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }
    }
