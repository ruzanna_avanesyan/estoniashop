<?php

    /**
     * This is the model class for table "users".
     *
     * The followings are the available columns in table 'users':
     *
     * @property string            $id
     * @property string            $email
     * @property string            $username
     * @property string            $password
     * @property string            $created
     * @property string            $reset_key
     *
     * The followings are the available model relations:
     * @property PcBuildItem[]     $pcBuildItems
     * @property PcCommentAnswer[] $pcCommentAnswers
     * @property PcComments[]      $pcComments
     */
    class Users extends CActiveRecord {
        public $confirm_password;
        public $current_password;

        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return 'users';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('email, username, password, confirm_password', 'required', 'on' => 'reg'),
                array('username, password', 'required', 'on' => 'login'),
                array('password,confirm_password', 'required', 'on' => 'reset'),
                array('email, current_password', 'required', 'on' => 'change_details'),
                array('email', 'required', 'on' => 'forgot'),
                array('current_password,confirm_password,password', 'required', 'on' => 'change_pass'),
                array('email', 'email'),
                array('reset_key', 'safe'),
                array('email,username', 'unique', 'on' => 'reg'),
                array('password', 'length', 'min' => 6, 'on' => 'reg,reset,change_pass'),
                array('email, username, password', 'length', 'max' => 255, 'on' => 'reg'),
                array('confirm_password', 'compare', 'compareAttribute' => 'password', 'operator' => '=', 'message' => 'Passwords Mismatch', 'on' => 'reg,change_pass,reset'),
                array('id, email, username, password, created', 'safe', 'on' => 'search'),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'pcBuildItems'     => array(self::HAS_MANY, 'PcBuildItem', 'user_id'),
                'pcCommentAnswers' => array(self::HAS_MANY, 'PcCommentAnswer', 'user_id'),
                'pcComments'       => array(self::HAS_MANY, 'PcComments', 'user_id'),
            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'               => 'ID',
                'email'            => Yii::app()->controller->translation['email'],
                'username'         => Yii::app()->controller->translation['username'],
                'password'         => Yii::app()->controller->translation['password'],
                'confirm_password' => Yii::app()->controller->translation['verify_password'],
                'current_password' => Yii::app()->controller->translation['current_password'],
                'created'          => 'Created',
            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria = new CDbCriteria;

            $criteria->compare('id', $this->id, true);
            $criteria->compare('email', $this->email, true);
            $criteria->compare('username', $this->username, true);
            $criteria->compare('password', $this->password, true);
            $criteria->compare('created', $this->created, true);

            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }

        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return Users the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }
    }
