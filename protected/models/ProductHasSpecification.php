<?php

/**
 * This is the model class for table "product_has_specification".
 *
 * The followings are the available columns in table 'product_has_specification':
 * @property string $id
 * @property string $product_id
 * @property string $specification_group_id
 * @property string $specification_item_id
 * @property string $specificatin_items_value_id
 *
 * The followings are the available model relations:
 * @property SpecificationsItemsValues $specificatinItemsValue
 * @property Product $product
 * @property SpecificationsGroup $specificationGroup
 * @property SpecificationsItems $specificationItem
 */
class ProductHasSpecification extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product_has_specification';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, specification_group_id, specification_item_id, specificatin_items_value_id', 'required'),
			array('product_id, specification_group_id, specification_item_id, specificatin_items_value_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, product_id, specification_group_id, specification_item_id, specificatin_items_value_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'specificatinItemsValue' => array(self::BELONGS_TO, 'SpecificationsItemsValues', 'specificatin_items_value_id'),
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
			'specificationGroup' => array(self::BELONGS_TO, 'SpecificationsGroup', 'specification_group_id'),
			'specificationItem' => array(self::BELONGS_TO, 'SpecificationsItems', 'specification_item_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_id' => 'Product',
			'specification_group_id' => 'Specification Group',
			'specification_item_id' => 'Specification Item',
			'specificatin_items_value_id' => 'Specificatin Items Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('product_id',$this->product_id,true);
		$criteria->compare('specification_group_id',$this->specification_group_id,true);
		$criteria->compare('specification_item_id',$this->specification_item_id,true);
		$criteria->compare('specificatin_items_value_id',$this->specificatin_items_value_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductHasSpecification the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
