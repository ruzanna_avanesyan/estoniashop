<?php

/**
 * This is the model class for table "product_pc_build_options".
 *
 * The followings are the available columns in table 'product_pc_build_options':
 * @property string $id
 * @property string $product_pc_build_id
 * @property string $op_type
 * @property string $val
 * @property integer $count
 *
 * The followings are the available model relations:
 * @property ProductPcBuild $productPcBuild
 */
class ProductPcBuildOptions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product_pc_build_options';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_pc_build_id, op_type, val, count', 'required'),
			array('count', 'numerical', 'integerOnly'=>true),
			array('product_pc_build_id', 'length', 'max'=>11),
			array('op_type', 'length', 'max'=>3),
			array('val', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, product_pc_build_id, op_type, val, count', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'productPcBuild' => array(self::BELONGS_TO, 'ProductPcBuild', 'product_pc_build_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_pc_build_id' => 'Product Pc Build',
			'op_type' => 'Op Type',
			'val' => 'Val',
			'count' => 'Count',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('product_pc_build_id',$this->product_pc_build_id,true);
		$criteria->compare('op_type',$this->op_type,true);
		$criteria->compare('val',$this->val,true);
		$criteria->compare('count',$this->count);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductPcBuildOptions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
