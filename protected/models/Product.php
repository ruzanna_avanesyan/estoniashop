<?php

    /**
     * This is the model class for table "product".
     *
     * The followings are the available columns in table 'product':
     *
     * @property string                    $id
     * @property string                    $category_id
     * @property string                    $brand_id
     * @property integer                   $sort_order
     * @property integer                   $status
     * @property string                    $created_date
     * @property string                    $modify_date
     * @property integer                   $reviews
     * @property string                    $date
     * @property string                    $is_pc_build
     * @property string                    $price_url
     * @property string                    $model
     * @property string                    $site_price_list_id
     *
     * The followings are the available model relations:
     * @property Category                  $category
     * @property Brand                     $brand
     * @property ProductComment[]          $productComments
     * @property ProductHasFilter[]        $productHasFilters
     * @property ProductHasSpecification[] $productHasSpecifications
     * @property ProductHasSpecification[] $productHasSpecificationsLimit
     * @property ProductImages[]           $productImages
     * @property ProductLabel[]            $productLabels
     * @property ProductVideos[]           $productVideoses
     * @property SitePriceListHasProduct   $price_item
     */
    class Product extends CActiveRecord {
        public $product_name;
        public $price;
        public $filter_id;
        public $price_from;
        public $price_to;
//	public $product_name;
        /**
         * @return string the associated database table name
         */
        public function tableName() {
            return 'product';
        }

        public function getPc_case_type() {
            $type = null;
            if ($this->pc_build) {
                $res = ProductPcBuildOptions::model()->findByAttributes(array('product_pc_build_id' => $this->pc_build->id, 'op_type' => 'case'));
                $type = $res ? $res->val : null;
            }

            return $type;
        }

        public function getPc_power() {
            $type = null;
            if ($this->pc_build) {
                $res = ProductPcBuildOptions::model()->findByAttributes(array('product_pc_build_id' => $this->pc_build->id, 'op_type' => 'pw'));
                $type = $res ? $res->val : null;
            }

            return $type;
        }

        public function getPc_model() {
            $type = null;
            if ($this->pc_build) {
                $res = ProductPcBuildOptions::model()->findByAttributes(array('product_pc_build_id' => $this->pc_build->id, 'op_type' => 'model'));
                $type = $res ? $res->val : null;
            }

            return $type;
        }

        public function getPc_mhz() {
            $type = null;
            if ($this->pc_build) {
                $res = ProductPcBuildOptions::model()->findByAttributes(array('product_pc_build_id' => $this->pc_build->id, 'op_type' => 'mhz'));
                $type = $res ? $res->val : null;
            }

            return $type;
        }

        public function getPc_pci() {
            $type = null;
            if ($this->pc_build) {
                $res = ProductPcBuildOptions::model()->findByAttributes(array('product_pc_build_id' => $this->pc_build->id, 'op_type' => 'pci'));
                $type = $res ? $res->val : null;
            }

            return $type;
        }

        public function getPc_cpu($count = false) {
            $type = null;
            if ($this->pc_build) {
                $res = ProductPcBuildOptions::model()->findByAttributes(array('product_pc_build_id' => $this->pc_build->id, 'op_type' => 'cpu'));
                if ($count && $res) {
                    $type = $res;
                }
                else {
                    $type = $res ? $res->val : null;
                }

            }

            return $type;
        }

        public function getPc_memory($count = false) {
            $type = null;
            if ($this->pc_build) {
                $res = ProductPcBuildOptions::model()->findByAttributes(array('product_pc_build_id' => $this->pc_build->id, 'op_type' => 'memory'));
                if ($count && $res) {
                    $type = $res;
                }
                else {
                    $type = $res ? $res->val : null;
                }
            }

            return $type;
        }

        public function getPc_multi() {
            $type = null;
            if ($this->pc_build) {
                $res = ProductPcBuildOptions::model()->findByAttributes(array('product_pc_build_id' => $this->pc_build->id, 'op_type' => 'multi'));
                $type = $res ? $res->val : null;
            }

            return $type;
        }

        public function getPc_storage($count = false) {
            $type = null;
            if ($this->pc_build) {
                $res = ProductPcBuildOptions::model()->findByAttributes(array('product_pc_build_id' => $this->pc_build->id, 'op_type' => 'storage'));
                if ($count && $res) {
                    $type = $res;
                }
                else {
                    $type = $res ? $res->val : null;
                }
            }

            return $type;
        }

        public function getPciCount($val) {
            $type = 0;
            if ($this->pc_build) {
                $res = ProductPcBuildOptions::model()->findByAttributes(array('product_pc_build_id' => $this->pc_build->id, 'op_type' => 'pci', 'val' => $val));
                $type = $res ? $res->count : 0;
            }

            return $type;
        }

        public function getCaseByVal($val) {
            $type = 0;
            if ($this->pc_build) {
                $res = ProductPcBuildOptions::model()->findAllByAttributes(array('product_pc_build_id' => $this->pc_build->id, 'op_type' => 'case', 'val' => $val));
                $type = $res ? 1 : 0;
            }

            return $type;
        }

        public function getBuild_type() {
            return $this->pc_build ? $this->pc_build->pc_type : 'other';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {
            // NOTE: you should only define rules for those attributes that
            // will receive user inputs.
            return array(
                array('category_id, brand_id, created_date, model, price_url, site_price_list_id', 'required'),
                array('sort_order, status, reviews', 'numerical', 'integerOnly' => true),
                array('category_id, brand_id,site_price_list_id,is_pc_build', 'length', 'max' => 11),
                array('date', 'length', 'max' => 4),
                array('price_url, model', 'length', 'max' => 255),
                array('modify_date,filter_id,price_from,price_to', 'safe'),
                // The following rule is used by search().
                // @todo Please remove those attributes that should not be searched.
                array('id, category_id, brand_id, sort_order, status, created_date, modify_date, reviews, date, price_url, model,site_price_list_id', 'safe', 'on' => 'search'),
            );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'category'                      => array(self::BELONGS_TO, 'Category', 'category_id'),
                'brand'                         => array(self::BELONGS_TO, 'Brand', 'brand_id'),
                'productComments'               => array(self::HAS_MANY, 'ProductComment', 'product_id'),
                'productHasFilters'             => array(self::HAS_MANY, 'ProductHasFilter', 'product_id'),
                'productHasSpecifications'      => array(self::HAS_MANY, 'ProductHasSpecification', 'product_id'),
                'productHasSpecificationsLimit' => array(self::HAS_MANY, 'ProductHasSpecification', 'product_id', 'limit' => 10),
                'productImages'                 => array(self::HAS_MANY, 'ProductImages', 'product_id'),
                'productLabels'                 => array(self::HAS_ONE, 'ProductLabel', 'product_id'),
                'pc_build'                      => array(self::HAS_ONE, 'ProductPcBuild', 'product_id'),
                'price_item'                    => array(self::HAS_ONE, 'SitePriceListHasProduct', 'product_id'),
                'productVideos'                 => array(self::HAS_MANY, 'ProductVideos', 'product_id'),
                'sitePriceListHasProducts'      => array(self::HAS_MANY, 'SitePriceListHasProduct', 'product_id'),
                'sitePriceList'                 => array(self::BELONGS_TO, 'SitePriceList', 'site_price_list_id'),


            );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
            return array(
                'id'                 => 'ID',
                'category_id'        => 'Category',
                'brand_id'           => 'Brand',
                'sort_order'         => 'Sort Order',
                'status'             => 'Status',
                'created_date'       => 'Created Date',
                'modify_date'        => 'Modify Date',
                'reviews'            => 'Reviews',
                'date'               => 'Date',
                'price_url'          => 'Price Url',
                'model'              => 'Model',
                'site_price_list_id' => 'Site Price List',
                'is_pc_build'        => 'Use in PC Build',

            );
        }

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         *
         * Typical usecase:
         * - Initialize the model fields with values from filter form.
         * - Execute this method to get CActiveDataProvider instance which will filter
         * models according to data in model fields.
         * - Pass data provider to CGridView, CListView or any similar widget.
         *
         * @return CActiveDataProvider the data provider that can return the models
         * based on the search/filter conditions.
         */
        public function search() {
            // @todo Please modify the following code to remove attributes that should not be searched.

            $criteria = new CDbCriteria;
            $criteria->compare('t.id', $this->id, true);
            $criteria->compare('t.category_id', $this->category_id, true);
            $criteria->compare('t.brand_id', $this->brand_id, true);
            $criteria->compare('t.sort_order', $this->sort_order);
            $criteria->compare('t.status', $this->status);
            $criteria->compare('t.created_date', $this->created_date, true);
            $criteria->compare('t.modify_date', $this->modify_date, true);
            $criteria->compare('t.reviews', $this->reviews);
            $criteria->compare('t.date', $this->date, true);
            $criteria->compare('t.site_price_list_id', $this->site_price_list_id, true);


            $criteria->with = array('productLabels');

            $criteria->compare('productLabels.name', $this->product_name, true);

            return new CActiveDataProvider($this, array(
                'criteria'   => $criteria,
                'sort'       => array(
                    'attributes'   => array(
                        'name' => array(
                            'asc'  => 'productLabels.name',
                            'desc' => 'productLabels.name DESC',
                        ),
                        //					'*',
                    ),
                    'defaultOrder' => 't.id DESC',
                ),
                'pagination' => array(
                    'pageSize' => 100,
                ),
            ));
        }


        public function suggestAddCompareItem($keyword, $limit = 20) {
            $criteria = new CDbCriteria();
            $criteria->select = array('t.id');
            $criteria->condition = 't.category_id = :cat_id AND status = 1';
            if (isset(Yii::app()->request->cookies['compare_products'])) {
                $criteria->condition .= ' AND t.id not IN (' . Yii::app()->request->cookies['compare_products']->value . ')';
            }
            $criteria->params = array(':cat_id' => Yii::app()->request->cookies['category_id']->value);
            $criteria->limit = $limit;
            $criteria->with = array(
                'productLabels' => array(
                    'select'    => 'productLabels.name',
                    'condition' => 'productLabels.name LIKE :keyword',
                    'params'    => array(':keyword' => "%$keyword%"),
                    'order'     => 'productLabels.name',
                ),
                'productImages' => array(
                    'select'    => array('productImages.image'),
                    'condition' => 'productImages.general = 1'
                )
            );
            $models = $this->findAll($criteria);

            $suggest = array();
            foreach ($models as $model) {
                $suggest[] = array(
                    'label' => $model->productImages[0]->image,
                    'value' => $model->productLabels->name,
                    'id'    => $model->id,       // return values from autocomplete
                );
            }

            return $suggest;
        }


        /**
         * Returns the static model of the specified AR class.
         * Please note that you should have this exact method in all your CActiveRecord descendants!
         *
         * @param string $className active record class name.
         *
         * @return Product the static model class
         */
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }
    }
