<?php

    class SiteController extends Controller {
        public $layout = 'column1';

        /**
         * Declares class-based actions.
         */
        public function actions() {
            return array(
                // captcha action renders the CAPTCHA image displayed on the contact page
                'captcha' => array(
                    'class'     => 'CCaptchaAction',
                    'backColor' => 0xFFFFFF,
                ),
                // page action renders "static" pages stored under 'protected/views/site/pages'
                // They can be accessed via: index.php?r=site/page&view=FileName
                'page'    => array(
                    'class' => 'CViewAction',
                ),
            );
        }

        public function actionIndex() {
            $this->redirect(Yii::app()->createUrl('product/category', array('path' => 'All')));
        }

        /**
         * This is the action to handle external exceptions.
         */
        public function actionError() {
            $this->layout = false;
            $error = Yii::app()->errorHandler->error;
            //TODO REMOVE BEFORE PRODUTION
            if (strpos($error['message'], 'index:') !== false) {
                $a = explode('index: ', $error['message']);
                $word = $a[1];
                $file = file($error['file']);
                if (strpos($file[$error['line'] - 1], 'translation') !== false) {
                    $t = new Translation();
                    $t->key = $word;
                    $t->save();
                    foreach (Languages::model()->findAll() as $l) {
                        $tl = new TranslationLabel();
                        $tl->translation_id = $t->id;
                        $tl->language_id = $l->id;
                        $tl->value = $l->id == 1 ? ucwords(str_replace('_', ' ', $word)) : null;
                        $tl->save();
                    }
                }
            }
            var_dump($error);
            die;
            if ($error = Yii::app()->errorHandler->error) {
                if (Yii::app()->request->isAjaxRequest) {
                    echo $error['message'];
                }
                else {
                    $this->render('error', $error);
                }
            }
        }

        /**
         * Displays the contact page
         */
        public function actionContact() {
            $model = new ContactForm;
            if (isset($_POST['ContactForm'])) {
                $model->attributes = $_POST['ContactForm'];
                if ($model->validate()) {
                    $message_model = new ContactMessages();
                    $message_model->name = strip_tags(trim($_POST['ContactForm']['name']));
                    $message_model->email = strip_tags(trim($_POST['ContactForm']['email']));
                    $message_model->subject = strip_tags(trim($_POST['ContactForm']['subject']));
                    $message_model->body = strip_tags(trim($_POST['ContactForm']['body']));
                    $message_model->save();

                    $r = $this->SendEmail($_POST['ContactForm']);
                    if ($r) {
                        Yii::app()->user->setFlash('contact', $this->translation['contact_message']);
                    }
                    else {
                        Yii::app()->user->setFlash('contact', $this->translation['contact_message_error']);
                    }

                    $this->refresh();
                }
            }
            $this->render('contact', array('model' => $model));
        }

        public function SendEmail($post) {
            if (isset($post)) {
                $mail = Yii::app()->params->adminEmail;
                $password = Yii::app()->params->password;

                // ������ �������������
                // $mailSMTP = new SendMailSmtpClass('�����', '������', '����', '��� �����������');
                $mailSMTP = new SendMailSmtpClass($mail, $password, 'ssl://smtp.mail.ru', $mail, 465); // ������� ��������� ������

                // ��������� ������
                $headers = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=utf-8\r\n"; // ��������� ������
                $headers .= "From:  <$mail>\r\n"; // �� ���� ������ !!! ��� e-mail, ����� ������� ���������� �����������

                // $result =  $mailSMTP->send('���� ������', '���� ������', '����� ������', '��������� ������');
                $result = $mailSMTP->send(
                    $mail,
                    $post['name'],
                    "Email-" . $post['email'] . " <br>
                     Phone-" . $post['subject'] . "<br>
                     Text-" . $post['body'],
                    $headers); // ���������� ������
                $session = new CHttpSession;
                $session->open();
                if ($result == true) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        public function actionPage($id) {
            $criteria = new CDbCriteria();
            $criteria->select = array('id');
            $criteria->condition = 't.status = 1 AND t.id = :id';
            $criteria->params = array(':id' => $id);
            $criteria->with = array(
                'staticPagesLabels' => array(
                    'select'    => array('staticPagesLabels.title', 'staticPagesLabels.description'),
                    'condition' => 'staticPagesLabels.language_id = :lang_id',
                    'params'    => array(':lang_id' => $this->langId)
                )
            );

            $model = StaticPages::model()->find($criteria);
//		Helpers::pr($model);

            $this->render('page', array(
                'model' => $model
            ));
        }

        public function actionLogin() {
            $model = new Users('login');
            $model_r = new Users('reg');

            if (isset($_POST['login'])) {
                $model->attributes = $_POST['Users'];
                if ($model->validate()) {
                    $user = $model->findByAttributes(array('username' => $model->username, 'password' => md5('veiko123' . $model->password)));
                    if ($user) {
                        Yii::app()->session['user'] = $user;
                        $this->redirect($this->createUrl('product/category', array('path' => 'All')));
                    }
                }
            }
            elseif (isset($_POST['register'])) {
                $model_r->attributes = $_POST['Users'];
                if ($model_r->validate()) {
                    $model_r->password = md5('veiko123' . $model_r->password);
                    $model_r->confirm_password = md5('veiko123' . $model_r->confirm_password);
                    $model_r->save();
                    Yii::app()->session['user'] = $model_r;
                    $this->redirect($this->createUrl('product/category', array('path' => 'All')));
                }
            }

            $this->render('login', array(
                'model'   => $model,
                'model_r' => $model_r
            ));
        }

        public function actionProfile() {
            if (!$this->_user) {
                $this->redirect($this->createUrl('product/category', array('path' => 'All')));
            }
            $model_edit = Users::model()->findByPk($this->_user->id);
            $model_edit->setScenario('change_details');
            $model_edit->password = null;

            $model_change_pass = Users::model()->findByPk($this->_user->id);
            $model_change_pass->setScenario('change_pass');
            $model_change_pass->password = null;

            if (isset($_POST['edit'])) {
                $model_edit->attributes = $_POST['Users'];
                if ($model_edit->validate()) {
                    if ($this->_user->password == md5('veiko123' . $model_edit->current_password)) {
                        $model_edit->save();
                        Yii::app()->session['user'] = $model_edit;
                        $this->redirect($this->createUrl('site/profile'));
                    }
                    else {
                        $model_edit->addError('current_password', $this->translation['wrong_password']);
                    }
                }
            }
            elseif (isset($_POST['change_password'])) {
                $model_change_pass->attributes = $_POST['Users'];
                if ($model_change_pass->validate()) {
                    if ($this->_user->password == md5('veiko123' . $model_change_pass->current_password)) {
                        $model_change_pass->password = md5('veiko123' . $model_change_pass->password);
                        $model_change_pass->confirm_password = md5('veiko123' . $model_change_pass->confirm_password);
                        $model_change_pass->save();
                        Yii::app()->session['user'] = $model_change_pass;
                        $this->redirect($this->createUrl('site/profile'));
                    }
                    else {
                        $model_change_pass->addError('current_password', $this->translation['wrong_password']);
                    }
                }
            }

            $this->render('profile', array(
                'model_edit'        => $model_edit,
                'model_change_pass' => $model_change_pass,
            ));
        }

        public function actionLogout() {
            unset(Yii::app()->session['user']);
            $this->redirect($this->createUrl('product/category', array('path' => 'All')));
        }

        public function actionForgotPassword() {
            $model = new Users('forgot');
            $sent = false;
            if (isset($_POST['Users'])) {
                $model->attributes = $_POST['Users'];
                if ($model->validate()) {
                    $user = $model->findByAttributes(array('email' => $model->email));
                    if ($user) {
                        $user->reset_key = md5('veiko123' . microtime());
                        $user->save();
                        $href = $this->createAbsoluteUrl('site/ResetPassword', array('k' => $user->reset_key));
                        $my_html = "<p>TO change your password please <a href='$href' target='_blank'>click here</a></p>";
                        Yii::import('ext.yii-mail.YiiMailMessage');
                        $message = new YiiMailMessage;
                        $message->setBody($my_html, 'text/html');
                        $message->subject = 'Reset Password';
                        $message->addTo($model->email);
                        $message->from = array('noreply@goldfinder.am' => 'Shop');
                        Yii::app()->mail->send($message);
                        $sent = true;
                    }
                    else {
                        $user->addError('email', $this->translation['wrong_email']);
                    }
                }
            }

            $this->render('forgot_password', array(
                'model' => $model,
                'sent'  => $sent
            ));
        }

        public function actionResetPassword($k) {
            $model_change_pass = Users::model()->findByAttributes(array('reset_key' => $k));
            if (!$model_change_pass) {
                $this->redirect($this->createUrl('product/category', array('path' => 'All')));
            }
            $model_change_pass->password = null;
            $model_change_pass->setScenario('reset');
            if(isset($_POST['Users'])){
                $model_change_pass->attributes = $_POST['Users'];
                if($model_change_pass->validate()){
                    $model_change_pass->password = md5('veiko123' . $model_change_pass->password);
                    $model_change_pass->confirm_password = md5('veiko123' . $model_change_pass->confirm_password);
                    $model_change_pass->save();
                    $this->redirect($this->createUrl('site/login'));
                }
            }
            $this->render('reset_password', array(
                'model_change_pass' => $model_change_pass
            ));
        }
    }
