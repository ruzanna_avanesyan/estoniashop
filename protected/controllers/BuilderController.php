<?php

    class BuilderController extends Controller {
        public $layout = 'column1';
        public $_pc_build;

        public function init() {
            parent::init();
            $this->_pc_build = isset(Yii::app()->session['_pc_build']) ? Yii::app()->session['_pc_build'] :
                array(
                    'items'   => array(),
                    'grouped' => PCBuilder::$types_gr,
                    'count'   => 0,
                    'total'   => 0,
                    'w'       => 0
                );
        }

        /**
         * Declares class-based actions.
         */
        public function actions() {
            return array(
                // captcha action renders the CAPTCHA image displayed on the contact page
                'captcha' => array(
                    'class'     => 'CCaptchaAction',
                    'backColor' => 0xFFFFFF,
                ),
                // page action renders "static" pages stored under 'protected/views/site/pages'
                // They can be accessed via: index.php?r=site/page&view=FileName
                'page'    => array(
                    'class' => 'CViewAction',
                ),
            );
        }

        public function actionIndex() {
            $crt_price = new CDbCriteria();
            $crt_price->select = array("CAST(REPLACE(price,'€','') AS DECIMAL) AS price");
            $crt_price->order = 'price ASC';

            $min_price = SitePriceListHasProduct::model()->find($crt_price);

            $crt_price->order = 'price DESC';
            $max_price = SitePriceListHasProduct::model()->find($crt_price);
            $filter_model = new Product();

            if (isset($_GET['Product'])) {
                $filter_model->attributes = $_GET['Product'];
            }

            $criteria = new CDbCriteria();
            $criteria->select = array('t.*', "CAST(REPLACE(sprice.price,'€','') AS DECIMAL) AS price");
            $criteria->addCondition('t.is_pc_build = 1');
            $criteria->group = 't.id';
            $criteria->join = "LEFT JOIN product_has_filter pf ON pf.product_id = t.id
                               LEFT JOIN site_price_list_has_product sprice ON sprice.product_id = t.id";

            if ($filter_model->category_id) {
                $criteria->addInCondition('t.category_id', $filter_model->category_id);
            }
            $in = array();
            if ($filter_model->filter_id) {
                foreach ($filter_model->filter_id as $k => $f) {
                    $in = array_merge($in, $f);
                    $criteria->addInCondition('pf.filter_id', $f);
                }
            }
            $provider = new CActiveDataProvider('Product', array(
                'criteria'   => $criteria,
                'pagination' => array(
                    'pageSize' => 10
                ),
            ));

            $criteria->having = ' 1=1 ';
            if ($filter_model->price_from) {
                $criteria->having .= ' AND price >= :price_from';
                $criteria->params[':price_from'] = $filter_model->price_from;
            }
            if ($filter_model->price_to) {
                $criteria->having .= ' AND price <= :price_to';
                $criteria->params[':price_to'] = $filter_model->price_to;
            }

            $criteria_category = new CDbCriteria();
            $criteria_category->join = "LEFT JOIN product ON product.category_id = t.id AND product.is_pc_build = 1";
            $criteria_category->addCondition('product.id is not null');
            $criteria_category->group = 't.id';
            $category = CHtml::listData(Category::model()->findAll($criteria_category), 'id', 'categoryLabels.name');

            $criteria_filter = new CDbCriteria();
            $criteria_filter->join = "LEFT JOIN filter_item ON filter_item.filter_id = t.id
                                      LEFT JOIN product_has_filter pf ON pf.filter_id = filter_item.id
                                      LEFT JOIN product ON product.id = pf.product_id AND product.is_pc_build = 1";
            $criteria_filter->group = 't.id';
            $criteria_filter->addCondition('product.id is not null');
            if ($filter_model->category_id) {
                $criteria_filter->addInCondition('product.category_id', $filter_model->category_id);
            }

            $filters = Filter::model()->findAll($criteria_filter);
            $filter_model->price_from = $filter_model->price_from ? $filter_model->price_from : $min_price->price;
            $filter_model->price_to = $filter_model->price_to ? $filter_model->price_to : $max_price->price;

            $this->render('index', array(
                'provider'     => $provider,
                'category'     => $category,
                'filter_model' => $filter_model,
                'filters'      => $filters,
                'min_price'    => $min_price,
                'max_price'    => $max_price,
            ));
        }

        public function actionAddToPC($id) {
            if (!Yii::app()->request->isAjaxRequest) {
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            $response = array('status' => true);
            $product = Product::model()->findByPk($id);
            $price = $product->price_item ? $product->price_item->price : 0;
            $w = $product->pc_power;

            if ($this->_pc_build['grouped']['power_supply']) {
                $pw = $this->_pc_build['grouped']['power_supply'][0]->getPc_power();
                if ($pw < ($this->_pc_build['w'] + $w)) {
                    $response['status'] = false;
                    $response['error'] = str_replace('{walt}', $pw, $this->translation['Power supply waltage is {walt}']);
                }
            }

            if ($product->build_type == 'cpu' && $response['status']) {
                if ($this->_pc_build['grouped']['motherboard']) {
                    $mt = $this->_pc_build['grouped']['motherboard'][0]->getPc_cpu(true);
                    if ($mt->val != $product->getPc_cpu()) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['Cpu socket type mismatch'];
                    }
                    elseif ($mt->count == count($this->_pc_build['grouped']['cpu'])) {
                        $response['status'] = false;
                        $response['error'] = str_replace('{count}', $mt->count, $this->translation['Motherboard cpu count is {count}']);
                    }
                }
                if (count($this->_pc_build['grouped']['cpu']) > 0 && $response['status']) {
                    if ($product->getPc_model() != $this->_pc_build['grouped']['cpu'][0]->getPc_model()) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['Cpu Models must be same'];
                    }
                }
            }
            elseif ($product->build_type == 'memory' && $response['status']) {

                $m_count = 0;
                if ($this->_pc_build['grouped']['memory']) {
                    if ($this->_pc_build['grouped']['memory'][0]->getPc_mhz() != $product->getPc_mhz()) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['Will work the down one'];
                        //TODO warning
                    }
                    else {
                        foreach ($this->_pc_build['grouped']['memory'] as $memory) {
                            $m_count += $memory->getPc_memory(true)->count;
                        }
                    }

                }

                if ($this->_pc_build['grouped']['motherboard'] && $response['status']) {
                    $mt = $this->_pc_build['grouped']['motherboard'][0]->getPc_memory(true);
                    $p_memory = $product->getPc_memory(true);
                    if ($mt->val != $p_memory->val) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['Memory type mismatch'];
                    }
                    elseif ($mt->count < ($m_count + $p_memory->count)) {
                        $response['status'] = false;
                        $response['error'] = str_replace('{count}', $mt->count, $this->translation['Motherboard memory count is {count}']);
                    }
                }
            }
            elseif ($product->build_type == 'video_card' && $response['status']) {
                $p_pci = $product->getPc_pci();
                if ($this->_pc_build['grouped']['motherboard']) {
                    $pci_count = $this->_pc_build['grouped']['motherboard'][0]->getPciCount($p_pci);
                    if ($pci_count == 0) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['VideoCard type mismatch'];
                    }
                    elseif ($this->_pc_build['grouped']['video_card']) {
                        if ($pci_count == count($this->_pc_build['grouped']['video_card'])) {
                            $response['status'] = false;
                            $response['error'] = str_replace('{count}', $pci_count, $this->translation['Motherboard videocard count is {count}']);
                        }
                    }
                }

                if ($this->_pc_build['grouped']['video_card'] && $response['status']) {
                    if ($product->getPc_multi() != 1) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['VideoCard must be SLI or CROSSFIRE'];
                    }
                    elseif ($product->getPc_model() != $this->_pc_build['grouped']['video_card'][0]->getPc_model()) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['VideoCard model must be same'];
                    }
                }
            }
            elseif ($product->build_type == 'storage' && $response['status']) {
                if ($this->_pc_build['grouped']['motherboard']) {
                    $mt = $this->_pc_build['grouped']['motherboard'][0]->getPc_storage(true);
                    if ($product->getPc_storage() != $mt->val) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['Motherboard dont have this storage type'];
                    }
                    elseif ($this->_pc_build['grouped']['storage'] && count($this->_pc_build['grouped']['storage']) == $mt->count) {
                        $response['status'] = false;
                        $response['error'] = str_replace('{count}', $mt->count, $this->translation['Motherboard storage count is {count}']);
                    }
                }
            }
            elseif ($product->build_type == 'case' && $response['status']) {
                if ($this->_pc_build['grouped']['case']) {
                    $response['status'] = false;
                    $response['error'] = $this->translation['Case already selected'];
                }
                elseif ($this->_pc_build['grouped']['motherboard']) {
                    $mt_case = $this->_pc_build['grouped']['motherboard'][0]->getPc_case_type();
                    if ($product->getCaseByVal($mt_case) == 1) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['Motherboard not supported by case'];
                    }
                }
            }
            elseif ($product->build_type == 'pci' && $response['status']) {
                if ($this->_pc_build['grouped']['motherboard']) {
                    $p_pci = $product->getPc_pci();
                    $pci_count = $this->_pc_build['grouped']['motherboard'][0]->getPciCount($p_pci);
                    if ($pci_count == 0) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['PCI type mismatch'];
                    }
                    elseif ($this->_pc_build['grouped']['pci']) {
                        $count = 0;
                        foreach ($this->_pc_build['grouped']['pci'] as $v) {
                            if ($p_pci == $v->getPc_pci()) {
                                $count++;
                            }
                        }
                        if ($count == $pci_count) {
                            $response['status'] = false;
                            $response['error'] = str_replace('{count}', $pci_count, $this->translation['PCI count is {count}']);
                        }

                    }
                }
            }
            elseif ($product->build_type == 'cooling_system' && $response['status']) {
                if ($this->_pc_build['grouped']['cpu']) {
                    if ($product->getPc_cpu() != $this->_pc_build['grouped']['cpu'][0]->getPc_cpu()) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['Cpu socket type mismatch'];
                    }
                }
                elseif ($this->_pc_build['grouped']['motherboard']) {
                    if ($product->getPc_cpu() != $this->_pc_build['grouped']['motherboard'][0]->getPc_cpu()) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['Motherboard socket type mismatch'];
                    }
                }
            }
            elseif ($product->build_type == 'motherboard' && $response['status']) {
                if ($this->_pc_build['grouped']['motherboard'] && $response['status']) {
                    $response['status'] = false;
                    $response['error'] = $this->translation['Motherboard already selected'];
                }
                //Cpu for motherboard
                if ($this->_pc_build['grouped']['cpu'] && $response['status']) {
                    $mt = $product->getPc_cpu(true);
                    if ($mt->val != $this->_pc_build['grouped']['cpu'][0]->getPc_cpu()) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['Cpu socket type mismatch'];
                    }
                    elseif ($mt->count > count($this->_pc_build['grouped']['cpu'])) {
                        $response['status'] = false;
                        $response['error'] = str_replace('{count}', $mt->count, $this->translation['Motherboard cpu count is {count}']);
                    }
                }
                //Memory for motherboard
                if ($this->_pc_build['grouped']['memory'] && $response['status']) {
                    $mt = $product->getPc_memory(true);
                    $p_memory = $product->getPc_memory(true);
                    if ($mt->val != $p_memory->val) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['Memory type mismatch'];
                    }
                    else {
                        $m_count = 0;
                        foreach ($this->_pc_build['grouped']['memory'] as $memory) {
                            $m_count += $memory->getPc_memory(true)->count;
                        }
                        if ($mt->count < $m_count) {
                            $response['status'] = false;
                            $response['error'] = str_replace('{count}', $mt->count, $this->translation['Motherboard memory count is {count}']);
                        }
                    }
                }
                //VideoCard for motherboard
                if ($this->_pc_build['grouped']['video_card'] && $response['status']) {
                    $p_pci = $this->_pc_build['grouped']['video_card'][0]->getPc_pci();
                    $pci_count = $product->getPciCount($p_pci);
                    if ($pci_count == 0) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['VideoCard type mismatch'];
                    }
                    elseif ($pci_count < count($this->_pc_build['grouped']['video_card'])) {
                        $response['status'] = false;
                        $response['error'] = str_replace('{count}', $pci_count, $this->translation['Motherboard videocard count is {count}']);
                    }
                }
                //Storage for motherboard
                if ($this->_pc_build['grouped']['storage'] && $response['status']) {
                    $mt = $product->getPc_storage(true);
                    if ($this->_pc_build['grouped']['storage'][0]->getPc_storage() != $mt->val) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['Motherboard dont have this storage type'];
                    }
                    elseif (count($this->_pc_build['grouped']['storage']) > $mt->count) {
                        $response['status'] = false;
                        $response['error'] = str_replace('{count}', $mt->count, $this->translation['Motherboard storage count is {count}']);
                    }
                }
                //Case for motherboard
                if ($this->_pc_build['grouped']['case'] && $response['status']) {
                    $mt_case = $product->getPc_case_type();
                    if ($this->_pc_build['grouped']['case'][0]->getCaseByVal($mt_case) == 1) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['Motherboard not supported by case'];
                    }
                }
                //PCI for motherboard
                if ($this->_pc_build['grouped']['pci'] && $response['status']) {
                    foreach ($this->_pc_build['grouped']['pci'] as $val) {
                        $p_pci = $val->getPc_pci();
                        $pci_count = $product->getPciCount($p_pci);
                        if ($pci_count == 0) {
                            $response['status'] = false;
                            $response['error'] = $this->translation['PCI type mismatch'];
                            break;
                        }
                        elseif ($this->_pc_build['grouped']['pci']) {
                            $count = 0;
                            foreach ($this->_pc_build['grouped']['pci'] as $v) {
                                if ($p_pci == $v->getPc_pci()) {
                                    $count++;
                                }
                            }
                            if ($count > $pci_count) {
                                $response['status'] = false;
                                $response['error'] = str_replace('{count}', $pci_count, $this->translation['PCI count is {count}']);
                                break;
                            }
                        }
                    }
                }
                //COOLING SYSTEM for motherboard
                if ($this->_pc_build['grouped']['cooling_system'] && $response['status']) {
                    if ($product->getPc_cpu() != $this->_pc_build['grouped']['cooling_system'][0]->getPc_cpu()) {
                        $response['status'] = false;
                        $response['error'] = $this->translation['Motherboard socket type mismatch'];
                    }
                }
            }

            if ($response['status']) {
                $this->_pc_build['count']++;
                $this->_pc_build['total'] += $price;
                $this->_pc_build['w'] += $w;

                $this->_pc_build['items'][$product->category_id] = isset($this->_pc_build['items'][$product->category_id]) ?
                    $this->_pc_build['items'][$product->category_id] : array('items' => array(), 'title' => $product->category->categoryLabels->name);

                $this->_pc_build['items'][$product->category_id]['items'][] = $product;

                $this->_pc_build['grouped'][$product->build_type][] = $product;

                Yii::app()->session['_pc_build'] = $this->_pc_build;
                $response['html'] = $this->renderPartial('/builder/_builder', array(), true);
            }

            echo json_encode($response);
            die;
        }

        public function actionRemoveFromPc($id) {
            $response = array('status' => true);
            $product = Product::model()->findByPk($id);
            $price = $product->price_item ? $product->price_item->price : 0;
            $w = $product->pc_power;

            if ($response['status']) {
                $this->_pc_build['count']--;
                $this->_pc_build['total'] -= $price;
                $this->_pc_build['w'] -= $w;

                foreach ($this->_pc_build['items'][$product->category_id]['items'] as $k => $v) {
                    if ($v->id = $product->id) {
                        unset($this->_pc_build['items'][$product->category_id]['items'][$k]);
                        break;
                    }
                }
                if (count($this->_pc_build['items'][$product->category_id]['items']) == 0) {
                    unset($this->_pc_build['items'][$product->category_id]);
                }

                foreach ($this->_pc_build['grouped'][$product->build_type] as $k => $v) {
                    if ($v->id = $product->id) {
                        unset($this->_pc_build['grouped'][$product->build_type][$k]);
                        break;
                    }
                }

                Yii::app()->session['_pc_build'] = $this->_pc_build;
                $response['html'] = $this->renderPartial('/builder/_builder', array(), true);
            }

            echo json_encode($response);
            die;
        }

        public function actionSave() {
            if (!$this->_user) {
                $this->redirect($this->createUrl('site/login'));
            }
            if (!$this->_pc_build['items']) {
                $this->redirect($this->createUrl('builder/index'));
            }
            $model = new PcBuildItem();

            if (isset($_POST['PcBuildItem'])) {
                $model->attributes = $_POST['PcBuildItem'];
                $model->user_id = $this->_user->id;
                $model->total = $this->_pc_build['total'];
                $model->waltage = $this->_pc_build['w'];
                if ($model->validate()) {
                    $model->save();

                    $save_items = array();
                    foreach ($this->_pc_build['items'] as $va) {
                        foreach ($va['items'] as $item) {
                            $save_items[] = array(
                                'pc_item_id' => $model->id,
                                'product_id' => $item->id,
                            );
                        }
                    }
                    $builder = Yii::app()->db->schema->commandBuilder;
                    $command = $builder->createMultipleInsertCommand('pc_item_has_product', $save_items);
                    $command->execute();

                    $path = realpath(Yii::app()->basePath . '/../vendor/image/products/');
                    $tmp_path = realpath(Yii::app()->basePath . '/../tmp_path/');
                    $save_images = array();
                    foreach ($model->images as $image) {
                        @rename($tmp_path . '/' . $image, $path . '/' . $image);
                        $save_images[] = array(
                            'pc_item_id' => $model->id,
                            'image'      => $image
                        );
                    }
                    $builder = Yii::app()->db->schema->commandBuilder;
                    $command = $builder->createMultipleInsertCommand('pc_item_images', $save_images);
                    $command->execute();

                    unset(Yii::app()->session['_pc_build']);
                    $this->redirect($this->createUrl('builder/view', array('id' => $model->id)));
                }
            }

            $this->render('save', array(
                'model' => $model
            ));
        }

        public function actionClearPcBuild() {
            if (isset(Yii::app()->session['_pc_build'])) {
                unset(Yii::app()->session['_pc_build']);
            }
            $this->redirect($this->createUrl('Builder/index'));
        }

        public function actionTmpUpload() {
            if (Yii::app()->request->isAjaxRequest) {
                $file = CUploadedFile::getInstanceByName('file');
                $ext = $file->getExtensionName();
                $name = md5(microtime());
                $full_name = $name . '.' . $ext;
                $file->saveAs(Yii::app()->basePath . '/../tmp_path/' . $full_name);
                $res = null;
                $size = round(filesize(Yii::app()->basePath . '/../tmp_path/' . $full_name) / 1024, 2) . 'KB';
                if (@is_array(getimagesize(Yii::app()->basePath . '/../tmp_path/' . $full_name))) {
                    $image = getimagesize(Yii::app()->basePath . '/../tmp_path/' . $full_name);
                    $res = $image[0] / $image[1];
                }
                else {
                    $image = false;
                }
                echo json_encode(array('files' => array(
                    'full_path' => Yii::app()->baseUrl . '/tmp_path/' . $full_name,
                    'file'      => $full_name,
                    'res'       => $res,
                    'size'      => $size
                )));
                die;
            }
        }

        public function actionView($id) {
            $model = PcBuildItem::model()->findByPk($id);
            if (!$model) {
                throw new CHttpException(404, 'The requested page does not exist.');
            }
            $this->render('view', array(
                'model' => $model
            ));
        }

        public function actionWriteReview($id) {
            if (!$this->_user) {
                echo json_encode(array('msg_type' => 'danger', 'msg' => $this->translation['Please sign in to write a review.']));
            }
            else {
                $model = new PcComments();
                $model->user_id = $this->_user->id;
                $model->pc_build_item_id = $id;
                $model->comment = $_POST['comment'];
                $model->save();
                echo json_encode(array('msg_type' => 'success', 'msg' => $this->translation['Your comment added.']));
            }
        }

        public function actionWriteReviewAnswer($id) {
            if (!$this->_user) {
                echo json_encode(array('msg_type' => 'danger', 'msg' => $this->translation['Please sign in to write a review.']));
            }
            else {
                $model = new PcCommentAnswer();
                $model->user_id = $this->_user->id;
                $model->comment_id = $id;
                $model->comment = $_POST['comment'];
                $model->save();
                echo json_encode(array('msg_type' => 'success', 'msg' => $this->translation['Your comment added.']));
            }
        }

        public function actionList() {
            $crt_price = new CDbCriteria();
            $crt_price->select = array("`total` AS price", 'total');
            $crt_price->order = 'price ASC';

            $min_price = PcBuildItem::model()->find($crt_price);

            $crt_price->order = 'price DESC';
            $max_price = PcBuildItem::model()->find($crt_price);
            $filter_model = new Product();

            if (isset($_GET['Product'])) {
                $filter_model->attributes = $_GET['Product'];
            }

            $criteria = new CDbCriteria();
            $criteria->select = array('t.*');
            $criteria->condition = '1=1';
            $criteria->group = 't.id';
            $criteria->join = "
                            LEFT JOIN pc_item_has_product ON pc_item_has_product.pc_item_id = t.id
                            LEFT JOIN product ON product.id = pc_item_has_product.product_id
                            LEFT JOIN product_has_filter pf ON pf.product_id = product.id";

            if ($filter_model->category_id) {
                $criteria->addInCondition('product.category_id', $filter_model->category_id);
                $criteria->addCondition('product.id is not null');
            }
            $in = array();
            if ($filter_model->filter_id) {
                foreach ($filter_model->filter_id as $k => $f) {
                    $in = array_merge($in, $f);
                    $criteria->addInCondition('pf.filter_id', $f);
                }
            }

            if ($filter_model->price_from) {
                $criteria->condition .= ' AND t.`total` >= :price_from';
                $criteria->params[':price_from'] = $filter_model->price_from;
            }
            if ($filter_model->price_to) {
                $criteria->condition .= ' AND t.`total` <= :price_to';
                $criteria->params[':price_to'] = $filter_model->price_to;
            }

            $provider = new CActiveDataProvider('PcBuildItem', array(
                'criteria'   => $criteria,
                'pagination' => array(
                    'pageSize' => 10
                ),
            ));

            $criteria_category = new CDbCriteria();
            $criteria_category->join = "LEFT JOIN product ON product.category_id = t.id AND product.is_pc_build = 1";
            $criteria_category->addCondition('product.id is not null');
            $criteria_category->group = 't.id';
            $category = CHtml::listData(Category::model()->findAll($criteria_category), 'id', 'categoryLabels.name');

            $criteria_filter = new CDbCriteria();
            $criteria_filter->join = "LEFT JOIN filter_item ON filter_item.filter_id = t.id
                                      LEFT JOIN product_has_filter pf ON pf.filter_id = filter_item.id
                                      LEFT JOIN product ON product.id = pf.product_id AND product.is_pc_build = 1";
            $criteria_filter->group = 't.id';
            $criteria_filter->addCondition('product.id is not null');
            if ($filter_model->category_id) {
                $criteria_filter->addInCondition('product.category_id', $filter_model->category_id);
            }

            $filters = Filter::model()->findAll($criteria_filter);
            $filter_model->price_from = $filter_model->price_from ? $filter_model->price_from : ($min_price ? $min_price->total : 0);
            $filter_model->price_to = $filter_model->price_to ? $filter_model->price_to : ($max_price ? $max_price->total : 0);

            $this->render('list', array(
                'provider'     => $provider,
                'category'     => $category,
                'filter_model' => $filter_model,
                'filters'      => $filters,
                'min_price'    => $min_price,
                'max_price'    => $max_price,
            ));
        }
    }
