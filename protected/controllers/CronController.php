<?php

/**
 * Created by PhpStorm.
 * User: voodoo
 * Date: 3/3/17
 * Time: 2:26 PM
 */
class CronController extends Controller
{

    public function actionClearTmpDir() {
        $dir = realpath(Yii::app()->basePath . '/../tmp_path/');
        foreach (glob($dir . "/*") as $file) {
            if (filemtime($file) < (time() - 86400)) {
                unlink($file);
            }
        }
    }

    public function run() {
        set_time_limit(0);
        $criteria = new CDbCriteria();
//        $criteria->condition = "last_update = :date";
//        $criteria->params = array(
//            ':date' =>  date("Y-m-d", strtotime("-1 days"))
//        );
        $criteria->with = array(
            'sitePriceList'
        );
        $site_price_list_has_product = SitePriceListHasProduct::model()->findAll($criteria);


        if(!empty($site_price_list_has_product)){
            foreach($site_price_list_has_product as $key => $value){
                $price_ = '';

                $html = file_get_contents($value->url);
                header("Content-Type: text/html; charset=utf-8");
                $doc = new DOMDocument();
                $doc->strictErrorChecking = false;
                $doc->recover = true;
                $doc->encoding = 'utf-8';
                libxml_use_internal_errors(true);
                $doc->loadHTML(mb_convert_encoding("<html><body>" . $html . "</body></html>", 'HTML-ENTITIES', 'UTF-8'));


                $xpath = new DOMXpath($doc);
                $elements = $xpath->query($value->sitePriceList->tag_name);
                if($elements->length == 2){
                    $price_ = explode($elements->item(1)->nodeValue,$elements->item(0)->nodeValue)[0];
                }else {
                    if (!is_null($elements)) {
                        foreach ($elements as $element) {

                            $nodes = $element->childNodes;
                            foreach ($nodes as $node) {
                                $price_ .= $node->nodeValue . "\n";
                            }
                        }
                    }
                }

                $site_has_urls = SitePriceListHasProduct::model()->findByPk($value->id);
                $site_has_urls->price = trim($price_);
                $site_has_urls->last_update = date('Y-m-d H:i:s');
                $site_has_urls->save();
            }

        }
    }

}