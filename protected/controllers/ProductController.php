<?php
class ProductController extends Controller{
    public $defaultAction = "category";

    public function actions(){
        return array(
            'suggestAddCompareItem'=>array(
                'class'=>'ext.actions.XSuggestAction',
                'modelName'=>'Product',
                'methodName'=>'suggestAddCompareItem',
            ),
        );
    }

    public function actionIndex($id){

        $criteria = new CDbCriteria();
        $criteria->select = array('t.category_id', 't.reviews', 't.price_url', 't.model');
        $criteria->condition = 't.id = :id';
        $criteria->params = array(':id' => $id);
        $criteria->with = array(
            'productLabels' => array(
                'select' => array('productLabels.name', 'productLabels.description'),
                'condition' => 'productLabels.language_id = :lag_id',
                'params' => array(':lag_id' =>$this->langId)

            ),
            'productImages' => array(
                'select' => array('productImages.general', 'productImages.image'),
                'order' => 'productImages.sort_order ASC, productImages.id'
            ),
            'productVideos' => array(
                'select' => array('productVideos.video'),
                'order' => 'productVideos.sort_order, productVideos.id'
            ),
            'brand' => array(
                'with' => array(
                    'brandLabels' => array(
                        'select' => array('brandLabels.name'),
                        'condition' => 'brandLabels.language_id = :lang_id',
                        'params' => array('lang_id' => $this->langId)
                    )
                )
            ),
            'productComments' => array(
                'with' => array(
                    'productCommentNegPosReviews' => array(
                        'select' => array('productCommentNegPosReviews.plus', 'productCommentNegPosReviews.minus')
                    )
                )
            ),
            'sitePriceList'
        );

        $model = Product::model()->find($criteria);
//        Helpers::pr($model);
        $compare_products = array();
        if(isset(Yii::app()->request->cookies['compare_products'])) {
            $compire_iems = explode(',',Yii::app()->request->cookies['compare_products']->value);


            $params =array();
            $condition = 't.status = 1 ANd t.id =:id';
            $with =  array(
                'productLabels' => array(
                    'select' => array('productLabels.name'),
                    'condition' => 'productLabels.language_id = :lang_id',
                    'params' => array(':lang_id' => $this->langId)
                ),
                'productImages' => array(
                    'select' => array('productImages.image','productImages.general'),
                    'condition' => 'productImages.general = 1',
                ),
                'productComments',

            );
            foreach($compire_iems as $key => $value){
                $params= array(':id' => $value);
                $path = $_GET['path'];
                if($path !== 'All'){
                    $condition .= ' AND t.category_id = :cat_id';
                    $params[':cat_id'] = $path;
                }
                $criteria = new CDbCriteria();
                $criteria->select = array('t.id');
                $criteria->condition = $condition;
                $criteria->params = $params;
                $criteria->with = $with;

                $compare_products[$key] = Product::model()->find($criteria);
            }
        }



        $price = '';

        if(!empty($model->price_url)) {
            $html = file_get_contents($model->price_url);
            header("Content-Type: text/html; charset=utf-8");
            $doc = new DOMDocument();
            $doc->strictErrorChecking = false;
            $doc->recover = true;
            $doc->encoding = 'utf-8';
            libxml_use_internal_errors(true);
            $doc->loadHTML(mb_convert_encoding("<html><body>" . $html . "</body></html>", 'HTML-ENTITIES', 'UTF-8'));


            $xpath = new DOMXpath($doc);
            /*[local-name()='author']/*[local-name()='first-name']*/
            $elements = $xpath->query($model->sitePriceList->tag_name);
            $price = '';
            if (!is_null($elements)) {
                foreach ($elements as $element) {
                    $nodes = $element->childNodes;
                    foreach ($nodes as $node) {
                        $price = $node->nodeValue . "\n";
                    }
                }
            }
        }


        $site_price_list_has_product = SitePriceListHasProduct::model()->with('sitePriceList')->findAllByAttributes(array('product_id' =>$model->id));

        $criteria = new CDbCriteria();
        $criteria->select = array('t.id');
        $criteria->condition = 't.id <> :id AND t.category_id = :cat_id';
        $criteria->params = array(':id' => $id,'cat_id' => $model->category_id);

        $criteria->limit = Yii::app()->params['similar_products_limit'];
        $criteria->order = 'RAND()';
        $criteria->with = array(
            'productLabels' => array(
                'select' => array('productLabels.name'),
                'condition' => 'productLabels.language_id = :lag_id',
                'params' => array(':lag_id' =>$this->langId)

            ),
            'productImages' => array(
                'select' => array('productImages.general', 'productImages.image'),
                'condition' => 'productImages.general = 1',
                'order' => 'productImages.sort_order ASC, productImages.id'
            ),
            'productComments'
        );

        $similar_products = Product::model()->findAll($criteria);


        $criteria = new CDbCriteria();
        $criteria->select = array('t.id');
        $criteria->condition = 't.category_id = :cat_id';
        $criteria->params = array('cat_id' => $model->category_id);
        $criteria->with = array(
            'specification' => array(
                'select' => array('specification.id'),
                'condition' => 'specification.status = 1',
                'order' => 'specification.sort_order ASC',
                'with' => array(
                    'specificationsGroupLabels' => array(
                        'select' => array('specificationsGroupLabels.name'),
                        'condition' => 'specificationsGroupLabels.language_id = :lang_id',
                        'params' => array(':lang_id' => $this->langId)
                    ),
                    'specificationsItems' => array(
                        'order' => 'specificationsItems.specification_id ASC',
                        'with' => array(
                            'specificationsItemsLabels' => array(
                                'condition' => 'specificationsItemsLabels.language_id = :lang_id',
                                'params' => array(':lang_id' => $this->langId)
                            ),
                            'specificationsItemsValues'
                        ),
                    )
                ),
            )
        );

        $specifications = CategoryHasSpecification::model()->findAll($criteria);

        $chosen_specifications = array();
        $chosen_specifications_model = ProductHasSpecification::model()->findAllByAttributes(array('product_id' => $id));
        foreach($chosen_specifications_model as $value){
            $chosen_specifications[$value->specification_item_id] = $value->specificatin_items_value_id;
        }
        $compare_button_disable = array('disable' =>false);

        if((isset(Yii::app()->request->cookies['category_id']->value) && Yii::app()->request->cookies['category_id']->value != $model->category_id)){
            $compare_button_disable['disable'] = true;
            $compare_button_disable['text'] = 'Cannot campare between different categories:';
        }
        elseif(isset(Yii::app()->request->cookies['compare_products']->value) && in_array($id, explode(',', Yii::app()->request->cookies['compare_products']->value))){
            $compare_button_disable['disable'] = true;
            $compare_button_disable['text'] = 'This item has already been added';
        }



        $params =  array(
            'model' => $model,
            'compare_products' => $compare_products,
            'specifications' => $specifications,
            'chosen_specifications' => $chosen_specifications,
            'similar_products' => $similar_products,
            'compare_button_disable' => $compare_button_disable,
            'price' => $price,
            'site_price_list_has_product' => $site_price_list_has_product
        );
        if(isset($_GET['path']) && $_GET['path'] != 'All') {
            $category_name = CategoryLabel::model()->findByAttributes(array('language_id' => $this->langId, 'category_id' => $_GET['path']));
            $params['category_name'] = $category_name;
        }

        $this->render('index', $params);
    }

    public function actionGetAllSpec(){
        $criteria = new CDbCriteria();
        $criteria->select = array('t.id');
        $criteria->condition = 't.category_id = :cat_id';
        $criteria->params = array('cat_id' => $_POST['cat_id']);
        $criteria->with = array(
            'specification' => array(
                'select' => array('specification.id'),
                'condition' => 'specification.status = 1',
                'order' => 'specification.sort_order ASC',
                'with' => array(
                    'specificationsGroupLabels' => array(
                        'select' => array('specificationsGroupLabels.name'),
                        'condition' => 'specificationsGroupLabels.language_id = :lang_id',
                        'params' => array(':lang_id' => $this->langId)
                    ),
                    'specificationsItems' => array(
                        'offset' => 0,
                        'order' => 'specificationsItems.specification_id ASC',
                        'with' => array(
                            'specificationsItemsLabels' => array(
                                'order' => 'specificationsItemsLabels.specifications_item_id ASC',
                                'condition' => 'specificationsItemsLabels.language_id = :lang_id',
                                'params' => array(':lang_id' => $this->langId)
                            ),
                            'specificationsItemsValues'
                        ),
                    )
                ),
            )
        );

        $specifications = CategoryHasSpecification::model()->findAll($criteria);
        $chosen_specifications = array();
        $chosen_specifications_model = ProductHasSpecification::model()->findAllByAttributes(array('product_id' => $_POST['id']));
        foreach($chosen_specifications_model as $value){
            $chosen_specifications[$value->specification_item_id] = $value->specificatin_items_value_id;
        }

        if(!empty($specifications)) {
            $arr = array('spec' => $this->convertModelToArray($specifications), 'chosen' => $chosen_specifications);
            echo CJSON::encode($arr);
            die();
        }else{
            echo 0;die;
        }
    }
    public function actionSearch()
    {
        $res =array();

        if (isset($_GET['term'])) {
            // sql query to get execute
            $qtxt ="SELECT id,image FROM product_images WHERE id LIKE :name";
            // preparing the sql query
            $command =Yii::app()->db->createCommand($qtxt);
            // assigning the get value
            $command->bindValue(":name", '%'.$_GET['term'].'%', PDO::PARAM_STR);
            //$res =$command->queryColumn(); // this is the function which was giving me result of only 1 column
            $res =$command->queryAll(); // I changed that to this to give me result of all column's specified in sql query.
        }
        echo CJSON::encode($res); // encoding the result to JSON
        Yii::app()->end();
    }
    public function actionCategory($path = 'All'){

//        Yii::app()->request->cookies->clear();
        if(isset(Yii::app()->request->cookies['category_id']) && Yii::app()->request->cookies['category_id']->value != $path && $path != 'All') {
            Yii::app()->request->cookies->clear();
        }

        $with = array(
            'productLabels' => array(
                'select' => array('productLabels.name'),
                'condition' => 'productLabels.language_id = :lang_id',
                'params' => array(':lang_id' => $this->langId)
            ),
            'productImages' => array(
                'select' => array('productImages.image','productImages.general'),
                'group' => 'productImages.id',
            ),
            'productVideos' => array(
                'select' => array('productVideos.id')
            ),
            'productComments' => array(
                'select' => array('productComments.id, productComments.rating')
            ),
        );
        if(isset($_GET['filter'])){
            $with['productHasFilters'] = array(
                'condition'=>'productHasFilters.filter_id IN ('. $_GET['filter'] .')',
                'together' => true
            );
        }

        $condition = 't.status = 1';
        if($path !== 'All'){
            $condition .= ' AND t.category_id = :cat_id';
            $params = array(':cat_id' => $path);
        }
        if(isset($_GET['brand'])){
            $condition .= ' ANd t.brand_id IN ('. $_GET['brand'] .')';
        }

        $page = isset($_GET['page']) ? intval($_GET['page']) : 1;  // define the variable to “LIMIT” the query
        if(isset($_GET['limit'])){
            $limit = $_GET['limit'];
        }else {
            $limit = Yii::app()->params['default_limit'];
        }

        $criteria = new CDbCriteria();
        $criteria->condition = $condition;
        $criteria->limit = $limit;
        $criteria->offset =((($page-1)*$limit));
        if($path !== 'All') {
            $criteria->params = $params;
        }
        $criteria->with = $with;

        if(isset($_GET['search'])){
            $criteria->compare( 'productLabels.name', $_GET['search'], true );
        }

        $model = Product::model()->findAll($criteria);

        $count = Product::model()->count($criteria);
        $pages = new CPagination($count);
        // results per page
        $pages->pageSize = $limit;
        $pages->applyLimit($criteria);

        //filter
        $criteria = new CDbCriteria();
        $criteria->select = array('t.id');
        $criteria->condition = 't.status = 1';
        $criteria->order = 't.sort_order ASC, t.id';
        $criteria->with = array(
            'categoryHasFilters' => array(
                'select' => array('categoryHasFilters.id'),
            ),
            'filterLabels' => array(
                'select' => array('filterLabels.name'),
                'condition' => 'filterLabels.language_id = :lang_id',
                'params' => array(':lang_id' =>$this->langId)
            ),
            'filterItems' => array(
                'select' => array('filterItems.id'),
                'order' => 'filterItems.sort_order ASC, filterItems.id',
                'with' => array(
                    'filterItemLabels' => array(
                        'select' => array('filterItemLabels.name'),
                        'condition' => 'filterItemLabels.language_id = :lang_id',
                        'params' => array(':lang_id' => $this->langId)
                    ),
                    'productHasFilters' => array(
                        'select' => array('productHasFilters.id'),
                    )
                )
            )
        );
        if($path !== 'All') {
            $criteria->with['categoryHasFilters']['condition'] = 'categoryHasFilters.category_id = :cat_id';
            $criteria->with['categoryHasFilters']['params'] = $params;
        }
        if(isset($_GET['brand'])){
            $criteria->with['filterItems']['with']['productHasFilters']['with']['product']['condition'] = 'product.brand_id IN ('. $_GET['brand'] .')';
        }
        if(isset($_GET['search'])){
            $criteria->with['filterItems']['with']['productHasFilters']['with']['product']['with']['productLabels']['condition'] = 'productLabels.name LIKE :search';
            $criteria->with['filterItems']['with']['productHasFilters']['with']['product']['with']['productLabels']['params'] = Array(':search' => '%'.  $_GET['search'] .'%');
        }

        $model_filters = Filter::model()->findAll($criteria);


        //brand
        $criteria = new CDbCriteria();
        $criteria->select = array('t.id');
        $criteria->condition = 't.status = 1';
        $criteria->order = 't.sort_order ASC, t.id';
        $criteria->with = array(
            'brandLabels' => array(
                'select' => array('brandLabels.name'),
                'condition' => 'brandLabels.language_id = :lang_id',
                'params' => array(':lang_id' =>$this->langId)
            ),
            'products' => array(
                'select' => array('products.id'),
            )
        );
        if($path != 'All'){
            $criteria->with['products']['condition'] = 'products.category_id = :cat_id';
            $criteria->with['products']['params'] = array(':cat_id' => $path);
        }
        if(isset($_GET['filter'])){
            $criteria->with['products']['with']['productHasFilters']['condition'] = 'productHasFilters.filter_id IN ('. $_GET['filter'] .')';
        }
        if(isset($_GET['search'])){
            $criteria->with['products']['with']['productLabels']['condition'] = 'productLabels.name LIKE :search';
            $criteria->with['products']['with']['productLabels']['params'] = Array(':search' => '%'.  $_GET['search'] .'%');
        }

        $model_brand = Brand::model()->findAll($criteria);

        $copmare_items = array();
        if(isset(Yii::app()->request->cookies['compare_products'])) {
            $copmare_items = explode(',', Yii::app()->request->cookies['compare_products']->value);
        }

        //specification
        $criteria = new CDbCriteria();
        $criteria->select = array('t.id');
        $criteria->condition = 't.category_id = :cat_id';
        $criteria->params = array('cat_id' => $path);
        $criteria->with = array(
            'specification' => array(
                'select' => array('specification.id'),
                'condition' => 'specification.status = 1',
                'order' => 'specification.sort_order ASC, specification.id',
                'with' => array(
                    'specificationsGroupLabels' => array(
                        'select' => array('specificationsGroupLabels.name'),
                        'condition' => 'specificationsGroupLabels.language_id = :lang_id',
                        'params' => array(':lang_id' => $this->langId)
                    ),
                    'specificationsItems' => array(
                        'with' => array(
                            'specificationsItemsLabels' => array(
                                'condition' => 'specificationsItemsLabels.language_id = :lang_id',
                                'params' => array(':lang_id' => $this->langId)
                            ),
                            'specificationsItemsValues'
                        )
                    )
                )
            )
        );

        $model_id = array();
        foreach($model as $value){
            $model_id[] = $value['id'];
        }

        $specifications = CategoryHasSpecification::model()->findAll($criteria);

        $chosen_specifications = array();
        foreach($model_id as $value) {
            $chosen_specifications_model = ProductHasSpecification::model()->findAllByAttributes(array('product_id' => $value));
            foreach ($chosen_specifications_model as $val) {
                $chosen_specifications[$value][$val->specification_item_id] = $val->specificatin_items_value_id;
            }
        }

        $this->render('category', array(
            'model' => $model,
            'model_brand' => $model_brand,
            'model_filters' => $model_filters,
            'page' => $pages,
            'item_count' => $count,
            'copmare_items' => $copmare_items,
            'specifications' => $specifications,
            'chosen_specifications' => $chosen_specifications,
            'model_id' => $model_id,
//            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAddComment(){
        if(Yii::app()->request->isAjaxRequest) {
            if (isset($_POST['Review'])) {

                $model = new ProductComment();
                $model->attributes = $_POST['Review'];
                if (!$model->validate()) {
                    echo CJSON::encode($model->getErrors());
                    die();
                }
                if ($model->save()) {
                    foreach ($_POST['Review']['pl-min'] as $val) {
                        if (!empty($val['plus']) || !empty($val['plus'])) {
                            $model_review = new ProductCommentNegPosReviews();
                            $model_review->product_comment_id = $model->id;
                            $model_review->plus = $val['plus'];
                            $model_review->minus = $val['minus'];
                            $model_review->save();
                        }
                    }
                    echo CJSON::encode('ok');
                    die();
                } else {
                    echo CJSON::encode('no ok');
                    die();
                }
            }
        }else{
            Yii::app()->end();
        }
    }

    public function actionFillComparePanelAjax(){

        if(Yii::app()->request->isAjaxRequest) {
            if (!isset(Yii::app()->request->cookies['category_id'])) {
                $cookie = new CHttpCookie('category_id', $_POST['cat_id']);
                $cookie->expire = time() + (60 * 60 * 24 * 365); // (1 year)
                Yii::app()->request->cookies['category_id'] = $cookie;
            }

            if (!isset(Yii::app()->request->cookies['compare_products'])) {
                $cookie = new CHttpCookie('compare_products', $_POST['id']);
                $cookie->expire = time() + (60 * 60 * 24 * 365); // (1 year)
                Yii::app()->request->cookies['compare_products'] = $cookie;
            } else {
                $arr = Yii::app()->request->cookies['compare_products']->value;
                if (!in_array($_POST['id'], explode(',', $arr))) {
                    $arr .= ',' . $_POST['id'];
                    Yii::app()->request->cookies['compare_products'] = new CHttpCookie('compare_products', $arr);
                }
            }
        }
    }

    public function actionDeleteComparePanelAjax(){

        $arr = Yii::app()->request->cookies['compare_products']->value;
        $array = explode(',',$arr);

        if(($key = array_search($_POST['id'], $array)) !== false) {
            unset($array[$key]);
        }
        $arr = implode(",", $array);
        Yii::app()->request->cookies['compare_products'] = new CHttpCookie('compare_products', $arr);
        //clear category_id cookie
        if(isset($_POST['last']) && $_POST['last'] == 1){
            if(isset(Yii::app()->request->cookies['category_id'])){
                unset(Yii::app()->request->cookies['category_id']);
            }
        }
        if(($key = array_search($_POST['id'], $array)) == false) {
            echo 1;die;
        }else{
            echo 0;die;
        }
    }

    public function actionCompareItemsFromCookieAjaxUrl(){
        if(!isset(Yii::app()->request->cookies['compare_products'])) {
            echo '';
            die;
        }else{
            echo Yii::app()->request->cookies['compare_products']->value;
            die;
        }
    }

    public function actionCompare($path){
        Yii::app()->clientScript->scriptMap=array(
            'jquery.js'=>false,
        );

//        Yii::app()->request->cookies->clear();
//        Helpers::pr(Yii::app()->request->cookies['category_id']);
        if(isset(Yii::app()->request->cookies['compare_products'])) {
            $compire_iems = explode(',',Yii::app()->request->cookies['compare_products']->value);

            $model = array();
            $params =array();
            $condition = 't.status = 1 ANd t.id =:id';
            $with =  array(
                'productLabels' => array(
                    'select' => array('productLabels.name'),
                    'condition' => 'productLabels.language_id = :lang_id',
                    'params' => array(':lang_id' => $this->langId)
                ),
                'productImages' => array(
                    'select' => array('productImages.image','productImages.general'),
                    'condition' => 'productImages.general = 1',
                ),
                'productComments',

            );
            foreach($compire_iems as $key => $value){
                $params= array(':id' => $value);
                if($path !== 'All'){
                    $condition .= ' AND t.category_id = :cat_id';
                    $params[':cat_id'] = $path;
                }
                $criteria = new CDbCriteria();
                $criteria->condition = $condition;
                $criteria->params = $params;
                $criteria->with = $with;

                $model[$key] = Product::model()->find($criteria);
                if(!empty($model[$key]->price_url)) {

                    $html = file_get_contents($model[$key]->price_url);
                    header("Content-Type: text/html; charset=utf-8");
                    $doc = new DOMDocument();
                    $doc->strictErrorChecking = false;
                    $doc->recover = true;
                    $doc->encoding = 'utf-8';
                    libxml_use_internal_errors(true);
                    $doc->loadHTML(mb_convert_encoding("<html><body>" . $html . "</body></html>", 'HTML-ENTITIES', 'UTF-8'));

                    $xpath = new DOMXpath($doc);
                    /*[local-name()='author']/*[local-name()='first-name']*/
                    $elements = $xpath->query(" //div [@class = 'price-new' or   @class = 'price']");
                    $price = '';
                    if (!is_null($elements)) {
                        foreach ($elements as $element) {

                            $nodes = $element->childNodes;
                            foreach ($nodes as $node) {
                                $price = $node->nodeValue . "\n";
                            }
                        }
                    }
                    $model[$key]['price'] = $price;
                }else{
                    $model[$key]['price'] = '';
                }

            }

            $criteria = new CDbCriteria();
            $criteria->select = array('t.id');
            $criteria->condition = 't.category_id = :cat_id';
            if($path !== 'All'){
                $criteria->params = array('cat_id' => $path);
            }else {
                $criteria->params = array('cat_id' => $model[0]->category_id);
            }
            $criteria->with = array(
                'specification' => array(
                    'select' => array('specification.id'),
                    'condition' => 'specification.status = 1',
                    'order' => 'specification.sort_order ASC, specification.id',

                    'with' => array(
                        'specificationsGroupLabels' => array(
                            'select' => array('specificationsGroupLabels.name'),
                            'condition' => 'specificationsGroupLabels.language_id = :lang_id',
                            'params' => array(':lang_id' => $this->langId)
                        ),
                        'specificationsItems' => array(
                            'with' => array(
                                'specificationsItemsLabels' => array(
                                    'condition' => 'specificationsItemsLabels.language_id = :lang_id',
                                    'together' => true,
                                    'params' => array(':lang_id' => $this->langId)
                                ),
                                'specificationsItemsValues' => array(
                                    'together' => true,
                                    'joinType' => 'INNER JOIN',
                                )
                            )
                        )
                    )
                )
            );

            $model_id = array();
            foreach($model as $value){
                $model_id[] = $value['id'];
            }

            $specifications = CategoryHasSpecification::model()->findAll($criteria);

            $chosen_specifications = array();
            foreach($model_id as $value) {
                $chosen_specifications_model = ProductHasSpecification::model()->findAllByAttributes(array('product_id' => $value));
                foreach ($chosen_specifications_model as $val) {
                    $chosen_specifications[$value][$val->specification_item_id] = $val->specificatin_items_value_id;
                }
            }
//            Helpers::pr($chosen_specifications);
        }else{
            $this->redirect(Yii::app()->createUrl('product/category', array('path' => $path)));
        }

        $this->render('compire', array(
            'model' => $model,
            'specifications' => $specifications,
            'chosen_specifications' => $chosen_specifications,
            'model_id' => $model_id,
        ));
    }

    public function actionAddCompareItemToCookie(){
        if(Yii::app()->request->isAjaxRequest) {
            if (!isset(Yii::app()->request->cookies['compare_products'])) {
                $cookie = new CHttpCookie('compare_products', $_POST['product_id']);
                $cookie->expire = time() + (60 * 60 * 24 * 365); // (1 year)
                Yii::app()->request->cookies['compare_products'] = $cookie;
            } else {
                $arr = Yii::app()->request->cookies['compare_products']->value;
                if (!in_array($_POST['product_id'], explode(',', $arr))) {
                    $arr .= ',' . $_POST['product_id'];
                    Yii::app()->request->cookies['compare_products'] = new CHttpCookie('compare_products', $arr);
                }
            }
            echo 1;die;
        }
    }

    public function convertModelToArray($models) {
        if (is_array($models)) {
            $arrayMode = TRUE;
        } else {
            $models = array($models);
            $arrayMode = FALSE;
        }
        $result = array();

        foreach ($models as $model) {
            if($model) {
                $attributes = $model->getAttributes();

                $relations = array();
                foreach ($model->relations() as $key => $related) {
                    if ($model->hasRelated($key)) {
                        $relations[$key] = $this->convertModelToArray($model->$key);
                    }
                }
                $all = array_merge($attributes, $relations);
                $all = array_filter($all, function($var){return !is_null($var);} );

                if ($arrayMode) {
//					Helpers::pr($all, true);
                    array_push($result, $all);
                } else {
                    $result = $all;
                }
            }
        }

        return $result;
    }

    public function actionAddWishList(){
        if(Yii::app()->request->isAjaxRequest){
            if (!isset(Yii::app()->request->cookies['wish_list_id'])) {
                $cookie = new CHttpCookie('wish_list_id', $_POST['id']);
                $cookie->expire = time() + (60 * 60 * 24 * 365); // (1 year)
                Yii::app()->request->cookies['wish_list_id'] = $cookie;
            } else {
                $arr = Yii::app()->request->cookies['wish_list_id']->value;
                if (!in_array($_POST['id'], explode(',', $arr))) {
                    $arr .= ',' . $_POST['id'];
                    Yii::app()->request->cookies['wish_list_id'] = new CHttpCookie('wish_list_id', $arr);
                    echo 'ok';
                }
            }
        }
        Yii::app()->end();
    }

    public function actionWishList(){
//        Helpers::pr(Yii::app()->request->cookies['wish_list_id']);
        if(isset(Yii::app()->request->cookies['wish_list_id'])) {
            $wish_list_iems = explode(',', Yii::app()->request->cookies['wish_list_id']->value);

            $model = array();
            $condition = 't.status = 1 ANd t.id =:id';
            $with = array(
                'category' => array(
                    'select' => array('category.id'),
                    'condition' => 'category.status = 1',
                    'with' => array(
                        'categoryLabels' => array(
                            'select' => array('categoryLabels.name'),
                            'condition' => 'categoryLabels.language_id = :lang_id',
                            'params' => array(':lang_id' => $this->langId)
                        )
                    )
                ),
                'productLabels' => array(
                    'select' => array('productLabels.name'),
                    'condition' => 'productLabels.language_id = :lang_id',
                    'params' => array(':lang_id' => $this->langId)
                ),
                'productImages' => array(
                    'select' => array('productImages.image', 'productImages.general'),
                    'condition' => 'productImages.general = 1',
                ),
            );

            foreach ($wish_list_iems as $key => $value) {
                $params = array(':id' => $value);

                $criteria = new CDbCriteria();
                $criteria->condition = $condition;
                $criteria->params = $params;
                $criteria->with = $with;

                $model[$key] = Product::model()->find($criteria);
                if (!empty($model[$key]->price_url)) {
//
//                $html = file_get_contents($model[$key]->price_url);
//                header("Content-Type: text/html; charset=utf-8");
//                $doc = new DOMDocument();
//                $doc->strictErrorChecking = false;
//                $doc->recover = true;
//                $doc->encoding = 'utf-8';
//                libxml_use_internal_errors(true);
//                $doc->loadHTML(mb_convert_encoding("<html><body>" . $html . "</body></html>", 'HTML-ENTITIES', 'UTF-8'));
//
//                $xpath = new DOMXpath($doc);
//                /*[local-name()='author']/*[local-name()='first-name']*/
//                $elements = $xpath->query(" //div [@class = 'price-new' or   @class = 'price']");
//                $price = '';
//                if (!is_null($elements)) {
//                    foreach ($elements as $element) {
//
//                        $nodes = $element->childNodes;
//                        foreach ($nodes as $node) {
//                            $price = $node->nodeValue . "\n";
//                        }
//                    }
//                }
//                $model[$key]['price'] = $price;
                } else {
                    $model[$key]['price'] = '';
                }
            }

            $notes = array();
            if( isset(Yii::app()->request->cookies['wish_list_note'])){
                $notes = json_decode(Yii::app()->request->cookies['wish_list_note']->value, true);
            }

//        Helpers::pr($model);


            $this->render('wish_list', array(
                'model' => $model,
                'notes' => $notes
            ));
        }else{
            $this->redirect(Yii::app()->user->returnUrl);
        }
    }

    public function actionPrice(){

        $html = file_get_contents('https://www.arvutitark.ee/est/tootekataloog/Nutiseadmed-Telefonid-Nutitelefonid369/ALCATEL-IDOL4-Metal-Silver-VR-TASUTA-Philips-Headset-SHL3260BK-235459');
        header("Content-Type: text/html; charset=utf-8");
        $doc = new DOMDocument();
        $doc->strictErrorChecking = false;
        $doc->recover = true;
        $doc->encoding = 'utf-8';
        libxml_use_internal_errors(true);
        $doc->loadHTML(mb_convert_encoding("<html><body>" . $html . "</body></html>", 'HTML-ENTITIES', 'UTF-8'));

        $xpath = new DOMXpath($doc);
        $elements = $xpath->query("//div [@class = 'product-price'] | //div [@class = 'product-price']/small");
        if($elements->length == 2){
            $price = explode($elements->item(1)->nodeValue,$elements->item(0)->nodeValue)[0];
        }else {

            $price = '';
            if (!is_null($elements)) {
                foreach ($elements as $element) {

                    $nodes = $element->childNodes;
                    foreach ($nodes as $node) {
                        $price .= $node->nodeValue . "\n";
                    }
                }
            }
        }

        Helpers::pr($price);
    }

    public function actionAddWishNote($id){
        if(Yii::app()->request->isAjaxRequest){
            if (!isset(Yii::app()->request->cookies['wish_list_note'])) {
                $arr = json_encode(array($id=>$_POST['value']));
                $cookie = new CHttpCookie('wish_list_note', $arr);
                $cookie->expire = time() + (60 * 60 * 24 * 365); // (1 year)
                Yii::app()->request->cookies['wish_list_note'] = $cookie;
                Yii::app()->end();
            } else {
                $arr = json_decode(Yii::app()->request->cookies['wish_list_note']->value, true);
                $arr[$id] = $_POST['value'];
                $arr = json_encode($arr);
                Yii::app()->request->cookies['wish_list_note'] = new CHttpCookie('wish_list_note', $arr);
                Helpers::pr(Yii::app()->request->cookies['wish_list_note']->value);

                Yii::app()->end();
            }
        }

    }

    public function actionDeleteWishItem($id){
        if(Yii::app()->request->isAjaxRequest){
            $arr = Yii::app()->request->cookies['wish_list_id']->value;
            $array = explode(',',$arr);

            if(($key = array_search($id, $array)) !== false) {
                unset($array[$key]);
            }
            $arr = implode(",", $array);
            Yii::app()->request->cookies['wish_list_id'] = new CHttpCookie('wish_list_id', $arr);

            if(($key = array_search($id, $array)) == false) {
                echo 1;die;
            }else{
                echo 0;die;
            }
        }
        Yii::app()->end();
    }
}