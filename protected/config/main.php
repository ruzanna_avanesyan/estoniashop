<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
    return array(
        'basePath'          => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
        'name'              => 'Yii Demo',
        'language'          => 'en',

        // preloading 'log' component
        'preload'           => array('log'),

        // autoloading model and component classes
        'import'            => array(
            'application.models.*',
            'application.components.*',
        ),

        'defaultController' => 'site',

        'modules'           => array(
            'ad_min' => array(
                'defaultController' => 'admins',
                'layout'            => 'application.modules.admin.views.layouts.main',
            ),

            // uncomment the following to enable the Gii tool

            'gii'    => array(
                'class'          => 'system.gii.GiiModule',
                'password'       => '1',
                // If removed, Gii defaults to localhost only. Edit carefully to taste.
                'ipFilters'      => array('127.0.0.1', '::1'),
                'generatorPaths' => array('bootstrap.gii'),
            ),
        ),

        // application components
        'components'        => array(
            'user'         => array(
                // enable cookie-based authentication
                'allowAutoLogin' => false,
            ),
            'clientScript' => array(
                'scriptMap' => array(
                    'jquery.js' => false,
                ),
            ),
            'mail'         => array(
                'class'            => 'ext.yii-mail.YiiMail',
                'transportType'    => 'smtp',
                'transportOptions' => array(
                    'host'     => 'smtp.spaceweb.ru',
                    'username' => 'noreply@goldfinder.am',
                    'password' => "sench445309",
                    //				'encryption' => 'ssl',
                    'port'     => '25',
                ),
                'viewPath'         => 'application.views.mail',
                'logging'          => true,
                'dryRun'           => false
            ),
            //		'db'=>array(
            //			'connectionString' => 'sqlite:protected/data/blog.db',
            //			'tablePrefix' => 'tbl_',
            //		),
            // uncomment the following to use a MySQL database
            'db'           => require(dirname(__FILE__) . '/db.php'),

            'errorHandler' => array(
                // use 'site/error' action to display errors
                'errorAction' => 'site/error',
            ),
            //		'urlManager'=>array(
            //			'urlFormat'=>'path',
            //
            //		),
            'log'          => array(
                'class'  => 'CLogRouter',
                'routes' => array(
                    array(
                        'class'  => 'CFileLogRoute',
                        'levels' => 'error, warning',
                    ),
                    // uncomment the following to show log messages on web pages
                    /*
                    array(
                        'class'=>'CWebLogRoute',
                    ),
                    */
                ),
            ),
        ),

        // application-level parameters that can be accessed
        // using Yii::app()->params['paramName']
        'params'            => require(dirname(__FILE__) . '/params.php'),
    );