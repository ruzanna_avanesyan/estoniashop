/*
SQLyog Ultimate v11.52 (64 bit)
MySQL - 5.5.54-0ubuntu0.14.04.1 : Database - veiko
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `admins` */

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL COMMENT 'md5',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active, 0 = no active',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `avatar` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

/*Data for the table `admins` */

insert  into `admins`(`id`,`email`,`password`,`status`,`created_date`,`avatar`,`name`) values (1,'e@e.e','4124bc0a9335c27f086f24ba207a4912',1,'2016-08-31 16:16:23','','admin_name'),(2,'harut.alagulyan@mail.ru','80f3e3bda293ef868e7e852a79c9ce3b',1,'2016-09-05 19:44:58','','harut ALAGULYAN'),(3,'av.ruzanna@gmail.com','4297f44b13955235245b2497399d7a93',1,'2016-09-28 15:41:08','','Ruzanna Avanesyan'),(6,'zanco@hot.ee','4297f44b13955235245b2497399d7a93',1,'2016-10-26 23:49:54','','Veiko'),(9,'eeee@wefwe.fwef','1488538381',1,'2017-03-03 17:53:01','','fdfddf'),(10,'eeee@wefwe.fwef','1488538441',1,'2017-03-03 17:54:01','','fdfddf'),(11,'eeee@wefwe.fwef','1488538501',1,'2017-03-03 17:55:02','','fdfddf'),(12,'eeee@wefwe.fwef','1488538561',1,'2017-03-03 17:56:01','','fdfddf'),(13,'eeee@wefwe.fwef','1488538621',1,'2017-03-03 17:57:01','','fdfddf'),(14,'eeee@wefwe.fwef','1488539761',1,'2017-03-03 18:16:01','','fdfddf'),(15,'eeee@wefwe.fwef','1488539821',1,'2017-03-03 18:17:01','','fdfddf'),(16,'eeee@wefwe.fwef','1488539881',1,'2017-03-03 18:18:01','','fdfddf'),(17,'eeee@wefwe.fwef','1488539941',1,'2017-03-03 18:19:01','','fdfddf'),(18,'eeee@wefwe.fwef','1488540001',1,'2017-03-03 18:20:01','','fdfddf'),(19,'eeee@wefwe.fwef','1488540061',1,'2017-03-03 18:21:01','','fdfddf'),(20,'eeee@wefwe.fwef','1488540121',1,'2017-03-03 18:22:01','','fdfddf'),(21,'eeee@wefwe.fwef','1488540181',1,'2017-03-03 18:23:01','','fdfddf'),(22,'eeee@wefwe.fwef','1488540241',1,'2017-03-03 18:24:01','','fdfddf'),(23,'eeee@wefwe.fwef','1488540301',1,'2017-03-03 18:25:01','','fdfddf'),(24,'eeee@wefwe.fwef','1488540361',1,'2017-03-03 18:26:01','','fdfddf'),(25,'eeee@wefwe.fwef','1488540421',1,'2017-03-03 18:27:01','','fdfddf'),(26,'eeee@wefwe.fwef','1488540481',1,'2017-03-03 18:28:01','','fdfddf'),(27,'eeee@wefwe.fwef','1488540541',1,'2017-03-03 18:29:01','','fdfddf'),(28,'eeee@wefwe.fwef','1488540601',1,'2017-03-03 18:30:01','','fdfddf'),(29,'eeee@wefwe.fwef','1488540661',1,'2017-03-03 18:31:01','','fdfddf'),(30,'eeee@wefwe.fwef','1488540721',1,'2017-03-03 18:32:01','','fdfddf'),(31,'eeee@wefwe.fwef','1488540781',1,'2017-03-03 18:33:01','','fdfddf'),(32,'eeee@wefwe.fwef','1488540841',1,'2017-03-03 18:34:01','','fdfddf'),(33,'eeee@wefwe.fwef','1488540901',1,'2017-03-03 18:35:01','','fdfddf'),(34,'eeee@wefwe.fwef','1488540961',1,'2017-03-03 18:36:01','','fdfddf'),(35,'eeee@wefwe.fwef','1488541021',1,'2017-03-03 18:37:01','','fdfddf'),(36,'eeee@wefwe.fwef','1488541081',1,'2017-03-03 18:38:01','','fdfddf'),(37,'eeee@wefwe.fwef','1488541141',1,'2017-03-03 18:39:01','','fdfddf'),(38,'eeee@wefwe.fwef','1488541201',1,'2017-03-03 18:40:01','','fdfddf'),(39,'eeee@wefwe.fwef','1488541261',1,'2017-03-03 18:41:01','','fdfddf'),(40,'eeee@wefwe.fwef','1488541321',1,'2017-03-03 18:42:01','','fdfddf'),(41,'eeee@wefwe.fwef','1488541381',1,'2017-03-03 18:43:01','','fdfddf'),(42,'eeee@wefwe.fwef','1488541441',1,'2017-03-03 18:44:01','','fdfddf'),(43,'eeee@wefwe.fwef','1488541501',1,'2017-03-03 18:45:01','','fdfddf'),(44,'eeee@wefwe.fwef','1488541561',1,'2017-03-03 18:46:01','','fdfddf'),(45,'eeee@wefwe.fwef','1488541621',1,'2017-03-03 18:47:01','','fdfddf'),(46,'eeee@wefwe.fwef','1488541681',1,'2017-03-03 18:48:01','','fdfddf'),(47,'eeee@wefwe.fwef','1488541741',1,'2017-03-03 18:49:01','','fdfddf'),(48,'eeee@wefwe.fwef','1488541801',1,'2017-03-03 18:50:01','','fdfddf'),(49,'eeee@wefwe.fwef','1488541861',1,'2017-03-03 18:51:01','','fdfddf'),(50,'eeee@wefwe.fwef','1488541921',1,'2017-03-03 18:52:01','','fdfddf'),(51,'eeee@wefwe.fwef','1488541981',1,'2017-03-03 18:53:01','','fdfddf'),(52,'eeee@wefwe.fwef','1488542041',1,'2017-03-03 18:54:01','','fdfddf'),(53,'eeee@wefwe.fwef','1488542101',1,'2017-03-03 18:55:01','','fdfddf'),(54,'eeee@wefwe.fwef','1488542161',1,'2017-03-03 18:56:01','','fdfddf'),(55,'eeee@wefwe.fwef','1488542221',1,'2017-03-03 18:57:01','','fdfddf'),(56,'eeee@wefwe.fwef','1488542281',1,'2017-03-03 18:58:01','','fdfddf'),(57,'eeee@wefwe.fwef','1488542341',1,'2017-03-03 18:59:01','','fdfddf'),(58,'eeee@wefwe.fwef','1488542401',1,'2017-03-03 19:00:01','','fdfddf'),(59,'eeee@wefwe.fwef','1488542461',1,'2017-03-03 19:01:01','','fdfddf'),(60,'eeee@wefwe.fwef','1488542521',1,'2017-03-03 19:02:01','','fdfddf');

/*Table structure for table `brand` */

DROP TABLE IF EXISTS `brand`;

CREATE TABLE `brand` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `brand` */

insert  into `brand`(`id`,`sort_order`,`status`,`created_date`,`image`) values (6,0,1,'2016-10-10 18:52:58','2b49c48947803dc8c6f138ed6a88e9a5.png'),(12,0,1,'2016-12-16 19:33:05','6adfcb0f7b9992784a8487043a0bd8bb.jpg'),(13,0,1,'2016-12-28 04:25:51','96d2baa4f56fe8bf748417eaf94b175e.png'),(14,0,1,'2017-01-18 06:27:11','f0e40503a8d04ae3a5e3d3442860a18a.png'),(15,0,1,'2017-01-18 06:39:42','b3ee7777a45da4f5bf550ba30bd11cff.png');

/*Table structure for table `brand_label` */

DROP TABLE IF EXISTS `brand_label`;

CREATE TABLE `brand_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `brand_id` (`brand_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `brand_label_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `brand_label_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

/*Data for the table `brand_label` */

insert  into `brand_label`(`id`,`brand_id`,`language_id`,`name`) values (7,6,1,'benq'),(14,12,1,'rthrthrthrt'),(21,6,7,NULL),(22,12,7,NULL),(37,15,1,'G.SKILL'),(38,15,7,''),(39,14,1,'Samsung'),(40,14,7,''),(41,13,1,'Asus'),(42,13,7,'');

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active,0= no active',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `category` */

insert  into `category`(`id`,`sort_order`,`status`,`created_date`,`modify_date`) values (3,0,1,'2016-09-12 17:03:29','2016-11-01 09:29:28'),(4,0,1,'2016-09-12 17:55:27','2016-11-01 08:13:58'),(5,1,1,'2016-10-27 20:25:44','2017-04-09 06:15:34'),(6,0,1,'2016-12-14 00:32:07','2016-12-13 12:33:14');

/*Table structure for table `category_has_filter` */

DROP TABLE IF EXISTS `category_has_filter`;

CREATE TABLE `category_has_filter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned NOT NULL,
  `filter_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `filter_id` (`filter_id`),
  CONSTRAINT `category_has_filter_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `category_has_filter_ibfk_2` FOREIGN KEY (`filter_id`) REFERENCES `filter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

/*Data for the table `category_has_filter` */

insert  into `category_has_filter`(`id`,`category_id`,`filter_id`) values (21,3,7),(22,3,9),(47,6,7),(53,5,10),(54,5,11),(55,5,13);

/*Table structure for table `category_has_specification` */

DROP TABLE IF EXISTS `category_has_specification`;

CREATE TABLE `category_has_specification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned NOT NULL,
  `specification_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `specification_id` (`specification_id`),
  CONSTRAINT `category_has_specification_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `category_has_specification_ibfk_2` FOREIGN KEY (`specification_id`) REFERENCES `specifications_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

/*Data for the table `category_has_specification` */

insert  into `category_has_specification`(`id`,`category_id`,`specification_id`) values (41,4,2),(46,3,2),(47,3,5),(48,3,6),(75,6,2),(76,6,5),(81,5,9),(82,5,10);

/*Table structure for table `category_label` */

DROP TABLE IF EXISTS `category_label`;

CREATE TABLE `category_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `category_label_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `category_label_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;

/*Data for the table `category_label` */

insert  into `category_label`(`id`,`category_id`,`language_id`,`name`) values (55,4,1,'Tablets en'),(57,3,1,'Mobiles '),(71,6,1,'Gadjets'),(92,4,7,NULL),(93,3,7,NULL),(95,6,7,NULL),(98,5,1,'Memory'),(99,5,7,'');

/*Table structure for table `contact_messages` */

DROP TABLE IF EXISTS `contact_messages`;

CREATE TABLE `contact_messages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=unread, 1= read',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `contact_messages` */

insert  into `contact_messages`(`id`,`status`,`created_date`,`name`,`email`,`subject`,`body`) values (1,1,'2016-09-13 16:08:21','re re','harut.alagulyan@mail.ru','erg re','gre gre gre'),(2,1,'2016-09-13 16:08:21','re re','harut.alagulyan@mail.ru','erg re','gre gre gre'),(3,1,'2016-12-14 00:56:28','vx','cvxzvx@fdsfd.sdgf','cvxzc','vzxcv'),(4,1,'2016-12-14 17:29:29','rherher','harut.alagulyan@mail.ru','rehreh','reherherherherh');

/*Table structure for table `filter` */

DROP TABLE IF EXISTS `filter`;

CREATE TABLE `filter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active, 0 = no active',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `filter` */

insert  into `filter`(`id`,`sort_order`,`status`,`created_date`,`modify_date`,`name`) values (7,0,1,'2016-09-06 19:54:39','2016-11-04 11:59:23',NULL),(9,0,1,'2016-09-12 20:18:21','2016-10-26 12:13:54',NULL),(10,1,1,'2016-12-01 22:03:18','2016-12-01 10:30:11',NULL),(11,1,1,'2016-12-01 22:08:08','2016-12-01 10:57:40',NULL),(13,1,1,'2016-12-09 00:09:17','2016-12-16 07:18:24',NULL);

/*Table structure for table `filter_item` */

DROP TABLE IF EXISTS `filter_item`;

CREATE TABLE `filter_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) unsigned NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `filter_id` (`filter_id`),
  CONSTRAINT `filter_item_ibfk_1` FOREIGN KEY (`filter_id`) REFERENCES `filter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

/*Data for the table `filter_item` */

insert  into `filter_item`(`id`,`filter_id`,`sort_order`) values (41,9,0),(42,9,0),(43,9,0),(55,7,1),(56,7,2),(57,7,3),(58,7,4),(59,7,5),(60,10,1),(61,10,2),(62,10,3),(63,10,4),(64,11,1),(65,11,2),(66,11,3);

/*Table structure for table `filter_item_label` */

DROP TABLE IF EXISTS `filter_item_label`;

CREATE TABLE `filter_item_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filter_item_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `filter_item_id` (`filter_item_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `filter_item_label_ibfk_1` FOREIGN KEY (`filter_item_id`) REFERENCES `filter_item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `filter_item_label_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=utf8;

/*Data for the table `filter_item_label` */

insert  into `filter_item_label`(`id`,`filter_item_id`,`language_id`,`name`) values (77,41,1,' ht'),(79,42,1,'h rth rt'),(81,43,1,'t hrt '),(98,55,1,'18'),(99,56,1,'20'),(100,57,1,'22'),(101,58,1,'24'),(102,59,1,'25'),(118,60,1,'4,5'),(119,61,1,'5'),(120,62,1,'5,2'),(121,63,1,'5,5'),(122,64,1,'Android'),(123,65,1,'Apple iOS'),(124,66,1,'Windows Phone'),(206,41,7,NULL),(207,42,7,NULL),(208,43,7,NULL),(209,55,7,NULL),(210,56,7,NULL),(211,57,7,NULL),(212,58,7,NULL),(213,59,7,NULL),(214,60,7,NULL),(215,61,7,NULL),(216,62,7,NULL),(217,63,7,NULL),(218,64,7,NULL),(219,65,7,NULL),(220,66,7,NULL);

/*Table structure for table `filter_label` */

DROP TABLE IF EXISTS `filter_label`;

CREATE TABLE `filter_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filter_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `filter_id` (`filter_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `filter_label_ibfk_1` FOREIGN KEY (`filter_id`) REFERENCES `filter` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `filter_label_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;

/*Data for the table `filter_label` */

insert  into `filter_label`(`id`,`filter_id`,`language_id`,`name`) values (52,9,1,'rewg '),(58,7,1,'SIZE'),(64,10,1,'Diagonaal'),(65,11,1,'Operatsioonisüsteem'),(75,13,1,'Ühenduvus'),(95,9,7,NULL),(96,7,7,NULL),(97,10,7,NULL),(98,11,7,NULL),(99,13,7,NULL);

/*Table structure for table `languages` */

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active, 0 = no active',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `iso` varchar(8) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `languages` */

insert  into `languages`(`id`,`sort_order`,`status`,`created_date`,`modify_date`,`iso`,`name`,`image`) values (1,0,1,'2016-09-06 14:24:42','2016-09-06 04:38:42','en','English','gb.png'),(7,0,1,'2016-12-19 15:46:08','0000-00-00 00:00:00','ru','rus',NULL);

/*Table structure for table `pc_build_item` */

DROP TABLE IF EXISTS `pc_build_item`;

CREATE TABLE `pc_build_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `total` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `pc_build_item_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pc_build_item` */

/*Table structure for table `pc_build_options` */

DROP TABLE IF EXISTS `pc_build_options`;

CREATE TABLE `pc_build_options` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pc_type` varchar(20) NOT NULL,
  `option` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `pc_build_options` */

insert  into `pc_build_options`(`id`,`pc_type`,`option`) values (1,'cpu','LGA1155'),(2,'memory','DDR3'),(3,'storage','ATA'),(6,'pci','PCIe X1'),(7,'cpu','LGA2011'),(8,'memory','DDR4'),(9,'storage','SATA'),(11,'pci','PCIe X16'),(21,'case','Mini ATX'),(22,'case','ATX'),(23,'case','Full ATX');

/*Table structure for table `pc_comment_answer` */

DROP TABLE IF EXISTS `pc_comment_answer`;

CREATE TABLE `pc_comment_answer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `comment` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `comment_id` (`comment_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `pc_comment_answer_ibfk_1` FOREIGN KEY (`comment_id`) REFERENCES `pc_comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pc_comment_answer_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pc_comment_answer` */

/*Table structure for table `pc_comments` */

DROP TABLE IF EXISTS `pc_comments`;

CREATE TABLE `pc_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `comment` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `pc_comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pc_comments` */

/*Table structure for table `pc_item_has_product` */

DROP TABLE IF EXISTS `pc_item_has_product`;

CREATE TABLE `pc_item_has_product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pc_item_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `count` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `pc_item_id` (`pc_item_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `pc_item_has_product_ibfk_1` FOREIGN KEY (`pc_item_id`) REFERENCES `pc_build_item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pc_item_has_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pc_item_has_product` */

/*Table structure for table `pc_item_images` */

DROP TABLE IF EXISTS `pc_item_images`;

CREATE TABLE `pc_item_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pc_item_id` int(11) unsigned NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pc_item_id` (`pc_item_id`),
  CONSTRAINT `pc_item_images_ibfk_1` FOREIGN KEY (`pc_item_id`) REFERENCES `pc_build_item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pc_item_images` */

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned NOT NULL,
  `brand_id` int(11) unsigned NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active, 0 = no active',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reviews` tinyint(1) NOT NULL DEFAULT '0',
  `date` varchar(4) NOT NULL DEFAULT '2016',
  `model` varchar(255) NOT NULL,
  `price_url` varchar(255) NOT NULL,
  `site_price_list_id` int(11) unsigned NOT NULL,
  `is_pc_build` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `brand_id` (`brand_id`),
  KEY `site_price_list_id` (`site_price_list_id`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_ibfk_3` FOREIGN KEY (`site_price_list_id`) REFERENCES `site_price_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

/*Data for the table `product` */

insert  into `product`(`id`,`category_id`,`brand_id`,`sort_order`,`status`,`created_date`,`modify_date`,`reviews`,`date`,`model`,`price_url`,`site_price_list_id`,`is_pc_build`) values (46,3,12,0,1,'2016-10-25 16:25:14','2017-05-12 06:47:00',0,'2016','regregerg','http://www.1a.ee/arvutikomponendid_vorgutooted/korpused_jahutid/arvuti_korpused/be_quiet_silent_base_800_window_atx_orange_bgw01',1,1),(47,3,13,0,1,'2016-10-25 17:13:33','2017-05-12 06:47:31',0,'2016','xfsdfsdf','https://www.euronics.ee/t/75129/nutitelefonid/nutitelefon-apple-iphone-7-plus-128-gb/mn4u2et-a',1,1),(48,3,6,0,1,'2016-10-25 17:15:50','2017-02-02 04:25:19',0,'2016','rgreg','http://www.1a.ee/telefonid_tahvelarvutid/mobiil_telefonid/mobiiltelefonid/apple_iphone_6s_32gb_gold',2,0),(49,3,6,0,1,'2016-10-25 17:16:59','2017-05-12 06:49:24',0,'2016','sadasd','http://www.1a.ee/telefonid_tahvelarvutid/mobiil_telefonid/mobiiltelefonid/huawei_p8_lite_dual_black',1,1),(50,3,6,0,1,'2016-10-25 18:38:42','2016-12-14 05:25:50',0,'2016','','http://www.1a.ee/telefonid_tahvelarvutid/mobiil_telefonid/mobiiltelefonid/huawei_p8_lite_dual_black',3,0),(51,4,6,0,1,'2016-10-25 18:40:08','2016-12-22 14:54:33',0,'2016','','http://www.1a.ee/telefonid_tahvelarvutid/mobiil_telefonid/mobiiltelefonid/huawei_p8_lite_dual_black',4,0),(57,5,15,1,1,'2016-12-28 20:38:33','2017-05-05 13:30:01',0,'2016','F4-3200C16D-32GTZA','https://www.galador.ee/1610-lauaarvutite-malud/3422183-memory-d4-3200-32gb-c16-gskill-tridz-k2-2x16gb-1-35v-tridentz-16',15,0),(61,3,6,0,1,'2017-03-13 17:29:54','2017-05-02 13:44:50',0,'2016','asdasdasd','http://www.1a.ee/telefonid_tahvelarvutid/mobiil_telefonid/mobiiltelefonid/apple_iphone_6s_32gb_gold',18,0),(62,3,6,0,1,'2017-05-12 18:34:00','2017-05-12 06:34:28',0,'2016','dfgdfg','http://asdasd.mn',16,1),(63,3,6,0,1,'2017-05-12 18:37:31','0000-00-00 00:00:00',0,'2016','fssdgdfg','http://dfsdffds.mn',10,1),(64,3,6,0,1,'2017-05-12 18:44:03','0000-00-00 00:00:00',0,'2016','sadasd','http://frdgfg.mn',10,1),(65,3,6,0,1,'2017-05-12 18:44:32','2017-05-12 06:45:27',0,'2016','sadasd','sadasd',11,1),(66,3,6,0,1,'2017-05-12 18:49:48','0000-00-00 00:00:00',0,'2016','sadasd','http://frdgfg.mn',11,1),(67,3,6,0,1,'2017-05-12 18:50:15','2017-05-12 06:50:55',0,'2016','sadasd','http://frdgfg.mn',9,1),(68,3,6,0,1,'2017-05-12 19:12:01','0000-00-00 00:00:00',0,'2016','sadasd','http://frdgfg.mn',11,1),(69,3,6,0,1,'2017-05-12 20:35:27','0000-00-00 00:00:00',0,'2016','sadasd','http://frdgfg.mn',11,1);

/*Table structure for table `product_comment` */

DROP TABLE IF EXISTS `product_comment`;

CREATE TABLE `product_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `content` text,
  `plus` text,
  `minus` text,
  `rating` tinyint(1) NOT NULL DEFAULT '0',
  `have` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_comment_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

/*Data for the table `product_comment` */

insert  into `product_comment`(`id`,`product_id`,`status`,`created_date`,`name`,`email`,`content`,`plus`,`minus`,`rating`,`have`) values (17,47,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',5,1),(18,48,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',5,1),(19,48,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',4,1),(20,48,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',3,0),(21,48,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',2,1),(22,47,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',5,1),(26,49,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',3,1),(27,49,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',2,1),(28,49,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',4,1),(29,49,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',1,1),(30,50,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',2,1),(31,50,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',3,1),(32,50,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',4,1),(33,50,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',2,0),(34,51,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',3,1),(35,51,0,'2016-10-25 16:39:02','Username','Username@gmail.com','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,',4,0),(37,51,0,'2016-12-14 00:53:24','Gag','gag@gag.com','dfngkdfngjnfdkidfngjndf\r\n\r\ngdfng\r\nfgdgkndfnknkdfngknkjngdfdfgh\r\nLorem Ipsum is simply dummy text o','dfhbdf','dfghdf',5,1),(39,46,0,'2017-02-01 15:55:29','tyhtyh','hasf@fgh.ty','rthrth',NULL,NULL,0,0),(40,46,0,'2017-02-01 15:56:08','tyjtyj','tyjty@rfg.gh','rhrthrth',NULL,NULL,0,0),(41,46,0,'2017-02-01 16:25:46','ytuty u','tyutyu@ewf.ewf','tyutyuty h rth rt hrt hrt h',NULL,NULL,0,0),(42,46,0,'2017-02-01 16:41:01','reger g','asf@wefr.wef','reger wg erg erg re',NULL,NULL,4,0),(43,57,0,'2017-04-11 21:09:46','test','asd@r.ee','',NULL,NULL,0,0),(44,57,0,'2017-04-11 21:10:05','test','asd@r.ee','',NULL,NULL,0,0),(45,61,0,'2017-04-13 23:54:35','dgf@dfg.hfdg','gdf','dfg',NULL,NULL,5,1),(46,57,0,'2017-05-06 19:31:45','8','8','',NULL,NULL,0,0),(47,57,0,'2017-05-09 17:50:18','arsen1500@list.ru','arsen1500@list.ru','',NULL,NULL,5,1),(48,46,0,'2017-05-09 17:51:38','arsen1500@list.ru','arsen1500@list.ru','',NULL,NULL,5,0);

/*Table structure for table `product_comment_neg_pos_reviews` */

DROP TABLE IF EXISTS `product_comment_neg_pos_reviews`;

CREATE TABLE `product_comment_neg_pos_reviews` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_comment_id` int(11) unsigned NOT NULL,
  `plus` text,
  `minus` text,
  PRIMARY KEY (`id`),
  KEY `product_comment_id` (`product_comment_id`),
  CONSTRAINT `product_comment_neg_pos_reviews_ibfk_1` FOREIGN KEY (`product_comment_id`) REFERENCES `product_comment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

/*Data for the table `product_comment_neg_pos_reviews` */

insert  into `product_comment_neg_pos_reviews`(`id`,`product_comment_id`,`plus`,`minus`) values (9,42,' ger ger',' gerg er g'),(10,42,'er ger','g er gre gre'),(11,42,' gerg re',' gre ger'),(12,45,'dfg','gdg'),(13,45,'gdg','fgd'),(14,48,'ddd','ddd'),(15,48,'ddd','ddd'),(16,48,'ddd','ddd'),(17,48,'ddd','dd');

/*Table structure for table `product_has_filter` */

DROP TABLE IF EXISTS `product_has_filter`;

CREATE TABLE `product_has_filter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `filter_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `filter_id` (`filter_id`),
  CONSTRAINT `product_has_filter_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_has_filter_ibfk_2` FOREIGN KEY (`filter_id`) REFERENCES `filter_item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8;

/*Data for the table `product_has_filter` */

insert  into `product_has_filter`(`id`,`product_id`,`filter_id`) values (141,48,56),(162,61,57),(165,57,60),(166,57,65),(168,46,57);

/*Table structure for table `product_has_specification` */

DROP TABLE IF EXISTS `product_has_specification`;

CREATE TABLE `product_has_specification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `specification_group_id` int(11) unsigned NOT NULL,
  `specification_item_id` int(11) unsigned NOT NULL,
  `specificatin_items_value_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `specification_group_id` (`specification_group_id`),
  KEY `specification_item_id` (`specification_item_id`),
  KEY `specificatin_items_value_id` (`specificatin_items_value_id`),
  CONSTRAINT `product_has_specification_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5569 DEFAULT CHARSET=utf8;

/*Data for the table `product_has_specification` */

insert  into `product_has_specification`(`id`,`product_id`,`specification_group_id`,`specification_item_id`,`specificatin_items_value_id`) values (3392,50,2,95,309),(3393,50,2,86,287),(3394,50,2,88,293),(3395,50,2,92,303),(3396,50,2,85,285),(3397,50,2,125,0),(3398,50,2,97,315),(3399,50,2,90,298),(3400,50,2,93,306),(3401,50,2,91,301),(3402,50,2,87,290),(3403,50,2,89,296),(3404,50,2,98,317),(3405,50,2,96,312),(3406,50,2,94,307),(3407,50,5,104,341),(3408,50,5,101,322),(3409,50,5,103,336),(3410,50,5,109,352),(3411,50,5,107,347),(3412,50,5,105,342),(3413,50,5,102,328),(3414,50,5,108,348),(3415,50,5,106,344),(3416,50,6,110,357),(3417,50,6,113,363),(3418,50,6,111,358),(3419,50,6,114,365),(3420,50,6,115,366),(3421,50,6,112,361),(3422,50,6,116,367),(3888,51,2,85,0),(3889,51,2,86,0),(3890,51,2,87,0),(3891,51,2,88,0),(3892,51,2,89,0),(3893,51,2,90,0),(3894,51,2,91,0),(3895,51,2,92,0),(3896,51,2,93,0),(3897,51,2,94,0),(3898,51,2,95,0),(3899,51,2,96,0),(3900,51,2,97,0),(3901,51,2,98,0),(3902,51,2,125,0),(4342,48,2,92,303),(4343,48,2,86,287),(4344,48,2,125,381),(4345,48,2,96,312),(4346,48,2,90,298),(4347,48,2,95,309),(4348,48,2,88,293),(4349,48,2,85,283),(4350,48,2,97,315),(4351,48,2,93,305),(4352,48,2,91,302),(4353,48,2,89,296),(4354,48,2,87,290),(4355,48,2,98,317),(4356,48,2,94,307),(4357,48,5,103,336),(4358,48,5,107,347),(4359,48,5,104,341),(4360,48,5,109,352),(4361,48,5,101,322),(4362,48,5,105,342),(4363,48,5,102,328),(4364,48,5,108,348),(4365,48,5,106,344),(4366,48,6,113,363),(4367,48,6,110,357),(4368,48,6,111,358),(4369,48,6,114,365),(4370,48,6,115,366),(4371,48,6,112,361),(4372,48,6,116,367),(4913,61,2,125,0),(4914,61,2,92,0),(4915,61,2,85,0),(4916,61,2,90,0),(4917,61,2,97,0),(4918,61,2,88,293),(4919,61,2,95,309),(4920,61,2,86,289),(4921,61,2,93,0),(4922,61,2,91,0),(4923,61,2,87,0),(4924,61,2,89,0),(4925,61,2,98,317),(4926,61,2,96,0),(4927,61,2,94,0),(4928,61,5,109,0),(4929,61,5,103,0),(4930,61,5,107,346),(4931,61,5,102,0),(4932,61,5,105,342),(4933,61,5,108,0),(4934,61,5,101,0),(4935,61,5,106,344),(4936,61,5,104,0),(4937,61,6,113,0),(4938,61,6,111,0),(4939,61,6,110,0),(4940,61,6,114,365),(4941,61,6,115,366),(4942,61,6,112,0),(4943,61,6,116,0),(4962,57,9,128,390),(4963,57,9,127,387),(4964,57,9,129,393),(4965,57,9,163,437),(4966,57,10,130,0),(4967,57,10,155,423),(4968,57,10,152,418),(4969,57,10,158,429),(4970,57,10,150,411),(4971,57,10,143,405),(4972,57,10,153,419),(4973,57,10,156,425),(4974,57,10,147,407),(4975,57,10,164,0),(4976,57,10,151,412),(4977,57,10,154,421),(4978,57,10,157,428),(4979,57,10,148,408),(5011,62,2,90,0),(5012,62,2,85,0),(5013,62,2,97,0),(5014,62,2,88,0),(5015,62,2,95,0),(5016,62,2,125,0),(5017,62,2,93,0),(5018,62,2,86,0),(5019,62,2,91,0),(5020,62,2,89,0),(5021,62,2,87,0),(5022,62,2,98,0),(5023,62,2,96,0),(5024,62,2,94,0),(5025,62,2,92,0),(5026,62,5,109,0),(5027,62,5,103,0),(5028,62,5,107,0),(5029,62,5,102,0),(5030,62,5,105,0),(5031,62,5,108,0),(5032,62,5,101,0),(5033,62,5,106,0),(5034,62,5,104,0),(5035,62,6,113,0),(5036,62,6,111,0),(5037,62,6,110,0),(5038,62,6,114,0),(5039,62,6,115,0),(5040,62,6,112,0),(5041,62,6,116,0),(5042,63,2,96,0),(5043,63,2,89,0),(5044,63,2,94,0),(5045,63,2,87,0),(5046,63,2,86,0),(5047,63,2,125,0),(5048,63,2,92,0),(5049,63,2,90,0),(5050,63,2,95,0),(5051,63,2,88,0),(5052,63,2,85,0),(5053,63,2,97,0),(5054,63,2,93,0),(5055,63,2,91,0),(5056,63,2,98,0),(5057,63,5,108,0),(5058,63,5,103,0),(5059,63,5,106,0),(5060,63,5,101,0),(5061,63,5,104,0),(5062,63,5,109,0),(5063,63,5,107,0),(5064,63,5,102,0),(5065,63,5,105,0),(5066,63,6,115,0),(5067,63,6,112,0),(5068,63,6,110,0),(5069,63,6,116,0),(5070,63,6,113,0),(5071,63,6,111,0),(5072,63,6,114,0),(5073,64,2,96,0),(5074,64,2,89,0),(5075,64,2,94,0),(5076,64,2,87,0),(5077,64,2,86,0),(5078,64,2,125,0),(5079,64,2,92,0),(5080,64,2,90,0),(5081,64,2,95,0),(5082,64,2,88,0),(5083,64,2,85,0),(5084,64,2,97,0),(5085,64,2,93,0),(5086,64,2,91,0),(5087,64,2,98,0),(5088,64,5,108,0),(5089,64,5,103,0),(5090,64,5,106,0),(5091,64,5,101,0),(5092,64,5,104,0),(5093,64,5,109,0),(5094,64,5,107,0),(5095,64,5,102,0),(5096,64,5,105,0),(5097,64,6,115,0),(5098,64,6,112,0),(5099,64,6,110,0),(5100,64,6,116,0),(5101,64,6,113,0),(5102,64,6,111,0),(5103,64,6,114,0),(5197,65,2,90,0),(5198,65,2,85,0),(5199,65,2,97,0),(5200,65,2,88,0),(5201,65,2,95,0),(5202,65,2,125,0),(5203,65,2,93,0),(5204,65,2,86,0),(5205,65,2,91,0),(5206,65,2,89,0),(5207,65,2,87,0),(5208,65,2,98,0),(5209,65,2,96,0),(5210,65,2,94,0),(5211,65,2,92,0),(5212,65,5,109,0),(5213,65,5,103,0),(5214,65,5,107,0),(5215,65,5,102,0),(5216,65,5,105,0),(5217,65,5,108,0),(5218,65,5,101,0),(5219,65,5,106,0),(5220,65,5,104,0),(5221,65,6,113,0),(5222,65,6,111,0),(5223,65,6,110,0),(5224,65,6,114,0),(5225,65,6,115,0),(5226,65,6,112,0),(5227,65,6,116,0),(5259,46,2,90,298),(5260,46,2,85,285),(5261,46,2,97,315),(5262,46,2,88,294),(5263,46,2,95,309),(5264,46,2,125,382),(5265,46,2,93,305),(5266,46,2,86,287),(5267,46,2,91,301),(5268,46,2,89,296),(5269,46,2,87,291),(5270,46,2,98,317),(5271,46,2,96,312),(5272,46,2,94,307),(5273,46,2,92,303),(5274,46,5,109,351),(5275,46,5,103,337),(5276,46,5,107,346),(5277,46,5,102,328),(5278,46,5,105,342),(5279,46,5,108,348),(5280,46,5,101,323),(5281,46,5,106,344),(5282,46,5,104,339),(5283,46,6,113,364),(5284,46,6,111,358),(5285,46,6,110,353),(5286,46,6,114,365),(5287,46,6,115,366),(5288,46,6,112,361),(5289,46,6,116,367),(5290,47,2,90,299),(5291,47,2,85,286),(5292,47,2,97,316),(5293,47,2,88,294),(5294,47,2,95,310),(5295,47,2,125,0),(5296,47,2,93,306),(5297,47,2,86,289),(5298,47,2,91,300),(5299,47,2,89,297),(5300,47,2,87,291),(5301,47,2,98,317),(5302,47,2,96,312),(5303,47,2,94,308),(5304,47,2,92,304),(5305,47,5,109,351),(5306,47,5,103,331),(5307,47,5,107,346),(5308,47,5,102,326),(5309,47,5,105,343),(5310,47,5,108,349),(5311,47,5,101,323),(5312,47,5,106,345),(5313,47,5,104,339),(5314,47,6,113,364),(5315,47,6,111,359),(5316,47,6,110,353),(5317,47,6,114,365),(5318,47,6,115,366),(5319,47,6,112,361),(5320,47,6,116,367),(5352,49,2,90,298),(5353,49,2,85,285),(5354,49,2,97,315),(5355,49,2,88,293),(5356,49,2,95,309),(5357,49,2,125,0),(5358,49,2,93,306),(5359,49,2,86,287),(5360,49,2,91,301),(5361,49,2,89,296),(5362,49,2,87,290),(5363,49,2,98,317),(5364,49,2,96,312),(5365,49,2,94,307),(5366,49,2,92,303),(5367,49,5,109,352),(5368,49,5,103,336),(5369,49,5,107,347),(5370,49,5,102,328),(5371,49,5,105,342),(5372,49,5,108,348),(5373,49,5,101,322),(5374,49,5,106,344),(5375,49,5,104,341),(5376,49,6,113,363),(5377,49,6,111,358),(5378,49,6,110,357),(5379,49,6,114,365),(5380,49,6,115,366),(5381,49,6,112,361),(5382,49,6,116,367),(5383,66,2,96,0),(5384,66,2,89,0),(5385,66,2,94,0),(5386,66,2,87,0),(5387,66,2,86,0),(5388,66,2,125,0),(5389,66,2,92,0),(5390,66,2,90,0),(5391,66,2,95,0),(5392,66,2,88,0),(5393,66,2,85,0),(5394,66,2,97,0),(5395,66,2,93,0),(5396,66,2,91,0),(5397,66,2,98,0),(5398,66,5,108,0),(5399,66,5,103,0),(5400,66,5,106,0),(5401,66,5,101,0),(5402,66,5,104,0),(5403,66,5,109,0),(5404,66,5,107,0),(5405,66,5,102,0),(5406,66,5,105,0),(5407,66,6,115,0),(5408,66,6,112,0),(5409,66,6,110,0),(5410,66,6,116,0),(5411,66,6,113,0),(5412,66,6,111,0),(5413,66,6,114,0),(5476,67,2,90,0),(5477,67,2,85,0),(5478,67,2,97,0),(5479,67,2,88,0),(5480,67,2,95,0),(5481,67,2,125,0),(5482,67,2,93,0),(5483,67,2,86,0),(5484,67,2,91,0),(5485,67,2,89,0),(5486,67,2,87,0),(5487,67,2,98,0),(5488,67,2,96,0),(5489,67,2,94,0),(5490,67,2,92,0),(5491,67,5,109,0),(5492,67,5,103,0),(5493,67,5,107,0),(5494,67,5,102,0),(5495,67,5,105,0),(5496,67,5,108,0),(5497,67,5,101,0),(5498,67,5,106,0),(5499,67,5,104,0),(5500,67,6,113,0),(5501,67,6,111,0),(5502,67,6,110,0),(5503,67,6,114,0),(5504,67,6,115,0),(5505,67,6,112,0),(5506,67,6,116,0),(5507,68,2,96,0),(5508,68,2,89,0),(5509,68,2,94,0),(5510,68,2,87,0),(5511,68,2,86,0),(5512,68,2,125,0),(5513,68,2,92,0),(5514,68,2,90,0),(5515,68,2,95,0),(5516,68,2,88,0),(5517,68,2,85,0),(5518,68,2,97,0),(5519,68,2,93,0),(5520,68,2,91,0),(5521,68,2,98,0),(5522,68,5,108,0),(5523,68,5,103,0),(5524,68,5,106,0),(5525,68,5,101,0),(5526,68,5,104,0),(5527,68,5,109,0),(5528,68,5,107,0),(5529,68,5,102,0),(5530,68,5,105,0),(5531,68,6,115,0),(5532,68,6,112,0),(5533,68,6,110,0),(5534,68,6,116,0),(5535,68,6,113,0),(5536,68,6,111,0),(5537,68,6,114,0),(5538,69,2,96,0),(5539,69,2,89,0),(5540,69,2,94,0),(5541,69,2,87,0),(5542,69,2,86,0),(5543,69,2,125,0),(5544,69,2,92,0),(5545,69,2,90,0),(5546,69,2,95,0),(5547,69,2,88,0),(5548,69,2,85,0),(5549,69,2,97,0),(5550,69,2,93,0),(5551,69,2,91,0),(5552,69,2,98,0),(5553,69,5,108,0),(5554,69,5,103,0),(5555,69,5,106,0),(5556,69,5,101,0),(5557,69,5,104,0),(5558,69,5,109,0),(5559,69,5,107,0),(5560,69,5,102,0),(5561,69,5,105,0),(5562,69,6,115,0),(5563,69,6,112,0),(5564,69,6,110,0),(5565,69,6,116,0),(5566,69,6,113,0),(5567,69,6,111,0),(5568,69,6,114,0);

/*Table structure for table `product_images` */

DROP TABLE IF EXISTS `product_images`;

CREATE TABLE `product_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `general` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=general',
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_images_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

/*Data for the table `product_images` */

insert  into `product_images`(`id`,`product_id`,`sort_order`,`general`,`image`) values (55,46,0,0,'8babda824e5eebfa54eb2dc2aa7019a2.jpg'),(58,46,0,1,'a03a4a597736267d6e0b1a7df8a4843d.jpg'),(59,47,0,1,'e96b2f766f4b5d7aae4163023b834994.jpg'),(60,47,0,0,'9a32764338be75dc9be67d9326c7309b.5-550x550'),(61,48,0,1,'aa2587714788510de8597a02857eb307.jpg'),(62,48,0,0,'1d88ccc8ba9c02e510e7b71caadc8fa7.jpg'),(63,48,0,0,'564f36a12b5d20f09126de04357de2cd.jpg'),(64,48,0,0,'aaa672104041dabdbecfa51e22500e62.6-550x550'),(65,49,0,1,'0a8674eb7e9b58edbc50d34619db4b29.6-550x550'),(66,49,0,0,'e9fb2d7f77a9eff4ec72f3f702d58f47.jpg'),(67,50,0,0,'3b7addad37dbea4602b09830f1f64747.jpg'),(68,50,0,0,'04569a31f7db6b42eb21f99ed4458ccc.5-550x550'),(69,50,0,0,'d837b0e7c909e5136f4e62e55456fe55.6-550x550'),(70,50,0,0,'1e361a221b05848d4fb9f10622b7377b.jpg'),(71,50,0,0,'efb99ff9d467989208cdf7bacbb58a02.jpg'),(72,51,0,1,'8d4e47683cf3eb90a768003a2cd44b43.jpg'),(80,50,0,1,'02d740e8ef7816dfaeefb5aae6d8ebd5.jpg'),(81,47,0,0,'4df78c081bcfb31b21240af2fcdc3a42.jpg'),(86,61,0,1,'f578a7900689b5fe4efbd34f716f5646.jpg'),(91,57,0,1,'f9e8dcf46018e30df3a6b453e7720bdb.png'),(92,57,0,0,'749dbdcb14e1b0e16fdccac45401d60d.Skill_Trident_Z_8GB_DDR4_3466_06');

/*Table structure for table `product_label` */

DROP TABLE IF EXISTS `product_label`;

CREATE TABLE `product_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `product_label_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_label_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=508 DEFAULT CHARSET=utf8;

/*Data for the table `product_label` */

insert  into `product_label`(`id`,`product_id`,`language_id`,`name`,`description`) values (325,50,1,'Philips 278G4DHSD/01','<h1><strong>Philips 278G4DHSD/01</strong></h1>\r\n\r\n<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<h2><img alt=\"\" src=\"/projects/estonaci/vendor/image/product_desc/monitor-20-aoc-e2050sw-01-chernyy-tn-wled--3845-550x550.jpg\" style=\"height:550px; width:550px\" /></h2>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n'),(363,50,7,NULL,NULL),(370,51,1,'Philips 274E5QHAW/00','<p><strong>Mõnitor kõigeks<img alt=\"\" src=\"https://www.klick.ee/media/catalog/product/cache/1/image/500x400/9df78eab33525d08d6e5fb8d27136e95/b/e/benq-xl2411z-gaming-m_nguri.jpg\" style=\"float:right; height:400px; width:500px\" /></strong></p>\r\n\r\n<p>Mängi mänge, tee videotöötlust või vaata filme! See monitor ei jää naljalt hätta ühegi eelnevalt mainitud tegevusega.<br />\r\n </p>\r\n\r\n<p><strong>Monitor igas asendis</strong></p>\r\n\r\n<p>Muuda kõrgust, kallet või keera monitor üldse püsti - selle monitoriga ei ole ükski asend probleemiks.</p>\r\n\r\n<p><br />\r\n<strong>FullHD</strong></p>\r\n\r\n<p>FullHD ehk 1920 x 1080 pikslit annab teile puhta ja selge pildi, et ükski detail ei jääks märkamata.</p>\r\n'),(371,51,7,'',''),(416,48,1,'BenQ XL2411Z Black','<h1><strong>BenQ RL2455HM</strong></h1>\r\n\r\n<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<h2><img alt=\"\" src=\"/projects/estonaci/vendor/image/product_desc/monitor-tft-24-benq-gl2450-led-black-916-550x550.jpg\" /></h2>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n'),(417,48,7,'',''),(464,61,1,'asdasd','<p>qweqwasd</p>\r\n'),(465,61,7,'qweqwed','<p>asdasdas</p>\r\n'),(468,57,1,'G.SKILL Trident Z Series 32GB (2x16GB)','<p>G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)B)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)G.SKILL Trident Z Series 32GB (2x16GB)</p>\r\n'),(469,57,7,'G.SKILL Trident Z Series 32GB (2x16GB)','<p>G.SKILL Trident Z Series 32GB (2x16GB)</p>\r\n'),(472,62,1,'',''),(473,62,7,'',''),(474,63,1,'',''),(475,63,7,'',''),(476,64,1,'',''),(477,64,7,'',''),(484,65,1,'',''),(485,65,7,'',''),(488,46,1,'BenQ RL2455HM','<h1><strong><span style=\"font-size:36px\">BenQ RL2455HM</span></strong></h1>\r\n\r\n<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>&nbsp;</p>\r\n'),(489,46,7,'',''),(490,47,1,'ASUS ROG SWIFT PG278Q','<p><strong><span style=\"font-size:36px\">ASUS ROG SWIFT PG278Q</span></strong></p>\r\n\r\n<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<h2><img alt=\"\" src=\"/projects/estonaci/vendor/image/product_desc/monitor-tft-24-benq-gl2450-led-black-916-550x550.jpg\" /></h2>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n'),(491,47,7,'',''),(494,49,1,'Dell U2414H','<h1><strong>Dell U2414H</strong></h1>\r\n\r\n<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<h2><img alt=\"\" src=\"/projects/estonaci/vendor/image/product_desc/monitor-tft-24-benq-gl2450-led-black-916-550x550.jpg\" /></h2>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n'),(495,49,7,'',''),(496,66,1,'',''),(497,66,7,'',''),(502,67,1,'',''),(503,67,7,'',''),(504,68,1,'',''),(505,68,7,'',''),(506,69,1,'',''),(507,69,7,'','');

/*Table structure for table `product_pc_build` */

DROP TABLE IF EXISTS `product_pc_build`;

CREATE TABLE `product_pc_build` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `pc_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_pc_build_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `product_pc_build` */

insert  into `product_pc_build`(`id`,`product_id`,`pc_type`) values (1,62,'motherboard'),(2,63,'motherboard'),(3,64,'cpu'),(4,65,'memory'),(5,46,'video_card'),(6,47,'storage'),(7,49,'case'),(8,66,'power_supply'),(9,67,'other'),(10,68,'cooling_system'),(11,69,'memory');

/*Table structure for table `product_pc_build_options` */

DROP TABLE IF EXISTS `product_pc_build_options`;

CREATE TABLE `product_pc_build_options` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_pc_build_id` int(11) unsigned NOT NULL,
  `op_type` enum('mhz','case','pw','cpu','memory','video_card','storage','power_supply','pci','model','multi') NOT NULL,
  `val` varchar(255) NOT NULL,
  `count` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_pc_build_id` (`product_pc_build_id`),
  CONSTRAINT `product_pc_build_options_ibfk_1` FOREIGN KEY (`product_pc_build_id`) REFERENCES `product_pc_build` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

/*Data for the table `product_pc_build_options` */

insert  into `product_pc_build_options`(`id`,`product_pc_build_id`,`op_type`,`val`,`count`) values (15,2,'cpu','LGA2011',12),(16,2,'memory','DDR4',12),(17,2,'storage','SATA',12),(18,2,'pci','PCIe X1',12),(19,2,'pci','PCIe X16',12),(20,2,'case','Full ATX',0),(21,2,'pw','12',0),(22,3,'cpu','LGA2011',0),(23,3,'model','aaa',0),(24,3,'pw','23',0),(37,4,'memory','DDR4',0),(38,4,'model','cxvxcv',0),(39,4,'mhz','34',0),(40,4,'pw','34',0),(45,5,'pci','PCIe X16',0),(46,5,'model','cxvxcv',0),(47,5,'pw','12',0),(48,5,'multi','1',0),(49,6,'cpu','SATA',0),(50,6,'pw','12',0),(52,7,'case','Mini ATX',0),(53,7,'case','ATX',0),(54,8,'pw','23',0),(59,10,'cpu','LGA2011',0),(60,10,'pw','77',0),(61,11,'memory','DDR4',12),(62,11,'model','dsfsdf',0),(63,11,'mhz','sdfsdf',0),(64,11,'pw','23',0);

/*Table structure for table `product_videos` */

DROP TABLE IF EXISTS `product_videos`;

CREATE TABLE `product_videos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `video` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_videos_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;

/*Data for the table `product_videos` */

insert  into `product_videos`(`id`,`product_id`,`sort_order`,`video`,`url`) values (77,50,0,'https://www.youtube.com/watch?v=ymZgue5-SR4',NULL),(106,46,0,'https://www.youtube.com/watch?v=2mFcxL51X_k',NULL);

/*Table structure for table `site_price_list` */

DROP TABLE IF EXISTS `site_price_list`;

CREATE TABLE `site_price_list` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `tag_name` varchar(255) DEFAULT NULL,
  `sipping_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

/*Data for the table `site_price_list` */

insert  into `site_price_list`(`id`,`status`,`sort_order`,`name`,`image`,`tag_name`,`sipping_date`) values (1,1,0,'1ae','246c67abe3ef57a8a8820c1283db6fe8.png','//div [@class = \'price-new\' or   @class = \'price\']','3-4 days'),(2,1,0,'turuliider','9f0be0875d71c098c175b7bb5f9f2c1d.jpg','//span[@class = \'our_price_display price\']','1 week'),(3,1,0,'ishop.ee','f1b3f69393c2f2e73a0f84c038dad113.jpg','//span[@id = \'price product-price product-price-new\']','2 day'),(4,1,0,'smartech.ee','97e30b8f8390714ae5a08d9cb76c5c3b.svg','//div[@class = \'product-info-price\']//strong','1 day '),(5,1,0,'knaitek','e4d86a9721f46705663a9e81d6117b0c.png','//span[@class = \'price\']','1 day'),(6,1,0,'disizone','d9d415b7336961f0b6163a0b22288fa2.gif','//span [@class = \'toode_hind\' or   @class = \'toode_shind\']//span[@class = \'bold\']','3-4 days'),(7,1,0,'photoPoint','df9a9f01d2038ba9d94ba0753024b301.png','//span [@class = \'price3\']','3-4 days'),(8,1,0,'fokus','3621df20b01909313571dd86d52fe6ca.jpg','//span [@id = \'our_price_display\']','3-4 days'),(9,1,0,'e-arvutipood','ca11876dd507fa337b86b0ad0772f8c3.png','//li [@class = \'product-price\']','3-4 days'),(10,1,0,'datagate','9e23b6c8819c57f16dffbba1f47a2803.png','//td [@class = \'prodBoxContPrice\']','3-4 days'),(11,1,0,'digisalons','a080de2f4bec98e5d4ebe08e5c2ecd8a.svg','//div [@class = \'product-view-full-box left\']//span [@class = \'price\']','3-4 days'),(12,1,0,'frog','d465c44caabd5ff3912563fb12c1caa5.png','//span [@id = \'our_price_display\']','3-4 days'),(14,1,0,'euronics','61e4dad71e80513dbb8ac73202ffd755.svg','//p [@class = \'price\']//span [@class = \'new-price\']','3-4 days'),(15,1,0,'galador','919b9b98c9bf6f3a442ea1a1d696e398.png','//div [@class = \'info__table\']//span [@class = \'bold\']','3-4 days'),(16,1,0,'klick','c23540f9722f282c8c2e29803b5084db.svg','//div [@class = \'col-md-4 col-sm-5 col-xs-12 product-shop\']//div [@class=\'price-box-configurable\' or @class=\'mobile-price col-xs-6\']//span [@class = \'regular-price\']//span [@class = \'price\']','3-4 days'),(17,1,0,'Onoff','8c16be4c8518ad158dee21338f820b59.svg','//div [@class = \'shop_prod__price\'] | //div [@class = \'shop_prod__price\']/div','2-5 days'),(18,1,0,'rde','19ada78ccce5936c262fafae501daae0.png','//div [@class = \'product_inf_product_price\']/meta/@content','2-5 days'),(19,1,0,'Arvutitark','260c00346de19d11cbdee96ce627b12d.png','//div [@class = \'product-price\'] | //div [@class = \'product-price\']/small','2-5 days');

/*Table structure for table `site_price_list_has_product` */

DROP TABLE IF EXISTS `site_price_list_has_product`;

CREATE TABLE `site_price_list_has_product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site_price_list` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `url` varchar(250) NOT NULL,
  `price` varchar(250) DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `site_price_list` (`site_price_list`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `site_price_list_has_product_ibfk_1` FOREIGN KEY (`site_price_list`) REFERENCES `site_price_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `site_price_list_has_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

/*Data for the table `site_price_list_has_product` */

insert  into `site_price_list_has_product`(`id`,`site_price_list`,`product_id`,`url`,`price`,`last_update`) values (32,1,61,'http://www.1a.ee/telefonid_tahvelarvutid/mobiil_telefonid/mobiiltelefonid/apple_iphone_6s_32gb_gold','','2017-05-03 01:44:51'),(33,2,61,'http://turuliider.ee/et/mobiiltelefonid/14808-apple-iphone-6s-32gb-gold.html','596 €','2017-05-03 01:44:51'),(34,6,61,'https://www.digizone.ee/et/181/Apple/190/Apple-mobiiltelefonid/370178/Apple-mobiiltelefon-iPhone-7-256GB-Black#cat=190&listmode=list&p=0&sort=&perpage=0&filter[21713]=368&iid=370178','979 €','2017-05-03 01:44:52'),(35,7,61,'https://www.photopoint.ee/telefonid/695057-apple-iphone-6s-32gb-gold?utm_source=hinnavaatlus.ee&utm_medium=textlink&utm_campaign=product','599 €','2017-05-03 01:44:53'),(36,16,61,'https://www.klick.ee/apple-iphone-6s-32gb-lukuvaba','595.\n99','2017-05-03 01:44:54'),(37,17,61,'http://www.onoff.ee/telefonid-ja-nutitooted/mobiiltelefonid/nutitelefonid/apple-iphone-6s-32gb-kuldne/','599 €','2017-05-03 01:44:54'),(38,18,61,'http://www.rde.ee/products/ee/388/46436/sort/1/filter/0_0_0_0/iPhone-6s-32GB-Gold-mobiiltelefon.html','593.88 €','2017-05-03 01:44:55'),(39,19,61,'https://www.arvutitark.ee/est/tootekataloog/Nutiseadmed-Telefonid-Nutitelefonid369/ALCATEL-IDOL4-Metal-Silver-VR-TASUTA-Philips-Headset-SHL3260BK-235459','229.00€','2017-05-03 01:44:55'),(42,4,57,'http://www.smartech.ee/et/products/arvutikomponendid/malud/gskill-ddr4-32gb-2x16gb-tridentz-3200mhz-cl16-16-16-xmp2','€ 438.70','2017-05-05 13:30:02'),(43,6,57,'https://www.digizone.ee/et/106/Arvutikaubad/351/M%C3%A4lud/440461/G-Skill-m%C3%A4lu-DDR4-16GB-2400MHz-CL15-(2x8GB)16GFX-AMD-Ryzen#iid=440461','184 €','2017-05-05 13:30:05');

/*Table structure for table `specifications` */

DROP TABLE IF EXISTS `specifications`;

CREATE TABLE `specifications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `specifications` */

/*Table structure for table `specifications_group` */

DROP TABLE IF EXISTS `specifications_group`;

CREATE TABLE `specifications_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `specifications_group` */

insert  into `specifications_group`(`id`,`sort_order`,`status`,`created_date`,`modify_date`) values (2,0,1,'2016-09-12 16:15:31','2016-12-28 06:39:43'),(5,0,1,'2016-09-12 16:58:00','2016-10-25 04:10:04'),(6,0,1,'2016-10-24 15:37:45','2016-10-25 04:14:16'),(7,0,1,'2016-10-25 16:15:35','2016-10-25 04:16:06'),(9,1,1,'2016-12-01 21:48:42','2017-01-18 07:05:11'),(10,1,1,'2016-12-05 20:59:38','2017-04-09 07:33:31');

/*Table structure for table `specifications_group_label` */

DROP TABLE IF EXISTS `specifications_group_label`;

CREATE TABLE `specifications_group_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `specifications_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `specifications_id` (`specifications_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `specifications_group_label_ibfk_1` FOREIGN KEY (`specifications_id`) REFERENCES `specifications_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `specifications_group_label_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=282 DEFAULT CHARSET=utf8;

/*Data for the table `specifications_group_label` */

insert  into `specifications_group_label`(`id`,`specifications_id`,`language_id`,`name`) values (29,5,1,'Power '),(31,6,1,'Operational Conditions'),(35,7,1,'Certificates'),(186,5,7,NULL),(187,6,7,NULL),(188,7,7,NULL),(202,2,1,'Display'),(203,2,7,''),(278,9,1,'Ekraani omadused'),(279,9,7,''),(280,10,1,'Other Features '),(281,10,7,'');

/*Table structure for table `specifications_items` */

DROP TABLE IF EXISTS `specifications_items`;

CREATE TABLE `specifications_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `specification_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `specification_id` (`specification_id`),
  CONSTRAINT `specifications_items_ibfk_1` FOREIGN KEY (`specification_id`) REFERENCES `specifications_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8;

/*Data for the table `specifications_items` */

insert  into `specifications_items`(`id`,`specification_id`) values (85,2),(86,2),(87,2),(88,2),(89,2),(90,2),(91,2),(92,2),(93,2),(94,2),(95,2),(96,2),(97,2),(98,2),(125,2),(101,5),(102,5),(103,5),(104,5),(105,5),(106,5),(107,5),(108,5),(109,5),(110,6),(111,6),(112,6),(113,6),(114,6),(115,6),(116,6),(119,7),(120,7),(121,7),(122,7),(123,7),(124,7),(127,9),(128,9),(129,9),(163,9),(130,10),(143,10),(147,10),(148,10),(150,10),(151,10),(152,10),(153,10),(154,10),(155,10),(156,10),(157,10),(158,10),(164,10);

/*Table structure for table `specifications_items_label` */

DROP TABLE IF EXISTS `specifications_items_label`;

CREATE TABLE `specifications_items_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `specifications_item_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `specifications_item_id` (`specifications_item_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `specifications_items_label_ibfk_1` FOREIGN KEY (`specifications_item_id`) REFERENCES `specifications_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `specifications_items_label_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1321 DEFAULT CHARSET=utf8;

/*Data for the table `specifications_items_label` */

insert  into `specifications_items_label`(`id`,`specifications_item_id`,`language_id`,`name`) values (201,101,1,'Input current'),(203,102,1,'Typical power consuption'),(205,103,1,'Standby power consuption'),(207,104,1,'Typical power consuption'),(209,105,1,'Maximum power consuption'),(211,106,1,'Input frequency'),(213,107,1,'Input voltage'),(215,108,1,'AC Input frequency'),(217,109,1,'AC Input voltage'),(219,110,1,'Operating altitude'),(221,111,1,'Non-operating altitude'),(223,112,1,' ter t'),(225,113,1,'Relativ humidity storage rang'),(227,114,1,'Relativ humidity operating rang'),(229,115,1,'Strong temperature rang'),(231,116,1,'Operating temperature rang'),(237,119,1,'Certification'),(239,120,1,'Energy Star certificate'),(241,121,1,'Energy Star certificate 1'),(243,122,1,'Energy Star certificate 2'),(245,123,1,'Energy Star certificate 3'),(247,124,1,'Energy Star certificate 4'),(853,101,7,NULL),(854,102,7,NULL),(855,103,7,NULL),(856,104,7,NULL),(857,105,7,NULL),(858,106,7,NULL),(859,107,7,NULL),(860,108,7,NULL),(861,109,7,NULL),(862,110,7,NULL),(863,111,7,NULL),(864,112,7,NULL),(865,113,7,NULL),(866,114,7,NULL),(867,115,7,NULL),(868,116,7,NULL),(869,119,7,NULL),(870,120,7,NULL),(871,121,7,NULL),(872,122,7,NULL),(873,123,7,NULL),(874,124,7,NULL),(951,85,1,'Graphic resolution'),(952,85,7,''),(953,86,1,'Video mode'),(954,86,7,''),(955,87,1,'Display type'),(956,87,7,''),(957,88,1,'3D compatible'),(958,88,7,''),(959,89,1,'Graphic resolution'),(960,89,7,''),(961,90,1,'Display surface'),(962,90,7,''),(963,91,1,'Video mode'),(964,91,7,''),(965,92,1,'HD type'),(966,92,7,''),(967,93,1,'HDCP'),(968,93,7,''),(969,94,1,'Mobile High-Definition Link'),(970,94,7,''),(971,95,1,'Viewable hight'),(972,95,7,''),(973,96,1,'Viewable width'),(974,96,7,''),(975,97,1,'Aspect ratio'),(976,97,7,''),(977,98,1,'Pixel pitch'),(978,98,7,''),(979,125,1,'Screen Size'),(980,125,7,''),(1285,127,1,'Ekraani diagonaal'),(1286,127,7,''),(1287,128,1,'Ekraani tüüp'),(1288,128,7,''),(1289,129,1,'Resolutsioon'),(1290,129,7,''),(1291,163,1,'Pikslitihedus (ppi)'),(1292,163,7,''),(1293,130,1,'Series'),(1294,130,7,''),(1295,143,1,'Memory Type'),(1296,143,7,''),(1297,147,1,'Multi-Channel Kit'),(1298,147,7,''),(1299,148,1,'Tested Speed'),(1300,148,7,''),(1301,150,1,'Tested Latency'),(1302,150,7,''),(1303,151,1,'Tested Voltage'),(1304,151,7,''),(1305,152,1,'Registered/Unbuffered'),(1306,152,7,''),(1307,153,1,'Error Checking'),(1308,153,7,''),(1309,154,1,'SPD Speed'),(1310,154,7,''),(1311,155,1,'SPD Voltage'),(1312,155,7,''),(1313,156,1,'Fan lncluded'),(1314,156,7,''),(1315,157,1,'Height'),(1316,157,7,''),(1317,158,1,'Warranty'),(1318,158,7,''),(1319,164,1,'Features'),(1320,164,7,'');

/*Table structure for table `specifications_items_values` */

DROP TABLE IF EXISTS `specifications_items_values`;

CREATE TABLE `specifications_items_values` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `specifications_item_id` int(11) unsigned NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `specifications_item_id` (`specifications_item_id`),
  CONSTRAINT `specifications_items_values_ibfk_1` FOREIGN KEY (`specifications_item_id`) REFERENCES `specifications_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=440 DEFAULT CHARSET=utf8;

/*Data for the table `specifications_items_values` */

insert  into `specifications_items_values`(`id`,`specifications_item_id`,`value`) values (283,85,'1560 X 1440'),(284,85,'2560 X 1440'),(285,85,'3560 X 1440'),(286,85,'4560 X 1440'),(287,86,'1400p'),(288,86,'2400p'),(289,86,'3400p'),(290,87,'led'),(291,87,'<i class=\"fa fa-times\"></i> No'),(292,87,'<i class=\"fa fa-check\"></i> Yes'),(293,88,'<i class=\"fa fa-times\"></i> No'),(294,88,'<i class=\"fa fa-check\"></i> Yes'),(295,89,'2560x1440'),(296,89,'3560x1440'),(297,89,'4560x1440'),(298,90,'Matt'),(299,90,'No matt'),(300,91,'1440p'),(301,91,'2440p'),(302,91,'3440p'),(303,92,'Wide Quad HD'),(304,92,'Wide Quad HD'),(305,93,'<i class=\"fa fa-check\"></i> Yes'),(306,93,'<i class=\"fa fa-times\"></i> No'),(307,94,'<i class=\"fa fa-check\"></i> Yes'),(308,94,'<i class=\"fa fa-times\"></i> No'),(309,95,'335.6mm'),(310,95,'435.6mm'),(311,95,'535.6mm'),(312,96,'535.6mm'),(313,96,'635.6mm'),(314,96,'735.6mm'),(315,97,'16:9'),(316,97,'17:9'),(317,98,'0.2331 x 0.2331'),(322,101,'1.0A'),(323,101,'1.5A'),(324,101,'2.0A'),(325,101,'2.5A'),(326,102,'35W'),(327,102,'45W'),(328,102,'55W'),(329,103,'0.5W'),(330,103,'1.0W'),(331,103,'1.5W'),(332,103,'<i class=\"fa fa-check\"></i> Yes'),(333,103,'<i class=\"fa fa-times\"></i> No'),(334,103,'<i class=\"fa fa-times\"></i> No'),(335,103,'<i class=\"fa fa-times\"></i> No'),(336,103,'<i class=\"fa fa-times\"></i> No'),(337,103,'<i class=\"fa fa-check\"></i> Yes'),(338,103,'<i class=\"fa fa-check\"></i> Yes'),(339,104,'35w'),(340,104,'45w'),(341,104,'50w'),(342,105,'87w'),(343,105,'90w'),(344,106,'50/60Hz'),(345,106,'60/70Hz'),(346,107,'100 - 240w'),(347,107,'200-500w'),(348,108,'50/60Hz'),(349,108,'60/80Hz'),(350,108,'50/60Hz'),(351,109,'100 - 240w'),(352,109,'200 - 240w'),(353,110,'0-1000'),(354,110,'0-2000'),(355,110,'0-3000'),(356,110,'0-4000'),(357,110,'0-5000'),(358,111,'0-12192'),(359,111,'0-22192'),(360,111,'0-32192'),(361,112,' ger ger g'),(362,113,'5-90'),(363,113,'10-80'),(364,113,'20-60'),(365,114,'10-60'),(366,115,'-20-60'),(367,116,'0-40'),(371,119,'TCO, RoHS'),(372,120,'<i class=\"fa fa-check\"></i> Yes'),(373,120,'<i class=\"fa fa-times\"></i> No'),(374,121,'1'),(375,122,'2'),(376,123,'3'),(377,124,'4'),(380,125,'18'),(381,125,'20'),(382,125,'22'),(383,125,'24'),(385,127,'4.5'),(386,127,'5'),(387,127,'5,2'),(388,127,'5,5'),(389,128,'Super AMOLED'),(390,128,'IPS LCD'),(391,128,'IPS'),(392,129,'1280 x 720 px'),(393,129,'1920 x 1080 px'),(394,130,'Trident Z'),(405,143,'32GB (2x16GB)'),(407,147,'<i class=\"fa fa-check\"></i> Yes'),(408,148,'<i class=\"fa fa-check\"></i> Yes'),(409,148,'<i class=\"fa fa-times\"></i> No'),(411,150,'<i class=\"fa fa-check\"></i> Yes'),(412,151,'<i class=\"fa fa-check\"></i> Yes'),(413,151,'<i class=\"fa fa-times\"></i> No'),(415,147,'<i class=\"fa fa-times\"></i> No'),(416,150,'<i class=\"fa fa-times\"></i> No'),(417,152,'<i class=\"fa fa-check\"></i> Yes'),(418,152,'<i class=\"fa fa-times\"></i> No'),(419,153,'<i class=\"fa fa-check\"></i> Yes'),(420,153,'<i class=\"fa fa-times\"></i> No'),(421,154,'<i class=\"fa fa-check\"></i> Yes'),(422,154,'<i class=\"fa fa-times\"></i> No'),(423,155,'<i class=\"fa fa-check\"></i> Yes'),(424,155,'<i class=\"fa fa-times\"></i> No'),(425,156,'<i class=\"fa fa-check\"></i> Yes'),(426,156,'<i class=\"fa fa-times\"></i> No'),(427,157,'<i class=\"fa fa-check\"></i> Yes'),(428,157,'<i class=\"fa fa-times\"></i> No'),(429,158,'<i class=\"fa fa-check\"></i> Yes'),(430,158,'<i class=\"fa fa-times\"></i> No'),(437,163,'326'),(438,127,'4'),(439,164,'');

/*Table structure for table `specifications_label` */

DROP TABLE IF EXISTS `specifications_label`;

CREATE TABLE `specifications_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `specifications_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `specifications_label` */

/*Table structure for table `static_page_label` */

DROP TABLE IF EXISTS `static_page_label`;

CREATE TABLE `static_page_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `static_page_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `static_page_id` (`static_page_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `static_page_label_ibfk_1` FOREIGN KEY (`static_page_id`) REFERENCES `static_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `static_page_label_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `static_page_label` */

/*Table structure for table `static_pages` */

DROP TABLE IF EXISTS `static_pages`;

CREATE TABLE `static_pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `static_pages` */

insert  into `static_pages`(`id`,`sort_order`,`status`,`created_date`,`modify_date`) values (2,0,1,'2016-09-12 19:34:04','2016-10-25 08:05:29'),(3,0,1,'2016-09-12 19:47:07','2016-12-13 12:49:54');

/*Table structure for table `static_pages_label` */

DROP TABLE IF EXISTS `static_pages_label`;

CREATE TABLE `static_pages_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `static_page_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `static_page_id` (`static_page_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `static_pages_label_ibfk_1` FOREIGN KEY (`static_page_id`) REFERENCES `static_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `static_pages_label_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `static_pages_label` */

insert  into `static_pages_label`(`id`,`static_page_id`,`language_id`,`title`,`description`) values (15,2,1,'Privacy Policy','<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n'),(18,3,1,'About us','<h1>Lorem Ipsum</h1>\r\n\r\n<h4>&quot;Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...&quot;</h4>\r\n\r\n<h5>&quot;There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...&quot;</h5>\r\n\r\n<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<h2>Why do we use it?</h2>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n'),(22,2,7,NULL,NULL),(23,3,7,NULL,NULL);

/*Table structure for table `translation` */

DROP TABLE IF EXISTS `translation`;

CREATE TABLE `translation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `modifie_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `key` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

/*Data for the table `translation` */

insert  into `translation`(`id`,`modifie_date`,`key`) values (1,'2016-12-13 12:47:09','home'),(2,'2016-10-28 06:02:49','contact_us'),(3,'0000-00-00 00:00:00','contact_message'),(4,'0000-00-00 00:00:00','contact_message_error'),(5,'0000-00-00 00:00:00','all_categories'),(6,'0000-00-00 00:00:00','compare'),(7,'0000-00-00 00:00:00','results'),(8,'0000-00-00 00:00:00','clear_filters'),(9,'0000-00-00 00:00:00','brand'),(10,'0000-00-00 00:00:00','found'),(11,'0000-00-00 00:00:00','comments:'),(12,'0000-00-00 00:00:00','videos'),(13,'0000-00-00 00:00:00','overview'),(14,'0000-00-00 00:00:00','specs'),(15,'0000-00-00 00:00:00','comments'),(16,'0000-00-00 00:00:00','rated_by'),(17,'0000-00-00 00:00:00','users'),(18,'0000-00-00 00:00:00','have_it'),(19,'0000-00-00 00:00:00','customers_recommend'),(20,'0000-00-00 00:00:00','add_a_review'),(21,'0000-00-00 00:00:00','specification'),(22,'0000-00-00 00:00:00','more'),(23,'0000-00-00 00:00:00','user_comments'),(24,'0000-00-00 00:00:00','name'),(25,'0000-00-00 00:00:00','email'),(26,'0000-00-00 00:00:00','content'),(27,'0000-00-00 00:00:00','plus'),(28,'0000-00-00 00:00:00','minus'),(29,'0000-00-00 00:00:00','overall_rating:'),(30,'0000-00-00 00:00:00','send'),(31,'0000-00-00 00:00:00','close');

/*Table structure for table `translation_label` */

DROP TABLE IF EXISTS `translation_label`;

CREATE TABLE `translation_label` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `translation_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `translation_id` (`translation_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `translation_label_ibfk_1` FOREIGN KEY (`translation_id`) REFERENCES `translation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `translation_label_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8;

/*Data for the table `translation_label` */

insert  into `translation_label`(`id`,`translation_id`,`language_id`,`value`) values (5,3,1,'Thank you for contacting us. We will respond to you as soon as possible.'),(7,4,1,'contact_message_error'),(9,5,1,'All Categories'),(11,6,1,'Compare'),(13,7,1,'Results:'),(15,8,1,'Clear filters'),(17,9,1,'brand'),(19,10,1,' Found :'),(21,11,1,'Comments:'),(23,12,1,'Videos'),(24,13,1,'Overview'),(25,14,1,'Specs'),(26,15,1,'Comments'),(27,16,1,'Rated by'),(28,17,1,'users'),(29,18,1,'Have it'),(30,19,1,'Customers recommend'),(31,20,1,'Add a Review'),(32,21,1,'Specification'),(34,22,1,'More'),(35,23,1,'Users comments'),(36,24,1,'Name'),(37,25,1,'Email'),(38,26,1,'Content'),(39,27,1,'Plus'),(40,28,1,'Minus'),(41,29,1,'Overall Rating:'),(42,30,1,'Send'),(45,31,1,'Close'),(47,2,1,'Contact us'),(48,1,1,'Home'),(143,3,7,NULL),(144,4,7,NULL),(145,5,7,NULL),(146,6,7,NULL),(147,7,7,NULL),(148,8,7,NULL),(149,9,7,NULL),(150,10,7,NULL),(151,11,7,NULL),(152,12,7,NULL),(153,13,7,NULL),(154,14,7,NULL),(155,15,7,NULL),(156,16,7,NULL),(157,17,7,NULL),(158,18,7,NULL),(159,19,7,NULL),(160,20,7,NULL),(161,21,7,NULL),(162,22,7,NULL),(163,23,7,NULL),(164,24,7,NULL),(165,25,7,NULL),(166,26,7,NULL),(167,27,7,NULL),(168,28,7,NULL),(169,29,7,NULL),(170,30,7,NULL),(171,31,7,NULL),(172,2,7,NULL),(173,1,7,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `users` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
