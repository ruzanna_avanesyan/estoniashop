/*
**
**
**
*/

ALTER TABLE `product` ADD COLUMN `is_pc_build` TINYINT(1) DEFAULT 0 NOT NULL
AFTER `site_price_list_id`;


CREATE TABLE `pc_build_options` (
  `id`      INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pc_type` VARCHAR(20)      NOT NULL,
  `option`  VARCHAR(255)     NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `product_pc_build` (
  `id`         INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` INT(11) UNSIGNED NOT NULL,
  `pc_type`    VARCHAR(255)     NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);


CREATE TABLE `product_pc_build_oprions` (
  `id`                  INT(11) UNSIGNED        NOT NULL AUTO_INCREMENT,
  `product_pc_build_id` INT(11) UNSIGNED        NOT NULL,
  `op_type`             ENUM('mhz', 'op', 'pw') NOT NULL,
  `val`                 VARCHAR(255)            NOT NULL,
  `count`               INT(5)                  NOT NULL,
  PRIMARY KEY (`id`)
);


ALTER TABLE `product_pc_build_oprions` ADD FOREIGN KEY (`product_pc_build_id`) REFERENCES `product_pc_build` (`id`)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

ALTER TABLE `product_pc_build_options` CHANGE `op_type` `op_type` ENUM('mhz', 'case', 'pw', 'cpu', 'memory', 'video_card', 'storage', 'power_supply', 'pci', 'model', 'multi')
CHARSET utf8
COLLATE utf8_general_ci NOT NULL;


ALTER TABLE `users` ADD COLUMN `reset_key` VARCHAR(255) NULL
AFTER `password`;

ALTER TABLE `pc_build_item` ADD COLUMN `waltage` FLOAT NOT NULL
AFTER `total`;


ALTER TABLE `pc_comments` ADD COLUMN `pc_build_item_id` INT(11) UNSIGNED NOT NULL
AFTER `user_id`, ADD FOREIGN KEY (`pc_build_item_id`) REFERENCES `pc_build_item` (`id`)
  ON UPDATE CASCADE
  ON DELETE CASCADE; 