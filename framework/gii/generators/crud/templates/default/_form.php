<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $form CActiveForm */
?>

<div class="col-xs-12" style="margin-top: 50px">
	<div class="box box-info">

		<?php echo "<?php \$form=\$this->beginWidget('CActiveForm', array(
	'id'=>'".$this->class2id($this->modelClass)."-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'class'=>'form-horizontal',
		'enctype'=>'multipart/form-data'
	)
)); ?>\n"; ?>
		<div class="box-body">
			<p class="note">Fields with <span class="required">*</span> are required.</p>

			<?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>

			<?php
			foreach($this->tableSchema->columns as $column)
			{
				if($column->autoIncrement)
					continue;
				?>
				<div class="form-group">
					<?php echo "<?php echo ".$this->generateActiveLabel($this->modelClass,$column)."; ?>\n"; ?>
					<div class="col-sm-5">
						<?php echo "<?php echo ".$this->generateActiveField($this->modelClass,$column)."; ?>\n"; ?>
					</div>
					<?php echo "<?php echo \$form->error(\$model,'{$column->name}'); ?>\n"; ?>
				</div>

				<?php
			}
			?>
			<div class="box-footer">
				<?php echo "<?php echo CHtml::submitButton(\$model->isNewRecord ? 'Create' : 'Save'); ?>\n"; ?>
			</div>
		</div>

		<?php echo "<?php \$this->endWidget(); ?>\n"; ?>

	</div>
</div><!-- form -->