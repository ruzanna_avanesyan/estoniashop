//drag and drop
$( function() {
    $( ".draggable2" ).draggable({
        opacity: 0.7,
        zIndex: 102,
        helper: "clone",
        containment: "html",
        scroll: false,
        start: function() {
            if (!$('.cmpr-pnl-wrpr').hasClass('add-cmp-ml')) {
                $('.cmpr-pnl__close').trigger('click');
            }

            $('body').append('<div id="blur"></div>');
        },
        stop: function() {
            $('#blur').remove();
        }
    });
    $( ".droppable" ).droppable({
        drop: function( event, ui ) {

            event.preventDefault();
            var inp = ui.draggable.parent().parent().parent().find('.js-add-to-cmpr-btn.js-add-to-cmpr');
            if(!inp.is(':checked')) {
                inp.data('cookie',1).trigger('click');
            }
        }
    });
    //$( ".drop2" ).droppable({
    //    drop: function( event, ui ) {
    //        event.preventDefault();
    //        var item = ui.draggable.closest('div.prdct-item');
    //        var id = item.attr('data-mspid');
    //        alert(id);
    //    }
    //});
});


function setCookie(name, value, expires, path, domain, secure) {

    expires instanceof Date ? expires = expires.toGMTString() : typeof(expires) == 'number' && (expires = (new Date(+(new Date) + expires * 1e3)).toGMTString());
    var r = [name + "=" + escape(value)], s, i;
    for(i in s = {expires: expires, path: path, domain: domain}){
        s[i] && r.push(i + "=" + s[i]);
    }
    return secure && r.push("secure"), document.cookie = r.join(";"), true;
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if(c.indexOf(name) == 0)
            return c.substring(name.length,c.length);
    }
    return "";
}
function fillComparePanelAjax(id, cat_id) {
    var url = getAjaxUrl();
    $.ajax({
        type: "post",
        url: url,
        data: 'id='+id +'&cat_id=' + cat_id,
        success: function (data) {

        }
    });
}
function deleteComparePanelAjax(id) {
    var item_c = $('.sctn__inr').children('.cmpr-pnl-list__item:not(.cmpr0)').length;
    var t= 0;
    if(item_c == 1){
        t = 1;
    }
    var url = getDeleteAjaxUrl();
    $.ajax({
        type: "post",
        url: url,
        data: 'id='+id+'&last='+t,
        success: function (data) {

        }
    });
}

function getCompareItemsFromCookieAjax(){
    var url = getCompareItemsFromCookieAjaxUrl();
    $.ajax({
        type: "post",
        url: url,
        success: function (data) {
            if(data != '') {
                var compare_ids = (data || "").split(",");
                for (var id = 0; id < compare_ids.length; id++) {
                    $(document).find(".prdct-item[data-mspid=" + compare_ids[id] + "] .js-add-to-cmpr-btn.js-add-to-cmpr").attr('data-cookie',1);
                    $(document).find(".prdct-item[data-mspid=" + compare_ids[id] + "] .js-add-to-cmpr-btn.js-add-to-cmpr").trigger('click');
                    console.log( $(document).find(".prdct-item[data-mspid=" + compare_ids[id] + "] .js-add-to-cmpr-btn.js-add-to-cmpr"));
                }
            }
        }
    });
}

function generateComparePageUrl() {
    var mspid, mspids, subcategory;
    $(".cmpr-pnl-list__item:not(.cmpr0)").each(function () {
        mspid = $(this).data("comparemspid"), mspids = mspids ? mspids + "," + mspid : mspid
    }), mspids = mspids || "", subcategory = getCategoryFromPanel();
    var url = "/compare/index.php?mspids=" + mspids + "&subcategory=" + subcategory;
    //$(".sctn__compare-btn").attr("href", url)
}
function alreadyAdded() {
    var alreadyAdded = !1;
    return $(".cmpr-pnl-list__item:not(.cmpr0)").each(function () {
        $(this).data("comparemspid") == $(".prdct-dtl__ttl").data("mspid") && (alreadyAdded = !0)
    }), alreadyAdded
}
function isComparePanelOpen() {
    var addCompareSideBar = $(".cmpr-pnl-wrpr");
    return !!addCompareSideBar.hasClass("add-cmp-ml")
}

function disableCompareCB(tooltipMSG) {
    $(".prdct-item__cmpr-chkbx").length ? ($(".prdct-item__cmpr-chkbx:not(:checked)").parent().parent().parent().attr("disabled", "disabled")) : ($(".js-pdp-cmpr").addClass("js-tltp").attr("data-disable", "true"),
        alreadyAdded() ? ($(".js-pdp-cmpr").addClass("prdct-dtl__tlbr-item--cmpr-dsbl"),
        isComparePanelFull()) : $(".js-pdp-cmpr").removeClass("prdct-dtl__tlbr-item--cmpr-dsbl"))
}
function enableCompareCB() {
    $(".prdct-item__cmpr-chkbx").length ? $(".prdct-item__cmpr-chkbx").removeAttr("disabled").parent().removeClass("js-tltp prdct-item__cmpr-dsbl") :
        alreadyAdded() ? $(".js-pdp-cmpr") : $(".js-pdp-cmpr").removeAttr("data-disable").removeClass("js-tltp prdct-dtl__tlbr-item--cmpr-dsbl")
}

function isComparePanelFull() {
    $blankCompareDiv = $(".cmpr-pnl-list__item.cmpr0");
    if ( 2 == $blankCompareDiv.length) {
        var tooltipMSG = "Cannot compare more than 5 products";
        return disableCompareCB(tooltipMSG), !0
    }
    return !1
}
function isDifferentCategory() {
    //var compareSubCategory = getCookie("compareSubCategory");
    //if (compareSubCategory && sub_category != compareSubCategory) {
    //    var tooltipMSG = "Cannot campare between different categories";
    //    return disableCompareCB(tooltipMSG), !0
    //}
    return !1
}
function getCategoryFromPanel() {
    return $(".cmpr-pnl-list__item:first").attr("data-subcategory") || ""
}
function getMSPidsFromPanel() {
    var compare_msp_ids = [];
    return $(".cmpr-pnl-list__item:not(.cmpr0)").each(function () {
        compare_msp_ids.push($(this).data("comparemspid"))
    }), compare_msp_ids || ""
}
function flyImage(imgtofly, id, title, $thisCB, $cookie) {
    $replaceThis = $(".cmpr-pnl-list__item.cmpr0");
    fillComparePanelAjax(id, $thisCB.data('cat'));
    var sub_category = getCategoryFromPanel();
    if(!$cookie) {
            flyingImageCount++, flyingImageCount >= 1 && 5 == $(".cmpr-pnl-list__item.cmpr0").length && (sub_category = $(".body-wrpr").data("category"), $(".cmpr-pnl-list__item-ttl .js-atcmplt").prop("disabled", !0)), $replaceThis.length - 1 <= flyingImageCount && disableCompareCB("Cannot compare more than 5 products");
        var top, left;
        flyingImageCount > 1 ? (top = $($replaceThis[flyingImageCount - 1]).offset().top + 20,
            left = $($replaceThis[flyingImageCount - 1]).offset().left + 30) : (top = $replaceThis.offset().top + 20,
            left = $replaceThis.offset().left + 30),
        isComparePanelOpen() || ($(".cmpr-pnl-wrpr").removeClass("add-cmp-mr").addClass("add-cmp-ml"),
            left -= 250);
        var width = imgtofly.width, $img = $(imgtofly).clone().removeAttr("class"), imgclone = $img.css({
            opacity: "0.7",
            position: "absolute",
            width: width,
            "z-index": "5000",
            top: $thisCB.offset().top,
            left: $thisCB.offset().left
        }).appendTo($("body")).animate({top: top, left: left, width: 30}, 700);
        imgclone.animate({width: 0, height: 0}, function () {
            $(this).detach();
            var img = imgtofly.src,
                img_alt = imgtofly.alt;
            $replaceThis = $(".cmpr-pnl-list__item.cmpr0:first"),
                addCompProdHtml($replaceThis, id, sub_category, img, img_alt, title),
                flyingImageCount--,
                $thisCB.removeAttr("disabled"),
            $(".js-pdp-cmpr").length && $(".prdct-dtl__ttl").data("mspid") == ui.item.mspid && disableCompareCB("Already added to compare panel"),
            1 > flyingImageCount && ( $(".cmpr-pnl-list__item-ttl .js-atcmplt").prop("disabled", !1));
        });
        $thisCB.parents().closest(".prdct-item").find('.compare-button').prop("disabled", 1);
    }else{
        var img = imgtofly.src,
            img_alt = imgtofly.alt;
        $replaceThis = $(".cmpr-pnl-list__item.cmpr0:first"),
        addCompProdHtml($replaceThis, id, sub_category, img, img_alt, title);
        $thisCB.parents().closest(".prdct-item").find('.compare-button').prop("disabled", 1);
    }
}
function addCompProdHtml($replaceThis, id, sub_category, img, img_alt, title) {
    var removeButton = ' <span class="cmpr-pnl-list__item-rmv js-cmpr-itm-rmv" title="Close">&#x2715;</span>';
    $replaceThis.attr("data-comparemspid", id), $replaceThis.attr("data-subcategory", sub_category), $replaceThis.find(".cmpr-pnl-list__img").attr("src", img), $replaceThis.find(".cmpr-pnl-list__img").attr("alt", img_alt), $replaceThis.find(".cmpr-pnl-list__item-ttl").html(title), $replaceThis.append(removeButton), $replaceThis.removeClass("cmpr0")
}
function setCookieCompareIDS(newMSPID) {
    var compare_msp_ids = [];
    compare_msp_ids = getMSPidsFromPanel(), setCookie("compareIDs", compare_msp_ids)
}
function compareAutoComplete() {
    return 0 !== $(".cmpr-pnl-wrpr .js-atcmplt").length ? ($(document).on("keydown.autocomplete", ".cmpr-pnl-wrpr .js-atcmplt", function () {
        $(this);
        $(".cmpr-pnl-wrpr .js-atcmplt").autocomplete({
            minLength: 1,
            delay: 110,
            autoFocus: !1,
            max: 10,
            open: function (event, ui) {
                $parent = $(this).closest(".srch-wdgt"), $(".ui-menu:visible").css({
                    width: $parent.width(),
                    left: "-=1",
                    top: "+=1"
                }), $parent.addClass("srch-wdgt--show-rslt")
            },
            close: function (event, ui) {
                $(this).closest(".srch-wdgt").removeClass("srch-wdgt--show-rslt")
            },
            source: function (request, response) {
                var term = $.trim(request.term.toLowerCase()), element = this.element, autocompleteCache = this.element.data("autocompleteCache") || {}, foundInAutocompleteCache = !1;
                term in autocompleteCache && 0 !== autocompleteCache[term].length && (response(autocompleteCache[term]), foundInAutocompleteCache = !0), foundInAutocompleteCache || (request.term = term, request.subcategory = getCategoryFromPanel(), request.mspids = getMSPidsFromPanel().toString(), $.ajax({
                    url: "/compare/auto_suggest.php",
                    dataType: "json",
                    data: request,
                    success: function (data) {
                        data = $.map(data, function (n, i) {
                            return n.index = i, n
                        }), autocompleteCache[term] = data, element.data("autocompleteCache", autocompleteCache), response(data)
                    }
                }))
            },
            select: function (event, ui) {
                var $form = $(this).closest("form");
                $form.find(".js-atcmplt").val(ui.item.value), $form.find(".js-atcmplt-id").val(ui.item.mspid), 5 == $(".cmpr-pnl-list__item.cmpr0").length && setCookie("compareSubCategory", ui.item.subcategory);
                var $replaceThis = $(this).closest(".cmpr-pnl-list__item"), id = ui.item.mspid, sub_category = ui.item.subcategory, img = ui.item.mainimage, title = ui.item.value, img_alt = ui.item.label;
                addCompProdHtml($replaceThis, id, sub_category, img, img_alt, title),
                    setCookieCompareIDS(ui.item.mspid),
                    //$(".prdct-item[data-mspid=" + ui.item.mspid + "] .prdct-item__cmpr-chkbx").attr("checked", !0),
                    isDifferentCategory() ? disableCompareCB("Cannot campare between different categories") : $(".cmpr-pnl-list__item.cmpr0").length < 2 ? disableCompareCB("Cannot add more than 5 products at a time") : $(".js-pdp-cmpr").length && $(".prdct-dtl__ttl").data("mspid") == ui.item.mspid ? disableCompareCB("Already added to compare panel") : enableCompareCB()
            }
        }).data("uiAutocomplete")._renderItem = function (ul, item) {
            var term = this.term.split(" ").join("|"), re = new RegExp("\\b(" + term + ")", "gi"), tempval = item.value.replace(re, "<b>$1</b>");
            return $("<li></li>").data("item.autocomplete", item).append("<a>" + tempval + "</a>").appendTo(ul)
        }
    }), !1) : void 0
}
var sub_category = $(".body-wrpr").data("category"),
    flyingImageCount = 0,
    check_if_added,
    removingOther = !1;

$(document).ready(function () {
    getCompareItemsFromCookieAjax();

    //$(".js-add-to-cmpr").bind('click', function() {
    //    alert("I am clicked :)");
    //});

    //remove item from Compare panel
    $("body").on("click", ".js-cmpr-itm-rmv", function () {
        if (!removingOther) {
            $lastIndex = $(this).parents(".sctn__inr").children().last(),
                $thisProduct = $(this).parent(".cmpr-pnl-list__item");
            var $blankProduct = $(".cmpr0:last").clone();
            return removingOther = !0,
                $thisProduct.slideUp("fast", function () {
                $blankProduct.insertAfter($lastIndex);
                var this_MSP_ID = $thisProduct.data("comparemspid");

                deleteComparePanelAjax(this_MSP_ID);
                    $(".prdct-dtl__ttl").length ? $(".prdct-dtl__ttl").data("mspid") === this_MSP_ID && $(".prdct-dtl__tlbr-item--cmpr").removeClass("prdct-dtl__tlbr-item--cmpr-dsbl").text("Add to compare") : $(".prdct-item[data-mspid=" + this_MSP_ID + "] .prdct-item__cmpr-chkbx").attr("checked", !1),
                        $thisProduct.remove(),
                        $(document).find(".prdct-item[data-mspid=" + this_MSP_ID + "] .js-add-to-cmpr-btn.js-add-to-cmpr").data('cookie',0);
                        $(document).find(".prdct-item[data-mspid=" + this_MSP_ID + "] .compare-button").removeAttr("disabled");
                        //$(document).find(".prdct-item[data-mspid=" + this_MSP_ID + "] .compare-button[data-cat!=" + $(this).data('cat') + "]").attr("disabled", "disabled");

                        if(6 == $(".cmpr-pnl-list__item.cmpr0").length) {
                            $(".prdct-item__cmpr-chkbx:not(:checked)").parent().parent().parent().removeAttr("disabled").parents().closest('.product-layout').removeClass('different-category');;
                            $(document).find(".prdct-item[data-cat!=" + $(this).data('cat') + "] .draggable2").draggable( 'enable' );
                        }

                        5 == $(".cmpr-pnl-list__item.cmpr0").length ? enableCompareCB() : isDifferentCategory() || enableCompareCB(),
                        removingOther = !1, generateComparePageUrl()
                }), !1
        }
    }),

    //add item in Compare panel
    $(".js-add-to-cmpr").on("change", function () {
        var id = $(this).parents().closest(".prdct-item").data("mspid"),
            imgtofly = $(this).parents().closest(".prdct-item").find(".prdct-item__img")[0],
            title = $(this).closest(".prdct-item").find(".prdct-item__name").text(),
            $thisCB = $(this);
        //$thisCB.attr("disabled", "disabled");

        if ($(this).val()) {
            isComparePanelFull();
            if ($(".cmpr-pnl-list__item.cmpr0").length < 2) {
                ($thisCB.prop("checked", !1),
                    disableCompareCB("Cannot add more than 5 products"),
                    $thisCB.trigger("mouseover"))
            } else {
                if ($(".js-pdp-cmpr").length && $(".prdct-dtl__ttl").data("mspid") == ui.item.mspid) {
                    ($thisCB.prop("checked", !1),
                        disableCompareCB("Already added to compare panel"),
                        $thisCB.trigger("mouseover"))
                }
                else {
                    flyImage(imgtofly, id, title, $thisCB, $thisCB.data('cookie'));
                    enableCompareCB();
                }
            }
        } else {
            (isComparePanelOpen() || $(".cmpr-pnl-wrpr").removeClass("add-cmp-mr").addClass("add-cmp-ml"),
                $(".cmpr-pnl-list__item[data-comparemspid='" + id + "']").find(".js-cmpr-itm-rmv").click()),
                isComparePanelFull();

        }
    }), $("body").on("click", ".cmpr-pnl__close", function () {
        var addCompareSideBar = $(".cmpr-pnl-wrpr");
        addCompareSideBar.hasClass("add-cmp-ml") ? (addCompareSideBar.removeClass("add-cmp-ml").addClass("add-cmp-mr"), addCompareSideBar.find(".cmpr-pnl").removeClass("cmpr-pnl--bx-shdw")) : (addCompareSideBar.removeClass("add-cmp-mr").addClass("add-cmp-ml"), addCompareSideBar.find(".cmpr-pnl").addClass("cmpr-pnl--bx-shdw"))
    }), $("body").on("click", ".js-cmpr-pnl-cls", function () {
        var addCompareSideBar = $(".cmpr-pnl-wrpr");
        addCompareSideBar.hasClass("add-cmp-ml") && (addCompareSideBar.removeClass("add-cmp-ml").addClass("add-cmp-mr"), addCompareSideBar.find(".cmpr-pnl").removeClass("cmpr-pnl--bx-shdw"))
    });

    $('.compare-button').on('click', function(){
        $(document).find(".prdct-item .compare-button[data-cat!=" + $(this).data('cat') + "]").attr("disabled", "disabled").parents().closest('.product-layout').addClass('different-category');

        $(document).find(".prdct-item[data-cat!=" + $(this).data('cat') + "]").find('.draggable2').addClass('ui-draggable-disabled');
        $(document).find(".prdct-item[data-cat!=" + $(this).data('cat') + "] .draggable2").draggable( 'disable' );

        $(this).find('.js-add-to-cmpr').trigger('click');

    });
});